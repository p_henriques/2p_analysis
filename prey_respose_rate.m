
folders = { ...
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180301\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180305\f2\deep';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180419\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f1_1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180427\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180503\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180504\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180509\f3';
    };

X = NaN(length(folders),4,3);
for f = 1:length(folders)
    datadir = folders{f};
    % Pedro Henriques, Mar 2018
    load(fullfile(datadir,'gm'),'gm');
    load(fullfile(datadir,'gmv'),'gmv');
    load(fullfile(datadir,'gmb'),'gmb');
    
    % Good convergences
    convix = gmb.convergences(:,3) == 1;
    pconv = intersect(sp,gmb.convergences(convix,1));
    
    k = 1;
    for v = 3:6
        % Stimulus on times
        [~, ~, vSTt, vEDt] = gcStimInfo(gmv.vistypz(v,1:5),gm.trfr,gm.frtime,0);
        
        % Stimulus epochs
        sp = find(ismember(gmv.visstim(:,1:5),gmv.vistypz(v,1:5),'rows'));
        
        % No convergence epochs
        npconv = setdiff(sp,pconv);
        
        % Convergences in stim on time
        convonix = gmb.convergences(:,2) >= vSTt & ...
            gmb.convergences(:,2) <= vEDt;
        pconvon = intersect(sp,gmb.convergences(convix & convonix,1));
        
        % Spontaneous convergences
        pnconvon = intersect(sp,gmb.convergences(convix & ~convonix,1));
        
        X(f,k,1:3) = [length(pconvon), length(npconv) length(pnconvon)];
        k = k+1;
    end
end

%%

X2 = squeeze(sum(X,2));
y = X2(:,1)./sum(X2(:,1:2),2);
boxplot(y)
hold on
plot(1,y,'bo')