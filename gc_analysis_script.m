datadir = 'Y:\Pedro2\2P\NI_AF7_OT\HuC_GC6f\180327\f1\pre_abl';

%% Load structures

load(fullfile(datadir,'gm'),'gm');
load(fullfile(datadir,'gmv'),'gmv');
load(fullfile(datadir,'gmb'),'gmb');
load(fullfile(datadir,'gmbt'),'gmbt');
load(fullfile(datadir,'gmbf'),'gmbf');
load(fullfile(datadir,'gmranat'),'gmranat');
if exist(fullfile(datadir,'gmrxanat4.mat'),'file')
    quadscan = 1;
    load(fullfile(datadir,'gmrxanat4'),'gmrxanat4');
    gmrxanat = gmrxanat4; clear gmrxanat4;
else
    quadscan = 0;
    load(fullfile(datadir,'gmrxanat'),'gmrxanat');
end
load(fullfile(datadir,'gmVCNV'),'gmVCNV');

nvis = size(gmv.vistypz,1);

%% Get ROIs to analyse

% Get OT rois
[otrois,~] = gcROIextractZBBIdxMask(datadir, ...
    'optic tectum - stratum periventriculare', ...
    'Masks_ZBB');

% Get NI rois
[nirois,~] = gcROIextractZBBIdxMask(datadir, ...
    'nucleus isthmus - chata positive', ...
    'Masks_ZBB');

% Anterior NI from AF7 DiI
[anirois,~] = gcROIextractZBBIdxMask(datadir, ...
    'nucleus isthmus - anterior - AF7_DiI', ...
    'Masks_ZBB');

[leftrois,~] = gcROIextractZBBIdxMask(datadir, ...
    'Left side','Masks_ZBB');

[rightrois,~] = gcROIextractZBBIdxMask(datadir, ...
    'Right side','Masks_ZBB');

%%  Sanity check

gcROIplotcentroids(catpad(2,otrois,nirois,anirois),[],gm,gmranat);

%% Set up some parameters

z2a = 0;    % Restrict analyse to a set Z (0 == no restriction)
vstype = 5;
brainside = 0; % 0 = left, 1 = right, 2 = both
visualr = 0;    % Select only visually responsive rois
vtest2 = 1; alpha = 0.01;   % ttest for visual responsiveness
exclusive = 0;  % Select rois which is only visually responsive to only the analysed stim

% Stimulus on time
[vST, vED, vSTt, vEDt] = gcStimInfo(gmv.vistypz(vstype(1),1:5), gm.trfr, gm.frtime, 0);
if quadscan
    vST = vST*4;
    vED = vED*4;
end

%% Choose which rois to analyse

rois = unique(cat(1,nirois,anirois));  % Which rois to analyse

%% Restrict ROIs by parameters

% Vistype responsive ROIs only
if visualr
    rois_vrix = any(gmVCNV.ROI_H_MW(rois,vstype),2);
    
    if exclusive
        remvis = setdiff(1:nvis,vstype);
        rois_vrix = rois_vrix & ~any(gmVCNV.ROI_H_MW(rois,remvis),2);
    end
    
    if vtest2
        pVs = NaN(length(rois),1);
        pVix = false(length(rois),1);
        for i = 1:length(rois)
            roi = rois(i);
            X = gmrxanat.roi(roi).Vprofiles(vstype).zProfiles';
            Xz = (X-nanmean(reshape(X,numel(X),1)))/ ...
                nanstd(reshape(X,numel(X),1));
            baseline = sum(Xz(1:vST-1,:),2);
            stimline = sum(Xz(vST:vED,:),2);
            [pVix(i),pVs(i)] = ttest2(baseline,stimline,'Alpha',alpha);
        end
    else
        pVix = true(length(rois),1);
    end
    rois2a = rois(rois_vrix & pVix);
else
    rois2a = rois;
end

% Side of the brain
if brainside == 0
    rois2a = intersect(rois2a,leftrois);
elseif brainside == 1
    rois2a = intersect(rois2a,rightrois);
end

% Z restriction
if z2a ~= 0
    roizs = gmVCNV.ROIinfo(rois2a,2);
    rois2a = rois2a(roizs == z2a);
end

nrois = length(rois2a);
fprintf('%d rois to analyse\n',nrois)

%% Convergence epochs

% Stimulus epochs
sp = find(ismember(gmv.visstim(:,1:5),gmv.vistypz(vstype,1:5),'rows'));

% Good convergences
convix = gmb.convergences(:,3) == 1;
pconv = intersect(sp,gmb.convergences(convix,1));

% No convergence epochs
npconv = setdiff(sp,pconv);

% Convergences in stim on time
convonix = gmb.convergences(:,2) >= vSTt & ...
    gmb.convergences(:,2) <= vEDt;
pconvon = intersect(sp,gmb.convergences(convix & convonix,1));

% Convergences outside stim on time
pnconvon = intersect(sp,gmb.convergences(convix & ~convonix,1));

% Multiple bout stimulus response epochs
msbtp = unique([gmbf.b([gmbf.b.stimbout] == 2).p]);

% One stimulus response epochs
osbtp = setdiff(unique([gmbf.b([gmbf.b.stimbout] == 1).p]),msbtp);

% Convergence epochs with multiple responses
msbtcp = intersect(pconvon,msbtp);

% Convergence epochs with one response
osbtcp = intersect(pconvon,osbtp);

% Get v,z and rep indexes for presentations
[npconv_v,npconv_z,npconv_rep] = gcPresentationInfo(npconv,gm,gmv);
[pnconvon_v,pnconvon_z,pnconvon_rep] = gcPresentationInfo(pnconvon,gm,gmv);
[pconvon_v,pconvon_z,pconvon_rep] = gcPresentationInfo(pconvon,gm,gmv);
[osbtcp_v,osbtcp_z,osbtcp_rep] = gcPresentationInfo(osbtcp,gm,gmv);
[msbtcp_v,msbtcp_z,msbtcp_rep] = gcPresentationInfo(msbtcp,gm,gmv);

% Get convergence times
pnconvonix = find(ismember(gmb.convergences(:,1),pnconvon));
osbtcpix = find(ismember(gmb.convergences(:,1),osbtcp));
msbtcpix = find(ismember(gmb.convergences(:,1),msbtcp));

%%  Plot reps

plotmean = 1;
usefilt = 0;
filtsz = 15;
x = linspace(0,gm.nfr*gm.frtime/1000,gmrxanat.nfr);

for i = 1:length(rois2a)
    figure
    roi = rois2a(i);
    roiz = gmVCNV.ROIinfo(roi,2);
    X = gmrxanat.roi(roi).Vprofiles(vstype).zProfiles';
    Xz = (X-nanmean(reshape(X,numel(X),1)))/ ...
        nanstd(reshape(X,numel(X),1));
    if usefilt
        Xzf = sgolayfilt(double(Xz(:)),2,filtsz);
        Xzf = reshape(Xzf,size(Xz));
    else
        Xzf = Xz;
    end
    
    % No response
    if plotmean
        p1 = shadedErrorBar(x,nanmean(Xzf(:,npconv_rep(npconv_z == roiz)),2), ...
            nansem(Xz(:,npconv_rep(npconv_z == roiz)),2), ...
            {'color',[0.7 0.7 0.7]},1);
    else
        p1 = plot(x,Xzf(:,npconv_rep(npconv_z == roiz)),'Color',[0.7 0.7 0.7]);
    end
    hold on
    
    % Response outside stim on
    if any(pnconvon_z == roiz)
        if plotmean
            p2 = shadedErrorBar(x,nanmean(Xzf(:,pnconvon_rep(pnconvon_z == roiz)),2), ...
                nansem(Xz(:,pnconvon_rep(pnconvon_z == roiz)),2), ...
                {'color',[0.3 0.3 0.3]},1);
        else
            p2 = plot(x,Xzf(:,pnconvon_rep(pnconvon_z == roiz)),'Color',[0.3 0.3 0.3]);
        end
        for j = 1:length(pnconvonix)
            p = gmb.convergences(pnconvonix(j),1);
            pst = gmb.convergences(pnconvonix(j),2);
            pit = findnearest(pst,x,0);
            prep = pnconvon_rep(pnconvon == p & pnconvon_z == roiz);
            if~isempty(prep)
                plot(pst,Xzf(pit,prep),'*','Color',[0.3 0.3 0.3])
            end
        end
    else
        if plotmean
            p2 = shadedErrorBar([nan nan],[nan nan],[nan nan],{'color',[0.3 0.3 0.3]});
        else
            p2 = plot(-999,'Color',[0.3 0.3 0.3]);
        end
    end
    
    % One response
    if any(osbtcp_z == roiz)
        if plotmean
            p3 = shadedErrorBar(x,nanmean(Xzf(:,osbtcp_rep(osbtcp_z == roiz)),2), ...
                nansem(Xz(:,osbtcp_rep(osbtcp_z == roiz)),2), ...
                {'color','r'},1);
        else
            p3 = plot(x,Xzf(:,osbtcp_rep(osbtcp_z == roiz)),'Color','r');
        end
        for j = 1:length(osbtcpix)
            p = gmb.convergences(osbtcpix(j),1);
            pst = gmb.convergences(osbtcpix(j),2);
            pit = findnearest(pst,x,0);
            prep = osbtcp_rep(osbtcp == p & osbtcp_z == roiz);
            if ~isempty(prep)
                plot(pst,Xzf(pit,prep),'*r')
            end
        end
    else
        if plotmean
            p3 = shadedErrorBar([nan nan],[nan nan],[nan nan],'r');
        else
            p3 = plot(-999,'r');
        end
    end
    
    % Multiple responses
    if any(msbtcp_z == roiz)
        if plotmean
            p4 = shadedErrorBar(x,nanmean(Xzf(:,msbtcp_rep(msbtcp_z == roiz)),2), ...
                nansem(Xz(:,msbtcp_rep(msbtcp_z == roiz)),2), ...
                {'color','b'},1);
        else
            p4 = plot(x,Xzf(:,msbtcp_rep(msbtcp_z == roiz)),'Color','b');
        end
        for j = 1:length(msbtcpix)
            p = gmb.convergences(msbtcpix(j),1);
            pst = gmb.convergences(msbtcpix(j),2);
            pit = findnearest(pst,x,0);
            prep = msbtcp_rep(msbtcp == p & msbtcp_z == roiz);
            if ~isempty(prep)
                plot(pst,Xzf(pit,prep),'*b')
            end
        end
    else
        if plotmean
            p4 = shadedErrorBar([nan nan],[nan nan],[nan nan],'b');
        else
            p4 = plot(-999,'b');
        end
    end
    
    line([vSTt vSTt],[-2 10],'Color','k','LineStyle','--')
    line([vEDt vEDt],[-2 10],'Color','k','LineStyle','--')
    if plotmean
        legend([p1.mainLine(1) p2.mainLine(1) ...
            p3.mainLine(1) p4.mainLine(1)], ...
            {'No conv','Stim OFF conv', ...
            'Stim ON - One bout','Stim ON - Multi bouts'});
    else
        legend([p1(1) p2(1) p3(1) p4(1)], ...
            {'No conv','Stim OFF conv', ...
            'Stim ON - One bout','Stim ON - Multi bouts'});
    end
    axis([0 x(end) -2 10])
    title(sprintf('ROI # %d',rois2a(i)))
    xlabel('Time')
    ylabel('z-scored F')
end

%%  Find convergence modulated ROIs

nrois = length(gmVCNV.ROIinfo);

pVs = NaN(nrois,1);
pVix = false(nrois,1);
for roi = 1:nrois
    X = gmrxanat.roi(roi).Vprofiles(vstype).zProfiles';
    Xz = (X-nanmean(reshape(X,numel(X),1)))/ ...
        nanstd(reshape(X,numel(X),1));
    baseline = sum(Xz(1:vST-1,:),2);
    stimline = sum(Xz(vST:vED,:),2);
    [pVix(roi),pVs(roi)] = ttest2(baseline,stimline,'Alpha',alpha);
end
