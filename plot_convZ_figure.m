
datadir = 'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f2';
filespp = strsplit(datadir,filesep);
filename = strjoin(filespp(end-1:end),'_');

load(fullfile(datadir,'gmranat'));
load(fullfile(datadir,'gm'));
load(fullfile(datadir,'gmb'));
load(fullfile(datadir,'gmbt'));
load(fullfile(datadir,'gmConvMod'));

%%

saveplots = 0;
savedir = '\\128.40.168.141\data\Pedro\NI_project\Paper\Paper figures\Figure 7\data\ConvZ';
nfr = gm.nfr;
frtime = gm.frtime/1000;
xoffset = 6;
ylims = [-1 8];

roi = 7166;

z = gmConvMod.rois(roi).z;        
ixx = find(gmConvMod.rois(roi).F_on > 2);

for j = 1:length(gmConvMod.rois(roi).F_on)
    p = gmConvMod.rois(roi).pconv_on(j);
    v = gmConvMod.rois(roi).vis_on(j);
    zconv = gmConvMod.rois(roi).Z_on(j);
    cst = gmConvMod.rois(roi).Cst_on(j);
    
    % Get epoch times
    cit_on = gmConvMod.rois(roi).Cit_on(j);
    itst_on = cit_on-nfr/2+1;
    ited_on = cit_on+nfr/2;
    its_on = Tresampl(itst_on:ited_on,1);
    itspreix_on = find(diff(its_on(1:nfr/2)) < 1);
    if ~isempty(itspreix_on)
        its_on(1:itspreix_on) = -flip(1:itspreix_on);
    end
    itspostix_on = find(diff(its_on(nfr/2:end)) < 1);
    if ~isempty(itspostix_on)
        its_on(itspostix_on+nfr/2:end) = its_on(itspostix_on+nfr/2:end)+its_on(itspostix_on+nfr/2-1)+1;
    end
    ptime_on = its_on.*frtime;
    
    fig = figure('Position',[634,565,560,179], ...
        'name',sprintf('ROI = %d; p = %d; VIS = %d; Zconv = %1.2f',roi,p,v,zconv));
    plot(ptime_on,gmConvMod.rois(roi).Fz_on(j,:),'r','LineWidth',2);
    hold on
    plot(ptime_on,gmConvMod.rois(roi).Fz_on_nrsp{j}','k');
    line([ST_on(1,3) ST_on(1,3)],ylims,'Color','b','LineStyle','--')
    line([ST_on(end,3) ST_on(end,3)],ylims,'Color','b','LineStyle','--')
    line([cst cst],ylims,'Color','k','LineStyle','-','LineWidth',2)
    axis([cst-xoffset cst+xoffset ylims])
    ylabel('zF')
    set(gca,'Box','off','TickDir','out')
    
    fig2 = figure('Position',[634,565,120,179]);
    scatterSpread([],catpad(2,gmConvMod.rois(roi).F_on_nrsp{j},gmConvMod.rois(roi).F_on(j)))
    axis([0 3 ylims])
    set(gca,'XTick',1:2,'Box','off','TickDir','out')
    
    if saveplots
        savefig(fig,fullfile(savedir,sprintf('%s_%d_p%d',filename,roi,p)))
        print(fig,fullfile(savedir,[sprintf('%s_%d_p%d',filename,roi,p) '.svg']), ...
            '-dsvg');
        savefig(fig2,fullfile(savedir,sprintf('%s_%d_p%d_dist',filename,roi,p)))
        print(fig2,fullfile(savedir,[sprintf('%s_%d_p%d_dist',filename,roi,p) '.svg']), ...
            '-dsvg');
        close(fig)
        close(fig2)
    end
end