%%

folders = { ...
'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180301\f1';
'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180305\f2\deep';
'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180419\f1';
'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f1_1';
'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f2';
'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180427\f2';
'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180503\f2';
'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180504\f1';
'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180509\f3';
};

nfolders = length(folders);

%%  Load Mask database names

zbbanatix = [1:75 77 79 113 302 305:306];
load('ZBBMaskDatabase','ZBBMaskDatabaseNames');
ZBBMaskDatabaseNames = ZBBMaskDatabaseNames(zbbanatix);
nmsks = length(ZBBMaskDatabaseNames);

%%

zbbmsk = [];
roiszbbC = [];
HMW = [];
TMW = [];
ConvM = [];
T = [];
h = waitbar(0, 'Init');
for f = 1:length(folders)
    waitbar(f/length(folders),h, 'Processing...')
    datadir = folders{f};
    
    load(fullfile(datadir,'gmConvMod.mat'));
    load(fullfile(datadir,'gmranat'));
    
    
    HMW = cat(1,HMW,gmConvMod.H_mw_pv_corr);
    TMW = cat(1,TMW,gmConvMod.H_mw_ts);
    ConvM = cat(1,ConvM, ...
        cat(2,[gmConvMod.rois.Z_on_Lconv_mean]',[gmConvMod.rois.Z_on_Rconv_mean]', ...
        [gmConvMod.rois.Z_off_Lconv_mean]',[gmConvMod.rois.Z_off_Rconv_mean]'));
    T = cat(1,T,ones(size(gmConvMod.rois,2),1).*f);
    
    nzs = size(gmranat.z,2);
    for z = 1:nzs
        zbbmsk = cat(1,zbbmsk, ...
            cat(1,gmranat.z(z).STATScrop.Masks_ZBB));
        roiszbbC = cat(1,roiszbbC, ...
            cat(1,gmranat.z(z).STATScrop.Centroid_ZBB));
    end
    
    clear CvR gmranat
end
zbbmsk = logical(zbbmsk(:,zbbanatix));
close(h);

%%  Image of fraction of conv+ cells in reference brain

Cix = 3;    % Z conv type
alpha = 0.05;  % Z value which values above will be considered conv+
maptype = 1;    % 1 = conv+ fraction; 2 = mean Zconv
zbbmskix = [78];    % ZBB mask ID to analyse
plotboundaries = 0; % Plot mask boundaries
plotmskixs = [80 81];   % ZBB mask IDs to plot boundaries
binspace = 5; % microns
binoffset = 10; % microns
axisoffset = 5; % pixels
ylabs = {'Fraction of Conv+ ROIs','Mean Conv Z score'};

roisix = sum(zbbmsk(:,zbbmskix),2) > 0;

% Bin edges
edgx = floor(min(roiszbbC(:,1)))-binoffset:binspace:ceil(max(roiszbbC(:,1)))+binoffset;
edgy = floor(min(roiszbbC(:,2)))-binoffset:binspace:ceil(max(roiszbbC(:,2)))+binoffset;

[N,Xedges,Yedges,binX,binY] = histcounts2(roiszbbC(:,1),roiszbbC(:,2),edgx,edgy);
A = NaN(size(N));
for i = 1:size(N,1)
    for j = 1:size(N,2)
        ix = binX == i & binY == j;
        if any(ix)
            if maptype == 1
                A(i,j) = sum(HMW(ix & roisix,Cix) < alpha)/sum(ix & roisix);
            elseif maptype == 2
                A(i,j) = nanmean(TMW(ix & roisix,Cix));
            end
        end
    end
end
A(A == 0) = nan;

figure
imagesc(A);
set(gca,'XDir','reverse');
axis off equal
colormap hot
cbar = colorbar;
ylabel(cbar,ylabs{maptype})

if plotboundaries
    % Plot anatomical boundaries
    hold on       
    for i = 1:length(plotmskixs)
        mskrange = round(min(roiszbbC(zbbmsk(:,plotmskixs(i)),3))): ...
            round(max(roiszbbC(zbbmsk(:,plotmskixs(i)),3)));
        msk = reshape(full(ZBBMaskDatabase(:,plotmskixs(i))),[616,1030,420]);
        msk = msk(:,:,mskrange);
        [~,mskix] = max(squeeze(sum(sum(msk,1),2)));
        mskB = bwboundaries(msk(:,:,mskix));
        
        for j = 1:length(mskB)
            boundary = [mskB{j}-[edgy(1) edgx(1)]]./binspace;
            plot(boundary(:,1), boundary(:,2),':', ...
                'Color','w','LineWidth',1);
        end
    end
end

[r,c] = find(~isnan(A));
axis([min(c)-axisoffset max(c)+axisoffset ...
    min(r)-axisoffset max(r)+axisoffset])

%% Fraction of conv mod cells per anatomical region (sorted)

alpha = 0.01;
nmin = 50;
fish = unique(T(:,1));
M = NaN(nmsks,length(fish));
for i = 1:length(fish)
    for j = 1:nmsks
        ix = zbbmsk(:,j) == 1 & T(:,1) == fish(i);
        if sum(ix) > nmin
            M(j,i) = sum(HMW(ix,Cix) < alpha)/sum(ix);
        end
    end
end
Mm = nanmean(M,2);
Me = nansem(M,2);

[s,six] = sort(Mm,'descend','MissingPlacement','last');

figure('position',[50,190,1453,496])
bar(s(~isnan(s)));
hold on
errorbar(s(~isnan(s)),Me(six(~isnan(s))),'.k')
set(gca,'XTick',1:sum(~isnan(s)), ...
    'XTickLabel',ZBBMaskDatabaseNames(six(~isnan(s))), ...
    'XTickLabelRotation',45, ...
    'TickDir','out','Box','off')
xlabel('Anatomical ID')
ylabel('Fraction of Conv Modulated cells')
    
%%

