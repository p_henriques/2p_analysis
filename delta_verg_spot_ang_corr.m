
X = [];
h = waitbar(0, 'Init');
for f = 1:length(folders)
    waitbar(f/length(folders),h, 'Processing...')
    datadir = folders{f};
    load(fullfile(datadir,'gmb'));
    
    cix = find(gmb.convergences(:,3) == 1);
    nconv = length(cix);
    cp = gmb.convergences(cix,1);
    ld = diff(gmb.convergences(cix,7:8),1,2);
    rd = diff(-gmb.convergences(cix,9:10),1,2);
    
    A = NaN(nconv,1);
    for i = 1:nconv
        p = cp(i);
        t = gmb.convergences(cix(i),2);
        tix = findnearest(t,gmb.p(p).tt);
        fr = gmb.p(p).fr(tix);
        if ~isempty(gmb.p(p).vis_traj)
            fix = find(gmb.p(p).vis_traj(:,2) == fr);
            if ~isempty(fix)
                fix = round(mean(fix));
                A(i) = gmb.p(p).vis_traj(fix,3);
            end
        end
    end
    nix = isnan(A);
    
    A(nix) = [];
    ld(nix) = [];
    rd(nix) = [];
    
    X = cat(1,X,[ones(length(A),1).*f ld rd A]);
end
close(h)

%%

load(fullfile(datadir,'gm'));

fish = 5;
ix = X(:,1) == fish;

a = X(ix,4);
da = X(ix,2)-X(ix,3);

ar = -spotangle_v2(a,gm.visgeom.sweepamp,gm.visgeom.amax,gm.visgeom.R,gm.visgeom.fish2screen);
[rho,pv] = corr(ar,da);
[x,xix] = sort(ar,'descend');
y = da(xix);
fitobj = fit(x,y,'poly1');
fitbouds = predint(fitobj,x,0.95,'functional','on');

figure
plot(ar,da,'o')
hold on
plot(x,fitobj(x),'k')
plot(x,fitbouds,'k--')
fill([x;flip(x)]',[fitbouds(:,1);flip(fitbouds(:,2))]', ...
    'k','FaceAlpha',0.1,'EdgeAlpha',0)
xlabel('Spot angle')
ylabel('Delta eye vergence')
axis([-80 80 -40 40])
set(gca,'Box','off','TickDir','out')