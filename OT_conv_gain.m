datadir = 'Y:\Pedro2\2P\NI_AF7_OT\HuC_GC6f\180413\f1';

% Run tix function just in case
gcAddtixToStruct(datadir)

%% Load structures

load(fullfile(datadir,'gm'),'gm');
load(fullfile(datadir,'gmv'),'gmv');
load(fullfile(datadir,'gmb'),'gmb');
load(fullfile(datadir,'gmbt'),'gmbt');
load(fullfile(datadir,'gmbf'),'gmbf');
load(fullfile(datadir,'gmranat'),'gmranat');
if exist(fullfile(datadir,'gmrxanat4.mat'),'file')
    quadscan = 1;
    load(fullfile(datadir,'gmrxanat4'),'gmrxanat4');
    gmrxanat = gmrxanat4; clear gmrxanat4;
else
    quadscan = 0;
    load(fullfile(datadir,'gmrxanat'),'gmrxanat');
end
load(fullfile(datadir,'gmVCNV'),'gmVCNV');

%% Get ROIs to analyse

try
    % Get OT rois
    [otrois,~] = gcROIextractZBBIdxMask(datadir, ...
        'optic tectum - stratum periventriculare', ...
        'Masks_ZBB');
    
    % Get NI rois
    [nirois,~] = gcROIextractZBBIdxMask(datadir, ...
        'nucleus isthmus - chata positive', ...
        'Masks_ZBB');
    
    % Anterior NI from AF7 DiI
    [anirois,~] = gcROIextractZBBIdxMask(datadir, ...
        'nucleus isthmus - anterior - AF7_DiI', ...
        'Masks_ZBB');
    
    [leftrois,~] = gcROIextractZBBIdxMask(datadir, ...
        'Left side','Masks_ZBB');
    
    [rightrois,~] = gcROIextractZBBIdxMask(datadir, ...
        'Right side','Masks_ZBB');
    
    %  Sanity check
    
    gcROIplotcentroids(catpad(2,otrois,nirois,anirois),[],gm,gmranat);
    
    reg = 1;
catch
    reg = 0;
end


%% Loop through visual stimuli

showplots = 0;

nvis = size(gmv.vistypz,1);
rois = 1:length(gmrxanat.roi);
nrois = length(rois);
nzs = size(gmranat.z,2);

if quadscan
    frtime = gmrxanat.frtime/1000;
    nfr = gmrxanat.nfr;
    llim = 15*4;
    hlim = 20*4;
else
    frtime = gm.frtime/1000;
    nfr = gm.nfr;
    llim = 15;
    hlim = 20;
end
rlen = llim+hlim;

h = waitbar(0,'Initializing');
R = NaN(nrois,6,nvis);
for v = 1:nvis
    waitbar(v/nvis,h,'Processing')
    % Stimulus on time
    [vST, vED, vSTt, vEDt] = gcStimInfo(gmv.vistypz(v,1:5), gm.trfr, gm.frtime, 0);
    if quadscan
        vST = vST*4;
        vED = vED*4;
    end
    stimlen = length(vST:vED);
    
    %% Convergence epochs
    
    % Stimulus epochs
    sp = find(ismember(gmv.visstim(:,1:5),gmv.vistypz(v,1:5),'rows'));
    
    % Good convergences
    convix = gmb.convergences(:,3) == 1;
    pconv = intersect(sp,gmb.convergences(convix,1));
    
    % No convergence epochs
    npconv = setdiff(sp,pconv);
    
    % Convergences in stim on time
    convonix = gmb.convergences(:,2) >= vSTt & ...
        gmb.convergences(:,2) <= vEDt;
    pconvon = intersect(sp,gmb.convergences(convix & convonix,1));
    
    % Convergences outside stim on time
    pnconvon = intersect(sp,gmb.convergences(convix & ~convonix,1));
    
    % Epochs with multiple high vergence bouts
    msbtix = [gmbf.b.stimbout] == 2 & [gmbf.b.Vergbout] == 1;
    msbtp = unique([gmbf.b(msbtix).p]);
    
    % Epochs with only one bout
    osbtp = setdiff(unique([gmbf.b([gmbf.b.stimbout] == 1).p]),msbtp);
    
    % Convergence epochs with multiple bouts
    msbtcp = intersect(pconvon,msbtp);
    
    % Convergence epochs with one bout
    osbtcp = intersect(pconvon,osbtp);
    
    % Get v,z and rep indexes for presentations
    [npconv_v,npconv_z,npconv_rep] = gcPresentationInfo(npconv,gm,gmv);
    [pnconvon_v,pnconvon_z,pnconvon_rep] = gcPresentationInfo(pnconvon,gm,gmv);
    [pconvon_v,pconvon_z,pconvon_rep] = gcPresentationInfo(pconvon,gm,gmv);
    [osbtcp_v,osbtcp_z,osbtcp_rep] = gcPresentationInfo(osbtcp,gm,gmv);
    [msbtcp_v,msbtcp_z,msbtcp_rep] = gcPresentationInfo(msbtcp,gm,gmv);
    
    % Get convergence times
    pnconvonix = find(ismember(gmb.convergences(:,1),pnconvon));
    osbtcpix = find(ismember(gmb.convergences(:,1),osbtcp));
    msbtcpix = find(ismember(gmb.convergences(:,1),msbtcp));
    
    %%
    
    xt = 0:frtime:(nfr-1)*frtime;
    filtsz = 11;
    if quadscan
        filtsz = filtsz*4;
        if mod(filtsz,2) == 0
            filtsz = filtsz+1;
        end
    end
    
    for roi = 1:nrois
        z = gmVCNV.ROIinfo(roi,2);
        nreps = size(gmrxanat.roi(roi).Vprofiles(v).zProfiles,1);
        
        X = gmrxanat.roi(roi).Vprofiles(v).zProfiles';
        Xz = (X-nanmean(reshape(X,numel(X),1))) / ...
            nanstd(reshape(X,numel(X),1));
        Xzf = sgolayfilt(double(Xz(:)),2,filtsz);
        Xzf = reshape(Xzf,size(Xz));
        
        [~,mix] = max(Xzf(vST:vED,:),[],1);
        mix = mix+vST-1;
        
        RON = NaN(rlen,nreps);
        ROFF = NaN(vST,nreps);
        for r = 1:nreps
            RON(:,r) = Xz(mix(r)-llim+1:mix(r)+hlim,r);
            ROFF(:,r) = Xz(1:vST,r);
        end
        roiONR = nansum(RON);
        roiOFFR = nansum(ROFF);
        
        roi_npconv_ix = npconv_rep(npconv_z == z);
        roi_pconvon_ix = pconvon_rep(pconvon_z == z);
        
        roi_npconv_ONR = roiONR(roi_npconv_ix);
        roi_pconvon_ONR = roiONR(roi_pconvon_ix);
        roi_npconv_OFFR = roiOFFR(roi_npconv_ix);
        roi_pconvon_OFFR = roiOFFR(roi_pconvon_ix);
        
        R(roi,1,v) = nanmean(roi_npconv_ONR);
        R(roi,2,v) = nanmean(roi_pconvon_ONR);
        if ~isempty(roi_npconv_ONR) && ~isempty(roi_pconvon_ONR)
            R(roi,3,v) = ranksum(roi_npconv_ONR,roi_pconvon_ONR);
        end
        R(roi,4,v) = nanmean(roi_npconv_OFFR);
        R(roi,5,v) = nanmean(roi_pconvon_OFFR);
        if ~isempty(roi_npconv_OFFR) && ~isempty(roi_pconvon_OFFR)
            R(roi,6,v) = ranksum(roi_npconv_OFFR,roi_pconvon_OFFR);
        end
        
        if R(roi,3,v) <= 0.03 && R(roi,6,v) >= 0.05 && showplots
            
            centix = find([gmranat.z(z).STATScrop.Label] == gmVCNV.ROIinfo(roi,1));
            cent = gmranat.z(z).STATScrop(centix).Centroid;
            
            figure('name',sprintf('ROI = %d; VIS = %d; Z = %d', ...
                roi,v,z))
            subplot(1,2,1)
            imagesc(gm.zimg(z).aligngood2,[0 20])
            axis('square','off'); colormap(gca,'gray');
            newcolorbar
            scatter(cent(1),cent(2),'r','filled')
            set(gca,'YDir','reverse');
            axis('square','off');
            subplot(1,2,2)
            p1 = shadedErrorBar(xt,nanmean(Xzf(:,roi_npconv_ix),2), ...
                nansem(Xzf(:,roi_npconv_ix),2),'k',1);
            hold on
            p2 = shadedErrorBar(xt,nanmean(Xzf(:,roi_pconvon_ix),2), ...
                nansem(Xzf(:,roi_pconvon_ix),2),'b',1);
%             ylims = ylim;
            ylims = [-1.5 3];
            line([vSTt vSTt],[ylims(1) ylims(2)],'LineStyle','--','Color','k')
            line([vEDt vEDt],[ylims(1) ylims(2)],'LineStyle','--','Color','k')
            axis([xt(1) xt(end) ylims],'square')
            xlabel('Time (s)');
            ylabel('Z-scored F (filtered)')
            legend([p1.mainLine(1) p2.mainLine(1)], ...
                {'NO-Conv','Conv'},'Location','best');
        end
    end
end
close(h)

%%  Plot modulation maps

caxisrange = [-70 70];
imrange = [0 20];
pthr = 1;
showimg = 1;
viss = 1:nvis;
zs = 1:nzs;

for v = viss
    RMthr = 0;
    sigrois = R(:,3,v) <= pthr & R(:,6,v) >= 0.05;
    
    x = -1.5:0.01:3;
    cmap = lbmap(length(x),'RedBlue');
    
    nzs = size(gmv.averageresp.z,2);
    RM = round(R(:,2,v)-R(:,1,v),2);
    RMix = abs(RM) >= RMthr;
    Csz = -log10(R(:,3,v))*15;
    Csz(Csz == 0) = nan;
    
    C = [];
    for z = 1:nzs
        C = cat(1,C,cat(1,gmranat.z(z).STATScrop.Centroid));
    end
        
    for z = zs
        zix = gmVCNV.ROIinfo(:,2) == z;
        roisix = zix & RMix & sigrois;
        
        if any(roisix)
            
            fig = figure('name',sprintf('VIS = %d; Z = %d',v,z), ...
                'position',[747 571 309 293]);
            if showimg
                imagesc(gm.zimg(z).aligngood2,imrange)
                axis('off','square')
                colormap(gca,'gray')
                hold on
                newcolorbar
            end
            scatter(C(roisix,1),C(roisix,2),Csz(roisix),RM(roisix),'filled')
            axis('off','square')
            colormap redbluecmap
            set(gca,'YDir','reverse')
            caxis(caxisrange)
            title(sprintf('Z = %d',z));
            
            if ~exist(fullfile(datadir,'analysis','figures','modulation'),'dir')
                mkdir(fullfile(datadir,'analysis','figures','modulation'));
            end
            saveas(fig,fullfile(datadir,'analysis','figures','modulation', ...
                sprintf('Modulation_plot_VIS_%d_Z_%d.fig',v,z)))
        end
    end
end


%%

ylims = [-1.5 3];
rois2a = find(any(R(:,3,4:6) <= 0.05,3) & ...
    any(R(:,6,4:6) >= 0.05,3) & ...
    ~any(R(:,3,1:2) <= 0.05,3));
if reg
    rois2a = intersect(otrois,rois2a);
end

[roispsort,roispix] = sort(min(squeeze(R(rois2a,3,4:6)),[],2),'ascend');
% [roispsort,roispix] = sort(max(abs(squeeze(R(rois2a,2,4:6)) - ...
%     squeeze(R(rois2a,1,4:6))),[],2),'descend');

rois2a = rois2a(roispix);
rois2apv = squeeze(R(rois2a,3,:));

for i = 11:20
    roi = rois2a(i);
    z = gmVCNV.ROIinfo(roi,2);
    nreps = size(gmrxanat.roi(roi).Vprofiles(v).zProfiles,1);
    
    centix = find([gmranat.z(z).STATScrop.Label] == gmVCNV.ROIinfo(roi,1));
    cent = gmranat.z(z).STATScrop(centix).Centroid;
    
    figure('name',sprintf('ROI = %d; Z = %d', ...
        roi,z),'Position',[410,388,1034,463]);
    subplot(2,4,[1,2,5,6])
    imagesc(gm.zimg(z).aligngood2,[0 20])
    axis('square','off'); colormap(gca,'gray');
    newcolorbar
    scatter(cent(1),cent(2),'r','filled')
    set(gca,'YDir','reverse');
    axis('square','off');
    
    for v = 3:6
        [vST, vED, vSTt, vEDt] = gcStimInfo(gmv.vistypz(v,1:5), gm.trfr, gm.frtime, 0);
        if quadscan
            vST = vST*4;
            vED = vED*4;
        end
        stimlen = length(vST:vED);
        
        %% Convergence epochs
        
        % Stimulus epochs
        sp = find(ismember(gmv.visstim(:,1:5),gmv.vistypz(v,1:5),'rows'));
        
        % Good convergences
        convix = gmb.convergences(:,3) == 1;
        pconv = intersect(sp,gmb.convergences(convix,1));
        
        % No convergence epochs
        npconv = setdiff(sp,pconv);
        
        % Convergences in stim on time
        convonix = gmb.convergences(:,2) >= vSTt & ...
            gmb.convergences(:,2) <= vEDt;
        pconvon = intersect(sp,gmb.convergences(convix & convonix,1));
        
        % Convergences outside stim on time
        pnconvon = intersect(sp,gmb.convergences(convix & ~convonix,1));
        
        % Multiple bout stimulus response epochs
        msbtp = unique([gmbf.b([gmbf.b.stimbout] == 2).p]);
        
        % One stimulus response epochs
        osbtp = setdiff(unique([gmbf.b([gmbf.b.stimbout] == 1).p]),msbtp);
        
        % Convergence epochs with multiple responses
        msbtcp = intersect(pconvon,msbtp);
        
        % Convergence epochs with one response
        osbtcp = intersect(pconvon,osbtp);
        
        % Get v,z and rep indexes for presentations
        [npconv_v,npconv_z,npconv_rep] = gcPresentationInfo(npconv,gm,gmv);
        [pnconvon_v,pnconvon_z,pnconvon_rep] = gcPresentationInfo(pnconvon,gm,gmv);
        [pconvon_v,pconvon_z,pconvon_rep] = gcPresentationInfo(pconvon,gm,gmv);
        [osbtcp_v,osbtcp_z,osbtcp_rep] = gcPresentationInfo(osbtcp,gm,gmv);
        [msbtcp_v,msbtcp_z,msbtcp_rep] = gcPresentationInfo(msbtcp,gm,gmv);
        
        % Get convergence times
        pnconvonix = find(ismember(gmb.convergences(:,1),pnconvon));
        osbtcpix = find(ismember(gmb.convergences(:,1),osbtcp));
        msbtcpix = find(ismember(gmb.convergences(:,1),msbtcp));
        
        % Plots stuff
        
        if any(v == 3:4)
            plottile = v;
        else
            plottile = v+2;
        end
        
        pv = R(roi,3,v);
        
        X = gmrxanat.roi(roi).Vprofiles(v).zProfiles';
        Xz = (X-nanmean(reshape(X,numel(X),1)))/ ...
            nanstd(reshape(X,numel(X),1));
        Xzf = sgolayfilt(double(Xz(:)),2,filtsz);
        Xzf = reshape(Xzf,size(Xz));
        
        roi_npconv_ix = npconv_rep(npconv_z == z);
        roi_pconvon_ix = pconvon_rep(pconvon_z == z);
        
        subplot(2,4,plottile)
        p1 = shadedErrorBar(xt,nanmean(Xzf(:,roi_npconv_ix),2), ...
            nansem(Xzf(:,roi_npconv_ix),2),'k',1);
        hold on
        p2 = shadedErrorBar(xt,nanmean(Xzf(:,roi_pconvon_ix),2), ...
            nansem(Xzf(:,roi_pconvon_ix),2),'b',1);
        line([vSTt vSTt],[ylims(1) ylims(2)],'LineStyle','--','Color','k')
        line([vEDt vEDt],[ylims(1) ylims(2)],'LineStyle','--','Color','k')
        axis([xt(1) xt(end) ylims],'square')
        
        if any(v == [3,5])
            ylabel('Z-scored F')
        end
        if any(v == 5:6)
            xlabel('Time (s)')
        end
        if pv < 0.05
            color = 'r';
        else
            color = 'k';
        end
        title(sprintf('V = %d; p = %0.4f',v,pv),'Color',color)
    end
end
