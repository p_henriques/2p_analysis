load('Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\analysis\Convergence\Z_conv_matrix_181015.mat');
load('Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\analysis\Clustering\VRV_clustering\rawtraces\mf.mat')

%%  Load Mask database names

zbbanatix = [2:3 6:34 44:46 48:74 77 113 305:306];
load('ZBBMaskDatabase','ZBBMaskDatabaseNames');
ZBBMaskDatabaseNames = ZBBMaskDatabaseNames(zbbanatix);

%%

Tmf = [mf.fishID mf.roilabels];
Tix = ismember(T,Tmf,'rows');
T = T(Tix);
nrois = length(T);
roiszbbC = roiszbbC(Tix,:);
ConvM = ConvM(Tix,:);
zbbmsk = zbbmsk(Tix,:);

MFix = mf.superMFix_x;
nMFix = setdiff([1:nrois]',MFix);
Idx = mf.superIdx_x;
cnamez = mf.supernamez_x;
ccol = [1 0 1; 0 1 0; .5 .5 .5];   % Cluster color

%%

usefrac = 1;
maxZ = 20000;
zthr = 3;
ylabs = {'Prey-like','WF','Unclustered'};
wfix = Idx == 1;
plix = Idx == 2 | Idx == 3;

% msk2plt = [64 30 26 9 65 31 39 45 62];
% msklabs = {'aCh-A','OT-neuropil','nIII','Gt', ...
%     'pCh-A','OT-SPV','Pt','Teg','Ch-B'};
msk2plt = [64 65 62];
msklabs = {'aCh-A','pCh-A','Ch-B'};
nmsks = length(msk2plt);

X = NaN(nmsks,3,2,4);
k = 1;
for i = 1:2:4
    for m = 1:nmsks        
        lix = roiszbbC(MFix,2) > 308;
        mix = zbbmsk(MFix,msk2plt(m));
        
        % Prey-like responsive
        ix1 = MFix(mix & plix & lix);
        ix2 = MFix(mix & plix & ~lix);
        Zipsi = cat(1,ConvM(ix1,i),ConvM(ix2,i+1));
        Zipsinull = cat(1,ConvM(ix1,i+4),ConvM(ix2,i+5));
        [r,~] = find(abs(Zipsi) > maxZ);
        Zipsi(r,:) = [];
        Zipsinull(r,:) = [];
        if usefrac
            X(m,1,k,1) = sum(Zipsi >= zthr)/size(Zipsi,1);
            X(m,1,k,2) = nan;
            X(m,1,k,3) = sum(Zipsinull >= zthr)/size(Zipsinull,1);
            X(m,1,k,4) = nan;
        else
            X(m,1,k,1) = nanmean(Zipsi);
            X(m,1,k,2) = nansem(Zipsi);
            X(m,1,k,3) = nanmean(Zipsinull);
            X(m,1,k,4) = nansem(Zipsinull);
        end
        
        % WF responsive        
        ix1 = MFix(mix & wfix & lix);
        ix2 = MFix(mix & wfix & ~lix);
        Zipsi = cat(1,ConvM(ix1,i),ConvM(ix2,i+1));
        Zipsinull = cat(1,ConvM(ix1,i+4),ConvM(ix2,i+5));
        [r,~] = find(abs(Zipsi) > maxZ);
        Zipsi(r,:) = [];
        Zipsinull(r,:) = [];
        if usefrac
            X(m,2,k,1) = sum(Zipsi >= zthr)/size(Zipsi,1);
            X(m,2,k,2) = nan;
            X(m,2,k,3) = sum(Zipsinull >= zthr)/size(Zipsinull,1);
            X(m,2,k,4) = nan;
        else
            X(m,2,k,1) = nanmean(Zipsi);
            X(m,2,k,2) = nan;
            X(m,2,k,3) = nanmean(Zipsinull);
            X(m,2,k,4) = nansem(Zipsinull);
        end
        
        % Not clustered        
        lix = roiszbbC(nMFix,2) > 308;
        mix = zbbmsk(nMFix,msk2plt(m));
        ix1 = nMFix(mix & lix);
        ix2 = nMFix(mix & ~lix);
        Zipsi = cat(1,ConvM(ix1,i),ConvM(ix2,i+1));
        Zipsinull = cat(1,ConvM(ix1,i+4),ConvM(ix2,i+5));
        [r,~] = find(abs(Zipsi) > maxZ);
        Zipsi(r,:) = [];
        Zipsinull(r,:) = [];
        if usefrac
            X(m,3,k,1) = sum(Zipsi >= zthr)/size(Zipsi,1);
            X(m,3,k,2) = nan;
            X(m,3,k,3) = sum(Zipsinull >= zthr)/size(Zipsinull,1);
            X(m,3,k,4) = nan;
        else
            X(m,3,k,1) = nanmean(Zipsi);
            X(m,3,k,2) = nansem(Zipsi);
            X(m,3,k,3) = nanmean(Zipsinull);
            X(m,3,k,4) = nansem(Zipsinull);
        end
    end
    k = k+1;
end

% Plot
[~,bar_xtick2,hb] = errorbar_groups_ph(X(:,:,1,1)',X(:,:,1,2)','bar_colors',ccol);
set(gca,'XTickLabel',msklabs, ...
    'XTickLabelRotation',45, ...
    'TickDir','out','Box','off')
xlabel('Anatomical ID')
if usefrac
    ylabel(sprintf('Fraction VRV CZ-score + (> %d)',zthr))
else
    ylabel('Mean CZ-score (Ipsi conv)')
end
% ylim([-0.5 3.5])
p1 = line(bar_xtick2(1:2,:)-(0.9/2),repmat(X(:,1,1,3),1,2)', ...
    'LineStyle',':','Color','k','LineWidth',2);
line(bar_xtick2(1:2,:)+(0.9/2),repmat(X(:,2,1,3),1,2)', ...
    'LineStyle',':','Color','k','LineWidth',2);
line(bar_xtick2(2:3,:)+(0.9/2),repmat(X(:,3,1,3),1,2)', ...
    'LineStyle',':','Color','k','LineWidth',2);
legend([hb;p1],[ylabs 'Shuffle'],'Location','northoutside')


[~,bar_xtick2,hb] = errorbar_groups_ph(X(:,:,2,1)',X(:,:,2,2)','bar_colors',ccol);
set(gca,'XTickLabel',msklabs, ...
    'XTickLabelRotation',45, ...
    'TickDir','out','Box','off')
xlabel('Anatomical ID')
if usefrac
    ylabel(sprintf('Fraction VRV CZ-score + (> %d)',zthr))
else
    ylabel('Mean SpCZ-score (Ipsi conv)')
end
% ylim([-0.5 3.5])
p1 = line(bar_xtick2(1:2,:)-(0.9/2),repmat(X(:,1,2,3),1,2)', ...
    'LineStyle',':','Color','k','LineWidth',2);
line(bar_xtick2(1:2,:)+(0.9/2),repmat(X(:,2,2,3),1,2)', ...
    'LineStyle',':','Color','k','LineWidth',2);
line(bar_xtick2(2:3,:)+(0.9/2),repmat(X(:,3,2,3),1,2)', ...
    'LineStyle',':','Color','k','LineWidth',2);
legend([hb;p1],[ylabs 'Shuffle'],'Location','northoutside')

%%

zthr = 3;
ylabs = {'Prey-like','WF','Unclustered'};
wfix = Idx == 1;
plix = Idx == 2 | Idx == 3;

% msk2plt = [64 30 26 9 65 31 39 45 62];
% msklabs = {'aCh-A','OT-neuropil','nIII','Gt', ...
%     'pCh-A','OT-SPV','Pt','Teg','Ch-B'};

msk2plt = [64 65 62];
msklabs = {'aCh-A','pCh-A','Ch-B'};

nmsks = length(msk2plt);

X = NaN(nmsks,3,2,2);
N = NaN(nmsks,2);
k = 1;
for i = 1:2:4
    for m = 1:nmsks        
        lix = roiszbbC(:,2) > 308;
        mix = zbbmsk(:,msk2plt(m));
        lix2 = roiszbbC(MFix,2) > 308;
        mix2 = zbbmsk(MFix,msk2plt(m));                                
        
        Zipsi_all = cat(1,ConvM(mix & lix,i),ConvM(mix & ~lix,i+1));
        den = sum(Zipsi_all > zthr);
                
        % Prey-like responsive        
        ix1 = MFix(mix2 & plix & lix2);
        ix2 = MFix(mix2 & plix & ~lix2);
        Zipsi = cat(1,ConvM(ix1,i),ConvM(ix2,i+1));
        Zipsinull = cat(1,ConvM(ix1,i+4),ConvM(ix2,i+5));
        num = sum(Zipsi > zthr);
        num_null = sum(Zipsinull > zthr);
                
        X(m,1,k,1) = num/den;        
        X(m,1,k,2) = num_null/den;             
        
        % WF responsive        
        ix1 = MFix(mix2 & wfix & lix2);
        ix2 = MFix(mix2 & wfix & ~lix2);
        Zipsi = cat(1,ConvM(ix1,i),ConvM(ix2,i+1));
        Zipsinull = cat(1,ConvM(ix1,i+4),ConvM(ix2,i+5));
        num = sum(Zipsi > zthr);
        num_null = sum(Zipsinull > zthr);
                
        X(m,2,k,1) = num/den;
        X(m,2,k,2) = num_null/den;
        
        % Not clustered   
        lix3 = roiszbbC(nMFix,2) > 308;
        mix3 = zbbmsk(nMFix,msk2plt(m));
        
        ix1 = nMFix(mix3 & lix3);
        ix2 = nMFix(mix3 & ~lix3);
        Zipsi = cat(1,ConvM(ix1,i),ConvM(ix2,i+1));
        Zipsinull = cat(1,ConvM(ix1,i+4),ConvM(ix2,i+5));
        num = sum(Zipsi > zthr);
        num_null = sum(Zipsinull > zthr);
                
        X(m,3,k,1) = num/den;
        X(m,3,k,2) = num_null/den;
       
        N(m,k) = den;
    end
    k = k+1;
end

for i = 1:2
    figure
    b1 = bar(X(:,:,i,1),'stacked');
    set(gca,'XTickLabel',msklabs, ...
        'XTickLabelRotation',45, ...
        'TickDir','out','Box','off')
    for i = 1:3
        b1(i).FaceColor = ccol(i,:);
    end
    
    xlabel('Anatomical ID')
    ylabel(sprintf('Fraction CZ-score + (> %d)',zthr))
    legend(ylabs,'Location','northoutside')
end
