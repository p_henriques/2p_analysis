% Plot single responses from Conv positive neurons

folders = { ...
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180301\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180305\f2\deep';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180419\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f1_1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180427\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180503\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180504\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180509\f3';
    };

%%

zbbanatix = [1:3 5:74 77 113 305:306];
load('ZBBMaskDatabase','ZBBMaskDatabaseNames');
ZBBMaskDatabaseNames = ZBBMaskDatabaseNames(zbbanatix);
nmsks = length(ZBBMaskDatabaseNames);

%%

f = 5;
datadir = folders{f};
load(fullfile(datadir,'gmranat'));
load(fullfile(datadir,'gm'));
load(fullfile(datadir,'gmb'));
load(fullfile(datadir,'gmbt'));
load(fullfile(datadir,'gmConvMod'));

nfr = gm.nfr;
frtime = gm.frtime/1000;

% Concatenate ZBB masks for all rois
nzs = size(gmranat.z,2);
zbbmsk = [];
roisC = [];
roiszbbC = [];
for z = 1:nzs
    zbbmsk = cat(1,zbbmsk, ...
        cat(1,gmranat.z(z).STATScrop.Masks_ZBB));
    roisC = cat(1,roisC, ...
        cat(1,gmranat.z(z).STATScrop.Centroid));
    roiszbbC = cat(1,roiszbbC, ...
        cat(1,gmranat.z(z).STATScrop.Centroid_ZBB));
end
zbbmsk = logical(zbbmsk(:,zbbanatix));

%% Plot single conv responses

zbbix = 76;
zbbixx = zbbmsk(:,zbbix) == 1;
ft_width = 3;
fe_width = 3;
toffset = 5;    % Time offset (s)
useipsi = 0; % Use only ipsilateral convergence epochs

rois2a = find(zbbixx);

[c_srt,c_six] = sort(cellfun(@nanmean,{gmConvMod.rois(rois2a).Z_on}),'descend');

for i = 1:20
    roi = rois2a(c_six(i));
    z = gmConvMod.rois(roi).z;
        
    ixx = find(gmConvMod.rois(roi).F_on > 2);
    if useipsi
        lix = roiszbbC(roi,2) > 308;
        if lix
            ixx = ixx(gmConvMod.rois(roi).Lconv_on(ixx) == 1);
        else
            ixx = ixx(gmConvMod.rois(roi).Lconv_on(ixx) == 0);
        end
    end
    
    % Resampled tail
    Tresampl = cat(1,gmbt.p(gm.zindices(z).e).resampled);
    
    if ~isempty(ixx)        
        for j = 1:length(ixx)
            p = gmConvMod.rois(roi).pconv_on(ixx(j));
            v = gmConvMod.rois(roi).vis_on(ixx(j));
            zconv = gmConvMod.rois(roi).Z_on(ixx(j));
            cst = gmConvMod.rois(roi).Cst_on(ixx(j));
            
            % Spot traj
            ST_on = gmb.p(p).vis_traj(:,[2:3,7]);
            ST_on = ST_on(abs(ST_on(:,2)) < 999,:);  % Remove bogus values
            sang_on = spotangle_v2(ST_on(:,2), ...
                gm.visgeom.sweepamp,gm.visgeom.amax, ...
                gm.visgeom.R,gm.visgeom.fish2screen);   % Transform to angles
            % Get epoch times
            cit_on = gmConvMod.rois(roi).Cit_on(ixx(j));
            itst_on = cit_on-nfr/2+1;
            ited_on = cit_on+nfr/2;
            its_on = Tresampl(itst_on:ited_on,1);
            itspreix_on = find(diff(its_on(1:nfr/2)) < 1);
            if ~isempty(itspreix_on)
                its_on(1:itspreix_on) = -flip(1:itspreix_on);
            end
            itspostix_on = find(diff(its_on(nfr/2:end)) < 1);
            if ~isempty(itspostix_on)
                its_on(itspostix_on+nfr/2:end) = its_on(itspostix_on+nfr/2:end)+its_on(itspostix_on+nfr/2-1)+1;
            end
            ptime_on = its_on.*frtime;
            % Tail
            ttime = gmbt.p(p).tt;
            T = gmbt.p(p).cumtail;
            Tf = filtfilt(ones(1, ft_width)./ft_width, 1, T);
            % Eyes
            etime = gmb.p(p).tt;
            Er = gmb.p(p).Rangles;
            El = gmb.p(p).Langles;
            Ev = El-Er;
            Evf = filtfilt(ones(1, fe_width)./fe_width, 1, Ev);
            
            % Plot
            figure('name',sprintf('ROI = %d; p = %d; VIS = %d; Zconv = %1.2f',roi,p,v,zconv))
            subplot(2,1,1)
            plot(ptime_on,gmConvMod.rois(roi).Fz_on(ixx(j),:),'r','LineWidth',2);
            hold on
            plot(ptime_on,gmConvMod.rois(roi).Fz_on_nrsp{ixx(j)}','k');
            ylims = ylim;
            line([ST_on(1,3) ST_on(1,3)],ylims,'Color','b','LineStyle','--')
            line([ST_on(end,3) ST_on(end,3)],ylims,'Color','b','LineStyle','--')
            line([cst cst],ylims,'Color','k','LineStyle','-','LineWidth',2)
            xlim([ptime_on(1)+toffset ptime_on(end)-toffset])
            ylabel('zF')
            set(gca,'Box','off','TickDir','out')
            
            subplot(2,1,2)
            plot(ttime,Tf,'g')
            hold on
            plot(etime,Evf,'k')
            axis([ptime_on(1)+toffset ptime_on(end)-toffset -120 120])
            ylabel('Angle')
            yyaxis right
            plot(ST_on(:,3),sang_on,'Color',[.6 .6 .6])
            axis([ptime_on(1)+toffset ptime_on(end)-toffset -75 75])
            xlabel('Epoch time (s)')
            ylabel('Spot angle')
            set(gca,'Box','off','TickDir','out')
        end
    end
end
