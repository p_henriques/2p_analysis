%%  Build ROI x features matrix

folders = { ...
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180301\f1';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180305\f2\deep';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180419\f1';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f1_1';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f2';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180427\f2';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180503\f2';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180504\f1';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180509\f3';
    };

nfolders = length(folders);

%%

Rinfo = table;
R = [];
roiszbbC = [];
zbbmsk = [];
h = waitbar(0, 'Init');
for f = 1:nfolders
    waitbar(f/length(folders),h, 'Processing...')
    datadir = folders{f};
    load(fullfile(datadir,'gmROIfvec'));
    load(fullfile(datadir,'gmranat'));  
    
    if f == 1
        Rnames = gmROIfvec.reg_names;
    end
    
    X = gmROIfvec.Xr;    
    R = cat(1,R,X);
    
    % ZBB masks and centroids
    nzs = size(gmranat.z,2);
    ZM = [];    % Mask
    C = []; % Centroids
    for z = 1:nzs
        ZM = cat(1,ZM,cat(1,gmranat.z(z).STATScrop.Masks_ZBB));
        C = cat(1,C,cat(1,gmranat.z(z).STATScrop.Centroid_ZBB));
    end
    zbbmsk = cat(1,zbbmsk,ZM);
    roiszbbC = cat(1,roiszbbC,C);
    
    rois = 1:size(X,1);
    fish = ones(size(X,1),1).*f;    
    Rinfo = cat(1,Rinfo, ...
        table(fish,rois'));
end
zbbmsk = logical(zbbmsk);
close(h)

clear C ZM rois fish gmROIfvec gmranat X nzs z f datadir

%%

zbbanatix = [2:3 6:34 44:46 48:74 77 113 305:306];
load('ZBBMaskDatabase','ZBBMaskDatabaseNames');
ZBBMaskDatabaseNames = ZBBMaskDatabaseNames(zbbanatix);
nmsks = length(ZBBMaskDatabaseNames);
zbbmsk = zbbmsk(:,zbbanatix);

%%

rix = 1:10;
msk2plt = [9 25 26 30 31 39 53 54 62 64 65];
ylims = [-0.02 0.07];

for n = 1:length(msk2plt)
    mix = zbbmsk(:,msk2plt(n));
    
    M = R(mix,rix);
    y = nanmean(M);
    yerr = nansem(M);
    
    figure
    bar(y)
    hold on
    errorbar(y,yerr,'.k')
    set(gca,'XTickLabel',Rnames(rix), ...
        'XTickLabelRotation',45, ...
        'Box','off','TickDir','out')
    ylabel('Mean correlation coeff')
    title(ZBBMaskDatabaseNames{msk2plt(n)})
    ylim(ylims);
end