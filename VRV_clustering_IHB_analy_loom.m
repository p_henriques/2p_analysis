load('\\128.40.168.141\data\Pedro\2P\NI\NI_Optoma\HuC_H2B_GC6s\Analysis\Clustering\IHBclust\mf.mat');
load('\\128.40.168.141\data\Pedro\2P\NI\NI_Optoma\HuC_H2B_GC6s\Analysis\Clustering\loom_clust_kmeans_180909_IHB.mat');

zbbanatix = [2:3 6:34 44:46 48:74 77 113 305:306];
load('ZBBMaskDatabase');
ZBBMaskDatabaseNames = ZBBMaskDatabaseNames(zbbanatix);
ZBBMaskDatabase = ZBBMaskDatabase(:,zbbanatix);
nmsk = size(ZBBMaskDatabase,2);

% Visstim timings
load(fullfile(mf.datasetz{1},'gm'),'gm');
load(fullfile(mf.datasetz{1},'gmv'),'gmv');
vST = gm.trfr + 2;
vED = gm.trfr + 1 + ceil(1000.*gmv.vistypz( ...
    ismember(gmv.vistypz(:,1:4),gmclt.stims,'rows'),5)./gm.frtime);

%% Set up variables

vlabs = {'Loom','Loom-ctl','DS-CCW','DS-CW','DS-CCW (C)','DS-CW (C)', ...
    'Cluster','Fish'};

ccol = [1 0 0;
    nan nan nan;
    0 0 1;
    0 1 0];   % Cluster color

vsz = size(mf.X,2);
nfr = gm.nfr;
nbanners = 2;
bannerlen = ceil(vsz/10);
imrange = [-5 5];

ncls = length(mf.superunique_x);

% Mask indexes
aniix = 64;
pniix = 65;
chbix = 62;
gtix = 9;
otix = 31;
ocuix = 26;
ptix = 39;
tegix = 45;
otnix = 30;

% Combine them
zbbix = [aniix pniix chbix gtix otix ocuix];
nzbb = length(zbbix); 
zbbmsk = gmclt.zbbmsk;

MFix = mf.superMFix_x;  % Original indexes of cells in final set
Idx = mf.superIdx_x;
X = mf.X(MFix,:);
MFzbbix = zbbmsk(MFix,zbbix);    % Masks

%%

for i = 1:length(msk2plt)
    ixx = msk2plt(i);
    nrois = sum(gmclt.zbbmsk(MFix,ixx));
    M = [];
    B = [];
    for c = mf.superunique_x'
        oix = intersect(six,find(Idx == c & gmclt.zbbmsk(MFix,ixx)),'stable');
        M = cat(1,M,X(oix,:));
        B = cat(1,B,cat(2,ones(length(oix),1).*c, ...
            mf.fishID(oix)));
    end
    Idx_ban = repmat(B(:,1),[1 bannerlen]);
    fish_ban = repmat(B(:,2),[1 bannerlen]);
    
    if nrois >= 20
        fig = figure('Position',[405,158,1064,797]);
        ax1 = subplot(1,14,1:12);
        imagesc(M,[-5 5])
        colormap(flip(brewermap(255,'RdBu'),1));
        for j = 1:length(vED)
            line([nfr nfr].*(j-1),[0 nrois],'Color','k','LineStyle','-');
            line([vST vST]+nfr.*(j-1),[0 nrois],'Color','b','LineStyle','--');
            line([vED(j) vED(j)]+nfr.*(j-1),[0 nrois],'Color','r','LineStyle','--');
        end
        set(ax1,'XTick',110/2:110:vsz, ...
            'XTickLabel',vlabs(1:end-2), ...
            'XTickLabelRotation',45,'Box','off','TickDir','out')
        cedg = find(diff(B(:,1)) ~= 0)+0.5;
        if any(cedg)
            line([0 vsz+bannerlen*nbanners+bannerlen/2], ...
                [cedg, cedg]','Color','k','LineWidth',1,'LineStyle','--')
        end
        title(ZBBMaskDatabaseNames{ixx})
        ylabel('ROIs')
        xlabel('Time')
        cbar = colorbar;
        ylabel(cbar,'Z-scored F')
        
        ax2 = subplot(1,14,13);
        imagesc(Idx_ban)
        colormap(ax2,ccol)
        set(ax2,'XTick',bannerlen/2,'XTickLabel',vlabs(end-1), ...
            'YTick',[],...
            'XTickLabelRotation',45,'Box','off','TickDir','out')
        
        ax3 = subplot(1,14,14);
        imagesc(fish_ban)
        colormap(ax3,brewermap(9,'Set1'));
        set(ax3,'XTick',bannerlen/2,'XTickLabel',vlabs(end), ...
            'YTick',[],...
            'XTickLabelRotation',45,'Box','off','TickDir','out')
        
    end
end

%% Mean centroids

vED = gm.trfr + 1 + ceil(1000.*gmv.vistypz( ...
ismember(gmv.vistypz(:,1:4),mf.stims,'rows'),5)./gm.frtime);

figure
h = plot(mf.superCent_x','LineWidth',2);
for c = 1:ncls
    h(c).Color = ccol(mf.superunique_x(c),:);
end
ylims = ylim;
line([nfr nfr].*repmat([1:6],2,1)',ylims,'Color','k','LineStyle','-');
for j = 1:length(vED)
    line([vST vST]+nfr.*(j-1),ylims,'Color','b','LineStyle','--');
    line([vED(j) vED(j)]+nfr.*(j-1),ylims,'Color','r','LineStyle','--');
end
set(gca,'XTick',110/2:110:vsz, ...
    'XTickLabel',vlabs, ...
    'XTickLabelRotation',45, ...
    'Box','off','TickDir','out')
ylabel('zF')
legend(mf.supernamez_x,'Location','northoutside','Orientation','horizontal')


figure
imagesc(mf.superCent_x,imrange)
colormap(flip(brewermap(255,'RdBu'),1));
cbar = colorbar;
ylabel(cbar,'zF')
line([nfr nfr].*repmat([1:6],2,1)',[0 ncls]+0.5,'Color','k','LineStyle','-');
for j = 1:length(vED)
    line([vST vST]+nfr.*(j-1),[0 ncls]+0.5,'Color','b','LineStyle','--');
    line([vED(j) vED(j)]+nfr.*(j-1),[0 ncls]+0.5,'Color','r','LineStyle','--');
end
set(gca,'XTick',110/2:110:vsz,'XTickLabel',vlabs, ...
    'XTickLabelRotation',45, ...
    'YTick',1:ncls,'YTickLabel',mf.supernamez_x,...
    'Box','off','TickDir','out')
ylabel('Cluster')


%% Proportion of cells that are included in final cluster set

msk2plt = [aniix pniix chbix gtix otix ocuix];
figure
bar(sum(MFzbbix)./sum(gmclt.zbbmsk(:,msk2plt)))
set(gca,'Box','off','TickDir','out', ...
    'XTickLabel',ZBBMaskDatabaseNames(msk2plt),'XTickLabelRotation',45)
ylabel('Fraction of cell clustered')

%% Proportion of cells that are included in final cluster set (sorted)

msk2plt = 1:nmsk;
ix = gmclt.zbbmsk(MFix,msk2plt);
F = sum(ix)./sum(gmclt.zbbmsk(:,msk2plt));

[srt,six] = sort(F,'descend','MissingPlacement','last');
ix = six(~isnan(srt));
figure
bar(F(ix))
set(gca,'Box','off','TickDir','out', ...
    'XTick',1:length(ix), ...
    'XTickLabel',ZBBMaskDatabaseNames(msk2plt(ix)),'XTickLabelRotation',45)
ylabel('% Cells included in visual clusters')

%% Lateralization

msk2plt = [aniix pniix chbix gtix otix];

X = NaN(length(msk2plt),ncls);
for n = 1:length(msk2plt)
    for c = 1:ncls
        mix = gmclt.zbbmsk(MFix,msk2plt(n));
        C = roiszbbC(MFix(Idx == mf.superunique_x(c) & mix),:);
        X(n,c) = (sum(C(:,2) < 308)-sum(C(:,2) > 308))/size(C,1);
    end
end

for c = 1:ncls
    figure
    bar(X(:,c))
    ylim([-1 1])
    axis square
    set(gca,'XTickLabel',ZBBMaskDatabaseNames(msk2plt),...
        'YTick',-1:0.5:1, ...
        'XTickLabelRotation',90, 'YTickLabelRotation',90, ...
        'Box','off','TickDir','out')
    ylabel('Asymmetry index')
    title(sprintf('Cluster %d',mf.superunique_x(c)))
end

%% Fraction of ClusterID per area

H = NaN(nzbb-1,ncls);
for i = 1:nzbb-1
    for c = 1:ncls
        H(i,c) = sum(Idx(MFzbbix(:,i)) == mf.superunique_x(c)) /sum(MFzbbix(:,i));
    end
end

figure
h = bar(H,'stacked','FaceColor','flat');
for c = 1:ncls
    h(c).CData = repmat(ccol(mf.superunique_x(c),:),nzbb-1,1);
end
set(gca,'Box','off','TickDir','out', ...
    'XTickLabel',ZBBMaskDatabaseNames(zbbix),'XTickLabelRotation',45)
ylabel('Fraction of cells')
legend(mf.supernamez_x,'Location','northoutside','Orientation','horizontal')

%% Fraction of cluster ranked by brain area

msk2plt = 1:nmsk;
ncmax = 50;
H = NaN(length(msk2plt),ncls,3);
for i = 1:length(msk2plt)
    for c = 1:ncls
        ix = gmclt.zbbmsk(MFix,msk2plt(i)) & Idx == mf.superunique_x(c);
        if sum(ix) >= ncmax
            nr = sum(roiszbbC(MFix(ix),2) > 308);
            nl = sum(roiszbbC(MFix(ix),2) < 308);
            asyidx = (nr-nl)/(nr + nl);
            H(i,c,1) = nl/sum(ix);
            H(i,c,2) = nr/sum(ix);
            H(i,c,3) = asyidx;
        end
    end
end

for c = 1:ncls
    [srt,six] = sort(H(:,c,3),'descend','MissingPlacement','last');
    ix = six(~isnan(srt));
    figure
    bar(H(ix,c,3))
    set(gca,'XTick',1:length(ix), ...
        'XTickLabel',ZBBMaskDatabaseNames(msk2plt(ix)),'XTickLabelRotation',45, ...
        'Box','off','TickDir','out')
    ylim([-1 1])
    title(sprintf('Cluster %d - %s',c,mf.supernamez_x{c}))
    ylabel('Asymmetry index')
end

%% Fraction of cell per area per cluster

msk2plt = 1:nmsk;
ncmax = 2;
H = NaN(length(msk2plt),ncls,3);
for i = 1:length(msk2plt)
    for c = 1:ncls
        ix = gmclt.zbbmsk(MFix,msk2plt(i));
        if sum(ix) >= ncmax
            H(i,c) = sum(ix & Idx == mf.superunique_x(c))/sum(ix);
        end
    end
end

for c = 1:ncls
    [srt,six] = sort(H(:,c),'descend','MissingPlacement','last');
    ix = six(~isnan(srt));
    figure
    bar(H(ix,c))
    set(gca,'XTick',1:length(ix), ...
        'XTickLabel',ZBBMaskDatabaseNames(msk2plt(ix)),'XTickLabelRotation',45, ...
        'Box','off','TickDir','out')
    title(sprintf('Cluster %d - %s',c,mf.supernamez_x{c}))
    ylabel('Proportion of cells in cluster')
end


%% Ranked average distance of cell in area to cluster centroid

msk2plt = 1:nmsk;
ncmax = 5;
H = NaN(length(msk2plt),ncls);
for i = 1:length(msk2plt)
    for c = 1:ncls
        ix = gmclt.zbbmsk(MFix,msk2plt(i)) & Idx == mf.superunique_x(c);
        if sum(ix) >= ncmax
            H(i,c) = nanmean(mf.superBoutDistto_b_ifn_Cent_x(ix));
        end
    end
end

for c = 1:ncls
    [srt,six] = sort(H(:,c),'ascend','MissingPlacement','last');
    ix = six(~isnan(srt));
    figure
    bar(H(ix,c))
    set(gca,'XTick',1:length(ix), ...
        'XTickLabel',ZBBMaskDatabaseNames(msk2plt(ix)),'XTickLabelRotation',45, ...
        'Box','off','TickDir','out')
    title(sprintf('Cluster %d - %s',c,mf.supernamez_x{c}))
    ylabel('Average distance to cluster centroid')
end

%% Scatter all ROI centroids colored by cluster ID

plotbnd = 1;
savefigs = 0;
intscale = 0.25;
msk2plt = zbbix;
zspace = 10;
zs = round(roiszbbC(MFix,3));
zedg = prctile(zs,5):zspace:prctile(zs,95);
zcnt = round(((zedg*2)+zspace)./2);
zix = discretize(zs,zedg);
nzs = max(zix);
alphav = 0.3;
markersz = 20;

for z = 1:nzs
    fig = figure;
    scatter(roiszbbC(MFix(zix == z),1),roiszbbC(MFix(zix == z),2), ...
        markersz,Idx(zix == z),'filled')
    hold on
    colormap(ccol);
    
    if plotbnd
        % Plot boundaries
        for j = 1:length(msk2plt)
            msk = reshape(full(ZBBMaskDatabase(:,msk2plt(j))),[616,1030,420]);
            
            % Smooth mask
            M = msk(:,:,zcnt(z));
            if any(M(:))
                F = griddedInterpolant(double(M));
                [sx,sy,sz] = size(M);
                
                xq = (0:intscale:sx)';
                yq = (0:intscale:sy)';
                zq = (0:intscale:sz)';
                
                I = imgaussfilt(F({xq,yq}),3) > 0.5;
                mskB = bwboundaries(I);
                for m = 1:size(mskB,1)
                    mskB{m,1} = mskB{m,1}.*intscale;
                end
                
                % Plot boundaries
                for l = 1:length(mskB)
                    boundary = mskB{l};
                    plot(boundary(:,2), boundary(:,1),'-', ...
                        'Color','k','LineWidth',1);
                end
            end
        end
    end
    alpha(alphav)
    axis equal off
    set(gca,'ZDir','reverse','YDir','reverse')
    view([90 90])
    title(sprintf('Z = %d-%d',zedg(z),zedg(z)+zspace))
    
    if savefigs
        savedir = fullfile('Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\analysis\Clustering\VRV_clustering\figures\ZBB_areas\maps\All_brain');
        if ~exist(savedir,'dir')
            mkdir(savedir);
        end
        savefig(fig,fullfile(savedir, ...
            sprintf('ROI_scatter_map_z%d_z%d',zedg(z),zedg(z)+zspace)))
        close(fig)
    end
end

%% Plot all ROIs per cluster

plotbnd = 1;
intscale = 0.25;
msk2plt = zbbix;
zrange = round(prctile(roiszbbC(MFix,3),[5 95]));

for c = 1:ncls
    figure
    scatter(roiszbbC(MFix(Idx == mf.superunique_x(c)),1), ...
        roiszbbC(MFix(Idx == mf.superunique_x(c)),2), ...
        8,ccol(mf.superunique_x(c),:),'filled')
    hold on
    if plotbnd
        % Plot boundaries
        for j = 1:length(msk2plt)
            msk = reshape(full(ZBBMaskDatabase(:,msk2plt(j))),[616,1030,420]);
            
            % Smooth mask
            M = sum(msk(:,:,zrange(1):zrange(2)),3) > 0;
            if any(M(:))
                F = griddedInterpolant(double(M));
                [sx,sy,sz] = size(M);
                
                xq = (0:intscale:sx)';
                yq = (0:intscale:sy)';
                zq = (0:intscale:sz)';
                
                I = imgaussfilt(F({xq,yq}),3) > 0.5;
                mskB = bwboundaries(I);
                for m = 1:size(mskB,1)
                    mskB{m,1} = mskB{m,1}.*intscale;
                end
                
                % Plot boundaries
                for l = 1:length(mskB)
                    boundary = mskB{l};
                    plot(boundary(:,2), boundary(:,1),'-', ...
                        'Color','k','LineWidth',1);
                end
            end
        end
    end
    alpha 0.3
    axis equal off
    set(gca,'ZDir','reverse','YDir','reverse')
    view([90 90])
end

%% Plot NI ROIs

intscale = 0.25;
gaussfilt = 17;
sctsz = 150;
alphav = 0.3;

% msk2plt = [gtix ocuix otix chbix aniix pniix ptix tegix otnix];
% mskxoffset = [30 70 0 45+15 45 45+10 -90 -280 -140];
% mskyoffset = [0 0 0 40 0 0 0 80 5;
%     0 0 0 -40 0 0 0 -80 -5];
% mskrange = round(prctile(roiszbbC(:,3),[1 99]));

msk2plt = [chbix aniix pniix];  % Only NI
mskxoffset = [60 45 55];
mskyoffset = [40 0 0;
    -40 0 0];
mskrange = [100 250];

for c = 1:ncls    
    for s = 1:2
        figure
        for n = 1:length(msk2plt)
            mix = gmclt.zbbmsk(MFix,msk2plt(n));
            C = roiszbbC(MFix(Idx == mf.superunique_x(c) & mix),:);
            if s == 1
                C = C(C(:,2) > 308,:);
            else
                C = C(C(:,2) < 308,:);
            end
            %             mskrange = round([prctile(C(:,3),1) prctile(C(:,3),99)]);
            scatter(C(:,1)+mskxoffset(n),C(:,2)+mskyoffset(s,n), ...
                sctsz,ccol(mf.superunique_x(c),:),'filled')
            hold on
            msk = reshape(full(ZBBMaskDatabase(:,msk2plt(n))),[616,1030,420]);
            if s == 1
                msk(1:308,:,:) = 0;
            else
                msk(308:end,:,:) = 0;
            end
            I = bwmorph(sum(msk(:,:,mskrange(1):mskrange(2)),3) > 0,'close');
            
            % Smooth mask
            F = griddedInterpolant(double(I));
            [sx,sy,sz] = size(I);
            
            xq = (0:intscale:sx)';
            yq = (0:intscale:sy)';
            zq = (0:intscale:sz)';
            
            If = imgaussfilt(F({xq,yq}),gaussfilt) > 0.5;
            mskB = bwboundaries(If);
            for m = 1:size(mskB,1)
                mskB{m,1} = mskB{m,1}.*intscale;
            end
            
            for k = 1:length(mskB)
                boundary = mskB{k};
                plot(boundary(:,2)+mskxoffset(n), ...
                    boundary(:,1)+mskyoffset(s,n),'-', ...
                    'Color','k','LineWidth',1);
            end
        end
        axis off equal
        set(gca,'YDir','reverse')
        view([90 90])
        alpha(alphav)
        title(sprintf('Cluster: %s',mf.supernamez_x{c}))
    end
end

%% Plot zF and motor kin for cell in mask closest to cluster centroid

ffiltwidth = 4; % Fluorescence filter width
ffilter = ones(1,ffiltwidth)./ffiltwidth;   % Fluorescent filter

msk2plt = [aniix pniix chbix];

for n = 1:length(msk2plt)
    for c = 1:ncls
        mix = find(gmclt.zbbmsk(MFix,msk2plt(n)) & Idx == mf.superunique_x(c));
        if any(mix)
            [~,six] = min(mf.superBoutDistto_b_ifn_Cent_x(mix));
            ix = MFix(mix(six));
            fish = mf.fishID(ix);
            roi = mf.roilabels(ix);
            
            datadir = mf.datasetz{fish};
            load(fullfile(datadir,'gmrxanat.mat'),'gmrxanat')
            load(fullfile(datadir,'gm'),'gm')
            load(fullfile(datadir,'gmv'),'gmv')
            load(fullfile(datadir,'gmb'),'gmb')
            load(fullfile(datadir,'gmbt'),'gmbt')
            
            vix = find(ismember(gmv.vistypz(:,1:4),mf.stims(1:2,:),'rows'));
            for v = 1:2
                gcROIwaveplot(datadir,roi,3,'VIS',vix(v),1,gm,gmv,gmb,gmbt,gmrxanat)
            end
        end
    end
end

%% Plot mean visual responses for mask per cluster

vSTt = (gm.trfr+2)*gm.frtime./1000;
xlims = [7 25];
ylims = [-1 7];
imrange = [-5 5];

tinc = 1/1000;
LV = 0.490;
stsz = 10;
tstart = round(100.*(LV./tand(stsz./2)))./100;
t = [-tstart:tinc:0];
S = 2.*atand(LV*(1./abs(t)));
St = flip(-t,2)+vSTt;

clst = unique(Idx);
vst = [1,2];
msk2plt = [aniix pniix 62];
x = [1:nfr].*gm.frtime./1000;

for v = vst
    for c = 1:length(clst)
        figure('position',[680,139,448,839]);
        col = get(gca,'ColorOrder');
        subplot(length(msk2plt)+1,1,1)
        M = {};
        for n = 1:length(msk2plt)            
            rois2a = gmclt.zbbmsk(MFix,msk2plt(n));
            nrois2a = length(rois2a);
            
            ix = rois2a & Idx == clst(c);
            [~,six] = sort(mf.superBoutDistto_b_ifn_Cent_x(ix),'ascend');
            stfr = ((v-1)*nfr+1);
            X = mf.X(MFix(ix),:);
            M{n} = X(six,stfr:stfr+nfr-1);
            % Visstim timings
            vST = gm.trfr + 2;
            vED = gm.trfr + 1 + ceil(1000.*gmv.vistypz( ...
                ismember(gmv.vistypz(:,1:4),mf.stims(v,:),'rows'),5)./gm.frtime);
            
            y = nanmean(M{n});
            if any(y)
            yerr = nansem(M{n});
            shadedErrorBar(x,y,yerr,{'color',col(n,:)},1)
            hold on
            end
        end
        xlabel('Time (s)')
        ylabel('zF')
        set(gca,'Box','off','TickDir','out')
        axis([xlims ylims])
        hold on
        yyaxis right
        plot(St,S,'k','LineWidth',2)
        ylim([0 100])
        line([x(vST) x(vST)],[0 100],'Color','b', ...
            'LineWidth',2,'LineStyle','--')
        line([x(vED) x(vED)],[0 100],'Color','r', ...
            'LineWidth',2,'LineStyle','--')
        title(sprintf('%s, VIS: %d', ...
            mf.supernamez_x{c},mf.stims(v,1)))
        
        for n = 1:length(msk2plt)
            subplot(length(msk2plt)+1,1,n+1)
            imagesc(M{n},imrange);
            axis off
            colormap(flip(brewermap(255,'RdBu'),1));
            line([vST vST],[0 size(M{n},1)+0.5],'Color','b', ...
                'LineWidth',2,'LineStyle','--')
            line([vED vED],[0 size(M{n},1)+0.5],'Color','r', ...
                'LineWidth',2,'LineStyle','--')
            xlim([findnearest(x,xlims(1)) findnearest(x,xlims(2))])
            title(ZBBMaskDatabaseNames{msk2plt(n)})
        end
    end
end