
sz = [0,1,0; 1,0,1; 0,1,0];
datadir = '\\128.40.168.141\data\Pedro\Brain_References\HuC\GCamP\HuC_cyto\HuC_GC6f\160428\m';
files = dir(fullfile(datadir,'*.tif'));
nz = size(files,1);

Cn = zeros(700,1000,nz);
h = waitbar(0,'Init');
for i = 1:nz
    waitbar(i/nz,h,'processing')
    filename = files(i).name;
    Y = loadtiff(fullfile(datadir,filename));
    Y = single(Y);
    mY = mean(Y,3);
    Y = bsxfun(@minus, Y, mY);
    sY = sqrt(mean(Y.*Y, 3));
    Y = bsxfun(@times, Y, 1./sY);
    Yconv = imfilter(Y, sz);
    MASK = imfilter(ones(d1,d2), sz);
    Cn(:,:,i) = mean(Yconv.*Y, 3)./MASK;
end
close(h)