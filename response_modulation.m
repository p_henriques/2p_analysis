function [] = gcROIfvec(datadir)

showplots = 0;

%% Load structures

h = waitbar(0, 'Init');
waitbar(0,h, 'Loading structures')

load(fullfile(datadir,'gm'),'gm');
load(fullfile(datadir,'gmv'),'gmv');
load(fullfile(datadir,'gmb'),'gmb');
% load(fullfile(datadir,'gmbt'),'gmbt');
load(fullfile(datadir,'gmbf'),'gmbf');
% load(fullfile(datadir,'gmpc'),'gmpc');
load(fullfile(datadir,'gmranat'),'gmranat');
if exist(fullfile(datadir,'gmrxanat4.mat'),'file')
    quadscan = 1;
    load(fullfile(datadir,'gmrxanat4'),'gmrxanat4');
    gmrxanat = gmrxanat4; clear gmrxanat4;
else
    quadscan = 0;
    load(fullfile(datadir,'gmrxanat'),'gmrxanat');
end
load(fullfile(datadir,'gmVCNV'),'gmVCNV');
load(fullfile(datadir,'Xgc_all'),'Xgc_all')
load(fullfile(datadir,'gmMRRV_kin'),'gmMRRV')

%%  Determine various convergence epochs

nvis = size(gmv.vistypz,1);
rois = 1:length(gmrxanat.roi);
nrois = length(rois);
nzs = size(gmranat.z,2);
np = size(gmv.visstim,1);

vEDoffset = 15;
vEDoffsett = vEDoffset*gm.frtime./1000;
if quadscan
    frtime = gmrxanat.frtime/1000;
    nfr = gmrxanat.nfr;
    vEDoffset = vEDoffset*4;
else
    frtime = gm.frtime/1000;
    nfr = gm.nfr;
end

% Get actual stimulus times
vST = repmat(gm.trfr + 2,nvis,1);
vSTt = (gm.trfr+1)*gm.frtime./1000;
vED = gm.trfr + 1 + ceil(1000.*gmv.vistypz(:,5)./gm.frtime);
vEDt = vSTt + gmv.vistypz(:,5);

vttime = gmv.visstim(:,6);
vEDs = gm.trfr + 1 + ceil(1000.*vttime./gm.frtime);
vEDst = vSTt + vttime;
if quadscan
    vST = vST*4;
    vED = vED*4;
    vEDs = vEDs*4;
end

% Stimulus epochs
sp = 1:np;

% High vergence number of bouts during stim ON
vbtix = ([gmbf.b.Vergbout] == 1 | [gmbf.b.Convbout] == 1) & ...
    [gmbf.b.st] < vEDst([gmbf.b.p])' & ...
    [gmbf.b.st] > vSTt;

% High vergence bout count per epoch
vbtn = histcounts([gmbf.b(vbtix).p],1:np);

% Good convergences
convix = gmb.convergences(:,3) == 1;

% No convergence epochs
tnconv = setdiff(sp,gmb.convergences(convix,1));

% Convergences in stim ON time
convonix = gmb.convergences(:,2) >= vSTt & ...
    gmb.convergences(:,2) <= vEDst(gmb.convergences(:,1));
npnconvon = setdiff(1:np,gmb.convergences(convonix & convix,1));

% Convergences in pre stim ON time
convpreix = gmb.convergences(:,2) < vSTt;
pconvpreon = setdiff(1:np,gmb.convergences(convpreix & convix,1));

% Convergences in stim ON time + offset
convonix_offs = gmb.convergences(:,2) >= vSTt & ...
    gmb.convergences(:,2) <= vEDst(gmb.convergences(:,1))+vEDoffsett;
npnconvon_offs = setdiff(1:np,gmb.convergences(convonix_offs & convix,1));

% Epochs with convergences
pconv = intersect(sp,gmb.convergences(convix,1));

% Epochs without convergences
npconv = setdiff(sp,pconv);

% Epochs with convergences in stim ON time
pconvon = intersect(sp,gmb.convergences(convix & convonix,1));

% Epochs with convergences outside stim ON time (spontaneous)
pnconvon = intersect(sp,gmb.convergences(convix & ~convonix,1));

% Convergence epochs with multiple bout
msbtcp = pconvon(vbtn(pconvon) > 1);

% Convergence epochs with one bout
osbtcp = pconvon(vbtn(pconvon) == 1);

% Get v,z and rep indexes for presentations
[npconv_v,npconv_z,npconv_rep] = gcPresentationInfo(npconv,gm,gmv);
[pnconvon_v,pnconvon_z,pnconvon_rep] = gcPresentationInfo(pnconvon,gm,gmv);
[pconvon_v,pconvon_z,pconvon_rep] = gcPresentationInfo(pconvon,gm,gmv);
[osbtcp_v,osbtcp_z,osbtcp_rep] = gcPresentationInfo(osbtcp,gm,gmv);
[msbtcp_v,msbtcp_z,msbtcp_rep] = gcPresentationInfo(msbtcp,gm,gmv);
[npnconvon_v,npnconvon_z,npnconvon_rep] = gcPresentationInfo(npnconvon,gm,gmv);
[npnconvon_offs_v,npnconvon_offs_z,npnconvon_offs_rep] = gcPresentationInfo(npnconvon_offs,gm,gmv);
[pconvpreon_v,pconvpreon_z,pconvpreon_rep] = gcPresentationInfo(pconvpreon,gm,gmv);

%%  Compute visual response vectors and convergence modulation

filtwidth = 5;
filter1 = ones(1,filtwidth)./filtwidth;

repsratio = NaN(nzs,nvis);
R = zeros(nrois,nvis,2);
MD = zeros(nrois,3,2);
for roi = 1:nrois
    waitbar(roi/nrois,h, 'Processing...')
    z = gmVCNV.ROIinfo(roi,2);
    
    X = cat(1,gmrxanat.roi(roi).Vprofiles.zProfiles)';
    Xpre = X(1:vST(1)-1,:);
    baseline = nanmedian(Xpre(:));
    
    M = cell(5,1);
    for v = 1:nvis
        nreps = size(gmrxanat.roi(roi).Vprofiles(v).zProfiles,1);
        
        F = gmrxanat.roi(roi).Vprofiles(v).zProfiles';
        F = reshape(interpolate_NaNs(F(:)),size(F));
        dFF = (F-baseline)./baseline;
        dFFf = reshape(filtfilt(filter1,1,dFF(:)), ...
            size(dFF));
        vreps = npnconvon_rep(npnconvon_z == z & npnconvon_v == v);
        R(roi,v,1) = nanmean(nanmean(dFFf(vST(v):vED(v)+vEDoffset,vreps)));
        R(roi,v,2) = nanstd(nanmean(dFFf(vST(v):vED(v)+vEDoffset,vreps)));
        
        convreps = pconvon_rep(pconvon_z == z & pconvon_v == v);
        obconvreps = osbtcp_rep(osbtcp_z == z & osbtcp_v == v);
        mbconvreps = msbtcp_rep(msbtcp_z == z & msbtcp_v == v);
        nconvreps = npnconvon_offs_rep(npnconvon_offs_z == z & npnconvon_offs_v == v);
        convprereps = pconvpreon_rep(pconvpreon_z == z & pconvpreon_v == v);
        
        M{1} = [M{1} ...
            nanmean(dFFf(vST(v):vED(v)+vEDoffset,convreps))];
        M{2} = [M{2} ...
            nanmean(dFFf(vST(v):vED(v)+vEDoffset,obconvreps))];
        M{3} = [M{3} ...
            nanmean(dFFf(vST(v):vED(v)+vEDoffset,mbconvreps))];
        M{4} = [M{4} ...
            nanmean(dFFf(vST(v):vED(v)+vEDoffset,nconvreps))];
        M{5} = [M{5} ...
            nanmean(dFFf(vST(v):vED(v)+vEDoffset,convprereps))];
        
        repsratio(z,v) = length(vreps)/nreps;
    end
    
    % Convergence modulation
    MD(roi,1,1) = nanmean(M{4})-nanmean(M{1});
    if ~isnan(MD(roi,1,1))
        MD(roi,1,2) = ranksum(M{4},M{1});
    else
        MD(roi,1,2) = nan;
    end
    % Multiple response modulation
    MD(roi,2,1) = nanmean(M{3})-nanmean(M{2});
    if ~isnan(MD(roi,2,1))
        MD(roi,2,2) = ranksum(M{3},M{2});
    else
        MD(roi,2,2) = nan;
    end
    % Pre spontaneous convergence modulation
    MD(roi,3,1) = nanmean(M{5})-nanmean(M{4});
    if ~isnan(MD(roi,3,1))
        MD(roi,3,2) = ranksum(M{5},M{4});
    else
        MD(roi,3,2) = nan;
    end
end
close(h)

%% Multiple comparisons correction (Benjamini & Hochberg/Yekutieli false discovery rate)

MDcorr = MD;
fdr = 0.05; % false discovery rate

for i = 1:size(MD,2)
    [~, ~, ~, MDcorr(:,i,2)] = fdr_bh(MD(:,i,2),fdr,'pdep');
    MDcorr(MDcorr(:,i,2) > 1,i,2) = 1;
end

%%  Plot modulation maps

if showplots
    cmap = flip(brewermap(256,'RdBu'),1);
    imrange = [0 40];
    
    C = [];
    for z = 1:nzs
        C = cat(1,C,cat(1,gmranat.z(z).STATScrop.Centroid));
    end
    
    for z = 1:nzs
        zix = gmVCNV.ROIinfo(:,2) == z;
        
        figure('name',sprintf('Z = %d',z), ...
            'position',[206,283,1492,625])
        subplot(1,2,1)
        imagesc(gm.zimg(z).aligngood2,imrange)
        axis('off','square')
        colormap(gca,'gray')
        hold on
        newcolorbar
        scatter(C(zix,1),C(zix,2),[],MDcorr(zix,1,1).*(1-MDcorr(zix,1,2)),'filled')
        axis('off','square')
        colormap redbluecmap
        set(gca,'YDir','reverse')
        caxis([-0.5 0.5])
        title('Convergence modulation (GO vs NO-GO trials)')
        
        subplot(1,2,2)
        imagesc(gm.zimg(z).aligngood2,imrange)
        axis('off','square')
        colormap(gca,'gray')
        hold on
        newcolorbar
        scatter(C(zix,1),C(zix,2),[],MDcorr(zix,2,1).*(1-MDcorr(zix,2,2)),'filled')
        axis('off','square')
        colormap redbluecmap
        set(gca,'YDir','reverse')
        caxis([-0.5 0.5])
        title('Multiple response modulation')
    end
end

%% Build ROI finger matrix

X1 = zscore(Xgc_all,[],2);
X2 = gmMRRV.RRV;
X3 = MDcorr(:,:,1);
X3(isnan(X3)) = 0;

X = [X1 X2 X3];

Czbb = [];
Czbblab = [];
for z = 1:nzs
    Czbb = cat(1,Czbb,gmranat.z(z).STATScrop.Centroid_ZBB);
    Czbblab = cat(1,Czbblab,gmranat.z(z).STATScrop.Masks_ZBB);
end
Czbblab = logical(Czbblab);

gmROIfvec.X = X;
gmROIfvec.X_zpos = gmVCNV.ROIinfo(:,2);
gmROIfvec.X_Centroids_ZBB = Czbb;
gmROIfvec.X_ZBB_Labels = Czbblab;
gmROIfvec.R = R;
gmROIfvec.Modulation.MD = MD;
gmROIfvec.Modulation.MDcorr = MDcorr;

waitbar(1,h, 'Saving...')
save(fullfile(datadir,'gmROIfvec'),'gmROIfvec');
close(h)

end

