% Calculate visually responsive fractuon of cells for given ZBB id based
% on VCNV method

load('Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\analysis\Clustering\VRV_clustering\rawtraces\mf.mat')
load('Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\analysis\Clustering\kmeans\180707\clustering_vvec1_VisOnly_PCA_180707.mat','zbbmsk')

zbbanatix = [2:3 6:34 44:46 48:74 77 113 305:306];
load('ZBBMaskDatabase','ZBBMaskDatabaseNames');
ZBBMaskDatabaseNames = ZBBMaskDatabaseNames(zbbanatix);

zbbmsk = zbbmsk(:,zbbanatix);

ia = [];
for i = 1:length(mf.datasetz)
    load(fullfile(mf.datasetz{i},'gmVCNV.mat'))
    ia = cat(1,ia,gmVCNV.ROIinfo(mf.roilabels(mf.fishID == i),4));
end

%%

zbbix = [31];
ix = any(zbbmsk(:,zbbix),2);

iaf = sum(ia(ix))/sum(ix); % Fraction of zbbix VCNV responsive cells

iamff = sum(ia(mf.superMFix_x))/length(mf.superMFix_x); % Fraction of VRV visually responsive that are also VCNV responsive


