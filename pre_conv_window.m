%   Estimates the pre convergence iteration that should be used to separate
%   conv responsive ROIs with ROIs with pre-conv activity

datadir = 'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f2';

load(fullfile(datadir,'gm'));
load(fullfile(datadir,'CvR'));

nfr = gm.nfr;
convfix = 55;   % Conv it in CvR F vectors
convoffset = 4; % conv offset from which to look for F above zthr
zthr = 2;   % z threshold from which to consider an ROI as active
zthr2 = 1;  % lower z threshold to find it where ROI started to respond
wdw = [convfix-convoffset convfix+convoffset];  % Convergence window

%%  Get all ROI responses

F = [];
h = waitbar(0.5,'I');
for i = 1:size(CvR.rois,2)
    waitbar(i/size(CvR.rois,2),h,'Processing')
    F = cat(1,F,cat(2,CvR.rois(i).Xon.Var13{:})');
end
close(h)

%% Find active ROIs within conv window and response start

nrois = size(F,1);
% Sort Ca responses
mix = NaN(nrois,1);
for m = 1:nrois
    % Find instances where rois crosses
    % activity threshold
    ixx = find(abs(F(m,:)) > zthr);
    ixx2 = find(abs(F(m,:)) < zthr2);
    if ~isempty(ixx) && any(ismember(ixx,wdw(1):wdw(end)))
        cixx = findnearest(55,ixx);
        baseix = findnearest(ixx(cixx(1)),ixx2,-1);
        if ~isempty(baseix)
            mix(m) = ixx2(baseix);
        else
            mix(m) = ixx(cixx(1));
        end
    end
end

mix = mix-convfix;

%%  GMModel to separate distribution in 2

X = mix(~isnan(mix));
GMModel = fitgmdist(X,2,'Replicates',5);
[idx,nlogl,P] = cluster(GMModel,X);
ix = find(P(:,1) > 0.4 & P(:,1) < 0.6);
thr = median(X(ix));

%%  Plot

figure
histogram(X(idx == 1))
hold on
histogram(X(idx == 2))
ylims = ylim;
line([thr thr],[0 ylims(2)],'Color','k')
text(thr-10,ylims(2).*0.8,sprintf('Thr = %0.1f',thr))
legend({'Cluster 1','Cluster 2'},'Location','northwest')
xlabel('Frames from convergence')
ylabel('Counts')
