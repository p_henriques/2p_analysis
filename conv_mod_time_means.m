% Meam zF values for GO, NOGO, Spontaneous and auto-triggered epochs

folders = { ...
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180301\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180305\f2\deep';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180419\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f1_1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180427\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180503\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180504\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180509\f3';
    };

%%  Load Mask database names

zbbanatix = [2:3 6:34 44:46 48:74 77 113 305:306];
load('ZBBMaskDatabase','ZBBMaskDatabaseNames');
ZBBMaskDatabaseNames = ZBBMaskDatabaseNames(zbbanatix);
nmsks = length(ZBBMaskDatabaseNames);

%%

natix = [3,4];
atix = [5,6];
useunfilt = 0;
useactive = 0;  % Use onlt active cells
iathr = 2;  % Z threshold to consider cell active

zbbmsk = [];
roiszbbC = [];
T = [];
X = [];
h = waitbar(0, 'Init');
for f = 1:9
    waitbar(f/length(folders),h, 'Processing...')
    datadir = folders{f};
    
    load(fullfile(datadir,'gmConvMod'));
    load(fullfile(datadir,'gmranat.mat'));
    
    nrois = size(gmConvMod.rois,2);
    
    F = NaN(nrois,4,110,2,2);
    for i = 1:nrois
        ix1 = ismember(gmConvMod.rois(i).vis_on(:,1),natix);  % NO-GO
        ix2 = ismember(gmConvMod.rois(i).vis_on(:,1),atix);   % NO-GO AT
        if useunfilt
            N = cellfun(@nanmean,gmConvMod.rois(i).Fz_on_nrsp_nof,'UniformOutput',false);
        else
            N = cellfun(@nanmean,gmConvMod.rois(i).Fz_on_nrsp,'UniformOutput',false);
        end
        
        if useactive
            iaon = gmConvMod.rois(i).F_on >= iathr;
            iaoff = gmConvMod.rois(i).F_off >= iathr;
        else
            iaon = true(size(gmConvMod.rois(i).F_on,1),1);
            iaoff = true(size(gmConvMod.rois(i).F_off,1),1);
        end
        iaon1 = iaon & gmConvMod.rois(i).nbts_on == 1;
        iaon2 = iaon & gmConvMod.rois(i).nbts_on > 1;
        iaoff1 = iaoff & gmConvMod.rois(i).nbts_off == 1;
        iaoff2 = iaoff & gmConvMod.rois(i).nbts_off > 1;        
        
        if any(iaon)
            if useunfilt
                F(i,1,:,1,1) = nanmean(gmConvMod.rois(i).Fz_on_nof(iaon1,:));
                F(i,1,:,1,2) = nansem(gmConvMod.rois(i).Fz_on_nof(iaon1,:));
                F(i,1,:,2,1) = nanmean(gmConvMod.rois(i).Fz_on_nof(iaon2,:));
                F(i,1,:,2,2) = nansem(gmConvMod.rois(i).Fz_on_nof(iaon2,:));
            else
                F(i,1,:,1,1) = nanmean(gmConvMod.rois(i).Fz_on(iaon1,:));
                F(i,1,:,1,2) = nansem(gmConvMod.rois(i).Fz_on(iaon1,:));
                F(i,1,:,2,1) = nanmean(gmConvMod.rois(i).Fz_on(iaon2,:));
                F(i,1,:,2,2) = nansem(gmConvMod.rois(i).Fz_on(iaon2,:));
            end
        end
        if any(iaoff)
            if useunfilt
                F(i,3,:,1,1) = nanmean(gmConvMod.rois(i).Fz_off_nof(iaoff1,:));
                F(i,3,:,1,2) = nansem(gmConvMod.rois(i).Fz_off_nof(iaoff1,:));
                F(i,3,:,1,1) = nanmean(gmConvMod.rois(i).Fz_off_nof(iaoff2,:));
                F(i,3,:,1,2) = nansem(gmConvMod.rois(i).Fz_off_nof(iaoff2,:));
            else
                F(i,3,:,1,1) = nanmean(gmConvMod.rois(i).Fz_off(iaoff1,:));
                F(i,3,:,1,2) = nansem(gmConvMod.rois(i).Fz_off(iaoff1,:));
                F(i,3,:,2,1) = nanmean(gmConvMod.rois(i).Fz_off(iaoff2,:));
                F(i,3,:,2,2) = nansem(gmConvMod.rois(i).Fz_off(iaoff2,:));
            end
        end
        if any(ix1)
            F(i,2,:,1,1) = nanmean(cat(1,N{ix1}),1);
            F(i,2,:,1,2) = nansem(cat(1,N{ix1}),1);
        end
        if any(ix2)
            F(i,4,:,1,1) = nanmean(cat(1,N{ix2}),1);
            F(i,4,:,1,2) = nansem(cat(1,N{ix2}),1);
        end
    end
    X = cat(1,X,F);
    
    T = cat(1,T, ...
        cat(2,ones(size(gmConvMod.rois,2),1).*f, ...
        [1:size(gmConvMod.rois,2)]'));
    
    nzs = size(gmranat.z,2);
    for z = 1:nzs
        zbbmsk = cat(1,zbbmsk, ...
            cat(1,gmranat.z(z).STATScrop.Masks_ZBB));
        roiszbbC = cat(1,roiszbbC, ...
            cat(1,gmranat.z(z).STATScrop.Centroid_ZBB));
    end
    
    clear gmConvMod gmranat
end
zbbmsk = logical(zbbmsk(:,zbbanatix));
close(h);

%%

ylims = [-0.2 1.3];
zbbix = [9 26 30 31 62 64 65];
for n = 1:length(zbbix)
    ix = zbbmsk(:,zbbix(n)) == 1;
    
    M = NaN(length(zbbix),4,110,2,2);
    for j = 1:2
        for i = 1:length(zbbix)
            M(i,:,:,j,1) = squeeze(nanmean(X(ix,:,:,j,1),1));
            M(i,:,:,j,2) = squeeze(nansem(X(ix,:,:,j,1),1));
        end
    end
    
    figure
    for j = 1:2
        subplot(1,2,j)
        col = get(gca,'ColorOrder');
        H = {};
        for i = 1:size(M,2)
            H{i} = shadedErrorBar([],squeeze(M(1,i,:,j,1)),squeeze(M(1,i,:,j,2)), ...
                {'Color',col(i,:)},1);
            hold on
        end
        line([55 55],ylims,'Color','k')
        set(gca,'Box','off','TickDir','out')
        ylabel('zF')
        xlabel('Time (frames)')
        xlim([1 110]);
        ylim(ylims);
        title(ZBBMaskDatabaseNames{zbbix(n)})
%         legend([H{1}.mainLine;H{2}.mainLine;H{3}.mainLine;H{4}.mainLine], ...
%             {'GO','NO-GO','Spontaneous','Auto-Trigger'}, ...
%             'Location','southoutside','Orientation','horizontal')
        axis square
    end
end
