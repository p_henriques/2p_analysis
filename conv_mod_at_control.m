folders = { ...
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180301\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180305\f2\deep';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180419\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f1_1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180427\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180503\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180504\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180509\f3';
    };

nfolders = length(folders);

%%  Load Mask database names

zbbanatix = [2:3 6:34 44:46 48:74 77 113 305:306];
load('ZBBMaskDatabase')
ZBBMaskDatabaseNames = ZBBMaskDatabaseNames(zbbanatix);
ZBBMaskDatabase = ZBBMaskDatabase(:,zbbanatix);
nmsks = length(ZBBMaskDatabaseNames);

%%

tlabs = {'Mean LConvZ AT','Mean RConvZ AT', ...
    'Mean LConvZ Shuffle AT','Mean RConvZ Shuffle AT'};

zbbmsk = [];
roiszbbC = [];
ConvM = [];
T = [];
h = waitbar(0, 'Init');
for f = 1:length(folders)
    waitbar(f/length(folders),h, 'Processing...')
    datadir = folders{f};
    
    load(fullfile(datadir,'gmConvMod_at.mat'));
    load(fullfile(datadir,'gmranat'));
    
    nrois = size(gmConvMod_at.rois,2);
    ConvM = cat(1,ConvM, ...
        cat(2, ...
        [gmConvMod_at.rois.Z_at_Lconv_Mn]',[gmConvMod_at.rois.Z_at_Rconv_Mn]', ...
        [gmConvMod_at.rois.Z_null_at_Lconv_Mn]',[gmConvMod_at.rois.Z_null_at_Rconv_Mn]'));
    
    T = cat(1,T, ...
        cat(2,ones(nrois,1).*f, ...
        [1:nrois]'));
    
    nzs = size(gmranat.z,2);
    for z = 1:nzs
        zbbmsk = cat(1,zbbmsk, ...
            cat(1,gmranat.z(z).STATScrop.Masks_ZBB));
        roiszbbC = cat(1,roiszbbC, ...
            cat(1,gmranat.z(z).STATScrop.Centroid_ZBB));
    end
    
    clear gmConvMod_at gmranat
end
zbbmsk = logical(zbbmsk(:,zbbanatix));
close(h);

%% Maps mean Z

msk2plt = [31 30 9 39 26 62 65 64];
mskxoffset = [0 0 0 0 0 0 0 0];
mskyoffset = [0 0 0 0 0 0 0 0;
    0 0 0 0 0 0 0 0];
mskrange = round([prctile(roiszbbC(:,3),1) ...
    prctile(roiszbbC(:,3),99)]);
intscale = 0.25;
gaussfilt = 17;
zrange = 0.7;
col = flip(hot(255),1);
lut = linspace(0,zrange,size(col,1));
ylabs = {'AtZ-score','AtCZ-score shuffle'};

k = 1;
for i = 1:2:4
    figure
    for n = 1:length(msk2plt)
        for s = 1:2
            if s == 1
                six = roiszbbC(:,2) > 308;
            else
                six = roiszbbC(:,2) < 308;
            end
            mix = zbbmsk(:,msk2plt(n));
            Z = cat(1,ConvM(mix & six,i),ConvM(mix & ~six,i+1));
            
            ix = abs(Z) > 20;
            Z(ix) = nan;
            zv = nanmean(Z);
            color = col(findnearest(zv,lut),:);
            
            msk = reshape(full(ZBBMaskDatabase(:,msk2plt(n))),[616,1030,420]);
            if s == 1
                msk(1:308,:,:) = 0;
            else
                msk(308:end,:,:) = 0;
            end
            I = bwmorph(sum(msk(:,:,mskrange(1):mskrange(2)),3) > 0,'close');
            % Smooth mask
            F = griddedInterpolant(double(I));
            [sx,sy,sz] = size(I);
            
            xq = (0:intscale:sx)';
            yq = (0:intscale:sy)';
            zq = (0:intscale:sz)';
            
            If = imgaussfilt(F({xq,yq}),gaussfilt) > 0.5;
            mskB = bwboundaries(If);
            for m = 1:size(mskB,1)
                mskB{m,1} = mskB{m,1}.*intscale;
            end
            
            for m = 1:length(mskB)
                boundary = mskB{m};
                patch(boundary(:,1)+mskyoffset(s,n), ...
                    boundary(:,2)+mskxoffset(n),color)
            end
        end
    end
    axis off equal
    set(gca,'XDir','reverse','YDir','reverse')
    view([90 90])
    title(ylabs{k})
    k = k+1;
end

% Plot colorbar

cbar = permute(repmat(col,1,1,50),[1,3,2]);
figure
imshow(cbar)
axis on
set(gca,'YDir','normal','YTick', ...
    0:255/4:255,'YTickLabel',0:zrange/4:zrange,'Box','off')

%% Z distribution per anatomical region (sorted)

usefrac = 0;
zthr = 3;
nmin = 20; % Min number of cells to consider mask
alphav = 0.05;
topn = 50; % Use only top XX areas
msk2plt = [9 26 31 62 64 65 39 45 30];
% msk2plt = [9 25 26 30 31 55 62 64 65 39 45];
nmsks = length(msk2plt);
fish2use = 1:9;
ylabs = {'AT CZ-score'};

M = NaN(nmsks,3,2);
P = NaN(nmsks,3);
for n = 1:nmsks
    lix = roiszbbC(:,2) > 308;
    mix = zbbmsk(:,msk2plt(n)) & ismember(T(:,1),fish2use);
    Zipsi = cat(1,ConvM(mix & lix,1),ConvM(mix & ~lix,2));
    Zipsinull = cat(1,ConvM(mix & lix,3),ConvM(mix & ~lix,4));
    Zcontra = cat(1,ConvM(mix & ~lix,1),ConvM(mix & lix,2));
    Zcontranull = cat(1,ConvM(mix & ~lix,3),ConvM(mix & lix,4));
    if length(Zipsi) > nmin
        if usefrac
            M(n,1,1) = sum(Zipsi > zthr)./size(Zipsi,1)*100;
            M(n,2,1) = sum(Zcontra > zthr)./size(Zcontra,1)*100;
            % Null dist
            M(n,3,1) = sum(Zipsinull > zthr)./size(Zipsinull,1)*100;
            M(n,4,1) = sum(Zcontranull > zthr)./size(Zcontranull,1)*100;
        else
            M(n,1,1) = nanmean(Zipsi);
            M(n,1,2) = nansem(Zipsi);
            M(n,2,1) = nanmean(Zcontra);
            M(n,2,2) = nansem(Zcontra);
            % Null dist
            M(n,3,1) = nanmean(Zipsinull);
            M(n,3,2) = nansem(Zipsinull);
            M(n,4,1) = nanmean(Zcontranull);
            M(n,4,2) = nansem(Zcontranull);
            
            P(n,1) = ranksum(Zipsi,Zcontra);
            P(n,2) = ranksum(Zipsi,Zipsinull);
            P(n,3) = ranksum(Zcontra,Zcontranull);
        end
    end
end

% Plots

[srt,six] = sort(M(:,1,1), ...
    'descend','MissingPlacement','last');

ix = six(~isnan(srt));
if topn < length(ix)
    ix = ix(1:topn);
end

[~,bar_xtick2,hb] = errorbar_groups_ph(squeeze(M(ix,1:2,1))', ...
    squeeze(M(ix,1:2,2))','bar_width',0.9);
p1 = line(bar_xtick2-(0.9/2),repmat(M(ix,3,1),1,2)', ...
    'LineStyle',':','Color','k','LineWidth',2);
p2 = line(bar_xtick2+(0.9/2),repmat(M(ix,4,1),1,2)', ...
    'LineStyle',':','Color','k','LineWidth',2);
if ~usefrac
    for j = 1:length(ix)
        sigstar(bar_xtick2(:,j),P(ix(j),1))
    end
end
set(gca,'XTickLabel',ZBBMaskDatabaseNames(msk2plt(ix)), ...
    'XTickLabelRotation',45, ...
    'TickDir','out','Box','off')
xlabel('Anatomical ID')
if usefrac
    ylabel(sprintf('%% CZ+ ROIs (CZ-score > %d)',zthr))
else
    ylabel('Mean CZ-score')
end
legend([hb;p1],{'Ipsi conv','Contra conv','Shuffle'},'Location','northeast')
title(ylabs{1})
