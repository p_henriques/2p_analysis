
imrange = [0 40];
ffiltwidth = 5; % Calcium fluorescence filter
filter1 = ones(1,ffiltwidth)./ffiltwidth;
zthr = 1;   % z threshold from which to consider a cell active
zthr2 = 1;
ft_width = 3;   % tail filter
fe_width = 3;   % eye filter
savefigs = 0;   % Save the figures
ylims = [-2 7]; % zF ylims for plots

zbbanatix = [1:3 5:74 77 113 305:306];
load('ZBBMaskDatabase','ZBBMaskDatabaseNames');
ZBBMaskDatabaseNames = ZBBMaskDatabaseNames(zbbanatix);

datadir = 'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f2';

% Load structures
load(fullfile(datadir,'gmranat'));
load(fullfile(datadir,'gmrxanat'))
load(fullfile(datadir,'gm'))
load(fullfile(datadir,'gmv'))
load(fullfile(datadir,'gmb'))
load(fullfile(datadir,'gmbt'))
load(fullfile(datadir,'gmbf'))

% Get stimulus times
nfr = gm.nfr;
frtime = gm.frtime/1000;
time = 0:frtime:frtime*(nfr-1);

ne_t = size(gmv.visstim,1);
vST = gm.trfr + 2;
vED = gm.trfr + 1 + ceil(1000.*gmv.vistypz(:,5)./gm.frtime);
vDurMx = max(vED-vST);  % Maximum visual stimulus duration
vSTt = (gm.trfr+1)*gm.frtime./1000;
vttime = gmv.visstim(:,6);
vEDst = vSTt + vttime;

vbtix = ([gmbf.b.Vergbout] == 1 | [gmbf.b.Convbout] == 1) & ...
    [gmbf.b.st] < vEDst([gmbf.b.p])' & ...
    [gmbf.b.st] > vSTt;

vbtn = histcounts([gmbf.b(vbtix).p],0.5:ne_t+0.5);

% Good convergences
conv_g_ix = gmb.convergences(:,3) == 1;
% Stim on convergences
conv_on_ix = gmb.convergences(:,2) >= vSTt & ...
    gmb.convergences(:,2) <= vEDst(gmb.convergences(:,1));

% Concatenate ZBB masks for all rois
nzs = size(gmranat.z,2);
zbbmsk = [];
roisC = [];
roiszbbC = [];
for z = 1:nzs
    zbbmsk = cat(1,zbbmsk, ...
        cat(1,gmranat.z(z).STATScrop.Masks_ZBB));
    roisC = cat(1,roisC, ...
        cat(1,gmranat.z(z).STATScrop.Centroid));
    roiszbbC = cat(1,roiszbbC, ...
        cat(1,gmranat.z(z).STATScrop.Centroid_ZBB));
end
zbbmsk = logical(zbbmsk(:,zbbanatix));

%%

v_on = 4;   % Visual stimulus
ylims = [-1 7];
zbbix2a = [76]; % ZBB masks to plot

rois2a = zbbmsk(:,zbbix2a) == 1;
nrois = sum(rois2a);
rois2a_zs = [];
for i = 1:length(nrois)
    rois2a_zs = catpad(2,rois2a_zs, ...
        unique([gmrxanat.roi(rois2a(:,i)).z])');
end
rois2a_zs_u = unique(rois2a_zs);

% All z positions
for i = 1:length(rois2a_zs_u)
    z = rois2a_zs_u(i);
    % Epochs with spot presentations
    se = intersect(gm.zindices(z).e, ...
        find(gmv.visstim(:,1) > 77000 & gmv.visstim(:,1) < 82000));
    R = cell(length(zbbix2a),1);
    Lat = cell(length(zbbix2a),1);
    
    % Tail info
    Tresampl = cat(1,gmbt.p(gm.zindices(z).e).resampled);
    
    for n = 1:length(zbbix2a)
        rois = find([gmrxanat.roi.z]' == z & rois2a(:,n));
        nrois = length(rois);
        Lat{n} = roiszbbC(rois,2) > 308;
        
        % Get roi responses
        [v0,~,r0] = gcPresentationInfo(gm.zindices(z).e,gm,gmv);
        roinfr = length(v0)*gm.nfr;
        F = NaN(nrois,roinfr);
        for j = 1:nrois
            roi = rois(j);
            F0 = [];
            for k = 1:length(v0)
                F0 = cat(1,F0,gmrxanat.roi(roi).Vprofiles(v0(k)).zProfiles(r0(k),:)');
            end
            F0f = filtfilt(filter1,1,double(F0));
            F0fz = zscore(F0f);
            F(j,:) = F0fz;
        end
        R{n} = F;
    end
    
    % Stim on convergences
    pconv_on = intersect(se,gmb.convergences(conv_g_ix & conv_on_ix,1));
    [v_on_all,z_on_all,r_on_all] = gcPresentationInfo(pconv_on,gm,gmv);
    % No convergence
    npconv = setdiff(se,gmb.convergences(:,1));
    [v_n_all,z_n_all,r_n_all] = gcPresentationInfo(npconv,gm,gmv);
    % Stim off convergences
    pconv_off = intersect(se,gmb.convergences(conv_g_ix & ~conv_on_ix,1));
    [v_off_all,z_off_all,r_off_all] = gcPresentationInfo(pconv_off,gm,gmv);
    
    %% GO
    
    for n = 1:length(zbbix2a)
        vix = find(v_on_all == v_on & z_on_all == z);
        if any(vix)
            figure('name',sprintf('%s - Z = %d, GO responses', ...
                ZBBMaskDatabaseNames{zbbix2a(n)},z));
            for m = 1:length(vix)
                vixx = vix(m);
                p_on = pconv_on(vix(m));
                
                convix_on = find(gmb.convergences(:,1) == p_on & conv_g_ix & conv_on_ix);
                convix_on = convix_on(1); % First convergence with stim on
                convt_on = gmb.convergences(convix_on,2); % Convergence time
                convit_on = findnearest(convt_on,time,-1); % Convergence iteration
                
                r_on = r_on_all(vixx);
                visp_on = find(v0 == v_on & r0 == r_on);
                itst_on = nfr*(visp_on-1)+convit_on-nfr/2+1;
                ited_on = nfr*(visp_on-1)+convit_on+nfr/2;
                if length(Tresampl) >= ited_on
                    its_on = Tresampl(itst_on:ited_on,1);
                    itspreix_on = find(diff(its_on(1:nfr/2)) < 1);
                    if ~isempty(itspreix_on)
                        its_on(1:itspreix_on) = -flip(1:itspreix_on);
                    end
                    itspostix_on = find(diff(its_on(nfr/2:end)) < 1);
                    if ~isempty(itspostix_on)
                        its_on(itspostix_on+nfr/2:end) = its_on(itspostix_on+nfr/2:end)+its_on(itspostix_on+nfr/2-1)+1;
                    end
                    
                    ptime_on = its_on.*frtime;
                    ST_on = gmb.p(p_on).vis_traj(:,[2:3,7]);
                    ST_on = ST_on(abs(ST_on(:,2)) < 999,:);  % Remove bogus values
                    vitst_on = find(its_on == ST_on(1,1));
                    vited_on = find(its_on == ST_on(end,1));
                    sang_on = -spotangle_v2(ST_on(:,2), ...
                        gm.visgeom.sweepamp,gm.visgeom.amax, ...
                        gm.visgeom.R,gm.visgeom.fish2screen);
                    
                    % Tail conv
                    ttime1 = gmbt.p(p_on).tt;
                    T1 = gmbt.p(p_on).cumtail;
                    Tf1 = filtfilt(ones(1, ft_width)./ft_width, 1, T1);
                    % Eyes conv
                    etime1 = gmb.p(p_on).tt;
                    Er1 = gmb.p(p_on).Rangles;
                    El1 = gmb.p(p_on).Langles;
                    Elf1 = filtfilt(ones(1, fe_width)./fe_width, 1, El1);
                    Erf1 = filtfilt(ones(1, fe_width)./fe_width, 1, Er1);
                    Evf1 = Elf1-Erf1;
                    
                    M_on = R{n}(:,itst_on:ited_on); % roi responses
                    nrois2 = size(M_on,1);
                    
                    % Sort Ca responses
                    [~,mix] = max(M_on,[],2);
                    [mixsv,mixs] = sort(mix,'ascend');
                    
                    lix = ismember(mixs,find(Lat{n}));
                    rix = ismember(mixs,find(~Lat{n}));
                    
                    Xon = cat(1,M_on(mixs(lix),:), ...
                        M_on(mixs(rix),:));
                    
                    subplot(3,length(vix),m)
                    p1 = shadedErrorBar(1:size(Xon,2),mean(M_on(mixs(lix),:),1), ...
                        nansem(M_on(mixs(lix),:),1),'b',1);
                    hold on
                    p2 = shadedErrorBar(1:size(Xon,2),mean(M_on(mixs(rix),:),1), ...
                        nansem(M_on(mixs(rix),:),1),'r',1);
                    line([nfr/2 nfr/2],ylims,'Color','k','LineWidth',2)
                    line([vitst_on vitst_on],ylims, ...
                        'Color','b','LineWidth',1,'LineStyle','--')
                    line([vited_on vited_on],ylims, ...
                        'Color','r','LineWidth',1,'LineStyle','--')
                    set(gca,'Box','off','TickDir','out')
                    axis([vitst_on-5 vited_on+5 ylims],'off')
                    title(num2str(p_on))
                    
                    subplot(3,length(vix),m+length(vix))
                    imagesc(Xon,[-5 5])
                    colormap(flip(brewermap(255,'RdBu'),1));
                    line([nfr/2 nfr/2],[0 size(M_on,1)+1],'Color','k','LineWidth',2)
                    line([vitst_on vitst_on],[0 size(M_on,1)+1], ...
                        'Color','b','LineWidth',1,'LineStyle','--')
                    line([vited_on vited_on],[0 size(M_on,1)+1], ...
                        'Color','r','LineWidth',1,'LineStyle','--')
                    line([0 nfr],[sum(lix) sum(lix)]+0.5, ...
                        'Color','k','LineWidth',2,'LineStyle','-')
                    set(gca,'Box','off','TickDir','out')
                    xlim([vitst_on-5 vited_on+5])
                    axis off
                    
                    subplot(3,length(vix),m+length(vix)*2)
                    plot(ttime1,Tf1,'g')
                    hold on
                    plot(etime1,Elf1,'b')
                    plot(etime1,Erf1,'r')
                    plot(etime1,Evf1,'k')
                    ylim([-120 120])
                    yyaxis right
                    plot(ST_on(:,3),sang_on,'Color',[.6 .6 .6])
                    axis([ptime_on(vitst_on-5) ptime_on(vited_on+5) -75 75],'off')
                    set(gca,'Box','off','TickDir','out')
                end
            end
        end
        
        %% Spont
        
        vix = find(v_off_all == v_on & z_off_all == z);
        if any(vix)
            figure('name',sprintf('%s - Z = %d, Spont responses', ...
                ZBBMaskDatabaseNames{zbbix2a(n)},z));
            for m = 1:length(vix)
                vixx = vix(m);
                p_on = pconv_off(vix(m));
                
                convix_on = find(gmb.convergences(:,1) == p_on & conv_g_ix & ~conv_on_ix);
                if length(convix_on) > 1
                convix_on = convix_on(2); % First convergence
                end
                convt_on = gmb.convergences(convix_on,2); % Convergence time
                convit_on = findnearest(convt_on,time,-1); % Convergence iteration
                
                r_on = r_off_all(vixx);
                visp_on = find(v0 == v_on & r0 == r_on);
                itst_on = nfr*(visp_on-1)+convit_on-nfr/2+1;
                ited_on = nfr*(visp_on-1)+convit_on+nfr/2;
                if length(Tresampl) >= ited_on
                    its_on = Tresampl(itst_on:ited_on,1);
                    itspreix_on = find(diff(its_on(1:nfr/2)) < 1);
                    if ~isempty(itspreix_on)
                        its_on(1:itspreix_on) = -flip(1:itspreix_on);
                    end
                    itspostix_on = find(diff(its_on(nfr/2:end)) < 1);
                    if ~isempty(itspostix_on)
                        its_on(itspostix_on+nfr/2:end) = its_on(itspostix_on+nfr/2:end)+its_on(itspostix_on+nfr/2-1)+1;
                    end
                    ptime_on = its_on.*frtime;
                    
                    % Tail conv
                    ttime1 = gmbt.p(p_on).tt;
                    T1 = gmbt.p(p_on).cumtail;
                    Tf1 = filtfilt(ones(1, ft_width)./ft_width, 1, T1);
                    % Eyes conv
                    etime1 = gmb.p(p_on).tt;
                    Er1 = gmb.p(p_on).Rangles;
                    El1 = gmb.p(p_on).Langles;
                    Elf1 = filtfilt(ones(1, fe_width)./fe_width, 1, El1);
                    Erf1 = filtfilt(ones(1, fe_width)./fe_width, 1, Er1);
                    Evf1 = Elf1-Erf1;
                    
                    M_on = R{n}(:,itst_on:ited_on); % roi responses
                    nrois2 = size(M_on,1);
                    
                    % Sort Ca responses
                    [~,mix] = max(M_on,[],2);
                    [mixsv,mixs] = sort(mix,'ascend');
                    
                    lix = ismember(mixs,find(Lat{n}));
                    rix = ismember(mixs,find(~Lat{n}));
                    
                    Xon = cat(1,M_on(mixs(lix),:), ...
                        M_on(mixs(rix),:));
                    
                    subplot(3,length(vix),m)
                    p1 = shadedErrorBar(1:size(Xon,2),mean(M_on(mixs(lix),:),1), ...
                        nansem(M_on(mixs(lix),:),1),'b',1);
                    hold on
                    p2 = shadedErrorBar(1:size(Xon,2),mean(M_on(mixs(rix),:),1), ...
                        nansem(M_on(mixs(rix),:),1),'r',1);
                    line([nfr/2 nfr/2],ylims,'Color','k','LineWidth',2)
                    set(gca,'Box','off','TickDir','out')
                    axis([(nfr/2)-26-5 (nfr/2)+26+5 ylims],'off')
                    title(num2str(p_on))
                    
                    subplot(3,length(vix),m+length(vix))
                    imagesc(Xon,[-5 5])
                    colormap(flip(brewermap(255,'RdBu'),1));
                    line([nfr/2 nfr/2],[0 size(M_on,1)+1],'Color','k','LineWidth',2)
                    line([0 nfr],[sum(lix) sum(lix)]+0.5, ...
                        'Color','k','LineWidth',2,'LineStyle','-')
                    set(gca,'Box','off','TickDir','out')
                    xlim([(nfr/2)-26-5 (nfr/2)+26+5])
                    axis off
                    
                    subplot(3,length(vix),m+length(vix)*2)
                    plot(ttime1,Tf1,'g')
                    hold on
                    plot(etime1,Elf1,'b')
                    plot(etime1,Erf1,'r')
                    plot(etime1,Evf1,'k')
                    ylim([-120 120])
                    axis([ptime_on((nfr/2)-26-5) ptime_on((nfr/2)+26+5) -75 75],'off')
                    set(gca,'Box','off','TickDir','out')
                end
            end
        end
        
        %% NO-GO
        
        vix = find(v_n_all == v_on & z_n_all == z);
        
        if any(vix)
            figure('name',sprintf('%s - Z = %d, NO-GO responses', ...
                ZBBMaskDatabaseNames{zbbix2a(n)},z));
            for m = 1:length(vix)
                vixx = vix(m);
                p_on = npconv(vix(m));
                
                r_on = r_n_all(vixx);
                visp_on = find(v0 == v_on & r0 == r_on);
                itst_on = nfr*(visp_on-1)+1;
                ited_on = nfr*(visp_on);
                if length(Tresampl) >= ited_on
                    its_on = Tresampl(itst_on:ited_on,1);
                    itspreix_on = find(diff(its_on(1:nfr/2)) < 1);
                    if ~isempty(itspreix_on)
                        its_on(1:itspreix_on) = -flip(1:itspreix_on);
                    end
                    itspostix_on = find(diff(its_on(nfr/2:end)) < 1);
                    if ~isempty(itspostix_on)
                        its_on(itspostix_on+nfr/2:end) = its_on(itspostix_on+nfr/2:end)+its_on(itspostix_on+nfr/2-1)+1;
                    end
                    
                    ptime_on = its_on.*frtime;
                    ST_on = gmb.p(p_on).vis_traj(:,[2:3,7]);
                    ST_on = ST_on(abs(ST_on(:,2)) < 999,:);  % Remove bogus values
                    vitst_on = find(its_on == ST_on(1,1));
                    vited_on = find(its_on == ST_on(end,1));
                    sang_on = -spotangle_v2(ST_on(:,2), ...
                        gm.visgeom.sweepamp,gm.visgeom.amax, ...
                        gm.visgeom.R,gm.visgeom.fish2screen);
                    
                    % Tail conv
                    ttime1 = gmbt.p(p_on).tt;
                    T1 = gmbt.p(p_on).cumtail;
                    Tf1 = filtfilt(ones(1, ft_width)./ft_width, 1, T1);
                    % Eyes conv
                    etime1 = gmb.p(p_on).tt;
                    Er1 = gmb.p(p_on).Rangles;
                    El1 = gmb.p(p_on).Langles;
                    Elf1 = filtfilt(ones(1, fe_width)./fe_width, 1, El1);
                    Erf1 = filtfilt(ones(1, fe_width)./fe_width, 1, Er1);
                    Evf1 = Elf1-Erf1;
                    
                    M_on = R{n}(:,itst_on:ited_on); % roi responses
                    nrois2 = size(M_on,1);
                    
                    % Sort Ca responses
                    [~,mix] = max(M_on,[],2);
                    [mixsv,mixs] = sort(mix,'ascend');
                    
                    lix = ismember(mixs,find(Lat{n}));
                    rix = ismember(mixs,find(~Lat{n}));
                    
                    Xon = cat(1,M_on(mixs(lix),:), ...
                        M_on(mixs(rix),:));
                    
                    subplot(3,length(vix),m)
                    p1 = shadedErrorBar(1:size(Xon,2),mean(M_on(mixs(lix),:),1), ...
                        nansem(M_on(mixs(lix),:),1),'b',1);
                    hold on
                    p2 = shadedErrorBar(1:size(Xon,2),mean(M_on(mixs(rix),:),1), ...
                        nansem(M_on(mixs(rix),:),1),'r',1);
                    line([vitst_on vitst_on],ylims, ...
                        'Color','b','LineWidth',1,'LineStyle','--')
                    line([vited_on vited_on],ylims, ...
                        'Color','r','LineWidth',1,'LineStyle','--')
                    set(gca,'Box','off','TickDir','out')
                    axis([vitst_on-5 vited_on+5 ylims],'off')
                    title(num2str(p_on))
                    
                    subplot(3,length(vix),m+length(vix))
                    imagesc(Xon,[-5 5])
                    colormap(flip(brewermap(255,'RdBu'),1));
                    line([vitst_on vitst_on],[0 size(M_on,1)+1], ...
                        'Color','b','LineWidth',1,'LineStyle','--')
                    line([vited_on vited_on],[0 size(M_on,1)+1], ...
                        'Color','r','LineWidth',1,'LineStyle','--')
                    line([0 nfr],[sum(lix) sum(lix)]+0.5, ...
                        'Color','k','LineWidth',2,'LineStyle','-')
                    set(gca,'Box','off','TickDir','out')
                    xlim([vitst_on-5 vited_on+5])
                    axis off
                    
                    subplot(3,length(vix),m+length(vix)*2)
                    plot(ttime1,Tf1,'g')
                    hold on
                    plot(etime1,Elf1,'b')
                    plot(etime1,Erf1,'r')
                    plot(etime1,Evf1,'k')
                    ylim([-120 120])
                    yyaxis right
                    plot(ST_on(:,3),sang_on,'Color',[.6 .6 .6])
                    axis([ptime_on(vitst_on-5) ptime_on(vited_on+5) -75 75],'off')
                    set(gca,'Box','off','TickDir','out')
                end
            end
        end
        
        %% NO-GO AT
        
        if any(vix)
            vix = find(v_n_all == v_on+2 & z_n_all == z);
            
            figure('name',sprintf('%s - Z = %d, NO-GO (AT) responses', ...
                ZBBMaskDatabaseNames{zbbix2a(n)},z));
            for m = 1:length(vix)
                vixx = vix(m);
                p_on = npconv(vix(m));
                
                r_on = r_n_all(vixx);
                visp_on = find(v0 == v_on+2 & r0 == r_on);
                itst_on = nfr*(visp_on-1)+1;
                ited_on = nfr*(visp_on);
                if length(Tresampl) >= ited_on
                    its_on = Tresampl(itst_on:ited_on,1);
                    itspreix_on = find(diff(its_on(1:nfr/2)) < 1);
                    if ~isempty(itspreix_on)
                        its_on(1:itspreix_on) = -flip(1:itspreix_on);
                    end
                    itspostix_on = find(diff(its_on(nfr/2:end)) < 1);
                    if ~isempty(itspostix_on)
                        its_on(itspostix_on+nfr/2:end) = its_on(itspostix_on+nfr/2:end)+its_on(itspostix_on+nfr/2-1)+1;
                    end
                    
                    ptime_on = its_on.*frtime;
                    ST_on = gmb.p(p_on).vis_traj(:,[2:3,7]);
                    ST_on = ST_on(abs(ST_on(:,2)) < 999,:);  % Remove bogus values
                    vitst_on = find(its_on == ST_on(1,1));
                    vited_on = find(its_on == ST_on(end,1));
                    sang_on = -spotangle_v2(ST_on(:,2), ...
                        gm.visgeom.sweepamp,gm.visgeom.amax, ...
                        gm.visgeom.R,gm.visgeom.fish2screen);
                    
                    % Tail conv
                    ttime1 = gmbt.p(p_on).tt;
                    T1 = gmbt.p(p_on).cumtail;
                    Tf1 = filtfilt(ones(1, ft_width)./ft_width, 1, T1);
                    % Eyes conv
                    etime1 = gmb.p(p_on).tt;
                    Er1 = gmb.p(p_on).Rangles;
                    El1 = gmb.p(p_on).Langles;
                    Elf1 = filtfilt(ones(1, fe_width)./fe_width, 1, El1);
                    Erf1 = filtfilt(ones(1, fe_width)./fe_width, 1, Er1);
                    Evf1 = Elf1-Erf1;
                    
                    M_on = R{n}(:,itst_on:ited_on); % roi responses
                    nrois2 = size(M_on,1);
                    
                    % Sort Ca responses
                    [~,mix] = max(M_on,[],2);
                    [mixsv,mixs] = sort(mix,'ascend');
                    
                    lix = ismember(mixs,find(Lat{n}));
                    rix = ismember(mixs,find(~Lat{n}));
                    
                    Xon = cat(1,M_on(mixs(lix),:), ...
                        M_on(mixs(rix),:));
                    
                    subplot(3,length(vix),m)
                    p1 = shadedErrorBar(1:size(Xon,2),mean(M_on(mixs(lix),:),1), ...
                        nansem(M_on(mixs(lix),:),1),'b',1);
                    hold on
                    p2 = shadedErrorBar(1:size(Xon,2),mean(M_on(mixs(rix),:),1), ...
                        nansem(M_on(mixs(rix),:),1),'r',1);
                    line([vitst_on vitst_on],ylims, ...
                        'Color','b','LineWidth',1,'LineStyle','--')
                    line([vited_on vited_on],ylims, ...
                        'Color','r','LineWidth',1,'LineStyle','--')
                    set(gca,'Box','off','TickDir','out')
                    title(num2str(p_on))
                    axis([vitst_on-5 vited_on+5 ylims],'off')
                    
                    subplot(3,length(vix),m+length(vix))
                    imagesc(Xon,[-5 5])
                    colormap(flip(brewermap(255,'RdBu'),1));
                    line([vitst_on vitst_on],[0 size(M_on,1)+1], ...
                        'Color','b','LineWidth',1,'LineStyle','--')
                    line([vited_on vited_on],[0 size(M_on,1)+1], ...
                        'Color','r','LineWidth',1,'LineStyle','--')
                    line([0 nfr],[sum(lix) sum(lix)]+0.5, ...
                        'Color','k','LineWidth',2,'LineStyle','-')
                    set(gca,'Box','off','TickDir','out')
                    xlim([vitst_on-5 vited_on+5])
                    axis off
                    
                    subplot(3,length(vix),m+length(vix)*2)
                    plot(ttime1,Tf1,'g')
                    hold on
                    plot(etime1,Elf1,'b')
                    plot(etime1,Erf1,'r')
                    plot(etime1,Evf1,'k')
                    ylim([-120 120])
                    yyaxis right
                    plot(ST_on(:,3),sang_on,'Color',[.6 .6 .6])
                    axis([ptime_on(vitst_on-5) ptime_on(vited_on+5) -75 75],'off')
                    set(gca,'Box','off','TickDir','out')
                end
            end
        end
    end
end
