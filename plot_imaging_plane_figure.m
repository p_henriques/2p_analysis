datadir = 'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f2';

load(fullfile(datadir,'gm'),'gm')
load(fullfile(datadir,'gmranat'),'gmranat')
load(fullfile(datadir,'gmb'),'gmb')
load('ZBBMaskDatabase.mat','ZBBMaskDatabaseNames')
load(fullfile(datadir,'gmv'),'gmv');

%% Plot anatomy with mask boundaries

z = 3;
imrange = [0 40];
sp = 0.1;
msk2plt = [12,29,33,34,77,305,306];

figure
imagesc(gm.zimg(z).aligngood2,imrange)
colormap gray
axis square off
hold on
for m = 1:length(msk2plt)
    Mix = cat(1,gmranat.z(z).STATScrop.Masks_ZBB);
    Cani = cat(1,gmranat.z(z).STATScrop(Mix(:,msk2plt(m))).Centroid);
    for s = 1:2
        if s == 1
            ix = find(Cani(:,2) < gm.xDef/2);
        else
            ix = find(Cani(:,2) > gm.xDef/2);
        end
        K = convhull(Cani(ix,1),Cani(ix,2));
        x = Cani(ix(K),1);
        y = Cani(ix(K),2);
        np = length(x);
        px = interp1(1:np,x,1:sp:np,'pchip');
        py = interp1(1:np,y,1:sp:np,'pchip');
        
        plot(px,py,'b:','LineWidth',2)
    end
end


%%

z = 3;
p = 333;
imrange = [0 4];
fmaprange = [-8 8];
sp = 0.1;
msk2plt = [12,29,33,34,52,58,305,306];
usemean = 1;
mergeanat = 0;
scalef = 5;   % df map scale factor
frms = [-8:8];
imfilt = [3 3 5];

ix = find(gmb.convergences(:,1) == p);
if any(ix)
    ix = ix(1);
    cfrm = round(gmb.convergences(ix,2)/(gm.frtime/1000));
else
    [vST,vED] = gcStimInfo(gmv.visstim(p,1:6),gm.trfr,gm.frtime,1);
    cfrm = (vED+vST)/2;
end

I = load(fullfile(datadir,'ml_align_filt',num2str(p-1)));
I = I.im_align_filt;
Iz = (I-gm.zimg(z).aligngood2)./gm.zimg(z).aligngood2;
Izm = imboxfilt3(Iz,imfilt);

if usemean
    ni = 1;
else
    ni = length(frms);
end
for i = 1:ni
    figure
    if usemean
        Im = mean(Izm(:,:,cfrm+frms(1):cfrm+frms(end)),3);
    else
        Im = Izm(:,:,cfrm+frms(i));
    end
    
    if mergeanat
        thisanatomy = gm.zimg(z).aligngood2;
        thisanatomy = scaleif(thisanatomy,0,1,0,20);
        thismerge = repmat(thisanatomy,1,1,3);
        fmap = Im;
        
        fmap(abs(fmap)<fmaprange(1)) = 0;
        fmap = fmap./fmaprange(2);
        fmap(fmap>1) = 1;
        fmap(fmap<-1) = -1;
        
        pfmap = fmap;
        nfmap = fmap;
        pfmap(pfmap<0) = 0;
        nfmap(nfmap>0) = 0;
        nfmap = -nfmap;
        
        thismerge = thismerge.*.2; % scale anatomy
        thismerge(:,:,1) = thismerge(:,:,1) + pfmap.*scalef;
        thismerge(:,:,3) = thismerge(:,:,3) + nfmap.*scalef;
        
        imshow(thismerge);
    else
        imagesc(Im,imrange)
        colormap hot
    end
    axis square off
    hold on
    for m = 1:length(msk2plt)
        Mix = cat(1,gmranat.z(z).STATScrop.Masks_ZBB);
        Cani = cat(1,gmranat.z(z).STATScrop(Mix(:,msk2plt(m))).Centroid) + ...
            [gm.xpx gm.ypx].*2.5;
        for s = 1:2
            if s == 1
                ixx = find(Cani(:,2) < gm.xDef/2);
            else
                ixx = find(Cani(:,2) > gm.xDef/2);
            end
            K = convhull(Cani(ixx,1),Cani(ixx,2));
%             K = boundary(Cani(ixx,:),0.9);
            x = Cani(ixx(K),1);
            y = Cani(ixx(K),2);
            np = length(x);
            px = interp1(1:np,x,1:sp:np,'pchip');
            py = interp1(1:np,y,1:sp:np,'pchip');
            
            plot(px,py,'w:','LineWidth',2)
        end
    end
    if usemean
        title(sprintf('p = %d; Mean conv',p))
    else
        title(sprintf('p = %d, t = %d',p,frms(i)))
    end
end

%% Plot mean dFF maps

z = 3;
imrange = [0 20];
fmaprange = [-8 8];
sp = 0.1;
msk2plt = [12,29,34,77,305,306];
nvis = size(gmv.vistypz,1);

thisanatomy = gm.zimg(z).aligngood2;
thisanatomy = scaleif(thisanatomy,0,1,imrange(1),imrange(2));

for v = 1:nvis
    figure
    thismerge = repmat(thisanatomy,1,1,3);
    fmap = gmv.averageresp.z(z).v(v).dFFm;
    
    fmap(abs(fmap)<fmaprange(1)) = 0;
    fmap = fmap./fmaprange(2);
    fmap(fmap>1) = 1;
    fmap(fmap<-1) = -1;
    
    pfmap = fmap;
    nfmap = fmap;
    pfmap(pfmap<0) = 0;
    nfmap(nfmap>0) = 0;
    nfmap = -nfmap;
    
    thismerge = thismerge.*.2; % scale anatomy
    thismerge(:,:,1) = thismerge(:,:,1) + pfmap.*0.8;
    thismerge(:,:,3) = thismerge(:,:,3) + nfmap.*0.8;
    
    imshow(thismerge);
    axis square off
    title(num2str(gmv.vistypz(v,1:4)))
    
    hold on
    for m = 1:length(msk2plt)
        Mix = cat(1,gmranat.z(z).STATScrop.Masks_ZBB);
        Cani = cat(1,gmranat.z(z).STATScrop(Mix(:,msk2plt(m))).Centroid);
        for s = 1:2
            if s == 1
                ix = find(Cani(:,2) < gm.xDef/2);
            else
                ix = find(Cani(:,2) > gm.xDef/2);
            end
            K = convhull(Cani(ix,1),Cani(ix,2));
            x = Cani(ix(K),1);
            y = Cani(ix(K),2);
            np = length(x);
            px = interp1(1:np,x,1:sp:np,'pchip');
            py = interp1(1:np,y,1:sp:np,'pchip');
            
            plot(px,py,'w:','LineWidth',2)
        end
    end
end

%% Colormap

c = bipolar(255);
a = ones(255,1,3);
for i = 1:3
    a(:,:,i) = c(:,i);
end
figure
imshow(repmat(a,1,50,1))