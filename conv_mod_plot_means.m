% Plot mean fluorescent responses for left and right sides of brain of an
% anatomical region and for left and right convergences

folders = { ...
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180301\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180305\f2\deep';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180419\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f1_1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180427\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180503\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180504\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180509\f3';
    };

%%

zbbanatix = [1:3 5:74 77 113 305:306];
load('ZBBMaskDatabase','ZBBMaskDatabaseNames');
ZBBMaskDatabaseNames = ZBBMaskDatabaseNames(zbbanatix);
nmsks = length(ZBBMaskDatabaseNames);

%%

zbbix = [11 28 76 77];
nmsk = length(zbbix);

Mon = cell(nmsk,4);
Moff = cell(nmsk,4);
h = waitbar(0,'Init');
for f = 1:length(folders)
    waitbar(f/length(folders),h,'Processing')
    datadir = folders{f};
    load(fullfile(datadir,'gmConvMod'));
    load(fullfile(datadir,'gmranat'));
    
    % Concatenate ZBB masks for all rois
    nzs = size(gmranat.z,2);
    zbbmsk = [];
    roiszbbC = [];
    for z = 1:nzs
        zbbmsk = cat(1,zbbmsk, ...
            cat(1,gmranat.z(z).STATScrop.Masks_ZBB));
        roiszbbC = cat(1,roiszbbC, ...
            cat(1,gmranat.z(z).STATScrop.Centroid_ZBB));
    end
    zbbmsk = logical(zbbmsk(:,zbbanatix));
    
    for m = 1:nmsk
        zbbixx = zbbmsk(:,zbbix(m)) == 1;
        rois2a = find(zbbixx);
        nrois = length(rois2a);
        
        for i = 1:nrois
            roi = rois2a(i);
            z = gmConvMod.rois(roi).z;
            
            % ON
            lcix_on = find(gmConvMod.rois(roi).Lconv_on);
            lcixx_on = find(abs(gmConvMod.rois(roi).F_on(lcix_on)) > 2);
            rcix_on = find(~gmConvMod.rois(roi).Lconv_on);
            rcixx_on = find(abs(gmConvMod.rois(roi).F_on(rcix_on)) > 2);
                          
            if ~isempty(lcixx_on)
                Mon{m,1} = cat(1,Mon{m,1},gmConvMod.rois(roi).Fz_on(lcix_on(lcixx_on),:));
                Mon{m,2} = cat(1,Mon{m,2},cat(2, ...
                    ones(length(lcixx_on),1).*roi, ...
                    ones(length(lcixx_on),1).*f, ...
                    gmConvMod.rois(roi).vis_on(lcix_on(lcixx_on),1), ...
                    repmat(roiszbbC(roi,2) > 308,length(lcixx_on),1)));
            end
            if ~isempty(rcixx_on)
                Mon{m,3} = cat(1,Mon{m,3},gmConvMod.rois(roi).Fz_on(rcix_on(rcixx_on),:));
                Mon{m,4} = cat(1,Mon{m,4},cat(2, ...
                    ones(length(rcixx_on),1).*roi, ...
                    ones(length(rcixx_on),1).*f, ...
                    gmConvMod.rois(roi).vis_on(rcix_on(rcixx_on),1), ...
                    repmat(roiszbbC(roi,2) > 308,length(rcixx_on),1)));
            end
            
            % OFF
            lcix_off = find(gmConvMod.rois(roi).Lconv_off);
            lcixx_off = find(abs(gmConvMod.rois(roi).F_off(lcix_off)) > 2);
            rcix_off = find(~gmConvMod.rois(roi).Lconv_off);
            rcixx_off = find(abs(gmConvMod.rois(roi).F_off(rcix_off)) > 2);
            
            if ~isempty(lcixx_off)
                Moff{m,1} = cat(1,Moff{m,1},gmConvMod.rois(roi).Fz_off(lcix_off(lcixx_off),:));
                Moff{m,2} = cat(1,Moff{m,2},cat(2, ...
                    ones(length(lcixx_off),1).*roi, ...
                    ones(length(lcixx_off),1).*f, ...
                    gmConvMod.rois(roi).vis_off(lcix_off(lcixx_off),1), ...
                    repmat(roiszbbC(roi,2) > 308,length(lcixx_off),1)));
            end
            if ~isempty(rcixx_off)
                Moff{m,3} = cat(1,Moff{m,3},gmConvMod.rois(roi).Fz_off(rcix_off(rcixx_off),:));
                Moff{m,4} = cat(1,Moff{m,4},cat(2, ...
                    ones(length(rcixx_off),1).*roi, ...
                    ones(length(rcixx_off),1).*f, ...
                    gmConvMod.rois(roi).vis_on(rcix_off(rcixx_off),1), ...
                    repmat(roiszbbC(roi,2) > 308,length(rcixx_off),1)));
            end
        end
    end
end
close(h)

%% Split betwen visstim and conv type

VIS = [3 4;5 6];
labs = {'CCW','CW'};

for i = 1:size(VIS,1)
    vis = VIS(i,:);
    figure('name',labs{i})
    subplot(1,2,1)
    
    ix1 = TL(:,4) == 1 & ismember(TL(:,3),vis);
    ix2 = TL(:,4) == 0 & ismember(TL(:,3),vis);
    shadedErrorBar(1:110,nanmean(Lc(ix1,:)),nansem(Lc(ix1,:)),'b',1);
    hold on
    shadedErrorBar(1:110,nanmean(Lc(ix2,:)),nansem(Lc(ix2,:)),'r',1);
    line([56 56],[0 6],'Color','k')
    axis([0 110 0 6],'square')
    title('Left conv')
    xlabel('Time (fr)')
    ylabel('zF')
    set(gca,'Box','off','TickDir','out')
    
    subplot(1,2,2)
    
    ix1 = TR(:,4) == 1 & ismember(TR(:,3),vis);
    ix2 = TR(:,4) == 0 & ismember(TR(:,3),vis);
    p1 = shadedErrorBar(1:110,nanmean(Rc(ix1,:)),nansem(Rc(ix1,:)),'b',1);
    hold on
    p2 = shadedErrorBar(1:110,nanmean(Rc(ix2,:)),nansem(Rc(ix2,:)),'r',1);
    line([56 56],[0 6],'Color','k')
    axis([0 110 0 6],'square')
    title('Right conv')
    xlabel('Time (fr)')
    ylabel('zF')
    set(gca,'Box','off','TickDir','out')
    legend([p1.mainLine; p2.mainLine], ...
        {'Left side','Right side'},'Location','best')
end

%% Pull by laterality to stim

for m = 1:nmsk
    X1 = cat(1,Mon{m,1}(Mon{m,2}(:,4) == 0,:),Mon{m,3}(Mon{m,4}(:,4) == 2,:));
    X2 = cat(1,Mon{m,1}(Mon{m,2}(:,4) == 1,:),Mon{m,3}(Mon{m,4}(:,4) == 0,:));
    
    figure('name',ZBBMaskDatabaseNames{zbbix(m)})
    p1 = shadedErrorBar(1:110,nanmean(X1),nansem(X1),'b',1);
    hold on
    p2 = shadedErrorBar(1:110,nanmean(X2),nansem(X2),'r',1);
    axis([0 110 0 6],'square')
    line([56 56],[0 6],'Color','k')
    xlabel('Time (fr)')
    ylabel('zF')
    set(gca,'Box','off','TickDir','out')
    legend([p1.mainLine; p2.mainLine], ...
        {'Ipsilateral','Contralateral'},'Location','best')
end
