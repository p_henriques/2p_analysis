
imrange = [0 40];
ffiltwidth = 5; % Calcium fluorescence filter
filter1 = ones(1,ffiltwidth)./ffiltwidth;
zthr = 2;   % z threshold from which to consider a cell active
zthr2 = 1;
savefigs = 0;
offset = 20;
ft_width = 3;
fe_width = 3;
savefigs = 1;

zbbanatix = [1:3 5:74 77 113 305:306];
load('ZBBMaskDatabase','ZBBMaskDatabaseNames');
ZBBMaskDatabaseNames = ZBBMaskDatabaseNames(zbbanatix);

zbbix2a = [76 77 11];

% datadir = folders{f};

% Load structures
load(fullfile(datadir,'gmranat'));
load(fullfile(datadir,'gmrxanat'))
load(fullfile(datadir,'gm'))
load(fullfile(datadir,'gmv'))
load(fullfile(datadir,'gmb'))
load(fullfile(datadir,'gmbt'))
load(fullfile(datadir,'gmbf'))

%%

% Get stimulus times
nfr = gm.nfr;
frtime = gm.frtime/1000;
time = 0:frtime:frtime*(nfr-1);

ne_t = size(gmv.visstim,1);
vST = gm.trfr + 2;
vED = gm.trfr + 1 + ceil(1000.*gmv.vistypz(:,5)./gm.frtime);
vDurMx = max(vED-vST);  % Maximum visual stimulus duration
vSTt = (gm.trfr+1)*gm.frtime./1000;
vttime = gmv.visstim(:,6);
vEDst = vSTt + vttime;

vbtix = ([gmbf.b.Vergbout] == 1 | [gmbf.b.Convbout] == 1) & ...
    [gmbf.b.st] < vEDst([gmbf.b.p])' & ...
    [gmbf.b.st] > vSTt;

vbtn = histcounts([gmbf.b(vbtix).p],0.5:ne_t+0.5);

% Good convergences
conv_g_ix = gmb.convergences(:,3) == 1;
% Stim on convergences
conv_on_ix = gmb.convergences(:,2) >= vSTt & ...
    gmb.convergences(:,2) <= vEDst(gmb.convergences(:,1));

% Concatenate ZBB masks for all rois
nzs = size(gmranat.z,2);
zbbmsk = [];
roisC = [];
roiszbbC = [];
for z = 1:nzs
    zbbmsk = cat(1,zbbmsk, ...
        cat(1,gmranat.z(z).STATScrop.Masks_ZBB));
    roisC = cat(1,roisC, ...
        cat(1,gmranat.z(z).STATScrop.Centroid));
    roiszbbC = cat(1,roiszbbC, ...
        cat(1,gmranat.z(z).STATScrop.Centroid_ZBB));
end
zbbmsk = logical(zbbmsk(:,zbbanatix));

%%

rois2a = zbbmsk(:,zbbix2a) == 1;
nrois = sum(rois2a);
if all(nrois >= 10)
    rois2a_zs = [];
    for i = 1:length(nrois)
        rois2a_zs = catpad(2,rois2a_zs, ...
            unique([gmrxanat.roi(rois2a(:,i)).z])');
    end
    rois2a_zs_u = unique(rois2a_zs);
    
    % All z positions
    for i = 1:length(rois2a_zs_u)
        z = rois2a_zs_u(i);
        if all(any(ismember(rois2a_zs,z)))
            % Epochs with spot presentations
            se = intersect(gm.zindices(z).e, ...
                find(gmv.visstim(:,1) > 77000 & gmv.visstim(:,1) < 82000));
            R = cell(length(zbbix2a),1);
            Lat = cell(length(zbbix2a),1);
            
            % Tail info
            Tresampl = cat(1,gmbt.p(gm.zindices(z).e).resampled);
            
            for n = 1:length(zbbix2a)
                rois = find([gmrxanat.roi.z]' == z & rois2a(:,n));
                nrois = length(rois);
                Lat{n} = roiszbbC(rois,2) > 308;
                
                % Get roi responses
                [v0,~,r0] = gcPresentationInfo(gm.zindices(z).e,gm,gmv);
                roinfr = length(v0)*gm.nfr;
                F = NaN(nrois,roinfr);
                for j = 1:nrois
                    roi = rois(j);
                    F0 = [];
                    for k = 1:length(v0)
                        F0 = cat(1,F0,gmrxanat.roi(roi).Vprofiles(v0(k)).zProfiles(r0(k),:)');
                    end
                    F0f = filtfilt(filter1,1,F0);
                    F0fz = zscore(F0f);
                    F(j,:) = F0fz;
                end
                R{n} = F;
            end
            
            % Stim on convergences
            pconv_on = intersect(se,gmb.convergences(conv_g_ix & conv_on_ix,1));
            [v_on_all,~,r_on_all] = gcPresentationInfo(pconv_on,gm,gmv);
            % Stim off convergences
            pconv_off = intersect(se,gmb.convergences(conv_g_ix & ~conv_on_ix,1));
            [v_off_all,~,r_off_all] = gcPresentationInfo(pconv_off,gm,gmv);
            % No convergence
            npconv = setdiff(se,gmb.convergences(:,1));
            [v_n_all,~,r_n_all] = gcPresentationInfo(npconv,gm,gmv);
            
            %%
            
            % All convergence stim ON trials
            for l = 1:length(pconv_on)
                p_on = pconv_on(l);
                v_on = v_on_all(l);
                r_on = r_on_all(l);
                
                % Find a close stim off epoch of the same vstim
                p_off_v = pconv_off(v_off_all == v_on);
                p_off_vix = findnearest(p_on,p_off_v,0);
                if ~isempty(p_off_vix)
                    p_off_vix = p_off_vix(1);
                    p_off = p_off_v(p_off_vix);
                    if p_off == p_on
                        if p_off_vix ~= 1
                            p_off = p_off_v(p_off_vix-1);
                        elseif p_off_vix < length(p_off_v)
                            p_off = p_off_v(p_off_vix+1);
                        end
                    end
                    [~,~,r_off] = gcPresentationInfo(p_off,gm,gmv);
                    
                    % Find a close no convergence epoch of the same vstim
                    p_n_v = npconv(v_n_all == v_on);
                    p_n_vix = findnearest(p_on,p_n_v,0);
                    if ~isempty(p_n_vix)
                        p_n_vix = p_n_vix(1);
                        p_n = p_n_v(p_n_vix);
                        [~,~,r_n] = gcPresentationInfo(p_n,gm,gmv);
                        
                        % Find stim on convergence
                        convix_on = find(gmb.convergences(:,1) == p_on & conv_g_ix & conv_on_ix);
                        convix_on = convix_on(1); % First convergence with stim on
                        convt_on = gmb.convergences(convix_on,2); % Convergence time
                        convit_on = findnearest(convt_on,time,-1); % Convergence iteration
                        % Find stim off convergence
                        convix_off = find(gmb.convergences(:,1) == p_off & conv_g_ix & ~conv_on_ix);
                        convix_off = convix_off(1); % First convergence with stim off
                        convt_off = gmb.convergences(convix_off,2); % Convergence time
                        convit_off = findnearest(convt_off,time,-1); % Convergence iteration
                        
                        % Visual epoch for this z
                        visp_on = find(v0 == v_on & r0 == r_on);
                        visp_off = find(v0 == v_on & r0 == r_off);
                        visp_n = find(v0 == v_on & r0 == r_n);
                        % Iterations centered on convergence
                        itst_on = nfr*(visp_on-1)+convit_on-nfr/2+1;
                        ited_on = nfr*(visp_on-1)+convit_on+nfr/2;
                        itst_off = nfr*(visp_off-1)+convit_off-nfr/2+1;
                        ited_off = nfr*(visp_off-1)+convit_off+nfr/2;
                        itst_n = nfr*(visp_n-1)+1;
                        ited_n = nfr*(visp_n);
                        
                        if itst_on > 1 && ited_on <= roinfr && ...
                                itst_off > 1 && ited_off <= roinfr && ...
                                itst_n > 1 && ited_n <= roinfr
                            %%
                            % Get epoch times
                            its_on = Tresampl(itst_on:ited_on,1);
                            its_off = Tresampl(itst_off:ited_off,1);
                            its_n = Tresampl(itst_n:ited_n,1);
                            itspreix_on = find(diff(its_on(1:nfr/2)) < 1);
                            itspreix_off = find(diff(its_off(1:nfr/2)) < 1);
                            itspreix_n = find(diff(its_n(1:nfr/2)) < 1);
                            if ~isempty(itspreix_on)
                                its_on(1:itspreix_on) = -flip(1:itspreix_on);
                            end
                            if ~isempty(itspreix_off)
                                its_off(1:itspreix_off) = -flip(1:itspreix_off);
                            end
                            if ~isempty(itspreix_n)
                                its_n(1:itspreix_n) = -flip(1:itspreix_n);
                            end
                            itspostix_on = find(diff(its_on(nfr/2:end)) < 1);
                            itspostix_off = find(diff(its_off(nfr/2:end)) < 1);
                            itspostix_n = find(diff(its_n(nfr/2:end)) < 1);
                            if ~isempty(itspostix_on)
                                its_on(itspostix_on+nfr/2:end) = its_on(itspostix_on+nfr/2:end)+its_on(itspostix_on+nfr/2-1)+1;
                            end
                            if ~isempty(itspostix_off)
                                its_off(itspostix_off+nfr/2:end) = its_off(itspostix_off+nfr/2:end)+its_off(itspostix_off+nfr/2-1)+1;
                            end
                            if ~isempty(itspostix_n)
                                its_n(itspostix_n+nfr/2:end) = its_n(itspostix_n+nfr/2:end)+its_n(itspostix_n+nfr/2-1)+1;
                            end
                            ptime_on = its_on.*frtime;
                            ptime_off = its_off.*frtime;
                            ptime_n = its_n.*frtime;
                            
                            % Stimulus trajectory stim on
                            ST_on = gmb.p(p_on).vis_traj(:,[2:3,7]);
                            ST_on = ST_on(abs(ST_on(:,2)) < 999,:);  % Remove bogus values
                            vitst_on = find(its_on == ST_on(1,1));
                            vited_on = find(its_on == ST_on(end,1));
                            sang_on = -spotangle_v2(ST_on(:,2), ...
                                gm.visgeom.sweepamp,gm.visgeom.amax, ...
                                gm.visgeom.R,gm.visgeom.fish2screen);   % Transform to angles
                            
                            % Stimulus trajectory stim off
                            ST_off = gmb.p(p_off).vis_traj(:,[2:3,7]);
                            ST_off = ST_off(abs(ST_off(:,2)) < 999,:);  % Remove bogus values
                            vitst_off = find(its_off == ST_off(1,1));
                            vited_off = find(its_off == ST_off(end,1));
                            sang_off = -spotangle_v2(ST_off(:,2), ...
                                gm.visgeom.sweepamp,gm.visgeom.amax, ...
                                gm.visgeom.R,gm.visgeom.fish2screen);   % Transform to angles
                            
                            % Stimulus trajectory no convergence
                            ST_n = gmb.p(p_n).vis_traj(:,[2:3,7]);
                            ST_n = ST_n(abs(ST_n(:,2)) < 999,:);  % Remove bogus values
                            vitst_n = ST_n(1,1);
                            vited_n = ST_n(end,1);
                            sang_n = -spotangle_v2(ST_n(:,2), ...
                                gm.visgeom.sweepamp,gm.visgeom.amax, ...
                                gm.visgeom.R,gm.visgeom.fish2screen);   % Transform to angles
                            
                            %% Behaviour
                            
                            % Tail conv
                            ttime1 = gmbt.p(p_on).tt;
                            T1 = gmbt.p(p_on).cumtail;
                            Tf1 = filtfilt(ones(1, ft_width)./ft_width, 1, T1);
                            % Eyes conv
                            etime1 = gmb.p(p_on).tt;
                            Er1 = gmb.p(p_on).Rangles;
                            El1 = gmb.p(p_on).Langles;
                            Elf1 = filtfilt(ones(1, fe_width)./fe_width, 1, El1);
                            Erf1 = filtfilt(ones(1, fe_width)./fe_width, 1, Er1);
                            Evf1 = Elf1-Erf1;
                            
                            % Tail ~conv
                            ttime2 = gmbt.p(p_n).tt;
                            T2 = gmbt.p(p_n).cumtail;
                            Tf2 = filtfilt(ones(1, ft_width)./ft_width, 1, T2);
                            % Eyes ~conv
                            etime2 = gmb.p(p_n).tt;
                            Er2 = gmb.p(p_n).Rangles;
                            El2 = gmb.p(p_n).Langles;
                            Elf2 = filtfilt(ones(1, fe_width)./fe_width, 1, El2);
                            Erf2 = filtfilt(ones(1, fe_width)./fe_width, 1, Er2);
                            Evf2 = Elf2-Erf2;
                            
                            % Tail off
                            ttime3 = gmbt.p(p_off).tt;
                            T3 = gmbt.p(p_off).cumtail;
                            Tf3 = filtfilt(ones(1, ft_width)./ft_width, 1, T3);
                            % Eyes ~conv
                            etime3 = gmb.p(p_off).tt;
                            Er3 = gmb.p(p_off).Rangles;
                            El3 = gmb.p(p_off).Langles;
                            Elf3 = filtfilt(ones(1, fe_width)./fe_width, 1, El3);
                            Erf3 = filtfilt(ones(1, fe_width)./fe_width, 1, Er3);
                            Evf3 = Elf3-Erf3;
                            
                            %%
                            
                            %                             figure('name',sprintf('Fish = %d; Z = %d; p = %d', ...
                            %                                 f,z,p_on), ...
                            %                                 'Position',[486,83,746,896])
                            k = 1;
                            for n = 1:length(zbbix2a)
                                M_on = R{n}(:,itst_on:ited_on); % roi responses
                                nrois2 = size(M_on,1);
                                M_off = R{n}(:,itst_off:ited_off); % roi responses
                                M_n = R{n}(:,itst_n:ited_n); % roi responses
                                
                                % Sort Ca responses
                                mix = NaN(nrois2,1);
                                for m = 1:nrois2
                                    % Find instances where rois crosses
                                    % activity threshold
                                    ixx = find(abs(M_on(m,:)) > zthr);
                                    ixx2 = find(abs(M_on(m,:)) < zthr2);
                                    if ~isempty(ixx)
                                        % Are these after convergence?
                                        if any(ixx >= nfr/2)
                                            cixx = find(ixx >= nfr/2);
                                            % In first it where roi crosses
                                            % thr, go back to see where
                                            % goes back to baseline
                                            baseix = findnearest(ixx(cixx(1)),ixx2,-1);
                                            if ~isempty(baseix)
                                                mix(m) = ixx2(baseix);
                                            else
                                                mix(m) = ixx(cixx(1));
                                            end
                                        elseif any(ixx < nfr/2)
                                            cixx = find(ixx < nfr/2);
                                            baseix = findnearest(ixx(cixx(end)),ixx2,-1);
                                            if ~isempty(baseix)
                                                mix(m) = ixx2(baseix);
                                            else
                                                mix(m) = ixx(end);
                                            end
                                        else
                                            mix(m) = 1;
                                        end
                                    else
                                        mix(m) = 1;
                                    end
                                end
                                
                                [mixsv,mixs] = sort(mix,'ascend');
                                mixs_pre_actv = find(mixsv > 1);
                                if ~isempty(mixs_pre_actv)
                                    mixs_pre_actv = mixs_pre_actv(1)-0.5;
                                end
                                mixs_post_actv = find(mixsv >= nfr/2);
                                if ~isempty(mixs_post_actv)
                                    mixs_post_actv = mixs_post_actv(1)-0.5;
                                end
                                
                                lix = ismember(mixs,find(Lat{n})) & ...
                                    any(abs(M_on(mixs,55-8:55+8)) > zthr,2);
                                rix = ismember(mixs,find(~Lat{n})) & ...
                                    any(abs(M_on(mixs,55-8:55+8)) > zthr,2);
                                Xon = cat(1,M_on(mixs(lix),:), ...
                                    M_on(mixs(rix),:));
                                Xn = cat(1,M_n(mixs(lix),:), ...
                                    M_n(mixs(rix),:));
                                Xoff = cat(1,M_off(mixs(lix),:), ...
                                    M_off(mixs(rix),:));
                                
                                fig = figure('position',[680,118,554,860], ...
                                    'name',sprintf('p_%d_MaskID_%d',p_on,zbbix2a(n)));
                                subplot(4,1,1)
                                imagesc(Xon,[-5 5])
                                colormap(flip(brewermap(255,'RdBu'),1));
                                line([nfr/2 nfr/2],[0 size(M_on,1)+1],'Color','k','LineWidth',2)
                                line([vitst_on vitst_on],[0 size(M_on,1)+1], ...
                                    'Color','b','LineWidth',1,'LineStyle','--')
                                line([vited_on vited_on],[0 size(M_on,1)+1], ...
                                    'Color','r','LineWidth',1,'LineStyle','--')
                                line([0 nfr],[sum(lix) sum(lix)]+0.5, ...
                                    'Color','k','LineWidth',2,'LineStyle','-')
                                set(gca,'Box','off','TickDir','out')
                                ylabel('ROIs')
                                title(ZBBMaskDatabaseNames{zbbix2a(n)})
                                
                                subplot(4,1,2:3)
                                p1 = shadedErrorBar(1:size(Xon,2),mean(M_on(mixs(lix),:)), ...
                                    nansem(M_on(mixs(lix),:)),'b',1);
                                hold on
                                p2 = shadedErrorBar(1:size(Xon,2),mean(M_on(mixs(~lix),:)), ...
                                    nansem(M_on(mixs(~lix),:)),'r',1);
                                ylims = ylim;
                                ylims(1) = -1;
                                axis([1 110 ylims])
                                line([nfr/2 nfr/2],ylims,'Color','k','LineWidth',2)
                                line([vitst_on vitst_on],ylims, ...
                                    'Color','b','LineWidth',1,'LineStyle','--')
                                line([vited_on vited_on],ylims, ...
                                    'Color','r','LineWidth',1,'LineStyle','--')
                                set(gca,'Box','off','TickDir','out')
                                ylabel('zF')                                
                                legend([p1.mainLine; p2.mainLine], ...
                                    {'Left','Right',}, ...
                                    'Location','northwest')
                                
                                subplot(4,1,4)
                                plot(ttime1,Tf1,'g')
                                hold on
                                plot(etime1,Elf1,'b')
                                plot(etime1,Erf1,'r')
                                plot(etime1,Evf1,'k')
                                axis([ptime_on(1) ptime_on(end) -120 120])
                                ylabel('Angle')
                                yyaxis right
                                plot(ST_on(:,3),sang_on,'Color',[.6 .6 .6])
                                axis([ptime_on(1) ptime_on(end) -75 75])
                                xlabel('Epoch time (s)')
                                ylabel('Spot angle')
                                set(gca,'Box','off','TickDir','out')
                                title(num2str(gmv.vistypz(v_on,1:4)))
                                
                                if savefigs
                                    if ~exist(fullfile(datadir,'figures','Convergence','Conv_trig_resp', ...
                                            ZBBMaskDatabaseNames{zbbix2a(n)}),'dir')
                                        mkdir(fullfile(datadir,'figures','Convergence','Conv_trig_resp', ...
                                            ZBBMaskDatabaseNames{zbbix2a(n)}))
                                    end
                                    savefig(fig,fullfile(datadir,'figures','Convergence','Conv_trig_resp', ...
                                        ZBBMaskDatabaseNames{zbbix2a(n)},fig.Name))
                                    close(fig)
                                end
                                
                                %                                 k = k+1;
                                
                                %                                 subplot(length(zbbix2a)+1,3,k)
                                %                                 imagesc(Xn,[-5 5])
                                %                                 colormap(flip(brewermap(255,'RdBu'),1));
                                %                                 line([vitst_n vitst_n],[0 size(M_n,1)+1], ...
                                %                                     'Color','b','LineWidth',1,'LineStyle','--')
                                %                                 line([vited_n vited_n],[0 size(M_n,1)+1], ...
                                %                                     'Color','r','LineWidth',1,'LineStyle','--')
                                %                                 line([0 nfr],[sum(lix) sum(lix)]+0.5, ...
                                %                                     'Color','k','LineWidth',2,'LineStyle','-')
                                %                                 set(gca,'Box','off','TickDir','out')
                                %                                 k = k+1;
                                %
                                %                                 subplot(length(zbbix2a)+1,3,k)
                                %                                 imagesc(Xoff,[-5 5])
                                %                                 colormap(flip(brewermap(255,'RdBu'),1));
                                %                                 line([vitst_off vitst_off],[0 size(M_off,1)+1], ...
                                %                                     'Color','b','LineWidth',1,'LineStyle','--')
                                %                                 line([vited_off vited_off],[0 size(M_off,1)+1], ...
                                %                                     'Color','r','LineWidth',1,'LineStyle','--')
                                %                                 line([0 nfr],[sum(lix) sum(lix)]+0.5, ...
                                %                                     'Color','k','LineWidth',2,'LineStyle','-')
                                %                                 set(gca,'Box','off','TickDir','out')
                                %                                 k = k+1;                                                                
                            end
                            
                            %                             subplot(length(zbbix2a)+1,2,k)
                            %                             plot(ttime2,Tf2,'g')
                            %                             hold on
                            %                             plot(etime2,Elf2,'b')
                            %                             plot(etime2,Erf2,'r')
                            %                             plot(etime2,Evf2,'k')
                            %                             axis([ptime_n(1) ptime_n(end) -120 120])
                            %                             ylabel('Angle')
                            %                             yyaxis right
                            %                             plot(ST_n(:,3),sang_n,'Color',[.6 .6 .6])
                            %                             axis([ptime_n(1) ptime_n(end) -75 75])
                            %                             xlabel('Epoch time (s)')
                            %                             ylabel('Spot angle')
                            %                             title(num2str(gmv.vistypz(v_on,1:4)))
                            %                             k = k+1;
                            %
                            %                             subplot(length(zbbix2a)+1,3,k)
                            %                             plot(ttime3,Tf3,'g')
                            %                             hold on
                            %                             plot(etime3,Elf3,'b')
                            %                             plot(etime3,Erf3,'r')
                            %                             plot(etime3,Evf3,'k')
                            %                             axis([ptime_off(1) ptime_off(end) -120 120])
                            %                             ylabel('Angle')
                            %                             yyaxis right
                            %                             plot(ST_off(:,3),sang_off,'Color',[.6 .6 .6])
                            %                             axis([ptime_off(1) ptime_off(end) -75 75])
                            %                             xlabel('Epoch time (s)')
                            %                             ylabel('Spot angle')
                            %                             title(num2str(gmv.vistypz(v_on,1:4)))
                            %                             k = k+1;
                        end
                    end
                end
            end
        end
    end
end
