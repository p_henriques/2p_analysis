
% load('Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\analysis\Convergence\mean_Z_conv_matrix_180706.mat')

load('Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\analysis\Convergence\Z_conv_matrix_181015.mat');

ConvM(abs(ConvM) > 20) = nan;

%%

folders = { ...
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180301\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180305\f2\deep';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180419\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f1_1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180427\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180503\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180504\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180509\f3';
    };

nfolders = length(folders);

%%  Load Mask database names

zbbanatix = [2:3 6:34 44:46 48:74 77 113 305:306];
load('ZBBMaskDatabase');
ZBBMaskDatabaseNames = ZBBMaskDatabaseNames(zbbanatix);
ZBBMaskDatabase = ZBBMaskDatabase(:,zbbanatix);
nmsks = length(ZBBMaskDatabaseNames);

%%

tlabs = {'Mean LConvZ ON','Mean RConvZ ON','Mean LConvZ OFF','Mean RConvZ OFF', ...
    'Mean Null LConvZ ON','Mean Null RConvZ ON', ...
    'Mean Null LConvZ OFF','Mean Null RConvZ OFF'};

zbbmsk = [];
roiszbbC = [];
ConvM = [];
T = [];
h = waitbar(0, 'Init');
for f = 1:length(folders)
    waitbar(f/length(folders),h, 'Processing...')
    datadir = folders{f};
    
    load(fullfile(datadir,'gmConvMod.mat'));
    load(fullfile(datadir,'gmranat'));
    
    % Null distributions
    nrois = size(gmConvMod.rois,2);
    X = NaN(nrois,4);
    for i = 1:nrois
        lconvon = gmConvMod.rois(i).Lconv_on;
        lconvoff = gmConvMod.rois(i).Lconv_off;
        Lnullon = nanmean(gmConvMod.rois(i).Z_null_on(lconvon));
        Rnullon = nanmean(gmConvMod.rois(i).Z_null_on(~lconvon));
        Lnulloff = nanmean(gmConvMod.rois(i).Z_null_off(lconvoff));
        Rnulloff = nanmean(gmConvMod.rois(i).Z_null_off(~lconvoff));
        X(i,:) = [Lnullon,Rnullon,Lnulloff,Rnulloff];
    end
    
    ConvM = cat(1,ConvM, ...
        cat(2, ...
        [gmConvMod.rois.Z_on_Lconv_Mn]',[gmConvMod.rois.Z_on_Rconv_Mn]', ...
        [gmConvMod.rois.Z_off_Lconv_Mn]',[gmConvMod.rois.Z_off_Rconv_Mn]', ...
        X));
    
    T = cat(1,T, ...
        cat(2,ones(size(gmConvMod.rois,2),1).*f, ...
        [1:size(gmConvMod.rois,2)]'));
    
    nzs = size(gmranat.z,2);
    for z = 1:nzs
        zbbmsk = cat(1,zbbmsk, ...
            cat(1,gmranat.z(z).STATScrop.Masks_ZBB));
        roiszbbC = cat(1,roiszbbC, ...
            cat(1,gmranat.z(z).STATScrop.Centroid_ZBB));
    end
    
    clear gmConvMod gmranat
end
zbbmsk = logical(zbbmsk(:,zbbanatix));
close(h);


%% Z-conv matrix

convz = 2;  % z threshold to consider for z fraction map
ylabs = {'Fraction of Conv+ ROIs','Mean Conv Z score'};
zbbmskix = [63];    % ZBB mask ID to analyse
plotboundaries = 1; % Plot mask boundaries
mskbplt = {[64 65];[9 30 31];[36]};   % ZBB mask IDs to plot boundaries
binspace = 1; % microns
binoffset = 10; % microns
axisoffset = 5; % pixels

roisix = sum(zbbmsk(:,zbbmskix),2) > 0;
ndim = size(ConvM,2);

% Bin edges
edgx = floor(min(roiszbbC(:,1)))-binoffset:binspace:ceil(max(roiszbbC(:,1)))+binoffset;
edgy = floor(min(roiszbbC(:,2)))-binoffset:binspace:ceil(max(roiszbbC(:,2)))+binoffset;
fish = unique(T(:,1));
nfish = length(fish);

[N,Xedges,Yedges,binX,binY] = histcounts2(roiszbbC(:,1),roiszbbC(:,2),edgx,edgy);

%%

Amn = zeros([size(N),ndim,nfish]);
Afrc = zeros([size(N),ndim,nfish]);
h = waitbar(0,'Init');
for f = 1:nfish
    waitbar(f/nfish,h,'Processing')
    for d = 1:ndim
        for i = 1:size(N,1)
            for j = 1:size(N,2)
                ix = binX == i & binY == j & T(:,1) == fish(f);
                if any(ix)
                    Afrc(i,j,d,f) = nansum(abs(ConvM(ix & roisix,d)) >= convz)/sum(ix & roisix);
                    Amn(i,j,d,f) = nanmean(ConvM(ix & roisix,d));
                end
            end
        end
    end
end
close(h)
Amn(isnan(Amn)) = 0;
Afrc(isnan(Afrc)) = 0;

%% 2D Filter

medfsz = 2; % Size of median filter
gaussfsz = 5;   % Size of gaussian smoothing filter
fish2a = [1,3:9];   % Fish to use
ndim = size(Amn,3);
nfish = size(Amn,4);

% Soft median filter
Amnf = zeros(size(Amn));
Afrcf = zeros(size(Afrc));
for f = 1:nfish
    for d = 1:ndim
        Amnf(:,:,d,f) = medfilt2(Amn(:,:,d,f),[medfsz medfsz]);
        Afrcf(:,:,d,f) = medfilt2(Afrc(:,:,d,f),[medfsz medfsz]);
    end
end

% Average across fish
Amnfm = nanmean(Amnf(:,:,:,fish2a),4);
Afrcfm = nanmean(Afrcf(:,:,:,fish2a),4);

% Gaussian filter
Amnfmf = zeros([size(N) ndim]);
Afrcfmf = zeros([size(N),ndim]);
for d = 1:ndim
    Amnfmf(:,:,d) = imgaussfilt(Amnfm(:,:,d),gaussfsz);
    Afrcfmf(:,:,d) = imgaussfilt(Afrcfm(:,:,d),gaussfsz);
end

%%  Plot

clims1 = [0 0.03];
clims2 = [-0.1 0.1];
intscale = 0.25;

for k = 1:2
    for d = 1:ndim
        if k == 1
            figure
            thr = prctile(reshape(Afrcfmf(:,:,d),numel(N),1),99.9);
            imagesc(Afrcfmf(:,:,d),[0 thr])
            colormap hot
            col = 'w';
        else
            figure
            thr = prctile(reshape(Amnfmf(:,:,d),numel(N),1),99.9);
            imagesc(Amnfmf(:,:,d),[-thr thr])
            colormap(flip(brewermap(255,'RdBu'),1));
            col = 'k';
        end
        set(gca,'XDir','reverse');
        axis off equal
        
        if plotboundaries
            hold on
            for n = 1:size(mskbplt,1)
                for j = 1:length(mskbplt{n,1})
                    if j == 1
                        mskrange = round(min(roiszbbC(zbbmsk(:,mskbplt{n}(j)),3))): ...
                            round(max(roiszbbC(zbbmsk(:,mskbplt{n}(j)),3)));
                    end
                    msk = reshape(full(ZBBMaskDatabase(:,mskbplt{n}(j))),[616,1030,420]);
                    msk = msk(:,:,mskrange);
                    if j == 1
                        %                 [~,mskix] = max(squeeze(sum(sum(msk,1),2)));
                        mskix = round(mean(1:length(mskrange)));
                    end
                    
                    % Smooth mask
                    M = msk(:,:,mskix);
                    F = griddedInterpolant(double(M));
                    [sx,sy,sz] = size(M);
                    
                    xq = (0:intscale:sx)';
                    yq = (0:intscale:sy)';
                    zq = (0:intscale:sz)';
                    
                    Mf = imgaussfilt(F({xq,yq}),3) > 0.5;
                    mskB = bwboundaries(Mf);
                    for m = 1:size(mskB,1)
                        mskB{m,1} = mskB{m,1}.*intscale;
                    end
                    
                    for l = 1:length(mskB)
                        boundary = [mskB{l}-[edgy(1) edgx(1)]]./binspace;
                        plot(boundary(:,1), boundary(:,2),':', ...
                            'Color',col,'LineWidth',1);
                    end
                end
            end
        end
        title(tlabs{d})
    end
end

%% Z Maps

msk2plt = [9 25 26 30 31 39 53 54 62 64 65];
usezbbmsk = 0;
% mskxoffset = [0 15 10];
% mskyoffset = [0 0 42;
%     0 0 -47];
mskrange = round([prctile(roiszbbC(:,3),1) ...
    prctile(roiszbbC(:,3),99)]);
intscale = 0.25;
gaussfilt = 17;

col = flip(brewermap(255,'RdBu'),1);
for n = 1:length(msk2plt)
    for s = 1:2
        if s == 1
            six = roiszbbC(:,2) > 308;
        else
            six = roiszbbC(:,2) < 308;
        end
        mix = zbbmsk(:,msk2plt(n)) & six;
        C = roiszbbC(mix,:);
        Z = ConvM(mix,1);
        ixnan = isnan(Z);
        Z(ixnan) = [];
        C(ixnan,:) = [];
        Z(Z<-2) = -2;
        Z(Z>2) = 2;
        Z = round(scaledata(Z,1,255));
        figure
        scatter(C(:,1),C(:,2),100,col(Z,:),'filled')
        hold on
        if usezbbmsk
            msk = reshape(full(ZBBMaskDatabase(:,msk2plt(n))),[616,1030,420]);
            if s == 1
                msk(1:308,:,:) = 0;
            else
                msk(308:end,:,:) = 0;
            end
            I = bwmorph(sum(msk(:,:,mskrange(1):mskrange(2)),3) > 0,'close');
            % Smooth mask
            F = griddedInterpolant(double(I));
            [sx,sy,sz] = size(I);
            
            xq = (0:intscale:sx)';
            yq = (0:intscale:sy)';
            zq = (0:intscale:sz)';
            
            If = imgaussfilt(F({xq,yq}),gaussfilt) > 0.5;
            mskB = bwboundaries(If);
            for m = 1:size(mskB,1)
                mskB{m,1} = mskB{m,1}.*intscale;
            end
            
            for k = 1:length(mskB)
                boundary = mskB{k};
                plot(boundary(:,2), ...
                    boundary(:,1),'-', ...
                    'Color','k','LineWidth',1);
            end
        else
            K = convhull(C(:,1),C(:,2));
            x = C(K,1);
            y = C(K,2);
            np = length(x);
            xp = interp1(1:np,x,1:0.01:np);
            yp = interp1(1:np,y,1:0.01:np);
            plot(xp,yp,'k-')
        end
        axis off equal
        set(gca,'XDir','normal','YDir','reverse')
        view([90 90])
        alpha 0.3
        title([ZBBMaskDatabaseNames{msk2plt(n)} sprintf(' Side %s',s)])
    end
end

%% Maps mean Z

% msk2plt = [9 26 31 62 64 65 39 45 30];
% mskxoffset = [30 70 0 45+15 45 45+10 -90 -280 -140];
% mskyoffset = [0 0 0 40 0 0 0 80 5;
%     0 0 0 -40 0 0 0 -80 -5];

msk2plt = [31 30 9 39 26 62 65 64];
mskxoffset = [0 0 -20 0 0 0 0 0];
mskyoffset = [0 0 0 0 0 0 0 0;
    0 0 0 0 0 0 0 0];

mskrange = round([prctile(roiszbbC(:,3),1) ...
    prctile(roiszbbC(:,3),99)]);
intscale = 0.25;
gaussfilt = 17;
usefrac = 0;
cthr = 3;
zrange = [0.2 0.7];
abovenull = 0;
col = brewermap(255,'Oranges');
% col = RBcmap(512); col = col(255:end,:);
% col = flip(hot(255),1);
lut = linspace(zrange(1),zrange(2),size(col,1));
ylabs = {'CZ-score','SpCZ-score','CZ-score null','SpCZ-score null'};

k = 1;
for i = 1:2:8
    figure
    for n = 1:length(msk2plt)
        for s = 1:2
            if s == 1
                six = roiszbbC(:,2) > 308;
            else
                six = roiszbbC(:,2) < 308;
            end
            mix = zbbmsk(:,msk2plt(n));
            Z = cat(1,ConvM(mix & six,i),ConvM(mix & ~six,i+1));
            ix = abs(Z) > 50;
            Z(ix) = nan;
            if usefrac
                zv = sum(Z > cthr)/length(Z);
            else
                zv = nanmean(Z);
            end
            if i < 4 && abovenull
                Zn = cat(1,ConvM(mix & six,i),ConvM(mix & ~six,i+4));
                Zn(ix) = nan;
                if usefrac
                    zvn = sum(Zn > cthr)/length(Zn);
                else
                    zvn = nanmean(Zn);
                end
                zv = zv-zvn;
            end
            color = col(findnearest(zv,lut),:);
            
            msk = reshape(full(ZBBMaskDatabase(:,msk2plt(n))),[616,1030,420]);
            if s == 1
                msk(1:308,:,:) = 0;
            else
                msk(308:end,:,:) = 0;
            end
            I = bwmorph(sum(msk(:,:,mskrange(1):mskrange(2)),3) > 0,'close');
            % Smooth mask
            F = griddedInterpolant(double(I));
            [sx,sy,sz] = size(I);
            
            xq = (0:intscale:sx)';
            yq = (0:intscale:sy)';
            zq = (0:intscale:sz)';
            
            If = imgaussfilt(F({xq,yq}),gaussfilt) > 0.5;
            mskB = bwboundaries(If);
            for m = 1:size(mskB,1)
                mskB{m,1} = mskB{m,1}.*intscale;
            end
            
            for m = 1:length(mskB)
                boundary = mskB{m};
                patch(boundary(:,1)+mskyoffset(s,n), ...
                    boundary(:,2)+mskxoffset(n),color)
            end
        end
    end
    axis off equal
    set(gca,'XDir','reverse','YDir','reverse')
    view([90 90])
    title(ylabs{k})
    k = k+1;
end

% Plot colorbar

cbar = permute(repmat(col,1,1,50),[1,3,2]);
figure
imshow(cbar)
axis on
set(gca,'YDir','normal','YTick', ...
    0:255/4:255,'YTickLabel',zrange(1):zrange(2)/4:zrange(2),'Box','off')

%% Z distribution per anatomical region (sorted)

zthr = 3;   % CZ-score threshold by which to consider a CZ+ cell
joinfigs = 0; % Join CZscore and SpCZscore in same bar plot
nmin = 20;
alphav = 0.05;
topn = 50;
msk2plt = [9 26 31 62 64 65 39 45 30];
msklabs = {'Gt','nIII','OT-SPV','Ch-B','aCh-A', ...
    'pCh-A','Pt','Teg','OT-neuropil'};
% msk2plt = [9 25 26 30 31 55 62 64 65 39 45];
nmsks = length(msk2plt);
fish2use = 1:9;
ylabs = {'CZ-score','SpCZ-score'};

M = NaN(nmsks,2,3,3,2);
P = NaN(nmsks,2,3);
for i = 1:2:4
    for n = 1:nmsks
        lix = roiszbbC(:,2) > 308;
        mix = zbbmsk(:,msk2plt(n)) & ismember(T(:,1),fish2use);
        Zipsi = cat(1,ConvM(mix & lix,i),ConvM(mix & ~lix,i+1));
        Zipsinull = cat(1,ConvM(mix & lix,i+4),ConvM(mix & ~lix,i+5));
        Zcontra = cat(1,ConvM(mix & ~lix,i),ConvM(mix & lix,i+1));
        Zcontranull = cat(1,ConvM(mix & ~lix,i+4),ConvM(mix & lix,i+5));
        if length(Zipsi) > nmin
            %             [r,~] = find(abs(Zipsi) > 20);
            %             Zipsi(r,:) = [];
            %             Zipsinull(r,:) = [];
            %             [r,~] = find(abs(Zcontra) > 20);
            %             Zcontra(r,:) = [];
            %             Zcontranull(r,:) = [];
            
            % Mean
            M(n,i,1,1,1) = nanmean(Zipsi);
            M(n,i,1,2,1) = nansem(Zipsi);
            M(n,i,2,1,1) = nanmean(Zcontra);
            M(n,i,2,2,1) = nansem(Zcontra);
            % Null dist
            M(n,i,3,1,1) = nanmean(Zipsinull);
            M(n,i,3,2,1) = nansem(Zipsinull);
            M(n,i,4,1,1) = nanmean(Zcontranull);
            M(n,i,4,2,1) = nansem(Zcontranull);
            
            % Fraction
            M(n,i,1,1,2) = sum(Zipsi > zthr)./size(Zipsi,1);
            M(n,i,2,1,2) = sum(Zcontra > zthr)./size(Zcontra,1);
            % Null dist
            M(n,i,3,1,2) = sum(Zipsinull > zthr)./size(Zipsinull,1);
            M(n,i,4,1,2) = sum(Zcontranull > zthr)./size(Zcontranull,1);
            
            P(n,i,1) = ranksum(Zipsi,Zcontra);
            P(n,i,2) = ranksum(Zipsi,Zipsinull);
            P(n,i,3) = ranksum(Zcontra,Zcontranull);
        end
    end
end

% Plots
k = 1;
for i = 1:2:4
    for j = 1:2
        if j == 1
            [srt,six] = sort(M(:,i,1,1,1), ...
                'descend','MissingPlacement','last');
            ix = six(~isnan(srt));
            if topn < length(ix)
                ix = ix(1:topn);
            end
        end
        
        [~,bar_xtick2,hb] = errorbar_groups_ph(squeeze(M(ix,i,1:2,1,j))', ...
            squeeze(M(ix,i,1:2,2,j))','bar_width',0.9);
        p1 = line(bar_xtick2-(0.9/2),repmat(M(ix,i,3,1,j),1,2)', ...
            'LineStyle',':','Color','k','LineWidth',2);
        p2 = line(bar_xtick2+(0.9/2),repmat(M(ix,i,4,1,j),1,2)', ...
            'LineStyle',':','Color','k','LineWidth',2);
        if j == 1
            for m = 1:length(ix)
                sigstar(bar_xtick2(:,m),P(ix(m),i,1))
            end
        end
        set(gca,'XTickLabel',msklabs(ix), ...
            'XTickLabelRotation',45, ...
            'TickDir','out','Box','off')
        xlabel('Anatomical ID')
        if j ~= 1
            ylabel(sprintf('Fraction CZ+ ROIs (> %d)',zthr))
        else
            ylabel('Mean CZ-score')
        end
        legend([hb;p1],{'Ipsi conv','Contra conv','Shuffle'},'Location','northeast')
        title(ylabs{k})
    end
    k = k+1;
end

%% ConvZ Asymmetry index

% Conv ON
LL = abs(M(:,1,1,1))+abs(M(:,1,2,2));
LR = abs(M(:,1,1,2))+abs(M(:,1,2,1));
Asix = (LL-LR)./(LL+LR);

[srt,six] = sort(Asix,'descend','MissingPlacement','last');
ix = six(~isnan(srt) & any(PV_corr(six,1:2) < alphav,2));
figure('position',[418,314,877,496])
bar(Asix(ix,1))
set(gca,'XTick',1:length(ix), ...
    'XTickLabel',ZBBMaskDatabaseNames(ix), ...
    'XTickLabelRotation',45, ...
    'TickDir','out','Box','off')
ylabel('ConvZ ON Asymmetry index')
xlabel('Anatomical ID')

% Conv OFF
LL = abs(M(:,1,3,1))+abs(M(:,1,4,2));
LR = abs(M(:,1,3,2))+abs(M(:,1,4,1));
Asix = (LL-LR)./(LL+LR);

[srt,six] = sort(Asix,'descend','MissingPlacement','last');
ix = six(~isnan(srt) & any(PV_corr(six,1:2) < alphav,2));
figure('position',[418,314,877,496])
bar(Asix(ix,1))
set(gca,'XTick',1:length(ix), ...
    'XTickLabel',ZBBMaskDatabaseNames(ix), ...
    'XTickLabelRotation',45, ...
    'TickDir','out','Box','off')
ylabel('ConvZ OFF Asymmetry index')
xlabel('Anatomical ID')

%% mean Z-conv distribution per anatomical region (sorted) - all sides

nmin = 25;
alphav = 0.05;

M = NaN(nmsks,2,2);
PV = NaN(nmsks,2);
PV_corr = NaN(nmsks,2);
for i = 1:nmsks
    ix = zbbmsk(:,i) == 1;
    if sum(ix) > nmin
        M(i,1,:) = nanmean(ConvM(ix,9:10),1);
        M(i,2,:) = nansem(ConvM(ix,9:10),1);
        for j = 1:2
            PV(i,j) = signrank(ConvM(ix,j));
        end
    end
end
% Multiple comparisons
[~, ~, ~, PV_corr(:,1)] = fdr_bh(PV(:,1),alphav,'pdep');
[~, ~, ~, PV_corr(:,2)] = fdr_bh(PV(:,2),alphav,'pdep');

for i = 1:size(M,3)
    [srt,six] = sort(nanmean(abs(squeeze(M(:,1,i))),2), ...
        'descend','MissingPlacement','last');
    ix = six(~isnan(srt) & any(squeeze(PV_corr(six,i)) < alphav,2));
    
    figure('position',[418,314,877,496])
    bar(M(ix,1,i))
    hold on
    errorbar(M(ix,1,i),M(ix,2,i),'.k')
    set(gca,'XTick',1:length(ix), ...
        'XTickLabel',ZBBMaskDatabaseNames(ix), ...
        'XTickLabelRotation',45, ...
        'TickDir','out','Box','off')
    xlabel('Anatomical ID')
    ylabel('Conv Z-score')
end

%% Comparison of ConvON and OFF responses in NI

zbbix = 64;
ix = zbbmsk(:,zbbix);
M = ConvM(ix,1:4);
M(any(isnan(M),2),:) = [];
nreps = size(M,1);
X = cat(1,M(:,1:2),M(:,3:4));

figure('position',[427,364,241,420]);
bar(nanmedian(M))
hold on
errorbar(nanmedian(M),nansem(M),'.')
P = [signrank(M(:,1),M(:,2));
    signrank(M(:,1),M(:,3));
    signrank(M(:,3),M(:,4));
    signrank(M(:,2),M(:,4))];

% [~, ~, ~, P_corr] = fdr_bh(P(:),0.05,'pdep');
P_corr = P;
sigstar_ph([1,2],P_corr(1))
sigstar_ph([1,3],P_corr(2))
sigstar_ph([3,4],P_corr(3))
sigstar_ph([2,4],P_corr(4))
set(gca,'Box','off','TickDir','out', ...
    'XTickLabelRotation',45, ...
    'XTickLabel',{'L-Conv ON','R-Conv ON','L-Conv OFF','R-Conv OFF'})
ylabel('Mean CZ-Score')
title(ZBBMaskDatabaseNames{zbbix})

%% Conv+ fraction per anatomical region (sorted)

convz = 2;

ylabs = {'CZ','SpCZ'};
msk2plt = [9 26 31 62 64 65 39 45 30];
nmsks = length(msk2plt);
M = NaN(nmsks,2,2);

k = 1;
for i = 1:2:4
    for n = 1:nmsks
        lix = roiszbbC(:,2) > 308;
        mix = zbbmsk(:,msk2plt(n));
        Zipsi = cat(1,ConvM(mix & lix,i),ConvM(mix & ~lix,i+1));
        Zcontra = cat(1,ConvM(mix & ~lix,i),ConvM(mix & lix,i+1));
        M(n,1,k) = nansum(Zipsi > convz)/length(Zipsi);
        M(n,2,k) = nansum(Zcontra > convz)/length(Zcontra);
    end
    k = k+1;
end

for i = 1:2
    [srt,six] = sort(M(:,1,i),'descend','MissingPlacement','last');
    
    ix = six(~isnan(srt));
    errorbar_groups_ph(M(ix,1:2,1)',zeros(size(M(ix,1:2,1)))');
    set(gca,'XTickLabel',ZBBMaskDatabaseNames(msk2plt(ix)), ...
        'XTickLabelRotation',45, ...
        'TickDir','out','Box','off')
    xlabel('Anatomical ID')
    ylabel(sprintf('Fraction of %s+ ROIs (>%d)',ylabs{i},convz))
    legend({'Ipsilateral','Contralateral'},'Location','northeast')
end

%% Plot single conv responses

zbbix = 76;
s = 1;

zbbixx = find(zbbmsk(:,zbbix));
Cm = nanmean(ConvM(zbbixx,s),2);
% [srt,six] = sort(abs(abs(Cm)-nanmean(Cm)),'ascend');
% [~,ix] = max(Cm);
[srt,six] = sort(Cm,'descend');

fish = T(zbbixx(six),1);
rois = T(zbbixx(six),2);
f = mode(fish);
rois2a = rois(fish == f);

datadir = folders{f};
load(fullfile(datadir,'gmConvMod.mat'));

roi = rois2a(1);

%%

f = 5;
zbbix = 64;
zbbixx = zbbmsk(:,zbbix);
fix = T(:,1) == f;

rois2a = T(zbbixx & fix,2);

datadir = folders{f};
load(fullfile(datadir,'gmConvMod.mat'));

[lc_srt,lc_six] = sort(cat(1,gmConvMod.rois(rois2a).Z_on_Lconv_mean),'descend');
[rc_srt,rc_six] = sort(cat(1,gmConvMod.rois(rois2a).Z_on_Rconv_mean),'descend');

for i = 1:10
    roi = rois2a(rc_six(i));
    ix = find(~gmConvMod.rois(roi).Lconv_on);
    
    for j = 1:length(ix)
        figure
        plot(gmConvMod.rois(roi).Fz_on(ix(j),:),'b');
        hold on
        plot(gmConvMod.rois(roi).Fz_on_nrsp{ix(j)}','k');
    end
end
