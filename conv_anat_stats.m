
folders = { ...
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180301\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180305\f2\deep';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180419\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f1_1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180427\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180503\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180504\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180509\f3';
    };

%%

ffiltwidth = 5; % Calcium fluorescence filter
filter1 = ones(1,ffiltwidth)./ffiltwidth;
zthr = 2;   % z threshold from which to consider a cell active
wndfr = 8;
preprewnd = 56-(2*wndfr):56-wndfr;
prewnd = 56-wndfr:56;
periwnd = 56-(wndfr/2):56+(wndfr/2);
nshuffle = 500;

zbbanatix = [1:3 5:74 77 113 305:306];
load('ZBBMaskDatabase','ZBBMaskDatabaseNames');
ZBBMaskDatabaseNames = ZBBMaskDatabaseNames(zbbanatix);

zbbix2a = 1:length(zbbanatix);

%%

X = cell(length(zbbanatix),2);
h = waitbar(0,'Init');
for f = 1:length(folders)
    waitbar(f/length(folders),h,'Processing')
    datadir = folders{f};
    
    % Load structures
    load(fullfile(datadir,'gmranat'));
    load(fullfile(datadir,'gmrxanat'))
    load(fullfile(datadir,'gm'))
    load(fullfile(datadir,'gmv'))
    load(fullfile(datadir,'gmb'))
    load(fullfile(datadir,'gmbt'))
    load(fullfile(datadir,'gmbf'))
    
    %%
    
    % Get stimulus times
    nfr = gm.nfr;
    frtime = gm.frtime/1000;
    time = 0:frtime:frtime*(nfr-1);
    
    ne_t = size(gmv.visstim,1);
    vST = gm.trfr + 2;
    vED = gm.trfr + 1 + ceil(1000.*gmv.vistypz(:,5)./gm.frtime);
    vDurMx = max(vED-vST);  % Maximum visual stimulus duration
    vSTt = (gm.trfr+1)*gm.frtime./1000;
    vttime = gmv.visstim(:,6);
    vEDst = vSTt + vttime;
    
    vbtix = ([gmbf.b.Vergbout] == 1 | [gmbf.b.Convbout] == 1) & ...
        [gmbf.b.st] < vEDst([gmbf.b.p])' & ...
        [gmbf.b.st] > vSTt;
    
    vbtn = histcounts([gmbf.b(vbtix).p],0.5:ne_t+0.5);
    
    % Good convergences
    conv_g_ix = gmb.convergences(:,3) == 1;
    % Stim on convergences
    conv_on_ix = gmb.convergences(:,2) >= vSTt & ...
        gmb.convergences(:,2) <= vEDst(gmb.convergences(:,1));
    
    % Concatenate ZBB masks for all rois
    nzs = size(gmranat.z,2);
    zbbmsk = [];
    roisC = [];
    roiszbbC = [];
    for z = 1:nzs
        zbbmsk = cat(1,zbbmsk, ...
            cat(1,gmranat.z(z).STATScrop.Masks_ZBB));
        roisC = cat(1,roisC, ...
            cat(1,gmranat.z(z).STATScrop.Centroid));
        roiszbbC = cat(1,roiszbbC, ...
            cat(1,gmranat.z(z).STATScrop.Centroid_ZBB));
    end
    zbbmsk = logical(zbbmsk(:,zbbanatix));
    
    %%
    
    rois2a = zbbmsk(:,zbbix2a) == 1;
    nrois = sum(rois2a);
    rois2a_zs = [];
    for i = 1:length(nrois)
        rois2a_zs = catpad(2,rois2a_zs, ...
            unique([gmrxanat.roi(rois2a(:,i)).z])');
    end
    rois2a_zs_u = unique(rois2a_zs);
    rois2a_zs_u = rois2a_zs_u(~isnan(rois2a_zs_u));
    
    % All z positions
    for i = 1:length(rois2a_zs_u)
        z = rois2a_zs_u(i);
        % Epochs with spot presentations
        se = intersect(gm.zindices(z).e, ...
            find(gmv.visstim(:,1) > 77000 & gmv.visstim(:,1) < 82000));
        R = cell(length(zbbix2a),1);
        Lat = cell(length(zbbix2a),1);
        
        for n = 1:length(zbbix2a)
            rois = find([gmrxanat.roi.z]' == z & rois2a(:,n));
            nrois = length(rois);
            Lat{n} = roiszbbC(rois,2) > 308;
            
            % Get roi responses
            [v0,~,r0] = gcPresentationInfo(gm.zindices(z).e,gm,gmv);
            roinfr = length(v0)*gm.nfr;
            F = NaN(nrois,roinfr);
            for j = 1:nrois
                roi = rois(j);
                F0 = [];
                for k = 1:length(v0)
                    F0 = cat(1,F0,gmrxanat.roi(roi).Vprofiles(v0(k)).zProfiles(r0(k),:)');
                end
                F0f = filtfilt(filter1,1,F0);
                F0fz = zscore(F0f);
                F(j,:) = F0fz;
            end
            R{n} = F;
        end
        
        % Stim on convergences
        pconv_on = intersect(se,gmb.convergences(conv_g_ix & conv_on_ix,1));
        [v_on_all,~,r_on_all] = gcPresentationInfo(pconv_on,gm,gmv);
        % Stim off convergences
        pconv_off = intersect(se,gmb.convergences(conv_g_ix & ~conv_on_ix,1));
        [v_off_all,~,r_off_all] = gcPresentationInfo(pconv_off,gm,gmv);
        
        %%
        
        % Stim ON and OFF trials
        for t = 1:2
            if t == 1
                ps = pconv_on;
                vs = v_on_all;
                rs = r_on_all;
            else
                ps = pconv_off;
                vs = v_off_all;
                rs = r_off_all;
            end
            for l = 1:length(ps)
                p = ps(l);
                v = vs(l);
                r = rs(l);
                
                % Find stim on convergence
                if t == 1
                    convix = find(gmb.convergences(:,1) == p & conv_g_ix & conv_on_ix);
                else
                    convix = find(gmb.convergences(:,1) == p & conv_g_ix & ~conv_on_ix);
                end
                convix = convix(1); % First convergence with stim on
                convt = gmb.convergences(convix,2); % Convergence time
                convit = findnearest(convt,time,-1); % Convergence iteration
                
                % Stimulus angle
                if t == 1
                    ix = findnearest(convit,gmb.p(p).vis_traj(:,2));
                    ix = ix(1);
                    stang = spotangle_v2(gmb.p(p).vis_traj(ix,3), ...
                        gm.visgeom.sweepamp,gm.visgeom.amax, ...
                        gm.visgeom.R,gm.visgeom.fish2screen);
                else
                    stang = nan;
                end
                
                % Visual epoch for this z
                visp = find(v0 == v & r0 == r);
                
                % Iterations centered on convergence
                itst = nfr*(visp-1)+convit-nfr/2+1;
                ited = nfr*(visp-1)+convit+nfr/2;
                
                % Eye asymmetric delta
                eyead = diff(gmb.convergences(convix,7:8));
                
                % Number of bouts
                nbts = vbtn(p);
                
                if itst > 1 && ited <= roinfr
                    for n = 1:length(zbbix2a)
                        if size(R{n},1) > 1
                            M = R{n}(:,itst:ited); % roi responses
                            nrois2 = size(M,1);
                            
                            ia_peri = any(M(:,periwnd) > zthr,2);
                            ia_pre = any(M(:,prewnd) > zthr,2);
                            nia_peri = sum(ia_peri);
                            nia_pre = sum(ia_pre);
                            Mm_peri = nanmean(M(:,periwnd),2);
                            Mm_pre = nanmean(M(:,prewnd),2);
                            
                            lix = Lat{n} == 1;  % left
                            rix = Lat{n} == 0;  % right
                            nl = sum(lix);   % Number of left rois
                            nr = sum(rix);  % Number of right rois
                            nlia_peri = sum(ia_peri & lix);   % Number of left active cells
                            nlia_pre = sum(ia_pre & lix);   % Number of left active cells
                            nria_peri = sum(ia_peri & rix);   % Number of right active cells
                            nria_pre = sum(ia_pre & rix);   % Number of right active cells
                            lia_frac_peri = nlia_peri./nl;  % Fraction of left active cells
                            lia_frac_pre = nlia_pre./nl;  % Fraction of left active cells
                            ria_frac_peri = nria_peri./nr;  % Fraction of right active cells
                            ria_frac_pre = nria_pre./nr;  % Fraction of right active cells
                            tot_frac_peri = (nlia_peri+nria_peri)./(nl+nr);
                            tot_frac_pre = (nlia_pre+nria_pre)./(nl+nr);
                            asidx_frac_peri = (lia_frac_peri-ria_frac_peri)/(lia_frac_peri+ria_frac_peri);    % peri Asymmetry index
                            asidx_frac_pre = (lia_frac_pre-ria_frac_pre)/(lia_frac_pre+ria_frac_pre);    % pre Asymmetry index
                            
                            lzf_peri = nanmean(Mm_peri(ia_peri & lix));
                            lzf_pre = nanmean(Mm_pre(ia_pre & lix));
                            rzf_peri = nanmean(Mm_peri(ia_peri & rix));
                            rzf_pre = nanmean(Mm_pre(ia_pre & rix));
                            as_zf_peri = (lzf_peri-rzf_peri);
                            as_zf_pre = (lzf_pre-rzf_pre);
                            if sum(ia_peri & lix) > 1 && sum(ia_peri & rix) > 1
                                [~,~,~,stats] = ttest2(Mm_peri(ia_peri & lix),Mm_peri(ia_peri & rix));
                                tstat_peri = stats.tstat;
                            else
                                tstat_peri = nan;
                            end
                            if sum(ia_pre & lix) > 1 && sum(ia_pre & rix) > 1
                                [~,~,~,stats] = ttest2(Mm_pre(ia_pre & lix),Mm_pre(ia_pre & rix));
                                tstat_pre = stats.tstat;
                            else
                                tstat_pre = nan;
                            end
                            
                            % Determine shuffled hit rates
                            itst_s = randi(length(F0)-wndfr,nshuffle);
                            hr_shuff = NaN(nshuffle,3);
                            parfor s = 1:nshuffle
                                itsts = itst_s(s);
                                iteds = itst_s(s)+wndfr-1;
                                Ms = R{n}(:,itsts:iteds); % roi responses                                
                                ias = any(Ms > zthr,2);                                
                                
                                nlias = sum(ias & lix);
                                nrias = sum(ias & rix);   % Number of right active cells
                                lia_fracs = nlias./nl;  % Fraction of left active cells
                                ria_fracs = nrias./nr;  % Fraction of right active cells
                                tot_fracs = (nlias+nrias)./(nl+nr);
                                
                                hr_shuff(s,:) = [lia_fracs ria_fracs tot_fracs];
                            end
                            lia_frac_shuff = nanmean(hr_shuff(:,1),1);
                            ria_frac_shuff = nanmean(hr_shuff(:,2),1);
                            tot_frac_shuff = nanmean(hr_shuff(:,3),1);
                            
                            T = table(f, z, p, v, r, convit, stang, eyead, nbts, ...
                                nl, nr, nlia_peri, nria_peri, lia_frac_peri, ...
                                ria_frac_peri, tot_frac_peri, tot_frac_pre, asidx_frac_peri, ...
                                lzf_peri, rzf_peri, as_zf_peri, tstat_peri, ...
                                nlia_pre, nria_pre, lia_frac_pre, ...
                                ria_frac_pre, asidx_frac_pre, ...
                                lzf_pre, rzf_pre, as_zf_pre, tstat_pre, ...
                                lia_frac_shuff, ria_frac_shuff, tot_frac_shuff);
                            X{n,t} = cat(1,X{n,t},T);
                        end
                    end
                end
            end
        end
    end
end
close(h)

%%
savefigs = 0;

load('\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\analysis\Convergence\conv_anat_stats_180705.mat')

%%

zbbix = 77;
T = X{zbbix,1};

if savefigs
    savedir = fullfile('\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\analysis\Convergence\figures\Activation_asymmetry', ...
        ZBBMaskDatabaseNames{zbbix});
    if ~exist(savedir,'dir')
        mkdir(savedir)
    end
end

x = T.stang;

y = T.asidx_frac_peri;
lab = 'Anatomic asymmetric index (Peri)';
fig = figure;
scatterhist(x,y,'Kernel','on')
xlabel('Stimulus angle')
ylabel(lab)
ix = ~isnan(x) & ~isnan(y);
[rho,pv] = corr(x(ix),y(ix));
title(sprintf('Rho = %0.2f; p = %0.2f',rho,pv))
set(gca,'Box','off','TickDir','out')
if savefigs
    savefig(fig,fullfile(savedir,lab))
end

y = T.asidx_frac_pre;
lab = 'Anatomic asymmetric index (Pre)';
fig = figure;
scatterhist(x,y,'Kernel','on')
xlabel('Stimulus angle')
ylabel(lab)
ix = ~isnan(x) & ~isnan(y);
[rho,pv] = corr(x(ix),y(ix));
title(sprintf('Rho = %0.2f; p = %0.2f',rho,pv))
set(gca,'Box','off','TickDir','out')
if savefigs
    savefig(fig,fullfile(savedir,lab))
end

y = T.as_zf_peri;
lab = 'Anatomic delta zF (Peri)';
fig = figure;
scatterhist(x,y,'Kernel','on')
xlabel('Stimulus angle')
ylabel(lab)
ix = ~isnan(x) & ~isnan(y);
[rho,pv] = corr(x(ix),y(ix));
title(sprintf('Rho = %0.2f; p = %0.2f',rho,pv))
set(gca,'Box','off','TickDir','out')
if savefigs
    savefig(fig,fullfile(savedir,lab))
end

y = T.as_zf_pre;
lab = 'Anatomic delta zF (Pre)';
fig = figure;
scatterhist(x,y,'Kernel','on')
xlabel('Stimulus angle')
ylabel(lab)
ix = ~isnan(x) & ~isnan(y);
[rho,pv] = corr(x(ix),y(ix));
title(sprintf('Rho = %0.2f; p = %0.2f',rho,pv))
set(gca,'Box','off','TickDir','out')
if savefigs
    savefig(fig,fullfile(savedir,lab))
end

%%

msk2plt = [11 28 33 74 76 77];
nmsk = length(msk2plt);
tlabs = {'Peri convergence','Pre convergence'};

M = NaN(nmsk,2,2,2);
for n = 1:nmsk
    for i = 1:2
        T = X{msk2plt(n),i};
        if size(T,1) > 20
            M(n,i,1,1) = nanmean((T.nlia_peri+T.nria_peri)./(T.nl+T.nr));
            M(n,i,1,2) = nanstd((T.nlia_peri+T.nria_peri)./(T.nl+T.nr));
            M(n,i,2,1) = nanmean((T.nlia_pre+T.nria_pre)./(T.nl+T.nr));
            M(n,i,2,2) = nanstd((T.nlia_pre+T.nria_pre)./(T.nl+T.nr));
        end
    end
end

for i = 1:2
    [srt,six] = sort(M(:,1,i,1),'descend','MissingPlacement','last');
    six = six(~isnan(srt));
    
    errorbar_groups_ph(M(six,:,i,1)',M(six,:,i,2)','bar_width',0.7);
    set(gca,'XTickLabel',ZBBMaskDatabaseNames(msk2plt(six)),'XTickLabelRotation',45, ...
        'Box','off','TickDir','out')
    ylabel('Fraction of active cells')
    ylim([0 0.25])
    title(tlabs(i))
    legend({'Stim ON','Spontaneous'})
end

%% 


M = NaN(nmsk,2,2);
for n = 1:nmsk
    for i = 1:2
        T = X{msk2plt(n),i};
        if size(T,1) > 20
            M(n,i,1) = nanmean(T.nl+T.nr);
            M(n,i,2) = nanstd(T.nl+T.nr);            
        end
    end
end