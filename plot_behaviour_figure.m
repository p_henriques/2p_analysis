% Plot behaviour figure for paper figure 6
datadir = 'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180427\f2';

load(fullfile(datadir,'gm'),'gm')
load(fullfile(datadir,'gmbf'),'gmbf')
load(fullfile(datadir,'gmb'),'gmb')
load(fullfile(datadir,'gmbt'),'gmbt')

%%

p = 93;
ft_width = 3;   % tail filter
fe_width = 3;   % eye filter

% Tail at
ttime = gmbt.p(p).tt;
T = gmbt.p(p).cumtail;
Tf = filtfilt(ones(1, ft_width)./ft_width, 1, T);
% Eyes at
etime = gmb.p(p).tt;
Er = gmb.p(p).Rangles;
El = gmb.p(p).Langles;
Elf = filtfilt(ones(1, fe_width)./fe_width, 1, El);
Erf = filtfilt(ones(1, fe_width)./fe_width, 1, Er);
Evf = Elf-Erf;

ST = gmb.p(p).vis_traj(:,[2:3,7]);
ST = ST(abs(ST(:,2)) < 999,:);  % Remove bogus values
vitst = ST(1,1);
vited = ST(end,1);
sang = -spotangle_v2(ST(:,2), ...
    gm.visgeom.sweepamp,gm.visgeom.amax, ...
    gm.visgeom.R,gm.visgeom.fish2screen);

figure
plot(ttime,Tf,'g')
hold on
plot(etime,Elf,'b')
plot(etime,Erf,'r')
plot(etime,Evf,'k')
ylim([-120 120])
ylabel('Angle')
plot(ST(:,3),sang,'Color',[.6 .6 .6])
axis([ST(1,3) ST(end,3) -75 75])
xlabel('Epoch time (s)')
set(gca,'Box','off','TickDir','out')