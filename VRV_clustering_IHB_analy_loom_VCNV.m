% Calculate visually responsive fractuon of cells for given ZBB id based
% on VCNV method

load('\\128.40.168.141\data\Pedro\2P\NI\NI_Optoma\HuC_H2B_GC6s\Analysis\Clustering\IHBclust\mf.mat');
load('\\128.40.168.141\data\Pedro\2P\NI\NI_Optoma\HuC_H2B_GC6s\Analysis\Clustering\loom_clust_kmeans_180909_IHB.mat');

zbbanatix = [2:3 6:34 44:46 48:74 77 113 305:306];
load('ZBBMaskDatabase','ZBBMaskDatabaseNames');
ZBBMaskDatabaseNames = ZBBMaskDatabaseNames(zbbanatix);

zbbmsk = gmclt.zbbmsk;

ia = [];
for i = 1:length(mf.datasetz)
    load(fullfile(mf.datasetz{i},'gmVCNV.mat'))
    ia = cat(1,ia,gmVCNV.ROIinfo(:,4));
end

%%

zbbix = [62,64,65];
ix = any(zbbmsk(:,zbbix),2);
iaf = sum(ia(ix))/sum(ix);