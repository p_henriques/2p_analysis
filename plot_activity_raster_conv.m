
imrange = [0 40];
ffiltwidth = 5; % Calcium fluorescence filter
filter1 = ones(1,ffiltwidth)./ffiltwidth;
zthr = 2;   % z threshold from which to consider a cell active
zthr2 = 1;
savefigs = 1;

zbbanatix = [1:3 5:74 77 113 305:306];
load('ZBBMaskDatabase','ZBBMaskDatabaseNames');
ZBBMaskDatabaseNames = ZBBMaskDatabaseNames(zbbanatix);

for f = 1:length(folders)
    datadir = folders{f};
    
    % Load structures
    load(fullfile(datadir,'gmranat'));
    load(fullfile(datadir,'gmrxanat'))
    load(fullfile(datadir,'gm'))
    load(fullfile(datadir,'gmv'))
    load(fullfile(datadir,'gmb'))
    load(fullfile(datadir,'gmbt'))
    load(fullfile(datadir,'gmbf'))
    
    %%
    
    % Get stimulus times
    nfr = gm.nfr;
    frtime = gm.frtime/1000;
    time = 0:frtime:frtime*(nfr-1);
    
    ne_t = size(gmv.visstim,1);
    vST = gm.trfr + 2;
    vED = gm.trfr + 1 + ceil(1000.*gmv.vistypz(:,5)./gm.frtime);
    vDurMx = max(vED-vST);  % Maximum visual stimulus duration
    vSTt = (gm.trfr+1)*gm.frtime./1000;
    vttime = gmv.visstim(:,6);
    vEDst = vSTt + vttime;
    
    vbtix = ([gmbf.b.Vergbout] == 1 | [gmbf.b.Convbout] == 1) & ...
        [gmbf.b.st] < vEDst([gmbf.b.p])' & ...
        [gmbf.b.st] > vSTt;
    
    vbtn = histcounts([gmbf.b(vbtix).p],0.5:ne_t+0.5);
    
    % Good convergences
    conv_g_ix = gmb.convergences(:,3) == 1;
    % Stim on convergences
    conv_on_ix = gmb.convergences(:,2) >= vSTt & ...
        gmb.convergences(:,2) <= vEDst(gmb.convergences(:,1));
    
    % Concatenate ZBB masks for all rois
    nzs = size(gmranat.z,2);
    zbbmsk = [];
    roisC = [];
    roiszbbC = [];
    for z = 1:nzs
        zbbmsk = cat(1,zbbmsk, ...
            cat(1,gmranat.z(z).STATScrop.Masks_ZBB));
        roisC = cat(1,roisC, ...
            cat(1,gmranat.z(z).STATScrop.Centroid));
        roiszbbC = cat(1,roiszbbC, ...
            cat(1,gmranat.z(z).STATScrop.Centroid_ZBB));
    end
    zbbmsk = logical(zbbmsk(:,zbbanatix));
    
    for zbbix = 1:length(ZBBMaskDatabaseNames)
        rois2a = zbbmsk(:,zbbix) == 1;
        nrois = sum(rois2a);
        if nrois >= 10
            rois2a_zs = unique([gmrxanat.roi(rois2a).z]);
            
            %%  Run the loop through all stim on convergences
            
            for i = 1:length(rois2a_zs)
                z = rois2a_zs(i);
                
                % Epochs with spot presentations
                se = intersect(gm.zindices(z).e, ...
                    find(gmv.visstim(:,1) > 77000 & gmv.visstim(:,1) < 82000));
                rois = find([gmrxanat.roi.z]' == z & rois2a);
                nrois = length(rois);
                
                % Get roi responses
                [v0,~,r0] = gcPresentationInfo(gm.zindices(z).e,gm,gmv);
                roinfr = length(v0)*gm.nfr;
                F = NaN(nrois,roinfr);
                for j = 1:nrois
                    roi = rois(j);
                    F0 = [];
                    for k = 1:length(v0)
                        F0 = cat(1,F0,gmrxanat.roi(roi).Vprofiles(v0(k)).zProfiles(r0(k),:)');
                    end
                    F0f = filtfilt(filter1,1,F0);
                    F0fz = zscore(F0f);
                    F(j,:) = F0fz;
                end
                
                % Eyes
                E = cat(1,gmb.p(gm.zindices(z).e).resampled);
                % Tail
                T = cat(1,gmbt.p(gm.zindices(z).e).resampled);
                
                % Stim on convergences
                pconv_on = intersect(se,gmb.convergences(conv_g_ix & conv_on_ix,1));
                [v_on_all,~,r_on_all] = gcPresentationInfo(pconv_on,gm,gmv);
                % Stim off convergences
                pconv_off = intersect(se,gmb.convergences(conv_g_ix & ~conv_on_ix,1));
                [v_off_all,~,r_off_all] = gcPresentationInfo(pconv_off,gm,gmv);
                % No convergence
                npconv = setdiff(se,gmb.convergences(:,1));
                [v_n_all,~,r_n_all] = gcPresentationInfo(npconv,gm,gmv);
                
                %%
                
                for l = 1:length(pconv_on)
                    p_on = pconv_on(l);
                    v_on = v_on_all(l);
                    r_on = r_on_all(l);
                    
                    % Find a close stim off epoch of the same vstim
                    p_off_v = pconv_off(v_off_all == v_on);
                    p_off_vix = findnearest(p_on,p_off_v,0);
                    if ~isempty(p_off_vix)
                        p_off_vix = p_off_vix(1);
                        p_off = p_off_v(p_off_vix);
                        if p_off == p_on
                            if p_off_vix ~= 1
                                p_off = p_off_v(p_off_vix-1);
                            elseif p_off_vix < length(p_off_v)
                                p_off = p_off_v(p_off_vix+1);
                            end
                        end
                        [~,~,r_off] = gcPresentationInfo(p_off,gm,gmv);
                        
                        % Find a close no convergence epoch of the same vstim
                        p_n_v = npconv(v_n_all == v_on);
                        p_n_vix = findnearest(p_on,p_n_v,0);
                        if ~isempty(p_n_vix)
                            p_n_vix = p_n_vix(1);
                            p_n = p_n_v(p_n_vix);
                            [~,~,r_n] = gcPresentationInfo(p_n,gm,gmv);
                            
                            % Find stim on convergence
                            convix_on = find(gmb.convergences(:,1) == p_on & conv_g_ix & conv_on_ix);
                            convix_on = convix_on(1); % First convergence with stim on
                            convt_on = gmb.convergences(convix_on,2); % Convergence time
                            convit_on = findnearest(convt_on,time,-1); % Convergence iteration
                            % Find stim off convergence
                            convix_off = find(gmb.convergences(:,1) == p_off & conv_g_ix & ~conv_on_ix);
                            convix_off = convix_off(1); % First convergence with stim off
                            convt_off = gmb.convergences(convix_off,2); % Convergence time
                            convit_off = findnearest(convt_off,time,-1); % Convergence iteration
                            
                            % Visual epoch for this z
                            visp_on = find(v0 == v_on & r0 == r_on);
                            visp_off = find(v0 == v_on & r0 == r_off);
                            visp_n = find(v0 == v_on & r0 == r_n);
                            % Iterations centered on convergence
                            itst_on = nfr*(visp_on-1)+convit_on-nfr/2+1;
                            ited_on = nfr*(visp_on-1)+convit_on+nfr/2;
                            itst_off = nfr*(visp_off-1)+convit_off-nfr/2+1;
                            ited_off = nfr*(visp_off-1)+convit_off+nfr/2;
                            itst_n = nfr*(visp_n-1)+1;
                            ited_n = nfr*(visp_n);
                            
                            if itst_on > 1 && ited_on <= roinfr && ...
                                    itst_off > 1 && ited_off <= roinfr && ...
                                    itst_n > 1 && ited_n <= roinfr
                                
                                
                                M_on = F(:,itst_on:ited_on); % roi responses
                                [M_on,exix_on] = excisenansumrows(M_on,size(M_on,2)/2);
                                nrois2 = size(M_on,1);
                                rois2 = rois(~exix_on);                                
                                M_off = F(:,itst_off:ited_off); % roi responses
                                [M_off,exix_off] = excisenansumrows(M_off,size(M_off,2)/2);
                                M_n = F(:,itst_n:ited_n); % roi responses
                                [M_n,exix_n] = excisenansumrows(M_n,size(M_n,2)/2);
                                
                                % Sort Ca responses
                                mix = NaN(nrois2,1);
                                for m = 1:nrois2
                                    % Find instances where rois crosses
                                    % activity threshold
                                    ixx = find(abs(M_on(m,:)) > zthr);
                                    ixx2 = find(abs(M_on(m,:)) < zthr2);
                                    if ~isempty(ixx)
                                        % Are these after convergence?                                        
                                        if any(ixx >= nfr/2)
                                            cixx = find(ixx >= nfr/2);
                                            % In first it where roi crosses
                                            % thr, go back to see where
                                            % goes back to baseline
                                            baseix = findnearest(ixx(cixx(1)),ixx2,-1);
                                            if ~isempty(baseix)
                                                mix(m) = ixx2(baseix);
                                            else
                                                mix(m) = ixx(cixx(1));
                                            end
                                        elseif any(ixx < nfr/2)
                                            cixx = find(ixx < nfr/2);
                                            baseix = findnearest(ixx(cixx(end)),ixx2,-1);
                                            if ~isempty(baseix)
                                                mix(m) = ixx2(baseix);
                                            else
                                                mix(m) = ixx(end);
                                            end
                                        else
                                            mix(m) = 1;
                                        end                                        
                                    else
                                        mix(m) = 1;
                                    end
                                end
                                
                                [mixsv,mixs] = sort(mix,'ascend');
                                mixs_pre_actv = find(mixsv > 1);
                                if ~isempty(mixs_pre_actv)
                                    mixs_pre_actv = mixs_pre_actv(1)-0.5;
                                end
                                mixs_post_actv = find(mixsv >= nfr/2);
                                if ~isempty(mixs_post_actv)
                                    mixs_post_actv = mixs_post_actv(1)-0.5;
                                end
                                
                                % Get epoch times
                                its_on = T(itst_on:ited_on,1);
                                its_off = T(itst_off:ited_off,1);
                                itspreix_on = find(diff(its_on(1:nfr/2)) < 1);
                                itspreix_off = find(diff(its_off(1:nfr/2)) < 1);
                                if ~isempty(itspreix_on)
                                    its_on(1:itspreix_on) = -flip(1:itspreix_on);
                                end
                                if ~isempty(itspreix_off)
                                    its_off(1:itspreix_off) = -flip(1:itspreix_off);
                                end
                                itspostix_on = find(diff(its_on(nfr/2:end)) < 1);
                                itspostix_off = find(diff(its_off(nfr/2:end)) < 1);
                                if ~isempty(itspostix_on)
                                    its_on(itspostix_on+nfr/2:end) = its_on(itspostix_on+nfr/2:end)+its_on(itspostix_on+nfr/2-1)+1;
                                end
                                if ~isempty(itspostix_off)
                                    its_off(itspostix_off+nfr/2:end) = its_off(itspostix_off+nfr/2:end)+its_off(itspostix_off+nfr/2-1)+1;
                                end
                                ptime_on = its_on.*frtime;
                                ptime_off = its_off.*frtime;
                                
                                % Stimulus trajectory stim on
                                ST_on = gmb.p(p_on).vis_traj(:,[2:3,7]);
                                ST_on = ST_on(abs(ST_on(:,2)) < 999,:);  % Remove bogus values
                                vitst_on = find(its_on == ST_on(1,1));
                                vited_on = find(its_on == ST_on(end,1));
                                sang_on = -spotangle_v2(ST_on(:,2), ...
                                    gm.visgeom.sweepamp,gm.visgeom.amax, ...
                                    gm.visgeom.R,gm.visgeom.fish2screen);   % Transform to angles
                                % Stimulus trajectory stim off
                                ST_off = gmb.p(p_off).vis_traj(:,[2:3,7]);
                                ST_off = ST_off(abs(ST_off(:,2)) < 999,:);  % Remove bogus values
                                vitst_off = find(its_off == ST_off(1,1));
                                vited_off = find(its_off == ST_off(end,1));
                                sang_off = -spotangle_v2(ST_off(:,2), ...
                                    gm.visgeom.sweepamp,gm.visgeom.amax, ...
                                    gm.visgeom.R,gm.visgeom.fish2screen);   % Transform to angles
                                % Stimulus trajectory no convergence
                                ST_n = gmb.p(p_n).vis_traj(:,[2:3,7]);
                                ST_n = ST_n(abs(ST_n(:,2)) < 999,:);  % Remove bogus values
                                vitst_n = ST_n(1,1);
                                vited_n = ST_n(end,1);
                                sang_n = -spotangle_v2(ST_n(:,2), ...
                                    gm.visgeom.sweepamp,gm.visgeom.amax, ...
                                    gm.visgeom.R,gm.visgeom.fish2screen);   % Transform to angles
                                
                                %%  Plot responses
                                
                                fig = figure('name',sprintf('Fish = %d; Z = %d; p = %d; %s', ...
                                    f,z,p_on,ZBBMaskDatabaseNames{zbbix}), ...
                                    'position',[274,144,1345,797]);
                                % Plot Stim on responses
                                subplot(2,3,1)
                                imagesc(M_on(mixs,:),[-5 5])
                                colormap(flip(brewermap(255,'RdBu'),1));
                                line([nfr/2 nfr/2],[0 size(M_on,1)+1],'Color','k','LineWidth',2)
                                line([vitst_on vitst_on],[0 size(M_on,1)+1], ...
                                    'Color','b','LineWidth',1,'LineStyle','--')
                                line([vited_on vited_on],[0 size(M_on,1)+1], ...
                                    'Color','r','LineWidth',1,'LineStyle','--')
                                if ~isempty(mixs_pre_actv)
                                    line([0 nfr],[mixs_pre_actv mixs_pre_actv], ...
                                        'Color','k','LineWidth',1,'LineStyle','--')
                                    text(5,mean(find(mixsv > 1 & mixsv < nfr/2)),'Pre active')
                                end
                                if ~isempty(mixs_post_actv)
                                    line([0 nfr],[mixs_post_actv mixs_post_actv], ...
                                        'Color','k','LineWidth',1,'LineStyle','--')
                                    text(5,mean(find(mixsv >= nfr/2)),'Post active')
                                end
                                if any(mixsv == 1)
                                    text(5,mean(find(mixsv == 1)),'Not active')
                                end
                                title(sprintf('Stim-ON Convergece (p = %d)',p_on))
                                
                                subplot(2,3,4)
                                plot(ptime_on,E(itst_on:ited_on,2:3))
                                hold on
                                plot(ptime_on,E(itst_on:ited_on,4),'k')
                                plot(ptime_on,T(itst_on:ited_on,2))
                                axis([ptime_on(1) ptime_on(end) -75 75])
                                plot(ST_on(:,3),sang_on,'Color',[.6 .6 .6])
                                xlabel('Epoch time (s)')
                                title(num2str(gmv.vistypz(v_on,1:4)))
                                
                                % Plot Stim off responses
                                subplot(2,3,2)
                                imagesc(M_off(mixs,:),[-5 5])
                                colormap(flip(brewermap(255,'RdBu'),1));
                                line([nfr/2 nfr/2],[0 size(M_off,1)+1],'Color','k','LineWidth',2)
                                if ~isempty(mixs_pre_actv)
                                    line([0 nfr],[mixs_pre_actv mixs_pre_actv], ...
                                        'Color','k','LineWidth',1,'LineStyle','--')
                                end
                                if ~isempty(mixs_post_actv)
                                    line([0 nfr],[mixs_post_actv mixs_post_actv], ...
                                        'Color','k','LineWidth',1,'LineStyle','--')
                                end
                                title(sprintf('Stim-OFF Convergece (p = %d)',p_off))
                                
                                subplot(2,3,5)
                                plot(ptime_off,E(itst_off:ited_off,2:3))
                                hold on
                                plot(ptime_off,E(itst_off:ited_off,4),'k')
                                plot(ptime_off,T(itst_off:ited_off,2))
                                axis([ptime_off(1) ptime_off(end) -75 75])
                                plot(ST_off(:,3),sang_off,'Color',[.6 .6 .6])
                                xlabel('Epoch time (s)')
                                title(num2str(gmv.vistypz(v_on,1:4)))
                                
                                % Plot no convergence responses
                                subplot(2,3,3)
                                imagesc(M_n(mixs,:),[-5 5])
                                colormap(flip(brewermap(255,'RdBu'),1));
                                line([vitst_n vitst_n],[0 size(M_n,1)+1], ...
                                    'Color','b','LineWidth',1,'LineStyle','--')
                                line([vited_n vited_n],[0 size(M_n,1)+1], ...
                                    'Color','r','LineWidth',1,'LineStyle','--')
                                if ~isempty(mixs_pre_actv)
                                    line([0 nfr],[mixs_pre_actv mixs_pre_actv], ...
                                        'Color','k','LineWidth',1,'LineStyle','--')
                                end
                                if ~isempty(mixs_post_actv)
                                    line([0 nfr],[mixs_post_actv mixs_post_actv], ...
                                        'Color','k','LineWidth',1,'LineStyle','--')
                                end
                                title(sprintf('No Convergece (p = %d)',p_n))
                                
                                subplot(2,3,6)
                                plot(time,E(itst_n:ited_n,2:3))
                                hold on
                                plot(time,E(itst_n:ited_n,4),'k')
                                plot(time,T(itst_n:ited_n,2))
                                axis([time(1) time(end) -75 75])
                                plot(ST_n(:,3),sang_n,'Color',[.6 .6 .6])
                                xlabel('Epoch time (s)')
                                title(num2str(gmv.vistypz(v_on,1:4)))
                                
                                % Save figure
                                if savefigs
                                    if ~isdir(fullfile(datadir,'figures', ...
                                            'Convergence','Conv_trig_resp',ZBBMaskDatabaseNames{zbbix}))
                                        mkdir(fullfile(datadir,'figures', ...
                                            'Convergence','Conv_trig_resp',ZBBMaskDatabaseNames{zbbix}));
                                    end
                                    savefig(fig,fullfile(datadir,'figures', ...
                                        'Convergence','Conv_trig_resp',ZBBMaskDatabaseNames{zbbix}, ...
                                        sprintf('p_%d_z_%d_',p_on,z)));
                                    close(fig);
                                end
                                
                                % Plot cell locations
                                
                                C = roisC(rois2,:);
                                cix = mix >= vitst_on & mix <= vited_on;
                                if any(cix)
                                    fig = figure('name',sprintf('Fish = %d; Z = %d; p = %d; %s', ...
                                    f,z,p_on,ZBBMaskDatabaseNames{zbbix}), ...
                                    'position',[563,230,699,663]);
                                    imagesc(gm.zimg(z).aligngood2,imrange)
                                    axis('off','square')
                                    colormap(gca,'gray');
                                    hold on
                                    newcolorbar
                                    scatter(C(cix,1),C(cix,2), ...
                                        [],mix(cix)-nfr/2,'filled', ...
                                        'MarkerEdgeColor','k')
                                    axis('off','square')
                                    colormap(flip(brewermap(255,'RdBu'),1));
                                    set(gca,'YDir','reverse')
                                    caxis([-max(abs(mix(cix)-nfr/2)) ...
                                        max(abs(mix(cix)-nfr/2))])
                                    
                                    if savefigs
                                        savefig(fig,fullfile(datadir,'figures', ...
                                            'Convergence','Conv_trig_resp',ZBBMaskDatabaseNames{zbbix}, ...
                                            sprintf('p_%d_z_%d_map_',p_on,z)));
                                        close(fig)
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end
