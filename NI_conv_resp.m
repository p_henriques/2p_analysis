datadir = 'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180301\f1';

%% Load structures

load(fullfile(datadir,'gm'),'gm');
load(fullfile(datadir,'gmv'),'gmv');
load(fullfile(datadir,'gmb'),'gmb');
load(fullfile(datadir,'gmbt'),'gmbt');
load(fullfile(datadir,'gmbf'),'gmbf');
load(fullfile(datadir,'gmpc'),'gmpc');
load(fullfile(datadir,'gmranat'),'gmranat');
if exist(fullfile(datadir,'gmrxanat4.mat'),'file')
    quadscan = 1;
    load(fullfile(datadir,'gmrxanat4'),'gmrxanat4');
    gmrxanat = gmrxanat4; clear gmrxanat4;
else
    quadscan = 0;
    load(fullfile(datadir,'gmrxanat'),'gmrxanat');
end
load(fullfile(datadir,'gmVCNV'),'gmVCNV');

nvis = size(gmv.vistypz,1);

%% Get ROIs to analyse

% Get OT rois
[otrois,~] = gcROIextractZBBIdxMask(datadir, ...
    'optic tectum - stratum periventriculare', ...
    'Masks_ZBB');

% Get NI rois
[nirois,~] = gcROIextractZBBIdxMask(datadir, ...
    'nucleus isthmus - chata positive', ...
    'Masks_ZBB');

% Anterior NI from AF7 DiI
[anirois,~] = gcROIextractZBBIdxMask(datadir, ...
    'nucleus isthmus - anterior - AF7_DiI', ...
    'Masks_ZBB');

[leftrois,~] = gcROIextractZBBIdxMask(datadir, ...
    'Left side','Masks_ZBB');

[rightrois,~] = gcROIextractZBBIdxMask(datadir, ...
    'Right side','Masks_ZBB');

%%  Sanity check

gcROIplotcentroids(catpad(2,otrois,nirois,anirois),[],gm,gmranat);

%% Choose which rois to analyse

rois2a = unique(cat(1,nirois,anirois));  % Which rois to analyse
    
%% Visstim to analyse

stims = [
    110 0 0 0;
    110 2 0 0;
    80006 0 5 30;
    80007 0 5 30;
    81006 0 5 30;
    81007 0 5 30;
    ];

vis = find(ismember(gmv.vistypz(:,1:4),stims,'rows'));
vstypes = vis(3:end);

%% Convergence epochs

% Number of convergences
nconv = size(gmb.convergences,1);

sp = [];
for v = vstypes
    % Stimulus epochs
    sp = cat(1,sp, ...
        find(ismember(gmv.visstim(:,1:5),gmv.vistypz(v,1:5),'rows')));
end

% Good convergences
convix = gmb.convergences(:,3) == 1;
pconv = intersect(sp,gmb.convergences(convix,1));

% No convergence epochs
npconv = setdiff(sp,pconv);

% Convergences in stim on time
convonix = false(nconv,1);
for i = 1:nconv
    e = gmb.convergences(i,1);
    stim = gmv.visstim(e,1:5);
    [~, ~, vSTt, vEDt] = gcStimInfo(stim, gm.trfr, gm.frtime, 0);
    if gmb.convergences(i,2) >= vSTt && gmb.convergences(i,2) <= vEDt
        convonix(i) = 1;
    end
end
pconvon = intersect(sp,gmb.convergences(convix & convonix,1));

% Convergences outside stim on time
pnconvon = intersect(sp,gmb.convergences(convix & ~convonix,1));

% Multiple bout stimulus response epochs
msbtp = unique([gmbf.b([gmbf.b.stimbout] == 2).p]);

% One stimulus response epochs
osbtp = setdiff(unique([gmbf.b([gmbf.b.stimbout] == 1).p]),msbtp);

% Convergence epochs with multiple responses
msbtcp = intersect(pconvon,msbtp);

% Convergence epochs with one response
osbtcp = intersect(pconvon,osbtp);

% Get v,z and rep indexes for presentations
[npconv_v,npconv_z,npconv_rep] = gcPresentationInfo(npconv,gm,gmv);
[pnconvon_v,pnconvon_z,pnconvon_rep] = gcPresentationInfo(pnconvon,gm,gmv);
[pconvon_v,pconvon_z,pconvon_rep] = gcPresentationInfo(pconvon,gm,gmv);
[osbtcp_v,osbtcp_z,osbtcp_rep] = gcPresentationInfo(osbtcp,gm,gmv);
[msbtcp_v,msbtcp_z,msbtcp_rep] = gcPresentationInfo(msbtcp,gm,gmv);

% Stim position
pconvon_ang = NaN(length(pconvon),1);
for i = 1:length(pconvon)
    e = pconvon(i);
    cix = find(ismember(gmb.convergences(:,1),e) & convix & convonix);
    ct = gmb.convergences(cix,2);
    ct = ct(1); % Use position of first convergence during stim on
    vix = findnearest(ct,gmb.p(e).vis_traj(:,7));
    stpx = gmb.p(e).vis_traj(vix,3);
    if gmv.visstim(e,1) >= 79000 && gmv.visstim(e,1) <= 82000
        pconvon_ang(i) = spotangle_v2(stpx, ...
            gm.visgeom.sweepamp,gm.visgeom.amax, ...
            gm.visgeom.R,gm.visgeom.fish2screen);
    end
end

%% Plot Ca2+ Stim-On/Off responses for individual ROIs

filttype = 2;   % 0 - no filter; 1 - sgolay; 2 - zero-phase

filtsz = 3;
if quadscan
    filtsz = filtsz*4;
    if mod(filtsz,2) == 0
        filtsz = filtsz+1;
    end
end

nfr = gmrxanat.nfr;
nrois = length(rois2a);
ne = length(pconvon);
x = linspace(0,gm.nfr*gm.frtime/1000,gmrxanat.nfr);
x2 = (-nfr/2)+1:nfr/2;

for i = 1:nrois
    roi = rois2a(i);
    X = cat(1,gmrxanat.roi(roi).Vprofiles.zProfiles)';
    if any(isnan(X(:)))
        disp('here')
    end
    xbase = nanmean(X(:));
    xstd = nanstd(X(:));
    Xz = (X-xbase)./xstd;
    if filttype ~= 0
        if filttype == 1
            Xzf = sgolayfilt(double(Xz(:)),2,filtsz);
        elseif filttype == 2
            Xzf = filtfilt(ones(filtsz,1)./filtsz,1,Xz(:));
        end
        Xzf = reshape(Xzf,size(Xz));
    else
        Xzf = Xz;
    end
    
    ymin = prctile(Xzf(:),0.01)*1.1;
    ymax = prctile(Xzf(:),99.99)*1.1;
    z = gmrxanat.roi(roi).z;
    
    % Plot visstim responses
    figure('name',sprintf('ROI = %d',roi), ...
        'position',[442,362,1020,420])
    for v = 1:size(stims,1)
        X = gmrxanat.roi(roi).Vprofiles(vis(v)).zProfiles;
        Xz = (X-xbase)./xstd;
        if filttype ~= 0
            if filttype == 1
                Xzf = sgolayfilt(double(Xz(:)),2,filtsz);
            elseif filttype == 2
                Xzf = filtfilt(ones(filtsz,1)./filtsz,1,Xz(:));
            end
            Xzf = reshape(Xzf,size(Xz));
        else
            Xzf = Xz;
        end
        nreps = size(X,1);
        onreps = pconvon_rep(pconvon_z == z & pconvon_v == vis(v));
        offreps = pnconvon_rep(pnconvon_z == z & pnconvon_v == vis(v));
        noreps = setdiff(1:nreps,[onreps;offreps]);
        
        [vST,vED,~,~] = gcStimInfo(gmv.vistypz(vis(v),1:5), gm.trfr, gm.frtime, 0);
        
        subplot(2,4,v)
        plot(Xzf(noreps,:)','Color',[0.7 0.7 0.7])
        hold on
        plot(Xzf(offreps,:)','Color','r')
        plot(Xzf(onreps,:)','Color','b')
        plot(nanmean(Xzf),'k')
        line([vST vST],[ymin ymax],'LineStyle','--','Color','k')
        line([vED vED],[ymin ymax],'LineStyle','--','Color','k')
        axis([0 nfr ymin ymax]);
        title(mat2str(stims(v,:)))
        axis square
    end
    
    % Plot convergence aligned responses
    % Spontaneous
    zix = find(pnconvon_z == z);
    CaX = NaN(length(zix),nfr);
    CaC = NaN(length(zix),nfr);
    for j = 1:length(zix)
        zixx = zix(j);
        v = pnconvon_v(zixx);
        r = pnconvon_rep(zixx);
        e = pnconvon(zixx);
        
        cix = find(ismember(gmb.convergences(:,1),e) & convix & ~convonix);
        ct = gmb.convergences(cix,2);
        ct = ct(1);
        ctix = findnearest(ct,x);
        ixx = nfr*(r-1)+ctix-(nfr/2)+1:nfr*(r-1)+ctix+(nfr/2);
        
        X = gmrxanat.roi(roi).Vprofiles(v).zProfiles';
        Xz = (X-xbase)./xstd;
        if filttype ~= 0
            if filttype == 1
                Xzf = sgolayfilt(double(Xz(:)),2,filtsz);
            elseif filttype == 2
                Xzf = filtfilt(ones(filtsz,1)./filtsz,1,Xz(:));
            end
            Xzf = reshape(Xzf,size(Xz));
        else
            Xzf = Xz;
        end
        ca = Xzf(:,r);
        
        ixxx = ixx > 1 & ixx <= numel(Xzf);
        
        CaX(j,:) = ca;
        CaC(j,ixxx) = Xzf(ixx(ixxx));
    end
    subplot(2,4,7)
    p1 = shadedErrorBar(x2,nanmean(CaC,1),nansem(CaC,1),'-r',1);
    hold on
%     plot(x2,CaC,'-r');
%     plot(x2,nanmean(CaC),'-r','LineWidth',3)
    
    % Stim-on
    zix = find(pconvon_z == z);
    CaX = NaN(length(zix),nfr);
    CaC = NaN(length(zix),nfr);
    for j = 1:length(zix)
        zixx = zix(j);
        v = pconvon_v(zixx);
        r = pconvon_rep(zixx);
        e = pconvon(zixx);
        
        cix = find(ismember(gmb.convergences(:,1),e) & convix & convonix);
        ct = gmb.convergences(cix,2);
        ct = ct(1);
        ctix = findnearest(ct,x);
        ixx = nfr*(r-1)+ctix-(nfr/2)+1:nfr*(r-1)+ctix+(nfr/2);
        
        X = gmrxanat.roi(roi).Vprofiles(v).zProfiles';
        Xz = (X-xbase)./xstd;
        if filttype ~= 0
            if filttype == 1
                Xzf = sgolayfilt(double(Xz(:)),2,filtsz);
            elseif filttype == 2
                Xzf = filtfilt(ones(filtsz,1)./filtsz,1,Xz(:));
            end
            Xzf = reshape(Xzf,size(Xz));
        else
            Xzf = Xz;
        end
        ca = Xzf(:,r);
        
        ixxx = ixx > 1 & ixx <= numel(Xzf);
        
        CaX(j,:) = ca;
        CaC(j,ixxx) = Xzf(ixx(ixxx));
    end
    p2 = shadedErrorBar(x2,nanmean(CaC,1),nansem(CaC,1),'-b',1);
%     plot(x2,CaC,'-b'); hold on
%     plot(x2,nanmean(CaC),'-b','LineWidth',3)
    axis square
    axis([x2(1) x2(end) ymin ymax]);
    line([0 0],[ymin ymax],'LineStyle','--','Color','k')
    title('Conv-aligned')
end

%% Mean Convergence dFFm maps

drmin = 0.25;
drmax = 2;
min_anat = 0;
max_anat = 20;
labels = {'NO-GO trials', ...
    'GO trials - One response - Left', ...
    'GO trials - One response - Right', ...
    'GO trials - Multiple responses - Left', ...
    'GO trials - Multiple responses - Right'};

nzs = size(gmv.averageresp.z,2);
M = zeros(gm.yDef,gm.xDef,nzs,5);
for z = 1:nzs
    % Stim off
    ix1 = find(ismember([gmpc.con.pr],pnconvon(pnconvon_z == z)));
    for i = 1:length(ix1)
        e = gmpc.con(ix1(i)).pr;
        fr = gmpc.con(ix1(i)).fr;
        stim = gmv.visstim(e,1:5);
        [vST,vED,~,~] = gcStimInfo(stim, gm.trfr, gm.frtime, 0);
        if fr >= vST && fr <= vED
            ix1(i) = nan;
        end
    end
    ix1(isnan(ix1)) = [];
    
    % one response
    ix2 = find(ismember([gmpc.con.pr],osbtcp(osbtcp_z == z)));
    ix2_L = false(length(ix2),1);
    for i = 1:length(ix2)
        e = gmpc.con(ix2(i)).pr;
        fr = gmpc.con(ix2(i)).fr;
        stim = gmv.visstim(e,1:5);
        [vST,vED,~,~] = gcStimInfo(stim, gm.trfr, gm.frtime, 0);
        if fr < vST || fr > vED
            ix2(i) = nan;
        else
            if pconvon_ang(ismember(pconvon,e)) > 0
                ix2_L(i) = 1;
            end
        end
    end
    ix2_L(isnan(ix2)) = [];
    ix2(isnan(ix2)) = [];
    
    % multiple responses
    ix3 = find(ismember([gmpc.con.pr],msbtcp(msbtcp_z == z)));
    ix3_L = false(length(ix3),1);
    for i = 1:length(ix3)
        e = gmpc.con(ix3(i)).pr;
        fr = gmpc.con(ix3(i)).fr;
        stim = gmv.visstim(e,1:5);
        [vST,vED,~,~] = gcStimInfo(stim, gm.trfr, gm.frtime, 0);
        if fr < vST || fr > vED
            ix3(i) = nan;
        else
            if pconvon_ang(ismember(pconvon,e)) > 0
                ix3_L(i) = 1;
            end
        end
    end
    ix3_L(isnan(ix3)) = [];
    ix3(isnan(ix3)) = [];
    
    M(:,:,z,1) = mean(cat(3,gmpc.con(ix1).conv_dFFm),3);
    M(:,:,z,2) = mean(cat(3,gmpc.con(ix2(ix2_L)).conv_dFFm),3);
    M(:,:,z,3) = mean(cat(3,gmpc.con(ix2(~ix2_L)).conv_dFFm),3);
    M(:,:,z,4) = mean(cat(3,gmpc.con(ix3(ix3_L)).conv_dFFm),3);
    M(:,:,z,5) = mean(cat(3,gmpc.con(ix3(~ix3_L)).conv_dFFm),3);
end

%%
% Plot conv maps
for z = 1:nzs
    thisanatomy = gm.zimg(z).aligngood2;
    thisanatomy = scaleif(thisanatomy,0,1,min_anat,max_anat);
    
    figure('name',sprintf('Z = %d',z), ...
        'position',[128,310,1686,437])
    for c = 1:size(M,4)
        thismerge = repmat(thisanatomy,[1 1 3]);
        fmap = M(:,:,z,c);
        fmap(abs(fmap)<drmin) = 0;
        fmap = fmap./drmax;
        fmap(fmap>1) = 1;
        fmap(fmap<-1) = -1;
        
        
        pfmap = fmap;
        nfmap = fmap;
        pfmap(pfmap<0) = 0;
        nfmap(nfmap>0) = 0;
        nfmap = -nfmap;
        
        thismerge = thismerge.*.4; % scale anatomy
        thismerge(:,:,1) = thismerge(:,:,1) + pfmap.*.6;
        thismerge(:,:,3) = thismerge(:,:,3) + nfmap.*.6;
        subplot(2,3,c)
        imshow(thismerge);
        title(labels{c});
    end
end