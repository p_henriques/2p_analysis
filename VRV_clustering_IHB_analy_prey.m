load('Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\analysis\Clustering\VRV_clustering\rawtraces\mf.mat')
load('Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\analysis\Clustering\kmeans\180707\clustering_vvec1_VisOnly_PCA_180707.mat','zbbmsk','roiszbbC')
load('ZBBMaskDatabase.mat')
nmsk = size(ZBBMaskDatabase,2);

% Visstim timings
load(fullfile(mf.datasetz{1},'gm'),'gm');
load(fullfile(mf.datasetz{1},'gmv'),'gmv');
load(fullfile(mf.datasetz{1},'gmb'),'gmb');
vST = gm.trfr + 2;
vED = gm.trfr + 1 + ceil(1000.*gmv.vistypz(:,5)./gm.frtime);

%% Set up variables

vlabs = {'DF','LF','DS-CCW','DS-CW','DS-CCW (AT)','DS-CW (AT)', ...
    'Cluster','Fish'};

ccol = [0 1 0; 1 0 0; 0 0 1];   % Cluster color
vsz = size(mf.X,2);
nfr = gm.nfr;
nbanners = 2;
bannerlen = ceil(vsz/10);
imrange = [-5 5];

ncls = length(mf.superunique_x);

% Mask indexes
aniix = 305;
pniix = 306;
chbix = 77;
gtix = 12;
otix = 34;
ocuix = 29;

% Combine them
zbbix = [aniix pniix chbix gtix otix ocuix];
nzbb = length(zbbix);

MFix = mf.superMFix_x;  % Original indexes of cells in final set
Idx = mf.superIdx_x;
MFzbbix = zbbmsk(MFix,zbbix);    % Masks

%% Plot original VRVs of chosen mask sorted by distance to centroid

savefigs = 0;
msk2plt = chbix;    % Mask ids to plot

% Sort distances
[Ds,six] = sort(mf.superBoutDistto_b_ifn_Cent_x,'ascend');

X = mf.X(MFix,:);

for i = 1:length(msk2plt)
    ixx = msk2plt(i);
    nrois = sum(zbbmsk(MFix,ixx));
    M = [];
    B = [];
    for c = 1:ncls
        oix = intersect(six,find(Idx == c & zbbmsk(MFix,ixx)),'stable');
        M0 = X(oix,:);
        M = cat(1,M,M0);
        B = cat(1,B,cat(2,ones(length(oix),1).*c, ...
            mf.fishID(oix)));
    end  
    
    Idx_ban = repmat(B(:,1),[1 bannerlen]);
    fish_ban = repmat(B(:,2),[1 bannerlen]);
    
    if nrois >= 20
        fig = figure('Position',[405,158,1064,797]);
        ax1 = subplot(1,14,1:12);
        imagesc(M,[-5 5])
        colormap(flip(brewermap(255,'RdBu'),1));
        for j = 1:length(vED)
            line([nfr nfr].*(j-1),[0 nrois],'Color','k','LineStyle','-');
            line([vST vST]+nfr.*(j-1),[0 nrois],'Color','b','LineStyle','--');
            line([vED(j) vED(j)]+nfr.*(j-1),[0 nrois],'Color','r','LineStyle','--');
        end
        set(ax1,'XTick',110/2:110:vsz, ...
            'XTickLabel',vlabs(1:end-2), ...
            'XTickLabelRotation',45,'Box','off','TickDir','out')
        cedg = find(diff(B(:,1)) ~= 0)+0.5;
        if any(cedg)
            line([0 vsz+bannerlen*nbanners+bannerlen/2], ...
                [cedg, cedg]','Color','k','LineWidth',1,'LineStyle','--')
        end
        title(ZBBMaskDatabaseNames{ixx})
        ylabel('ROIs')
        xlabel('Time')
        cbar = colorbar;
        ylabel(cbar,'Z-scored F')
        
        ax2 = subplot(1,14,13);
        imagesc(Idx_ban)
        colormap(ax2,ccol)
        set(ax2,'XTick',bannerlen/2,'XTickLabel',vlabs(end-1), ...
            'YTick',[],...
            'XTickLabelRotation',45,'Box','off','TickDir','out')
        
        ax3 = subplot(1,14,14);
        imagesc(fish_ban)
        colormap(ax3,brewermap(9,'Set1'));
        set(ax3,'XTick',bannerlen/2,'XTickLabel',vlabs(end), ...
            'YTick',[],...
            'XTickLabelRotation',45,'Box','off','TickDir','out')
        
        if savefigs
            savedir = fullfile('Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\analysis\Clustering\VRV_clustering\figures\ZBB_areas\traces', ...
                ZBBMaskDatabaseNames{ixx});
            if ~exist(savedir,'dir')
                mkdir(savedir);
            end
            savefig(fig,fullfile(savedir,'Traces_Matrix'))
            close(fig)
        end
    end
end

%% Mean centroids

figure
h = plot(mf.superCent_x','LineWidth',2);
for c = 1:ncls
    h(c).Color = ccol(c,:);
end
ylims = ylim;
line([nfr nfr].*repmat([1:6],2,1)',ylims,'Color','k','LineStyle','-');
for j = 1:length(vED)
    line([vST vST]+nfr.*(j-1),ylims,'Color','b','LineStyle','--');
    line([vED(j) vED(j)]+nfr.*(j-1),ylims,'Color','r','LineStyle','--');
end
set(gca,'XTick',110/2:110:vsz, ...
    'XTickLabel',vlabs, ...
    'XTickLabelRotation',45, ...
    'Box','off','TickDir','out')
ylabel('zF')
legend(mf.supernamez_x,'Location','northoutside','Orientation','horizontal')


figure
imagesc(mf.superCent_x,imrange)
colormap(flip(brewermap(255,'RdBu'),1));
cbar = colorbar;
ylabel(cbar,'zF')
line([nfr nfr].*repmat([1:6],2,1)',[0 ncls]+0.5,'Color','k','LineStyle','-');
for j = 1:length(vED)
    line([vST vST]+nfr.*(j-1),[0 ncls]+0.5,'Color','b','LineStyle','--');
    line([vED(j) vED(j)]+nfr.*(j-1),[0 ncls]+0.5,'Color','r','LineStyle','--');
end
set(gca,'XTick',110/2:110:vsz,'XTickLabel',vlabs, ...
    'XTickLabelRotation',45, ...
    'YTick',1:ncls,'YTickLabel',mf.supernamez_x,...
    'Box','off','TickDir','out')
ylabel('Cluster')


%% Proportion of cells that are included in final cluster set

msk2plt = [aniix pniix chbix gtix otix ocuix];
figure
bar(sum(MFzbbix)./sum(zbbmsk(:,msk2plt)))
set(gca,'Box','off','TickDir','out', ...
    'XTickLabel',ZBBMaskDatabaseNames(msk2plt),'XTickLabelRotation',45)
ylabel('% Cells')

%% Proportion of cells that are included in final cluster set (sorted)

msk2plt = [2:3 6:34 44:46 48:74 77 305:306];
ix = zbbmsk(MFix,msk2plt);
F = sum(ix)./sum(zbbmsk(:,msk2plt));

[srt,six] = sort(F,'descend','MissingPlacement','last');
ix = six(~isnan(srt));
figure
bar(F(ix))
set(gca,'Box','off','TickDir','out', ...
    'XTick',1:length(ix), ...
    'XTickLabel',ZBBMaskDatabaseNames(msk2plt(ix)),'XTickLabelRotation',45)
ylabel('% Cells included in visual clusters')

%% Lateralization

msk2plt = [aniix pniix chbix gtix otix];

X = NaN(length(msk2plt),ncls);
for n = 1:length(msk2plt)
    for c = 1:ncls
        mix = zbbmsk(MFix,msk2plt(n));
        C = roiszbbC(MFix(Idx == c & mix),:);
        X(n,c) = (sum(C(:,2) < 308)-sum(C(:,2) > 308))/size(C,1);
    end
end
      
for c = 1:ncls
    figure
    bar(X(:,c))
    ylim([-1 1])
    axis square
    set(gca,'XTickLabel',ZBBMaskDatabaseNames(msk2plt),...
        'YTick',-1:0.5:1, ...
        'XTickLabelRotation',90, 'YTickLabelRotation',90, ...
        'Box','off','TickDir','out')
    ylabel('Asymmetry index')
    title(sprintf('Cluster %d',c))
end

%% Fraction of ClusterID per area

H = NaN(nzbb,ncls);
for i = 1:nzbb
    H(i,:) = histcounts(Idx(MFzbbix(:,i)),'Normalization','probability');
end

figure
h = bar(H,'stacked','FaceColor','flat');
for c = 1:ncls
    h(c).CData = repmat(ccol(c,:),nzbb,1);
end
set(gca,'Box','off','TickDir','out', ...
    'XTickLabel',ZBBMaskDatabaseNames(zbbix),'XTickLabelRotation',45)
ylabel('% Cells')
legend(mf.supernamez_x,'Location','northoutside','Orientation','horizontal')

%% Fraction of cluster ranked by brain area

msk2plt = [12,28,29,33,34,52,66,67,77,305,306];
msk2plt = [2:3 6:34 44:46 48:74 77 305:306];
ncmax = 50;
H = NaN(length(msk2plt),ncls,3);
for i = 1:length(msk2plt)
    for c = 1:ncls
        ix = zbbmsk(MFix,msk2plt(i)) & Idx == c;
        if sum(ix) >= ncmax
            nr = sum(roiszbbC(MFix(ix),2) > 308);
            nl = sum(roiszbbC(MFix(ix),2) < 308);
            asyidx = (nr-nl)/(nr + nl);
            H(i,c,1) = nl/sum(ix);
            H(i,c,2) = nr/sum(ix);
            H(i,c,3) = asyidx;
        end
    end
end

for c = 1:ncls
    [srt,six] = sort(H(:,c,3),'descend','MissingPlacement','last');
    ix = six(~isnan(srt));
    figure
    bar(H(ix,c,3))    
    set(gca,'XTick',1:length(ix), ...
        'XTickLabel',ZBBMaskDatabaseNames(msk2plt(ix)),'XTickLabelRotation',45, ...
        'Box','off','TickDir','out')
    ylim([-1 1])
    title(sprintf('Cluster %d - %s',c,mf.supernamez_x{c}))
    ylabel('Asymmetry index')
end

%% Fraction of total ROIs by cluster type

figure
histogram(Idx,'Normalization','probability')
set(gca,'Box','off','TickDir','out', ...
    'XTick',1:ncls, ...
    'XTickLabel',mf.supernamez_x,'XTickLabelRotation',45)
ylabel('Proportion of clustered cells')

%% Fraction of cell per area per cluster

msk2plt = [2:3 6:34 44:46 48:74 77 305:306];
ncmax = 30;
H = NaN(length(msk2plt),ncls,3);
for i = 1:length(msk2plt)
    for c = 1:ncls
        ix = zbbmsk(MFix,msk2plt(i));
        if sum(ix) >= ncmax
           H(i,c) = sum(ix & Idx == c)/sum(ix);
        end
    end
end

for c = 1:ncls
    [srt,six] = sort(H(:,c),'descend','MissingPlacement','last');
    ix = six(~isnan(srt));
    figure
    bar(H(ix,c))    
    set(gca,'XTick',1:length(ix), ...
        'XTickLabel',ZBBMaskDatabaseNames(msk2plt(ix)),'XTickLabelRotation',45, ...
        'Box','off','TickDir','out')
    title(sprintf('Cluster %d - %s',c,mf.supernamez_x{c}))
    ylabel('Proportion of cells in cluster')
end


%% Ranked average distance of cell in area to cluster centroid

msk2plt = [2:3 6:34 44:46 48:74 77 305:306];
ncmax = 20;
H = NaN(length(msk2plt),ncls);
for i = 1:length(msk2plt)
    for c = 1:ncls
        ix = zbbmsk(MFix,msk2plt(i)) & Idx == c;
        if sum(ix) >= ncmax
           H(i,c) = nanmean(mf.superBoutDistto_b_ifn_Cent_x(ix));
        end
    end
end

for c = 1:ncls
    [srt,six] = sort(H(:,c),'ascend','MissingPlacement','last');
    ix = six(~isnan(srt));
    figure
    bar(H(ix,c))    
    set(gca,'XTick',1:length(ix), ...
        'XTickLabel',ZBBMaskDatabaseNames(msk2plt(ix)),'XTickLabelRotation',45, ...
        'Box','off','TickDir','out')
    title(sprintf('Cluster %d - %s',c,mf.supernamez_x{c}))
    ylabel('Average distance to cluster centroid')
end

%% Scatter all ROI centroids colored by cluster ID

plotbnd = 1;
savefigs = 1;
intscale = 0.25;
msk2plt = [12 13 29 33 34 52 58 61 305 306];
zspace = 10;
zs = round(roiszbbC(MFix,3));
zedg = prctile(zs,5):zspace:prctile(zs,95);
zcnt = round(((zedg*2)+zspace)./2);
zix = discretize(zs,zedg);
nzs = max(zix);

for z = 1:nzs
    fig = figure;
    scatter(roiszbbC(MFix(zix == z),1),roiszbbC(MFix(zix == z),2), ...
        8,Idx(zix == z),'filled')
    hold on
    colormap(ccol);
    
    if plotbnd
        % Plot boundaries
        for j = 1:length(msk2plt)
            msk = reshape(full(ZBBMaskDatabase(:,msk2plt(j))),[616,1030,420]);
            
            % Smooth mask
            M = msk(:,:,zcnt(z));
            if any(M(:))
                F = griddedInterpolant(double(M));
                [sx,sy,sz] = size(M);
                
                xq = (0:intscale:sx)';
                yq = (0:intscale:sy)';
                zq = (0:intscale:sz)';
                
                I = imgaussfilt(F({xq,yq}),3) > 0.5;
                mskB = bwboundaries(I);
                for m = 1:size(mskB,1)
                    mskB{m,1} = mskB{m,1}.*intscale;
                end
                
                % Plot boundaries
                for l = 1:length(mskB)
                    boundary = mskB{l};
                    plot(boundary(:,2), boundary(:,1),'-', ...
                        'Color','k','LineWidth',1);
                end
            end
        end
    end
    alpha 0.3
    axis equal off
    set(gca,'ZDir','reverse','YDir','reverse')
    view([90 90])
    title(sprintf('Z = %d-%d',zedg(z),zedg(z)+zspace))
    
    if savefigs
        savedir = fullfile('Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\analysis\Clustering\VRV_clustering\figures\ZBB_areas\maps\All_brain');
        if ~exist(savedir,'dir')
            mkdir(savedir);
        end
        savefig(fig,fullfile(savedir, ...
            sprintf('ROI_scatter_map_z%d_z%d',zedg(z),zedg(z)+zspace)))
        close(fig)
    end
end

%% Plot all ROIs per cluster

for c = 1:ncls
    figure
    scatter(roiszbbC(MFix(Idx == c),1),roiszbbC(MFix(Idx == c),2), ...
        8,ccol(c,:),'filled')
    alpha 0.3
    axis equal off
    set(gca,'ZDir','reverse','YDir','reverse')
    view([90 90])
end

%% Plot NI ROIs

usezbbmsk = 1;
intscale = 0.25;
gaussfilt = 17;
sctsz = 150;
alphav = 0.5;

% msk2plt = [gtix ocuix otix chbix aniix pniix 52 58 33];
% mskxoffset = [30 70 0 45+15 45 45+10 -90 -280 -140];
% mskyoffset = [0 0 0 40 0 0 0 80 5;
%     0 0 0 -40 0 0 0 -80 -5];
% mskrange = round([prctile(roiszbbC(:,3),1) ...
%     prctile(roiszbbC(:,3),99)]);

msk2plt = [chbix aniix pniix];  % Only NI
mskxoffset = [60 45 55];
mskyoffset = [40 0 0;
    -40 0 0];
mskrange = [100 250];

for c = 1:ncls    
    for s = 1:2
        figure
        for n = 1:length(msk2plt)
            mix = zbbmsk(MFix,msk2plt(n));
            C = roiszbbC(MFix(Idx == c & mix),:);
            if s == 1
                C = C(C(:,2) > 308,:);
            else
                C = C(C(:,2) < 308,:);
            end
            %             mskrange = round([prctile(C(:,3),1) prctile(C(:,3),99)]);
            scatter(C(:,1)+mskxoffset(n),C(:,2)+mskyoffset(s,n), ...
                sctsz,ccol(c,:),'filled')
            hold on
            if usezbbmsk
                msk = reshape(full(ZBBMaskDatabase(:,msk2plt(n))),[616,1030,420]);
                if s == 1
                    msk(1:308,:,:) = 0;
                else
                    msk(308:end,:,:) = 0;
                end
                I = bwmorph(sum(msk(:,:,mskrange(1):mskrange(2)),3) > 0,'close');
                %                 I = bwconvhull(I,'objects');
                
                %                 stats = regionprops(I,{'Area','PixelIdxList'});
                %                 if length(stats) > 1
                %                     [~,six] = sort([stats.Area],'descend');
                %                     pxs = cat(1,stats(six(2:end)).PixelIdxList);
                %                     I(pxs) = 0;
                %                 end
                % Smooth mask
                F = griddedInterpolant(double(I));
                [sx,sy,sz] = size(I);
                
                xq = (0:intscale:sx)';
                yq = (0:intscale:sy)';
                zq = (0:intscale:sz)';
                
                If = imgaussfilt(F({xq,yq}),gaussfilt) > 0.5;
                mskB = bwboundaries(If);
                for m = 1:size(mskB,1)
                    mskB{m,1} = mskB{m,1}.*intscale;
                end
                
                for k = 1:length(mskB)
                    boundary = mskB{k};
                    plot(boundary(:,2)+mskxoffset(n), ...
                        boundary(:,1)+mskyoffset(s,n),'-', ...
                        'Color','k','LineWidth',1);
                end
            else
                K = convhull(C(:,1),C(:,2));
                x = C(K,1);
                y = C(K,2);
                np = length(x);
                xp = interp1(1:np,x,1:0.01:np)+mskxoffset(n);
                yp = interp1(1:np,y,1:0.01:np)+mskyoffset(s,n);
                plot(xp,yp,'k-')
            end
        end
        axis off equal
        set(gca,'YDir','reverse')
        view([90 90])
        alpha(alphav)
        title(mf.supernamez_x{c})
    end
end

%% Hierarchical clustering of unclustered cells' VRVs

ix = setdiff(1:size(mf.X,1),mf.superMFix_x);
Xnc = mf.X(ix,:);
Yk = pdist(Xnc,'correlation');
Zk = linkage(Yk,'average');
[H,~,op] = dendrogram(Zk,0,'Orientation','right');

%% Compare visual response signals between two brain areas

msk2plt = [aniix otix];    % Mask ids to plot

x1 = [1:110].*gm.frtime./1000;
s1p = setdiff(find(gmv.visstim(:,1) == 80006),gmb.convergences(:,1));
s2p = setdiff(find(gmv.visstim(:,1) == 80007),gmb.convergences(:,1));
s1p = s1p(1);
s2p = s2p(1);

ang1 = interp1(gmb.p(s1p).vis_traj(:,7), ...
    -spotangle_v2(gmb.p(s1p).vis_traj(:,3), ...
    gm.visgeom.sweepamp,gm.visgeom.amax,gm.visgeom.R,gm.visgeom.fish2screen), ...
    x1);
ang2 = interp1(gmb.p(s2p).vis_traj(:,7), ...    
    -spotangle_v2(gmb.p(s2p).vis_traj(:,3), ...
    gm.visgeom.sweepamp,gm.visgeom.amax,gm.visgeom.R,gm.visgeom.fish2screen), ...
    x1);

[vST,vED1] = gcStimInfo(gmv.vistypz(1,1:5),gm.trfr,gm.frtime,0);
[~,vED2] = gcStimInfo(gmv.vistypz(3,1:5),gm.trfr,gm.frtime,0);

X = mf.X(MFix,:);
M = NaN(ncls,2,2,2,110);
for c = 1:ncls
    figure
    hold on
    for i = 1:length(msk2plt)
        ixx = msk2plt(i);
        nrois = sum(zbbmsk(MFix,ixx));
        lix = Idx == c & zbbmsk(MFix,ixx) & roiszbbC(MFix,2) > 308;
        rix = Idx == c & zbbmsk(MFix,ixx) & roiszbbC(MFix,2) < 308;
        
        Ml = X(lix,:);
        Mr = X(rix,:);
        if c == 1
            Ml = Ml(:,1:110);
            Mr = Mr(:,1:110);
        elseif c == 2
            Ml = cat(1,Ml(:,331:440),Ml(:,551:end));
            Mr = cat(1,Mr(:,331:440),Mr(:,551:end));
        elseif c == 3
            Ml = cat(1,Ml(:,221:330),Ml(:,441:550));
            Mr = cat(1,Mr(:,221:330),Mr(:,441:550));
        end
        
        
        x = [1:size(Ml,2)].*gm.frtime./1000;
        yl = nanmean(Ml);
        ylerr = nansem(Ml);
        yr = nanmean(Mr);
        yrerr = nansem(Mr);
        
        M(c,i,1,1,:) = yl;
        M(c,i,1,2,:) = ylerr;
        M(c,i,2,1,:) = yr;
        M(c,i,2,2,:) = yrerr;
        
        if i == 1
            h1 = shadedErrorBar(x,yl,ylerr,'b',1);
            h2 = shadedErrorBar(x,yr,yrerr,'r',1);
        else
            h3 = shadedErrorBar(x,yl,ylerr,'g',1);
            h4 = shadedErrorBar(x,yr,yrerr,'m',1);
        end                        
    end
    
    if any(c == [2,3])
        yyaxis right
        if c == 2
            plot(x,ang1,'k','LineWidth',2)
        elseif c == 3
            plot(x,ang2,'k','LineWidth',2)
        end
        line([x(1) x(end)],[0 0],'Color','k','LineStyle','--')
        line([x(vST+((vED2-vST)/2)-1) x(vST+((vED2-vST)/2)-1)], ...
            [-75 75],'Color','k','LineStyle','--')
        line([x(vST-1) x(vST-1)],[-75 75],'Color','b','LineStyle','--','LineWidth',2)
        line([x(vED2-1) x(vED2-1)],[-75 75],'Color','r','LineStyle','--','LineWidth',2)
        ylabel('Spot angle')
        ylim([-75 75])
        yyaxis left
    else
        ylims = ylim;
        line([x(vST-1) x(vST-1)],ylims,'Color','b','LineStyle','--')
        line([x(vED1-1) x(vED1-1)],ylims,'Color','r','LineStyle','--')
    end
    title(mf.supernamez_x{c})
    legend([h1.mainLine;h2.mainLine;h3.mainLine;h4.mainLine], ...
        {[ZBBMaskDatabaseNames{msk2plt(1)} ' - Left'], ...
        [ZBBMaskDatabaseNames{msk2plt(1)} ' - Right'], ...
        [ZBBMaskDatabaseNames{msk2plt(2)} ' - Left'], ...
        [ZBBMaskDatabaseNames{msk2plt(2)} ' - Right']}, ...
        'Location','southoutside')
    xlabel('Time (s)')
    ylabel('zF')
    xlim([8 17])
    set(gca,'Box','off','TickDir','out')    
end

