
load('Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\analysis\Convergence\Conv_all_F_traces_nbts_180902');

%%

folders = { ...
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180301\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180305\f2\deep';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180419\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f1_1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180427\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180503\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180504\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180509\f3';
    };

nfolders = length(folders);

%%

natix = [3,4];
atix = [5,6];
useunfilt = 0;
useactive = 0;  % Use onlt active cells
iathr = 2;  % Z threshold to consider cell active

F1_on = [];
F2_on = [];
F1_off = [];
F2_off = [];
rois1_on = [];
rois2_on = [];
rois1_off = [];
rois2_off = [];
zbbmsk1_on = [];
zbbmsk2_on = [];
zbbmsk1_off = [];
zbbmsk2_off = [];
h = waitbar(0, 'Init');
for f = 1:9
    waitbar(f/length(folders),h, 'Folders')
    datadir = folders{f};
    
    load(fullfile(datadir,'gmConvMod'));
    load(fullfile(datadir,'gmranat.mat'));
    
    nrois = size(gmConvMod.rois,2);
    
    h2 = waitbar(0,'Init');
    for i = 1:nrois
        waitbar(i/nrois,h2, 'Rois')
        
        z = gmConvMod.rois(i).z;
        gmix = find([gmranat.z(z).STATScrop.tix] == i);
        ix1 = ismember(gmConvMod.rois(i).vis_on(:,1),natix);  % NO-GO
        ix2 = ismember(gmConvMod.rois(i).vis_on(:,1),atix);   % NO-GO AT
        
        if useactive
            iaon = gmConvMod.rois(i).F_on >= iathr;
            iaoff = gmConvMod.rois(i).F_off >= iathr;
        else
            iaon = true(size(gmConvMod.rois(i).F_on,1),1);
            iaoff = true(size(gmConvMod.rois(i).F_off,1),1);
        end
        iaon1 = iaon & gmConvMod.rois(i).nbts_on == 1;
        iaon2 = iaon & gmConvMod.rois(i).nbts_on > 1;
        iaoff1 = iaoff & gmConvMod.rois(i).nbts_off == 1;
        iaoff2 = iaoff & gmConvMod.rois(i).nbts_off > 1;
        
        if any(iaon)
            F1_on = cat(1,F1_on,gmConvMod.rois(i).Fz_on_nof(iaon1,:));
            F2_on = cat(1,F2_on,gmConvMod.rois(i).Fz_on_nof(iaon2,:));
            rois1_on = cat(1,rois1_on,ones(sum(iaon1),1).*i);
            rois2_on = cat(1,rois2_on,ones(sum(iaon2),1).*i);
            zbbmsk1_on = cat(1,zbbmsk1_on, ...
                logical(repmat(gmranat.z(z).STATScrop(gmix).Masks_ZBB,sum(iaon1),1)));
            zbbmsk2_on = cat(1,zbbmsk2_on, ...
                logical(repmat(gmranat.z(z).STATScrop(gmix).Masks_ZBB,sum(iaon2),1)));
        end
        if any(iaoff)
            F1_off = cat(1,F1_off,gmConvMod.rois(i).Fz_off_nof(iaoff1,:));
            F2_off = cat(1,F2_off,gmConvMod.rois(i).Fz_off_nof(iaoff2,:));
            rois1_off = cat(1,rois1_off,ones(sum(iaoff1),1).*i);
            rois2_off = cat(1,rois2_off,ones(sum(iaoff2),1).*i);
            zbbmsk1_off = cat(1,zbbmsk1_off, ...
                logical(repmat(gmranat.z(z).STATScrop(gmix).Masks_ZBB,sum(iaoff1),1)));
            zbbmsk2_off = cat(1,zbbmsk2_off, ...
                logical(repmat(gmranat.z(z).STATScrop(gmix).Masks_ZBB,sum(iaoff2),1)));
        end
    end
    close(h2)
    
    clear gmConvMod gmranat
end
close(h);

%%

load('ZBBMaskDatabase.mat','ZBBMaskDatabaseNames')
x = 1:110;
wndoffset = 2;
wndlen = 4;
usefilter = 0;
ffiltwidth = 3; % Calcium fluorescence filter
filter1 = ones(1,ffiltwidth)./ffiltwidth;
useactive = 1;
usepreactive = 1;
activev = 2;
ylims = [0.2 1.8];

msk2plt = [12,29,33,34,305,306];

for n = 1:length(msk2plt)
    mix1_on = zbbmsk1_on(:,msk2plt(n));
    mix2_on = zbbmsk2_on(:,msk2plt(n));
    mix1_off = zbbmsk1_off(:,msk2plt(n));
    mix2_off = zbbmsk2_off(:,msk2plt(n));
    
    M1_on = excisenansumrows(F1_on(mix1_on,:));
    M2_on = excisenansumrows(F2_on(mix2_on,:));
    M1_off = excisenansumrows(F1_off(mix1_off,:));
    M2_off = excisenansumrows(F2_off(mix2_off,:));
    
    if usefilter
        M1_on = filtfilt(filter1,1,M1_on')';
        M2_on = filtfilt(filter1,1,M2_on')';
        M1_off = filtfilt(filter1,1,M1_off')';
        M2_off = filtfilt(filter1,1,M2_off')';
    end
    
    if useactive
        y1_onix = any(M1_on(:,55+wndoffset:55+wndoffset+wndlen) >= activev,2);
        if usepreactive
            y1_onix = y1_onix & ~any(M1_on(:,55-wndoffset-wndlen:55-wndoffset) >= activev,2);
        end
    else
        y1_onix = true(size(M1_on,1),1);
    end
    y1_onf = nanmean(M1_on(y1_onix,:));
    y1err_onf = nansem(M1_on(y1_onix,:));
    
    if useactive
        y2_onix = any(M2_on(:,55+wndoffset:55+wndoffset+wndlen) >= activev,2);
        if usepreactive
            y2_onix = y2_onix & ~any(M2_on(:,55-wndoffset-wndlen:55-wndoffset) >= activev,2);
        end
    else
        y2_onix = true(size(M2_on,1),1);
    end
    y2_onf = nanmean(M2_on(y2_onix,:));
    y2err_onf = nansem(M2_on(y2_onix,:));
    
    if useactive
        y1_offix = any(M1_off(:,55+wndoffset:55+wndoffset+wndlen) >= activev,2);
        if usepreactive
            y1_offix = y1_offix & ~any(M1_off(:,55-wndoffset-wndlen:55-wndoffset) >= activev,2);
        end
    else
        y1_offix = true(size(M1_off,1),1);
    end
    y1_offf = nanmean(M1_off(y1_offix,:));
    y1err_offf = nansem(M1_off(y1_offix,:));
    
    if useactive
        y2_offix = any(M2_off(:,55+wndoffset:55+wndoffset+wndlen) >= activev,2);
        if usepreactive
            y2_offix = y2_offix & ~any(M2_off(:,55-wndoffset-wndlen:55-wndoffset) >= activev,2);
        end
    else
        y2_offix = true(size(M2_off,1),1);
    end
    y2_offf = nanmean(M2_off(y2_offix,:));
    y2err_offf = nansem(M2_off(y2_offix,:));
    
    figure
    subplot(1,2,1)
    shadedErrorBar(x,y1_onf,y1err_onf,'b',1)
    hold on
    shadedErrorBar(x,y2_onf,y2err_onf,'r',1)
    ylims = ylim;
    axis([x(1) x(end) ylims],'square')
    set(gca,'Box','off','TickDir','out')
    title('GO')
    
    subplot(1,2,2)
    shadedErrorBar(x,y1_offf,y1err_offf,'b',1)
    hold on
    shadedErrorBar(x,y2_offf,y2err_offf,'r',1)
    axis([x(1) x(end) ylims],'square')
    set(gca,'Box','off','TickDir','out')
    title('Spontaneous')
    
    mtit(ZBBMaskDatabaseNames{msk2plt(n)})
end