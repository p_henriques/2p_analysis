%%  Build ROI x features matrix

folders = { ...
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180301\f1';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180305\f2\deep';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180419\f1';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f1_1';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f2';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180427\f2';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180503\f2';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180504\f1';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180509\f3';
    };

nfolders = length(folders);

%%

vvec = 1;   % Visual vector to use

Xinfo = table;
X_all = [];
roiszbbC = [];
zbbmsk = [];
md_pv = [];
nanixall = [];
for f = 1:nfolders
    datadir = folders{f};
    load(fullfile(datadir,'gmROIfvec'));
    load(fullfile(datadir,'gmranat'));       
    
    if vvec == 1
        Xv = gmROIfvec.Xv;
    elseif vvec == 2
        Xv = gmROIfvec.Rmean(:,:,1);
    elseif vvec == 3
        Xv = gmROIfvec.Rmax;
    elseif vvec == 4
        load(fullfile(datadir,'Xgc_allz'),'Xgc_allz');
        if size(Xgc_allz,2) > 660
            Xgc_allz(:,(110*2)+1:110*4) = [];
        end
        Xv = Xgc_allz;
    end
    
    % Sizes of matrices
    rsz = size(gmROIfvec.Xr,2);
    vsz = size(Xv,2);
    msz = 1;
    
    X = [Xv gmROIfvec.Xr gmROIfvec.Modulation.MD(:,1,1)];
    [X,nanix] = excisenansumrows(X);
    nanixall = cat(1,nanixall,nanix);
    
    X_all = cat(1,X_all,X);
    
    % ZBB masks and centroids
    nzs = size(gmranat.z,2);
    ZM = [];    % Mask
    C = []; % Centroids
    for z = 1:nzs
        ZM = cat(1,ZM,cat(1,gmranat.z(z).STATScrop.Masks_ZBB));
        C = cat(1,C,cat(1,gmranat.z(z).STATScrop.Centroid_ZBB));
    end
    ZM = ZM(~nanix,:);
    C = C(~nanix,:);
    zbbmsk = cat(1,zbbmsk,ZM);
    roiszbbC = cat(1,roiszbbC,C);
    
    md_pv = cat(1,md_pv,gmROIfvec.Modulation.MDcorr(~nanix,:,2));
    rois = find(~nanix);
    fish = ones(size(X,1),1).*f;
    
    Xinfo = cat(1,Xinfo, ...
        table(fish, rois));
end
zbbmsk = logical(zbbmsk);

clear C ZM rois fish gmROIfvec gmranat X Xv nzs nanix z f datadir

%%  Define the matrix to be used for processing

usereg = 0;
usemd = 0;

nrois = size(X_all,1);
if ~usereg
    rsz = 0;
end
if ~usemd
    msz = 0;
end

X_all_z = X_all;
% X_all_z(:,1:vsz) = zscore(X_all_z(:,1:vsz),[],2);
% X_all_z(:,vsz+1:end) = zscore(X_all_z(:,vsz+1:end),[],1);

if ~usereg && ~usemd
    X_all_z = X_all_z(:,1:vsz);
    X_all = X_all(:,1:vsz);
end

%%  Load Mask database names

% zbbanatix = [1:75 77 79 113 302:306];
zbbanatix = [2:3 6:34 44:46 48:74 77 113 305:306];
load('ZBBMaskDatabase');
ZBBMaskDatabaseNames = ZBBMaskDatabaseNames(zbbanatix);
ZBBMaskDatabase = ZBBMaskDatabase(:,zbbanatix);
nmsks = length(ZBBMaskDatabaseNames);
zbbmsk = zbbmsk(:,zbbanatix);

%% Load previous data

load("Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\analysis\Clustering\kmeans\180707\clustering_vvec1_VisOnly_PCA_180707.mat");

%% PCA

[coeff,score,latent,tsquared,explained,mu] = pca(X_all_z,'Centered',false);
% npcs = findnearest(85,cumsum(explained));   % Use n pcs that explain 85% of variance
npcs = knee_pt(explained);

X_all_pcs = score(:,1:npcs);    % Get components
X_all_pcs_z = zscore(score(:,1:npcs),[],1); % z-score components

gmclt.pca.coeff = coeff;
gmclt.pca.score = score;
gmclt.pca.latent = latent;
gmclt.pca.tsquared = tsquared;
gmclt.pca.explained = explained;
gmclt.pca.mu = mu;
gmclt.pca.npcs = npcs;
gmclt.pca.X_all_pcs = X_all_pcs;

figure('name','PC explained variance')
plot(explained,'-o')
ylabel('Explained variance (%)')
xlabel('PC')
xlim([0 50])
yyaxis right
plot(cumsum(explained),'-o')
ylabel('Cumulative explained variance (%)')
grid on
line([npcs npcs],[0 100],'Color','k','LineStyle','--')
line([0 50],[sum(explained(1:npcs)) sum(explained(1:npcs))],'Color','k','LineStyle','--')
text(npcs+2,double(sum(explained(1:npcs))-4), ...
    sprintf('n = %d',npcs));
text(npcs+2,double(sum(explained(1:npcs))-8), ...
    sprintf('explained var = %0.1f%%',sum(explained(1:npcs))))

% PC weight matrix
W = gmclt.pca.coeff.^2;
figure('name','PC weights')
for i = 1:36
    subplot(6,6,i)
    bar(W(:,i));
    title(sprintf('PC %d',i))
end

%% Cluster using k-means

ks_kmeans = 1:50; % k values to iterate
usepca = 0;  % Use pca scores?

options = statset('kmeans');
options = statset(options,'UseParallel','always','MaxIter',500);

if usepca
    X = X_all_pcs_z;
else
    X = X_all_z;
end

gmclt.kmeans = [];
h = waitbar(0,'Iterating K');
for i = 1:length(ks_kmeans)
    waitbar(i/length(ks_kmeans),h,'Iterating K');
    
    k = ks_kmeans(i);
    
    [Idxk, Centk, Sumdistk, Dk] = kmeans(X, k, ....
        'distance','correlation','replicates',5,'options',options);
    
    gmclt.kmeans(i).k = k;
    gmclt.kmeans(i).Idxk = Idxk;
    gmclt.kmeans(i).Centk = Centk;
    gmclt.kmeans(i).Sumdistk = Sumdistk;
    gmclt.kmeans(i).Dk = Dk;
    
    for c = 1:k
        gmclt.kmeans(i).clustersizes(c) = sum(Idxk==c);
    end
end
close(h);

%%  Define K for kmeans

ks_kmeans = [gmclt.kmeans.k];
sse_kmeans = nansum(catpad(2,gmclt.kmeans.Sumdistk),1);
kopt_kmeans_ix = knee_pt(sse_kmeans);
kopt_kmeans = ks_kmeans(kopt_kmeans_ix);

figure('name','K determination - k-means - elbow method')
plot(ks_kmeans,sse_kmeans); hold on
plot(ks_kmeans(kopt_kmeans_ix),sse_kmeans(kopt_kmeans_ix),'ro');
text(ks_kmeans(kopt_kmeans_ix),sse_kmeans(kopt_kmeans_ix),sprintf('K = %d',kopt_kmeans));
xlabel('K')
ylabel('SSE')

%%  Cluster using GMM

gmmopt = statset('Display','off','MaxIter',500);

ks_gmm = 1:50; % k values to iterate
usepca = 1;  % Use pca scores?

if usepca
    X = X_all_pcs_z;
else
    X = X_all_z;
end

gmclt.gmm = [];
h = waitbar(0,'Iterating K');
for i = 1:length(ks_gmm)
    waitbar(i/length(ks_gmm),h,'Iterating K');
    
    k = ks_gmm(i);
    gmclt.gmm(i).GMModel = fitgmdist(X,k,'CovarianceType','full', ...
        'SharedCovariance',false,'Replicates',5,'Options',gmmopt, ...
        'RegularizationValue',0.01);
    gmclt.gmm(i).k = k;
    gmclt.gmm(i).Idxk = cluster(gmclt.gmm(i).GMModel,X);
end
close(h);

%%  Define K for gmm

ks_gmm = [gmclt.gmm.k];

AICk = NaN(size(gmclt.gmm,2),1);
for i = 1:size(AICk,1)
    AICk(i) = gmclt.gmm(i).GMModel.AIC;
end
kopt_gmm_aic_ix = knee_pt(AICk);
kopt_gmm_aic = ks_gmm(kopt_gmm_aic_ix);

BICk = NaN(size(gmclt.gmm,2),1);
for i = 1:size(BICk,1)
    BICk(i) = gmclt.gmm(i).GMModel.BIC;
end
[~,kopt_gmm_bic_ix] = min(BICk);
kopt_gmm_bic = ks_gmm(kopt_gmm_bic_ix);

fig = figure('name','GMModel''s Information criteria');
plot([gmclt.gmm.k],AICk);
hold on
plot(kopt_gmm_aic,AICk(kopt_gmm_aic_ix),'ko');
text(kopt_gmm_aic+2,AICk(kopt_gmm_aic_ix),sprintf('K = %d',kopt_gmm_aic));
ylabel('AIC')
yyaxis right
plot([gmclt.gmm.k],BICk);
hold on
plot(kopt_gmm_bic,BICk(kopt_gmm_bic_ix),'ko');
text(kopt_gmm_bic+2,BICk(kopt_gmm_bic_ix),sprintf('K = %d',kopt_gmm_bic));
ylabel('BIC')
xlabel('K')
title('GMModel''s Information criteria')

%% Choose which k and algorithm & sort clusters by hierarchical order and by distance to cluster centroid

clstalg = 1;    % 1 - kmeans; 2 - gmm
if clstalg == 1
    kopt = kopt_kmeans;
    kopt_ix = kopt_kmeans_ix;
elseif clstalg == 2
    kopt = kopt_gmm_bic;
    kopt_ix = kopt_gmm_bic_ix;
    %     kopt = kopt_gmm_aic;
    %     kopt_ix = kopt_gmm_aic_ix;
end

if clstalg == 1
    Idxk = gmclt.kmeans(kopt_ix).Idxk;
    Centk = gmclt.kmeans(kopt_ix).Centk;
    Sumdistk = gmclt.kmeans(kopt_ix).Sumdistk;
    Dk = gmclt.kmeans(kopt_ix).Dk;
elseif clstalg == 2
    Idxk = gmclt.gmm(kopt_ix).Idxk;
    Centk = gmclt.gmm(kopt_ix).GMModel.mu;
    Dk = posterior(gmclt.gmm(kopt_ix).GMModel,X);
end

Yk = pdist(Centk,'correlation');
Zk = linkage(Yk,'average');
fig = figure('Name','k means : Hierachical clustering of Centroids');
[H,~,op] = dendrogram(Zk,0,'Orientation','right');
ylabel('Cluster');
xlabel('Linkage (Unweighted average distance)')
axis square

c_horderk = flip(op,2); % hierarchical order of cluster Centroids

X_clustk = []; % Original data sorted into clusters, in cluster order (c_horder)
Idx_horderk = []; % vector of cluster identities in cluster order (c_horder)
roixx_clustk = []; % ROI indices (not labels) in in cluster order (c_horder)
Idxk_all = [];
for c = c_horderk
    niconvix = find(Idxk == c);
    if clstalg == 1
        [~,six] = sort(Dk(:,c),'ascend');
    elseif clstalg == 2
        [~,six] = sort(Dk(:,c),'descend');
    end
    sixx = intersect(six,niconvix,'stable');
    
    Idxk_all = cat(1,Idxk_all,sixx);
    X_clustk = [X_clustk; X_all_z(sixx,:)];
    Idx_horderk = [Idx_horderk; c.*ones(length(sixx),1)];
    roixx_clustk = [roixx_clustk; sixx];
end
fish_horderk = Xinfo.fish(Idxk_all);
cs_zbb_horderk = roiszbbC(Idxk_all);

%% Plot clusters matrix

imrange = [-3 3];
bannerlen = ceil(vsz/10);
nbanners = 3;

% Rois to analyse
% rois2a = 1:nrois;

for l = 63
    rois2a = find(zbbmsk(:,l) == 1);
    if length(rois2a) >= 50
        nrois2a = length(rois2a);
        
        vlabs = {'LF','DF','DS-CCW','DS-CW','DS-CCW (AT)','DS-CW (AT)', ...
            'Cluster','Fish','Z-pos (ZBB)'};
        
        % Visstim timings
        load(fullfile(folders{1},'gm'),'gm');
        load(fullfile(folders{1},'gmv'),'gmv');
        vST = gm.trfr + 2;
        vED = gm.trfr + 1 + ceil(1000.*gmv.vistypz(:,5)./gm.frtime);
        
        % Build matrices
        roisix = intersect(Idxk_all,rois2a,'stable');
        
        X_rois2a = X_all_z(roisix,:);
        Idxk_rois2a = Idxk(roisix);
        Idx_bannerk_rois2a = repmat(scaleif(Idxk_rois2a,imrange(1),imrange(2)),[1 bannerlen]);
        
        fish_rois2a = Xinfo.fish(roisix);
        if length(unique(fish_rois2a)) > 3
            fish_bannerk_rois2a = repmat(scaleif(fish_rois2a,imrange(1),imrange(2)),[1 bannerlen]);
            
            zcs_zbb_rois2a = roiszbbC(roisix);
            zcs_zbb_bannerk_rois2a = repmat(scaleif(zcs_zbb_rois2a,imrange(1),imrange(2)),[1 bannerlen]);
            
            cedg_rois2a = find(diff(Idxk_rois2a) ~= 0);
            
            fig = figure('name',sprintf('Clustered matrix - %s',ZBBMaskDatabaseNames{l}), ...
                'position',[148,84,1662,903]);
            if rsz > 0 && msz > 0
                subplot(1,6,1:3)
            end
            imagesc([X_rois2a(:,1:vsz) ...
                Idx_bannerk_rois2a fish_bannerk_rois2a zcs_zbb_bannerk_rois2a], ...
                imrange)
            ylabel('ROIs')
            hold on
            for i = 1:kopt
                c = c_horderk(i);
                sr = find(Idxk_rois2a==c);
                if any(sr)
                    text(floor(vsz+1+bannerlen/2),((max(sr)-min(sr))/2)+min(sr),num2str(c), ...
                        'HorizontalAlignment','center')
                end
            end
            line([0 vsz+bannerlen*nbanners+bannerlen/2], ...
                [cedg_rois2a, cedg_rois2a],'Color','k','LineWidth',2)
            if vvec == 1
                for i = 1:length(vED)
                    line([gm.nfr gm.nfr].*(i-1),[0 nrois2a],'Color','k','LineStyle','-');
                    line([vST vST]+gm.nfr.*(i-1),[0 nrois2a],'Color','b','LineStyle','--');
                    line([vED(i) vED(i)]+gm.nfr.*(i-1),[0 nrois2a],'Color','r','LineStyle','--');
                end
            end
            colormap(flip(brewermap(255,'RdBu'),1));
            title('Visual stimuli')
            if vvec == 1
                set(gca,'XTick',[110/2:110:vsz vsz+bannerlen/2:bannerlen:vsz+bannerlen*nbanners], ...
                    'XTickLabel',vlabs, ...
                    'XTickLabelRotation',45,'Box','off','TickDir','out')
            elseif vvec == 2
                set(gca,'XTick',[1:vsz floor(vsz+1+bannerlen/2):bannerlen:vsz+bannerlen*nbanners], ...
                    'XTickLabel',vlabs, ...
                    'XTickLabelRotation',45,'Box','off','TickDir','out')
            end
            cbar = colorbar;
            ylabel(cbar,'Z-scored F')
            
            if rsz > 0
                subplot(1,6,4:5)
                imagesc(X_rois2a(:,vsz+1:vsz+rsz),[-0.2 0.2])
                line([0 rsz+1],[cedg_rois2a, cedg_rois2a],'Color','k','LineWidth',2)
                title('Regressors')
                set(gca,'XTick',1:rsz,'XTickLabel',gmROIfvec.reg_names, ...
                    'XTickLabelRotation',45,'Box','off','TickDir','out','YTick',{})
                cbar2 = colorbar;
                ylabel(cbar2,'{\rho}')
            end
            
            if msz > 0
                subplot(1,6,6)
                imagesc(X_rois2a(:,end),[-1 1])
                line([0 msz+1],[cedg_rois2a, cedg_rois2a],'Color','k','LineWidth',2)
                title('Modulation')
                set(gca,'XTick',1:msz,'XTickLabel',{'Conv'}, ...
                    'XTickLabelRotation',45,'Box','off','TickDir','out','YTick',{})
                cbar3 = colorbar;
                ylabel(cbar3,'{\Delta} Mean Z-scored F')
            end
        end
    end
    %     saveas(fig, ...
    %         ['\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\analysis\clustering\kmeans\180602\figures\', ...
    %         sprintf('Visual_responses_sorted_%s.fig',ZBBMaskDatabaseNames{l})]);
    %     close(fig)
end

%% T-SNE visualization

P = [20 30 50 100];
E = [4 20 50 200];
L = [500 2000];
Ytsne  = cell(length(P),length(E),length(L));
for p = 1:length(P)
    for e = 1:length(E)
        for l = 1:length(L)
            Ytsne{p,e,l} = tsne(X_all_pcs_z,'Distance','correlation','Verbose',1, ...
                'NumDimensions',2,'Perplexity',P(p),'Exaggeration',E(e), ...
                'LearnRate',L(l),'Algorithm','barneshut','Standardize',false);
        end
    end
end

%% Plot tsne

for p = 1:length(P)
    figure
    k = 1;
    for l = 1:length(L)
        for e = 1:length(E)
            subplot(length(L),length(E),k)
            scatter(Ytsne{p,e,l}(:,1),Ytsne{p,e,l}(:,2),[],Idxk,'filled');
            title(sprintf('P = %d, E = %d, L = %d',P(p),E(e),L(l)))
            axis square
            alpha(0.01)
            k = k+1;
        end
    end
end


%%  Plot mean centroid vectors

yspace = 3;

M = NaN(kopt,vsz,2);
for i = 1:kopt
    c = op(i);
    ix = Idxk == c;
    M(i,:,1) = nanmean(X_all_z(ix,:),1);
    M(i,:,2) = nansem(X_all_z(ix,:),1);
end

figure
imagesc(M(:,:,1),[-3 3])
colormap(flip(brewermap(255,'RdBu'),1));
if vvec == 1
    for i = 1:length(vED)
        line([gm.nfr gm.nfr].*(i-1),[0 kopt+0.5],'Color','k','LineStyle','-');
        line([vST vST]+gm.nfr.*(i-1),[0 kopt+0.5],'Color','b','LineStyle','--');
        line([vED(i) vED(i)]+gm.nfr.*(i-1),[0 kopt+0.5],'Color','r','LineStyle','--');
    end
    set(gca,'XTick',[110/2:110:vsz vsz+bannerlen/2:bannerlen:vsz+bannerlen*nbanners], ...
        'XTickLabel',vlabs, ...
        'XTickLabelRotation',45,'Box','off','TickDir','out', ...
        'YTick',1:kopt,'YTickLabel',op, ...
        'YDir','normal','Box','off','TickDir','out')
    ylabel('Cluster')
    cbar = colorbar;
    ylabel(cbar,'zF')
end

figure
plot(M(:,:,1)'+[0:yspace:(kopt-1)*yspace],'k')
if vvec == 1
    for i = 1:length(vED)
        if i > 1
            line([gm.nfr gm.nfr].*(i-1),[0 (kopt-1)*yspace+yspace],'Color','k','LineStyle','-');
        end
        line([vST vST]+gm.nfr.*(i-1),[0 (kopt-1)*yspace+yspace],'Color','b','LineStyle','--');
        line([vED(i) vED(i)]+gm.nfr.*(i-1),[0 (kopt-1)*yspace+yspace],'Color','r','LineStyle','--');
    end
    set(gca,'XTick',110/2:110:vsz, ...
        'XTickLabel',vlabs(1:6), ...
        'XTickLabelRotation',45,'Box','off','TickDir','out', ...
        'YTick',0:yspace:(kopt-1)*yspace, ...
        'YTickLabel',op)
    xlim([0 vsz])
    xlabel('Stimuli')
    ylabel('Cluster')
end

%% Extract conv hitrate Z values

% Z = [];
% h = waitbar(0,'Init');
% for f = 1:nfolders
%     waitbar(f/length(folders),h,'Processing')
%     datadir = folders{f};
%     load(fullfile(datadir,'CvR'));
%     spZ = ([CvR.rois.hitrate_off_conv] - ...
%         [CvR.rois.hitrate_on_shuffle_mean]) ./ ...
%         [CvR.rois.hitrate_on_shuffle_std];
%     zpre = [CvR.rois.Z_on_pre];
%     zpost = [CvR.rois.Z_on_post];
%     Z = cat(1,Z, ...
%         cat(2,[CvR.rois.Z_on_conv]', ...
%         spZ',zpre',zpost'));
%     clear CvR
% end
% Z = Z(~nanixall,:);
% close(h);

%% Extract convmod Z values

Z = [];
h = waitbar(0,'Init');
for f = 1:nfolders
    waitbar(f/length(folders),h,'Processing')
    datadir = folders{f};
    load(fullfile(datadir,'gmConvMod'));
    Z = cat(1,Z, ...
        cat(2, ...
        [gmConvMod.rois.Z_on_Lconv_Mn]',[gmConvMod.rois.Z_on_Rconv_Mn]', ...
        [gmConvMod.rois.Z_off_Lconv_Mn]',[gmConvMod.rois.Z_off_Rconv_Mn]', ...
        cellfun(@nanmean,{gmConvMod.rois.Z_on})', ...
        cellfun(@nanmean,{gmConvMod.rois.Z_off})'));
    clear gmConvMod
end
Z = Z(~nanixall,:);
close(h);

%%  Mean Z per cluster ID

zbbix = 82;
MZ = NaN(kopt,2,size(Z,2));
for i = 1:kopt
    ix = zbbmsk(:,zbbix) == 1 & Idxk == i;
    MZ(i,1,:) = nanmean(Z(ix,:),1);
    MZ(i,2,:) = nansem(Z(ix,:),1);
end

Y = [MZ(op,1,1) MZ(op,1,2)];
Ye= [MZ(op,2,1) MZ(op,2,2)];
errorbar_groups_ph(Y',Ye');
set(gca,'XTickLabel',op,'Box','off','TickDir','out')
ylim([0 inf])
title('Conv ON')
ylabel('Z conv')
xlabel('Cluster')
legend Left Right

Y = [MZ(op,1,3) MZ(op,1,4)];
Ye= [MZ(op,2,3) MZ(op,2,4)];
errorbar_groups_ph(Y',Ye');
set(gca,'XTickLabel',op,'Box','off','TickDir','out')
ylim([0 inf])
title('Conv OFF')
ylabel('Z conv')
xlabel('Cluster')
legend Left Right

%%  Mean Z per superclusters

% For 21 clusters kmeans
% SC = {[20,12,21,6], ... % Spot responsive
%     [25 18 24 5], ...   % Spot-off sustained responsive
%     [19 14 23 11], ...  % Dark-flash-off sustained responsive
%     [22 13 9 1], ...  % Light-flash-off sustained responsive
%     [26 15 4], ...  % Luminance OFF responsive
%     [27 3 10 16 7]};  % Luminance ON responsive

% For 14 clusters kmeans
SC = {[12 7 11 3], ...   % Spot-off sustained responsive
    [4 13 2], ... % Spot responsive
    [14 1], ...  % Luminance OFF responsive
    [8 6 5]}; % Luminance ON responsive

nsc = length(SC);

zbbix = [82 83];
MZ = [];
for i = 1:length(zbbix)
    M = [];
    for j = 1:nsc
        ix = zbbmsk(:,zbbix(i)) == 1 & ismember(Idxk,SC{j});
        M = catpad(2,M,Z(ix,1));
    end
    MZ = catpad(3,MZ,M);
end
MZ(:,:,1) = [];

MM = squeeze(nanmean(MZ,1));    % Mean
ME = squeeze(nansem(MZ,1));    % Error
if length(zbbix) > 1
    MM = MM';
    ME = ME';
end

% Bar plot
[~,bxtick,~,~] = errorbar_groups_ph(MM,ME);

% Statistical tests
if length(zbbix) >= 2
    for i = 1:nsc
        for j = 1:length(zbbix)-1
            for l = 2:length(zbbix)
                ix1 = zbbmsk(:,zbbix(j)) == 1 & ismember(Idxk,SC{i});
                ix2 = zbbmsk(:,zbbix(l)) == 1 & ismember(Idxk,SC{i});
                p = ranksum(Z(ix1,1),Z(ix2,1));
                sigstar_ph([bxtick(j,i) bxtick(l,i)],p)
            end
        end
    end
end
xlabel('Super Clusters')
ylabel('Conv Z')
legend(ZBBMaskDatabaseNames{zbbix},'location','southoutside')

%% Scatter all supercluster centroids

SC = {[12 7 11 3], ...   % Spot-off sustained responsive
    [4 13 2], ... % Spot responsive
    [14 1], ...  % Luminance OFF responsive
    [8 6 5]}; % Luminance ON responsive

col = flip(brewermap(length(SC),'RdBu'),1);

intscale = 0.25;
zbbix = 78;
mskbplt = [82 83];

figure('name','All centroids')
for i = 1:length(SC)
    tix = SC{i}; % find cluster id (position in T vector)
    cix = ismember(Idxk,tix);
    ix = zbbmsk(:,zbbix) & cix;
    scatter(roiszbbC(ix,1),roiszbbC(ix,2),8,col(i,:),'filled')
    hold on
end

% Plot boundaries
for j = 1:length(mskbplt)
    if j == 1
        mskrange = round(min(roiszbbC(zbbmsk(:,mskbplt(j)),3))): ...
            round(max(roiszbbC(zbbmsk(:,mskbplt(j)),3)));
    end
    msk = reshape(full(ZBBMaskDatabase(:,mskbplt(j))),[616,1030,420]);
    msk = msk(:,:,mskrange);
    if j == 1
        [~,mskix] = max(squeeze(sum(sum(msk,1),2)));
    end
    
    % Smooth mask
    M = msk(:,:,mskix);
    F = griddedInterpolant(double(M));
    [sx,sy,sz] = size(M);
    
    xq = (0:intscale:sx)';
    yq = (0:intscale:sy)';
    zq = (0:intscale:sz)';
    
    I = imgaussfilt(F({xq,yq}),3) > 0.5;
    mskB = bwboundaries(I);
    for m = 1:size(mskB,1)
        mskB{m,1} = mskB{m,1}.*intscale;
    end
    
    % Plot boundaries
    for l = 1:length(mskB)
        boundary = mskB{l};
        plot(boundary(:,2), boundary(:,1),'-', ...
            'Color','k','LineWidth',1);
    end
end

set(gca,'ZDir','reverse','YDir','reverse')
axis equal off
alpha 0.1
view([90 90])

%% Plot superclusters by filtering ROIs

SC = {[12 7 11 3], ...   % Spot-off sustained responsive
    [4 13 2], ... % Spot responsive
    [14 1], ...  % Luminance OFF responsive
    [8 6 5]}; % Luminance ON responsive

% SC = num2cell(op);

col = flip(brewermap(length(SC),'RdBu'),1);

zbbmskix = [78];    % ZBB mask ID to analyse
mskbplt = {[82 83];[33 34 12]}; % Mask boundaries to plot
plotboundaries = 1; % Plot mask boundaries
binspace = 1; % microns
binoffset = 20; % microns

roisix = sum(zbbmsk(:,zbbmskix),2) > 0;

% Bin edges
edgx = floor(min(roiszbbC(:,1)))-binoffset:binspace:ceil(max(roiszbbC(:,1)))+binoffset;
edgy = floor(min(roiszbbC(:,2)))-binoffset:binspace:ceil(max(roiszbbC(:,2)))+binoffset;

[N,Xedges,Yedges,binX,binY] = histcounts2(roiszbbC(:,1),roiszbbC(:,2),edgx,edgy);
I = ones([size(N),3]);
for i = 1:size(N,1)
    for j = 1:size(N,2)
        ix = binX == i & binY == j;
        if any(ix)
            cix = Idxk(ix);
            C = NaN(length(cix),1);
            for m = 1:length(cix)
                for s = 1:length(SC)
                    if any(ismember(SC{s},cix(m)))
                        C(m) = s;
                    end
                end
            end
            C = C(~isnan(C));
            if any(C)
                I(i,j,:) = nanmean(col(C,:),1);
            end
        end
    end
end

%%

% Filter
fltsz = 1; 
intscale = 0.25;
If = I;
for i = 1:size(I,3)
        If(:,:,i) = imgaussfilt(If(:,:,i),fltsz);
end

imshow(If)
set(gca,'XDir','reverse');
        axis off equal

% Plot boundaries
if plotboundaries
    hold on
    for n = 1:size(mskbplt,1)
        for j = 1:length(mskbplt{n,1})
            if j == 1
                mskrange = round(min(roiszbbC(zbbmsk(:,mskbplt{n}(j)),3))): ...
                    round(max(roiszbbC(zbbmsk(:,mskbplt{n}(j)),3)));
            end
            msk = reshape(full(ZBBMaskDatabase(:,mskbplt{n}(j))),[616,1030,420]);
            msk = msk(:,:,mskrange);
            if j == 1
%                 [~,mskix] = max(squeeze(sum(sum(msk,1),2)));
                    mskix = round(mean(1:length(mskrange)));
            end
            
            % Smooth mask
            M = msk(:,:,mskix);
            F = griddedInterpolant(double(M));
            [sx,sy,sz] = size(M);
            
            xq = (0:intscale:sx)';
            yq = (0:intscale:sy)';
            zq = (0:intscale:sz)';
            
            Mf = imgaussfilt(F({xq,yq}),3) > 0.5;
            mskB = bwboundaries(Mf);
            for m = 1:size(mskB,1)
                mskB{m,1} = mskB{m,1}.*intscale;
            end
            
            % Plot boundaries
            for l = 1:length(mskB)
                boundary = [mskB{l}-[edgy(1) edgx(1)]]./binspace;
                plot(boundary(:,1), boundary(:,2),':', ...
                    'Color','k','LineWidth',1.5);
            end
        end
    end
end

%% Scatter super clusters separatly

load('\\128.40.168.141\data\Pedro\NI_project\Paper\Paper figures\Figure 2\data\surfaces.mat');

% For 14 clusters kmeans
SC = {[12 7 11 3], ...   % Spot-off sustained responsive
    [4 13 2], ... % Spot responsive
    [14 1], ...  % Luminance OFF responsive
    [8 6 5]}; % Luminance ON responsive

% chata lims
axlims = [480 525 385 435; ...
    480,525,180,230];
% vachtblims
% axlims = [490 545 385 455; ...
%     490 545 165 235];

zbbix = [82 83];   % ZBB mask indexes
sideix = [80 81];
sidelabs = {'Left','Right'};
sepcol = 0;
plotpatch = 0;
intscale = 0.25;
for s = 1:2
    for i = 1:length(SC)
        tix = SC{i}; % find cluster id (position in T vector)
        cix = ismember(Idxk,tix);
        
        figure
        % Separate by color
        if sepcol
            for j = 1:length(zbbix)
                ix = zbbmsk(:,zbbix(j)) & ...
                    zbbmsk(:,sideix(s)) == 1 & ...
                    cix;
                scatter(roiszbbC(ix,1),roiszbbC(ix,2),100,'filled')
                hold on
            end
        else
            ix = any(zbbmsk(:,zbbix),2) & ...
                zbbmsk(:,sideix(s)) == 1 & ...
                cix;
            scatter(roiszbbC(ix,1),roiszbbC(ix,2),100,'filled', ...
                'MarkerFaceAlpha',0.6)
            hold on
        end
        
        for j = 1:length(zbbix)
            if j == 1
                mskrange = round(min(roiszbbC(zbbmsk(:,zbbix(j)),3))): ...
                    round(max(roiszbbC(zbbmsk(:,zbbix(j)),3)));
            end
            msk = reshape(full(ZBBMaskDatabase(:,zbbix(j))),[616,1030,420]);
            msk = msk(:,:,mskrange);
            if j == 1
                [~,mskix] = max(squeeze(sum(sum(msk,1),2)));
            end
            
            % Smooth mask
            M = msk(:,:,mskix);
            F = griddedInterpolant(double(M));
            [sx,sy,sz] = size(M);
            
            xq = (0:intscale:sx)';
            yq = (0:intscale:sy)';
            zq = (0:intscale:sz)';
            
            I = imgaussfilt(F({xq,yq}),3) > 0.5;
            mskB = bwboundaries(I);
            for m = 1:size(mskB,1)
                mskB{m,1} = mskB{m,1}.*intscale;
            end
            
            % Plot boundaries
            for l = 1:length(mskB)
                boundary = mskB{l};
                plot(boundary(:,2), boundary(:,1),'-', ...
                    'Color','k','LineWidth',1);
            end
        end
        
        if plotpatch
            p1 = patch('Faces',chata_surf.faces, ...
                'Vertices',chata_surf.vertices, ...
                'FaceColor','k', ...
                'EdgeColor','none', ...
                'FaceAlpha',0.05);
            
            p2 = patch('Faces',vachtb_surf.faces, ...
                'Vertices',vachtb_surf.vertices, ...
                'FaceColor','k', ...
                'EdgeColor','none', ...
                'FaceAlpha',0.05);
        end
        
        set(gca,'ZDir','reverse','YDir','reverse')
        axis equal off
        axis(axlims(s,:))
        view([90 90])
        title(sprintf('%s side - Super Cluster %d (clusters: %s)', ...
            sidelabs{s},i,num2str(SC{i})))
    end
end

%% aNI/pNI asymmetry index per super cluster

% For 14 clusters kmeans
SC = {[12 7 11 3], ...   % Spot-off sustained responsive
    [4 13 2], ... % Spot responsive
    [14 1], ...  % Luminance OFF responsive
    [8 6 5]}; % Luminance ON responsive

zbbix = [82 83];    % anterior and posterior NI indexes
sideix = [80 81];   % side ix (left right)

A = NaN(length(SC),2);
for s = 1:2
    for i = 1:length(SC)
        tix = SC{i}; % find cluster id (position in T vector)
        cix = ismember(Idxk,tix);
        
        ixa = zbbmsk(:,zbbix(1)) & cix & zbbmsk(:,sideix(s)) == 1;
        ixat = zbbmsk(:,zbbix(1)) & zbbmsk(:,sideix(s)) == 1;
        ixp = zbbmsk(:,zbbix(2)) & cix & zbbmsk(:,sideix(s)) == 1;
        ixpt = zbbmsk(:,zbbix(2)) & zbbmsk(:,sideix(s)) == 1;
        
        ani_f = sum(ixa)/sum(ixat);
        pni_f = sum(ixp)/sum(ixpt);
        A(i,s) = (ani_f-pni_f)/(ani_f + pni_f);
    end
end

figure
bar(A)
ylim([-0.6 0.6])
ylabel('Asymetry index')
xlabel('Super cluster')
set(gca,'Box','off','TickDir','out')
axis square
legend Left Right

%% Mean Z conv per super cluster

zbbix = [82 83];
splitg = 0; % Split aNI and pNI

for i = 5:6
    M = cell(length(zbbix),1);
    for m = 1:length(zbbix)
        for j = 1:length(SC)
            ix = zbbmsk(:,zbbix(m)) == 1 & ismember(Idxk,SC{j});
            M{m,1} = catpad(2,M{m,1},Z(ix,i));
        end
    end
    
    if splitg
        X = [nanmean(M{1}); nanmean(M{2})];
        Xe = [nansem(M{1}); nansem(M{2})];
        errorbar_groups_ph(X,Xe)
        legend aNI pNI
    else
        X = nanmean(cat(1,M{1},M{2}));
        Xe = nansem(cat(1,M{1},M{2}));
        figure
        bar(X')
        hold on
        errorbar(X',Xe','.')
    end
    
    ylabel('Z Conv')
    xlabel('Super clusters')
    set(gca,'Box','off','TickDir','out')
    if i == 5
        title('Conv ON')
    else
        title('Conv OFF')
    end
end


%%  Plot all centroids

col = brewermap(kopt,'RdBu');
[~,colia] = ismember(Idxk,op);

figure('name','All centroids')
scatter3(roiszbbC(:,1),roiszbbC(:,2),roiszbbC(:,3),8,col(colia,:),'filled')

% p3 = patch('Faces',chata_surf.faces, ...
%     'Vertices',chata_surf.vertices, ...
%     'FaceColor','k', ...
%     'EdgeColor','none', ...
%     'FaceAlpha',1);

set(gca,'ZDir','reverse','YDir','reverse')
axis equal off
% axis([200 650 100 520 70 250]);

%%  Plot centroids of redefined clusters by cutting the dendogram

ncls = 28;   % Number of desired cluster
savefigs = 0;
T = cluster(Zk,'maxclust',6);

[H2,~,op2] = dendrogram(Zk,0,'Orientation','right','ColorThreshold',0.95);
ylabel('Cluster');
axis square

% Get cluster colors according to dendogram
yd = cat(1,H2.YData);
hc = cat(1,H2.Color);
col = NaN(kopt,3);
for i = 1:kopt
    [r,~] = find(yd == i);
    col(i,:) = hc(r(1),:);
end
if size(unique(col,'rows'),1) ~= ncls
    col = repmat([0 0 1],kopt,1);
end

% nilims = [480 525 385 435; ...
%     480,525,180,230];

zbbix = [82];   % ZBB mask indexes
sideix = [80 81];
sidelabs = {'Left','Right'};
for s = 1:2
    for i = 1:ncls
        tix = find(T == i); % find cluster id (position in T vector)
        cix = ismember(Idxk,tix);
        ix = sum(zbbmsk(:,zbbix),2) > 0 & ...
            zbbmsk(:,sideix(s)) == 1 & ...
            cix;
        color = col(ismember(op,tix(1)),:);
        fig = figure;
        scatterhist(roiszbbC(ix,1),roiszbbC(ix,2), ...
            'Kernel','on','Marker','*','Color',color,'legend','on')
        
        p1 = patch('Faces',chata_surf.faces, ...
            'Vertices',chata_surf.vertices, ...
            'FaceColor','k', ...
            'EdgeColor','none', ...
            'FaceAlpha',0.05);
        
        set(gca,'ZDir','reverse','YDir','reverse')
        axis equal off
        %         axis(nilims(s,:))
        view([90 90])
        title(sprintf('%s side - Super Cluster %d (%s)',sidelabs{s},i,num2str(tix)))
        
        if savefigs
            saveas(fig, ...
                ['\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\analysis\clustering\kmeans\180602\figures\ROI_scatter\', ...
                sprintf('ROI_scatter_%d_super_clusters_C%d_%sNI.fig',ncls,i,sidelabs{s})]);
            close(fig)
        end
    end
end

%%  Cluster histograms by ZBB area

savefig = 0;
[~,Lia_all] = ismember(Idxk,op);

for l = [12,33,34,76,82,83]
    ix = zbbmsk(:,l) == 1;
    if sum(ix) >= 50
        [~,Lia] = ismember(Idxk(ix),op);
        fig = figure('position',[353,108,754,643]);
        histogram(Lia,0.5:kopt+0.5,'Normalization','probability')
        hold on
        histogram(Lia_all,0.5:kopt+0.5,'Normalization','probability')
        set(gca,'XTick',1:kopt,'XTickLabel',op,'Box','off','TickDir','out')
        xlabel('Cluster ID')
        ylabel('probability')
        legend({ZBBMaskDatabaseNames{l},'All ROIs'})
        axis square
        if savefig
            saveas(fig, ...
                ['\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\analysis\clustering\kmeans\180602\figures\Clusters_histograms\' ...
                sprintf('Clusters_hist_%s.fig',ZBBMaskDatabaseNames{l})]);
            close(fig)
        end
    end
end

%% Compute distance between ZBB areas by their cluster probabilities

M = NaN(nmsks,kopt);
for m = 1:nmsks
    ix = zbbmsk(:,m) == 1;
    if sum(ix) >= 50
        [~,Lia] = ismember(Idxk(ix),op);
        M(m,:) = histcounts(Lia,0.5:kopt+0.5,'Normalization','probability');
    end
end

ix = find(~isnan(M(:,1)));
YkC = pdist(M(ix,:),'euclidean');
ZkC = linkage(YkC,'average');
fig = figure('Name','Hierachical clustering of cluster probabilities');
[HC,~,opC] = dendrogram(ZkC,0,'Orientation','right','Labels',ZBBMaskDatabaseNames(ix));
xlabel('Linkage (Unweighted average distance)')
axis square

figure
imagesc(M(ix(opC),:))
colormap hot
axis square
set(gca,'YDir','normal','YTick',1:length(ix), ...
    'YTickLabel',ZBBMaskDatabaseNames(ix(opC)), ...
    'XTick',1:kopt,'XTickLabel',op,'XDir','reverse', ...
    'Box','off','TickDir','out')
xlabel('Cluster')
ylabel('ZBB Mask')
cbar = colorbar;
ylabel(cbar,'Probability')

%%  Plot cluster centroids

col = brewermap(kopt,'RdBu');
[~,colia] = ismember(Idxk,op);

for i = 1:kopt
    niconvix = Idxk == i;
    figure
    %     color = col(colia(ix),:);
    color = 'b';
    scatter3(roiszbbC(niconvix,1),roiszbbC(niconvix,2),roiszbbC(niconvix,3),8,color,'filled')
    
    set(gca,'ZDir','reverse','YDir','reverse')
    axis equal off
    axis([200 650 100 520 70 250]);
    view([90 90]);
    title(sprintf('Cluster # %d',i))
end

%%  Cluster density by anatomical area

anatix = 63;
thisrois = zbbmsk(:,anatix) == 1;
anatkn = histcounts(Idxk(thisrois),0.5:kopt+0.5);
[anatkn_srt,anatkn_srtix] = sort(anatkn,'descend');

figure
niconvix = Idxk == anatkn_srtix(1);
scatter3(roiszbbC(niconvix,1),roiszbbC(niconvix,2),roiszbbC(niconvix,3),8,'filled')
hold on
niconvix = Idxk == anatkn_srtix(2);
scatter3(roiszbbC(niconvix,1),roiszbbC(niconvix,2),roiszbbC(niconvix,3),8,'filled')
% ix = Idxk == anatkn_srtix(3);
% scatter3(cs_zbb(ix,1),cs_zbb(ix,2),cs_zbb(ix,3),8,'filled')

set(gca,'ZDir','reverse','YDir','reverse')
axis equal

%%  Plot anatomy-masked centroids

col = brewermap(kopt,'RdBu');

anatix = 82:83;
for i = anatix
    thisrois = find(zbbmsk(:,i) == 1);
    if ~isempty(thisrois)
        col = brewermap(kopt,'RdBu');
        [~,colia] = ismember(Idxk(thisrois),op);
        
        figure('name',sprintf('%s',ZBBMaskDatabaseNames{i}))
        %         subplot(1,2,1)
        %         scatter3(roiszbbC(thisrois,1),roiszbbC(thisrois,2),roiszbbC(thisrois,3), ...
        %             8,col(colia,:),'filled')
        scatter(roiszbbC(thisrois,1),roiszbbC(thisrois,2), ...
            8,col(colia,:),'filled')
        axis off
        axis equal
        %         axis([200 650 100 520 70 250]);
        set(gca,'ZDir','reverse','YDir','reverse')
        title(sprintf('%s',ZBBMaskDatabaseNames{i}))
        %         subplot(1,2,2)
        %         histogram(Idxk,'Normalization','probability')
        %         hold on
        %         histogram(Idxk(thisrois),'Normalization','probability')
        %         axis square
        %         xlim([0.5 k+0.5])
    end
end

%% Plot centrois of NI cells

col = brewermap(kopt,'RdBu');
slab = {'Left','Right'};
anatix = 82:83;
for s = 1:2
    if s == 1
        six = roiszbbC(:,2) > 308;
    else
        six = roiszbbC(:,2) < 308;
    end
    figure
    for i = anatix
        thisrois = find(zbbmsk(:,i) == 1 & six);
        if ~isempty(thisrois)
            [~,colia] = ismember(Idxk(thisrois),op);
            scatter(roiszbbC(thisrois,1),roiszbbC(thisrois,2), ...
                100,col(colia,:),'filled')
            hold on
        end
    end
    axis off
    axis equal
    set(gca,'ZDir','reverse','YDir','reverse')
    alpha 0.5
    title(slab{s});
end

%%  Modulation maps

niconvix = md_pv(:,1) < alpha;

figure
scatter3(roiszbbC(niconvix,1),roiszbbC(niconvix,2),roiszbbC(niconvix,3),10,X_all(niconvix,end-1),'filled')
axis off
colormap(flip(brewermap(255,'RdBu'),1));
set(gca,'ZDir','reverse','YDir','reverse')
cbar = colorbar;
caxis([-2 2])
view([90 90]);
ylabel(cbar,'Modulation (Z)')

% p1 = patch('Faces',brain_surf.faces, ...
%     'Vertices',brain_surf.vertices, ...
%     'FaceColor','k', ...
%     'EdgeColor','none', ...
%     'FaceAlpha',.05);
%
% p2 = patch('Faces',af7_surf.faces, ...
%     'Vertices',af7_surf.vertices, ...
%     'FaceColor','k', ...
%     'EdgeColor','none', ...
%     'FaceAlpha',.05);
%
p3 = patch('Faces',chata_surf.faces, ...
    'Vertices',chata_surf.vertices, ...
    'FaceColor','k', ...
    'EdgeColor','none', ...
    'FaceAlpha',.05);

axis([200 650 100 520 70 250]);
axis equal