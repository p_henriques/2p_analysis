% Load previous save

load('Z:\Pedro\2P\NI\NI_Optoma\HuC_H2B_GC6s\Analysis\Clustering\loom_clust_kmeans_180903');

%%

folders = {
    'Z:\Pedro\2P\NI\NI_Optoma\HuC_H2B_GC6s\170414\f1';
    'Z:\Pedro\2P\NI\NI_Optoma\HuC_H2B_GC6s\170525\f1';
    'Z:\Pedro\2P\NI\NI_Optoma\HuC_H2B_GC6s\170525\f1_2'};

%%  Load Mask database names

zbbanatix = [2:3 6:34 44:46 48:74 77 113 305:306];
load('ZBBMaskDatabase');
ZBBMaskDatabaseNames = ZBBMaskDatabaseNames(zbbanatix);
ZBBMaskDatabase = ZBBMaskDatabase(:,zbbanatix);
nmsks = length(ZBBMaskDatabaseNames);

%%

datadir = folders{1};
load(fullfile(datadir,'gm'),'gm');
load(fullfile(datadir,'gmv'),'gmv');
load(fullfile(datadir,'gmb'),'gmb');

nfr = gm.nfr;   % Number of epoch frames
stims = [ ...
    5100,0,10,490;
    6100,0,10,490;
    78006,2,5,30;
    78007,2,5,30;
    79006,2,5,30;
    79007,2,5,30];
nstim = size(stims,1);
vsz = nstim*nfr;

vST = gm.trfr + 2;
vEDtot = gm.trfr + 1 + ceil(1000.*gmv.vistypz( ...
    ismember(gmv.vistypz(:,1:4),stims,'rows'),5)./gm.frtime);

gtix = 9;
ocuix = 26;
otix = 31;
chbix = 62;
aniix = 64;
pniix = 65;
ptix = 38;
tegix = 45;
otnix = 30;

imrange = [-5 5];
bannerlen = ceil(vsz/10);
nbanners = 3;

vlabs = {'Loom','Loom ctl','DS-CCW','DS-CW)','DS-CCW (Comp)','DS-CW (Comp)', ...
    'Cluster','Fish','Z-pos (ZBB)'};

%%

X_all = [];
Xinfo = table;
roiszbbC = [];
zbbmsk = [];
for f = 2:length(folders)
    datadir = folders{f};
    load(fullfile(datadir,'gmv'),'gmv');
    load(fullfile(datadir,'gm'),'gm');
    load(fullfile(datadir,'gmranat'),'gmranat');
    load(fullfile(datadir,'Xgc_all.mat'));
    
    nfr = gm.nfr;
    vix = find(ismember(gmv.vistypz(:,1:4),stims,'rows'));
    
    stfr = ((vix-1)*nfr)+1;
    X = [];
    for i = 1:nstim
        X = cat(2,X,Xgc_all(:,stfr(i):stfr(i)+nfr-1));
    end
    X_all = cat(1,X_all,X);
    
    % ZBB masks and centroids
    nzs = size(gmranat.z,2);
    ZM = [];    % Mask
    C = []; % Centroids
    for z = 1:nzs
        ZM = cat(1,ZM,cat(1,gmranat.z(z).STATScrop.Masks_ZBB));
        C = cat(1,C,cat(1,gmranat.z(z).STATScrop.Centroid_ZBB));
    end
    zbbmsk = cat(1,zbbmsk,ZM);
    roiszbbC = cat(1,roiszbbC,C);
    
    rois = 1:size(C,1);
    fish = ones(size(X,1),1).*f;
    Xinfo = cat(1,Xinfo, ...
        table(fish, rois'));
end
zbbmsk = logical(zbbmsk(:,zbbanatix));
X_all_z = zscore(X_all,[],2);
vsz = nstim*nfr;

%% PCA

[coeff,score,latent,tsquared,explained,mu] = pca(X_all_z,'Centered',false);
npcs = knee_pt(explained);
% npcs = findnearest(85,cumsum(explained));   % Use n pcs that explain 85% of variance

X_all_pcs = score(:,1:npcs);    % Get components
X_all_pcs_z = zscore(score(:,1:npcs),[],1); % z-score components

gmclt.pca.coeff = coeff;
gmclt.pca.score = score;
gmclt.pca.latent = latent;
gmclt.pca.tsquared = tsquared;
gmclt.pca.explained = explained;
gmclt.pca.mu = mu;
gmclt.pca.npcs = npcs;
gmclt.pca.X_all_pcs = X_all_pcs;

figure('name','PC explained variance')
plot(explained,'-o')
ylabel('Explained variance (%)')
xlabel('PC')
xlim([0 50])
yyaxis right
plot(cumsum(explained),'-o')
ylabel('Cumulative explained variance (%)')
grid on
line([npcs npcs],[0 100],'Color','k','LineStyle','--')
line([0 50],[sum(explained(1:npcs)) sum(explained(1:npcs))],'Color','k','LineStyle','--')
text(npcs+2,double(sum(explained(1:npcs))-4), ...
    sprintf('n = %d',npcs));
text(npcs+2,double(sum(explained(1:npcs))-8), ...
    sprintf('explained var = %0.1f%%',sum(explained(1:npcs))))

% PC weight matrix
W = gmclt.pca.coeff.^2;
figure('name','PC weights')
for i = 1:36
    subplot(6,6,i)
    bar(W(:,i));
    title(sprintf('PC %d',i))
end

%% Cluster using k-means

ks_kmeans = 1:50; % k values to iterate
usepca = 1;  % Use pca scores?

options = statset('kmeans');
options = statset(options,'UseParallel','always','MaxIter',500);

if usepca
    X = X_all_pcs_z;
else
    X = X_all_z;
end

gmclt.kmeans = [];
h = waitbar(0,'Iterating K');
for i = 1:length(ks_kmeans)
    waitbar(i/length(ks_kmeans),h,'Iterating K');
    
    k = ks_kmeans(i);
    
    [Idxk, Centk, Sumdistk, Dk] = kmeans(X, k, ....
        'distance','correlation','replicates',5,'options',options);
    
    gmclt.kmeans(i).k = k;
    gmclt.kmeans(i).Idxk = Idxk;
    gmclt.kmeans(i).Centk = Centk;
    gmclt.kmeans(i).Sumdistk = Sumdistk;
    gmclt.kmeans(i).Dk = Dk;
    
    for c = 1:k
        gmclt.kmeans(i).clustersizes(c) = sum(Idxk==c);
    end
end
close(h);

%%  Define K for kmeans

ks_kmeans = [gmclt.kmeans.k];
sse_kmeans = nansum(catpad(2,gmclt.kmeans.Sumdistk),1);
kopt_kmeans_ix = knee_pt(sse_kmeans);
kopt_kmeans = ks_kmeans(kopt_kmeans_ix);

figure('name','K determination - k-means - elbow method')
plot(ks_kmeans,sse_kmeans); hold on
plot(ks_kmeans(kopt_kmeans_ix),sse_kmeans(kopt_kmeans_ix),'ro');
text(ks_kmeans(kopt_kmeans_ix),sse_kmeans(kopt_kmeans_ix),sprintf('K = %d',kopt_kmeans));
xlabel('K')
ylabel('SSE')

%% Choose which k and algorithm & sort clusters by hierarchical order and by distance to cluster centroid

clstalg = 1;    % 1 - kmeans; 2 - gmm
if clstalg == 1
    kopt = kopt_kmeans;
    kopt_ix = kopt_kmeans_ix;
elseif clstalg == 2
    kopt = kopt_gmm_bic;
    kopt_ix = kopt_gmm_bic_ix;
    %     kopt = kopt_gmm_aic;
    %     kopt_ix = kopt_gmm_aic_ix;
end

if clstalg == 1
    Idxk = gmclt.kmeans(kopt_ix).Idxk;
    Centk = gmclt.kmeans(kopt_ix).Centk;
    Sumdistk = gmclt.kmeans(kopt_ix).Sumdistk;
    Dk = gmclt.kmeans(kopt_ix).Dk;
elseif clstalg == 2
    Idxk = gmclt.gmm(kopt_ix).Idxk;
    Centk = gmclt.gmm(kopt_ix).GMModel.mu;
    Dk = posterior(gmclt.gmm(kopt_ix).GMModel,X);
end

Yk = pdist(Centk,'correlation');
Zk = linkage(Yk,'average');
fig = figure('Name','k means : Hierachical clustering of Centroids');
[H,~,op] = dendrogram(Zk,0,'Orientation','right');
ylabel('Cluster');
xlabel('Linkage (Unweighted average distance)')
axis square

c_horderk = flip(op,2); % hierarchical order of cluster Centroids

X_clustk = []; % Original data sorted into clusters, in cluster order (c_horder)
Idx_horderk = []; % vector of cluster identities in cluster order (c_horder)
roixx_clustk = []; % ROI indices (not labels) in in cluster order (c_horder)
Idxk_all = [];
for c = c_horderk
    niconvix = find(Idxk == c);
    if clstalg == 1
        [~,six] = sort(Dk(:,c),'ascend');
    elseif clstalg == 2
        [~,six] = sort(Dk(:,c),'descend');
    end
    sixx = intersect(six,niconvix,'stable');
    
    Idxk_all = cat(1,Idxk_all,sixx);
    X_clustk = [X_clustk; X_all_z(sixx,:)];
    Idx_horderk = [Idx_horderk; c.*ones(length(sixx),1)];
    roixx_clustk = [roixx_clustk; sixx];
end
fish_horderk = Xinfo.fish(Idxk_all);
cs_zbb_horderk = roiszbbC(Idxk_all);

%% Plot clusters matrix

restrictcls = 1;
pltcls = [14,13,1,12,6];

% Rois to analyse
% rois2a = 1:nrois;

for l = [62,64,65]
    if restrictcls
        rois2a = find(zbbmsk(:,l) == 1 & ismember(Idxk,pltcls','rows'));
    else
        rois2a = find(zbbmsk(:,l) == 1);
    end
    
    if length(rois2a) >= 50
        nrois2a = length(rois2a);
        
        
        % Build matrices
        roisix = intersect(Idxk_all,rois2a,'stable');        
        
        X_rois2a = X_all_z(roisix,:);
        Idxk_rois2a = Idxk(roisix);
        Idx_bannerk_rois2a = repmat(scaleif(Idxk_rois2a,imrange(1),imrange(2)),[1 bannerlen]);
        
        fish_rois2a = Xinfo.fish(roisix);
        fish_bannerk_rois2a = repmat(scaleif(fish_rois2a,imrange(1),imrange(2)),[1 bannerlen]);
        
        zcs_zbb_rois2a = roiszbbC(roisix);
        zcs_zbb_bannerk_rois2a = repmat(scaleif(zcs_zbb_rois2a,imrange(1),imrange(2)),[1 bannerlen]);
        
        cedg_rois2a = find(diff(Idxk_rois2a) ~= 0);
        
        fig = figure('name',sprintf('Clustered matrix - %s',ZBBMaskDatabaseNames{l}), ...
            'position',[260,338,1230,472]);
        imagesc([X_rois2a(:,1:vsz) ...
            Idx_bannerk_rois2a fish_bannerk_rois2a zcs_zbb_bannerk_rois2a], ...
            imrange)
        ylabel('ROIs')
        hold on
        for i = 1:kopt
            c = c_horderk(i);
            sr = find(Idxk_rois2a==c);
            if any(sr)
                text(floor(vsz+1+bannerlen/2),((max(sr)-min(sr))/2)+min(sr),num2str(c), ...
                    'HorizontalAlignment','center')
            end
        end
        line([0 vsz+bannerlen*nbanners+bannerlen/2], ...
            [cedg_rois2a, cedg_rois2a]+0.5,'Color','k','LineWidth',2)
        for i = 1:length(vEDt)
            line([gm.nfr gm.nfr].*(i-1),[0 nrois2a],'Color','k','LineStyle','-');
            line([vST vST]+gm.nfr.*(i-1),[0 nrois2a],'Color','b','LineStyle','--');
            line([vEDtot(i) vEDtot(i)]+gm.nfr.*(i-1),[0 nrois2a],'Color','r','LineStyle','--');
        end
        colormap(flip(brewermap(255,'RdBu'),1));
        title('Visual stimuli')
        set(gca,'XTick',[110/2:110:vsz vsz+bannerlen/2:bannerlen:vsz+bannerlen*nbanners], ...
            'XTickLabel',vlabs, ...
            'XTickLabelRotation',45,'Box','off','TickDir','out')
        cbar = colorbar;
        ylabel(cbar,'Z-scored F')
    end
end

%% Plot mean cluster response

yspace = 3;

M = NaN(kopt,vsz,2);
for i = 1:kopt
    c = op(i);
    ix = Idxk == c;
    M(i,:,1) = nanmean(X_all_z(ix,:),1);
    M(i,:,2) = nansem(X_all_z(ix,:),1);
end

figure
imagesc(M(:,:,1),[-3 3])
colormap(flip(brewermap(255,'RdBu'),1));
for i = 1:length(vEDtot)
    line([gm.nfr gm.nfr].*(i-1),[0 kopt+0.5],'Color','k','LineStyle','-');
    line([vST vST]+gm.nfr.*(i-1),[0 kopt+0.5],'Color','b','LineStyle','--');
    line([vEDtot(i) vEDtot(i)]+gm.nfr.*(i-1),[0 kopt+0.5],'Color','r','LineStyle','--');
end
set(gca,'XTick',[110/2:110:vsz vsz+bannerlen/2:bannerlen:vsz+bannerlen*nbanners], ...
    'XTickLabel',vlabs, ...
    'XTickLabelRotation',45,'Box','off','TickDir','out', ...
    'YTick',1:kopt,'YTickLabel',op, ...
    'YDir','normal','Box','off','TickDir','out')
ylabel('Cluster')
cbar = colorbar;
ylabel(cbar,'zF')

figure
plot(M(:,:,1)'+[0:yspace:(kopt-1)*yspace],'k')
for i = 1:length(vEDtot)
    if i > 1
        line([gm.nfr gm.nfr].*(i-1),[0 (kopt-1)*yspace+yspace],'Color','k','LineStyle','-');
    end
    line([vST vST]+gm.nfr.*(i-1),[0 (kopt-1)*yspace+yspace],'Color','b','LineStyle','--');
    line([vEDtot(i) vEDtot(i)]+gm.nfr.*(i-1),[0 (kopt-1)*yspace+yspace],'Color','r','LineStyle','--');
end
set(gca,'XTick',110/2:110:vsz, ...
    'XTickLabel',vlabs(1:nstim), ...
    'XTickLabelRotation',45,'Box','off','TickDir','out', ...
    'YTick',0:yspace:(kopt-1)*yspace, ...
    'YTickLabel',op)
xlim([0 vsz])
xlabel('Stimuli')
ylabel('Cluster')

%%  Cluster histograms by ZBB area

[~,Lia_all] = ismember(Idxk,op);
msk2plt = [9 26 30 31 62 63 64 65];

for l = msk2plt
    ix = zbbmsk(:,l) == 1;
    [~,Lia] = ismember(Idxk(ix),op);
    fig = figure('position',[353,108,754,643]);
    histogram(Lia,0.5:kopt+0.5,'Normalization','probability')
    hold on
    histogram(Lia_all,0.5:kopt+0.5,'Normalization','probability')
    set(gca,'XTick',1:kopt,'XTickLabel',op,'Box','off','TickDir','out')
    xlabel('Cluster ID')
    ylabel('probability')
    legend({ZBBMaskDatabaseNames{l},'All ROIs'})
    axis square
end

%% Compute distance between ZBB areas by their cluster probabilities

M = NaN(nmsks,kopt);
for m = 1:nmsks
    ix = zbbmsk(:,m) == 1;
    if sum(ix) >= 50
        [~,Lia] = ismember(Idxk(ix),op);
        M(m,:) = histcounts(Lia,0.5:kopt+0.5,'Normalization','probability');
    end
end

ix = find(~isnan(M(:,1)));
YkC = pdist(M(ix,:),'euclidean');
ZkC = linkage(YkC,'average');
fig = figure('Name','Hierachical clustering of cluster probabilities');
[HC,~,opC] = dendrogram(ZkC,0,'Orientation','right','Labels',ZBBMaskDatabaseNames(ix));
xlabel('Linkage (Unweighted average distance)')
axis square

figure
imagesc(M(ix(opC),:))
colormap hot
axis square
set(gca,'YDir','normal','YTick',1:length(ix), ...
    'YTickLabel',ZBBMaskDatabaseNames(ix(opC)), ...
    'XTick',1:kopt,'XTickLabel',op,'XDir','reverse', ...
    'Box','off','TickDir','out')
xlabel('Cluster')
ylabel('ZBB Mask')
cbar = colorbar;
ylabel(cbar,'Probability')

%% Plot all ROIs per mask

restrictfish = 1;
fish2plt = [1 3];
alphascatt = 0;
msk2plt = [gtix ocuix otix chbix aniix pniix ptix tegix otnix];
sprclst = {[14],[13],[1],[12]};
sprclstlab = {'Pre-like - Left VF', ...
    'Pre-like - Right VF','Loom Early','Loom Late'};

mskxoffset = [30 70 0 45+15 45 45+10 -90 -280 -140];
mskyoffset = [0 0 0 40 0 0 0 80 5;
    0 0 0 -40 0 0 0 -80 -5];
% mskrange = round([prctile(roiszbbC(:,3),1) ...
%     prctile(roiszbbC(:,3),99)]);
mskrange = [114 220];

intscale = 0.25;
gaussfilt = 17;

k = 1;
for c = 1:length(sprclst)
    idxix = ismember(Idxk,sprclst{c}','rows');
    figure
    hold on
    ccol = get(gca,'ColorOrder');
    for s = 1:2
        for n = 1:length(msk2plt)
            mix = zbbmsk(:,msk2plt(n)) & idxix;
            if restrictfish
                mix = mix & ismember(Xinfo.fish,fish2plt);
            end
            C = roiszbbC(mix,:);
            DD = scale2one(log(min(Dk(:,sprclst{c}),[],2)));
            D = DD(mix);
            lix = C(:,2) > 308;
            if s == 1
                C = C(lix,:);
                D = D(lix);
            else
                C = C(~lix,:);
                D = D(~lix);
            end
            
            if alphascatt
                for j = 1:size(C,1)
                    scatter(C(j,1)+mskxoffset(n),C(j,2)+mskyoffset(s,n), ...
                        50,ccol(c,:),'filled','MarkerFaceAlpha',1-(D(j)), ...
                        'MarkerEdgeAlpha',1-(D(j)))
                end
            else
                scatter(C(:,1)+mskxoffset(n),C(:,2)+mskyoffset(s,n), ...
                    50,ccol(c,:),'filled')
                alpha 0.2
            end
            
            msk = reshape(full(ZBBMaskDatabase(:,msk2plt(n))),[616,1030,420]);
            if s == 1
                msk(1:308,:,:) = 0;
            else
                msk(308:end,:,:) = 0;
            end
            I = bwmorph(sum(msk(:,:,mskrange(1):mskrange(2)),3) > 0,'close');
            
            % Smooth mask
            F = griddedInterpolant(double(I));
            [sx,sy,sz] = size(I);
            
            xq = (0:intscale:sx)';
            yq = (0:intscale:sy)';
            zq = (0:intscale:sz)';
            
            If = imgaussfilt(F({xq,yq}),gaussfilt) > 0.5;
            mskB = bwboundaries(If);
            for m = 1:size(mskB,1)
                mskB{m,1} = mskB{m,1}.*intscale;
            end
            
            for j = 1:length(mskB)
                boundary = mskB{j};
                plot(boundary(:,2)+mskxoffset(n), ...
                    boundary(:,1)+mskyoffset(s,n),'-', ...
                    'Color','k','LineWidth',1);
            end
        end
    end
    axis off equal
    set(gca,'XDir','reverse','YDir','normal')
    view([90 90])
    title(sprintf('Super Cluster %s',sprclstlab{c}))
    k = k+1;
end

%% Plot NI ROIs

msk2plt = [chbix aniix pniix];
sprclstix = 1;
restrictfish = 1;
fish2plt = [1 3];
alphascatt = 0;
alphav = 0.6;

if sprclstix == 1
    sprclst = {[14],[13],[1],[12]};
    sprclstlab = {'Pre-like - Left VF', ...
        'Pre-like - Right VF','Loom Early','Loom Late'};
elseif sprclstix == 2
    sprclst = {[13 14],[1 12]};
    sprclstlab = {'Prey','Loom'};
end

sidelab = {'Left','Right'};

mskxoffset = [45+15 45 45+10];
mskyoffset = [40 0 0;
    -40 0 0];
% mskrange = round([prctile(roiszbbC(:,3),1) ...
%     prctile(roiszbbC(:,3),99)]);
mskrange = [114 220];

intscale = 0.25;
gaussfilt = 17;

for s = 1:2
    for c = 1:length(sprclst)
        idxix = ismember(Idxk,sprclst{c}','rows');
        if sprclstix == 1 || (sprclstix == 2 && c == 1)
            figure
            hold on
            ccol = get(gca,'ColorOrder');
        end
        for n = 1:length(msk2plt)
            mix = zbbmsk(:,msk2plt(n)) & idxix;
            if restrictfish
                mix = mix & ismember(Xinfo.fish,fish2plt);
            end
            C = roiszbbC(mix,:);
            DD = scale2one(log(min(Dk(:,sprclst{c}),[],2)));
            D = DD(mix);
            lix = C(:,2) > 308;
            if s == 1
                C = C(lix,:);
                D = D(lix);
            else
                C = C(~lix,:);
                D = D(~lix);
            end
            if alphascatt
                for j = 1:size(C,1)
                    scatter(C(j,1)+mskxoffset(n),C(j,2)+mskyoffset(s,n), ...
                        100,ccol(c,:),'filled','MarkerFaceAlpha',1-(D(j)), ...
                        'MarkerEdgeAlpha',1-(D(j)))
                end
            else
                scatter(C(:,1)+mskxoffset(n),C(:,2)+mskyoffset(s,n), ...
                    100,ccol(c,:),'filled')
                alpha(alphav)
            end
            msk = reshape(full(ZBBMaskDatabase(:,msk2plt(n))),[616,1030,420]);
            if s == 1
                msk(1:308,:,:) = 0;
            else
                msk(308:end,:,:) = 0;
            end
            I = bwmorph(sum(msk(:,:,mskrange(1):mskrange(2)),3) > 0,'close');
            
            % Smooth mask
            F = griddedInterpolant(double(I));
            [sx,sy,sz] = size(I);
            
            xq = (0:intscale:sx)';
            yq = (0:intscale:sy)';
            zq = (0:intscale:sz)';
            
            If = imgaussfilt(F({xq,yq}),gaussfilt) > 0.5;
            mskB = bwboundaries(If);
            for m = 1:size(mskB,1)
                mskB{m,1} = mskB{m,1}.*intscale;
            end
            
            for j = 1:length(mskB)
                boundary = mskB{j};
                plot(boundary(:,2)+mskxoffset(n), ...
                    boundary(:,1)+mskyoffset(s,n),'-', ...
                    'Color','k','LineWidth',1);
            end
        end
        axis off equal
        set(gca,'XDir','reverse','YDir','normal')
        view([90 90])
        title(sprintf('Super Cluster %s - %s',sprclstlab{c},sidelab{s}))
    end
end

%% Plot mean visual responses for mask per cluster

vSTt = (gm.trfr+2)*gm.frtime./1000;
xlims = [7 25];
ylims = [-1 6];
imrange = [-5 5];

tinc = 1/1000;
LV = 0.490;
stsz = 10;
tstart = round(100.*(LV./tand(stsz./2)))./100;
t = [-tstart:tinc:0];
S = 2.*atand(LV*(1./abs(t)));
St = flip(-t,2)+vSTt;

clst = [1 12];
vst = 1;
msk2plt = [aniix pniix 62];
x = [1:nfr].*gm.frtime./1000;

for v = vst
    for c = 1:length(clst)
        figure('position',[680,139,448,839]);
        col = get(gca,'ColorOrder');
        subplot(length(msk2plt)+1,1,1)
        M = {};
        for n = 1:length(msk2plt)
            mix = msk2plt(n);
            rois2a = find(zbbmsk(:,mix));
            nrois2a = length(rois2a);
            
            % Build matrices
            roisix = intersect(Idxk_all,rois2a,'stable');
            Idxk_rois2a = Idxk(roisix);
            
            stfr = ((v-1)*nfr+1);
            M{n} = X_all_z(roisix(Idxk_rois2a == clst(c)),stfr:stfr+nfr-1);
            % Visstim timings
            vST = gm.trfr + 2;
            vED = gm.trfr + 1 + ceil(1000.*gmv.vistypz( ...
                ismember(gmv.vistypz(:,1:4),stims(v,:),'rows'),5)./gm.frtime);
            
            y = nanmean(M{n});
            yerr = nansem(M{n});
            shadedErrorBar(x,y,yerr,{'color',col(n,:)},1)
            hold on
        end
        xlabel('Time (s)')
        ylabel('zF')
        set(gca,'Box','off','TickDir','out')
        axis([xlims ylims])
        hold on
        yyaxis right
        plot(St,S,'k','LineWidth',2)
        ylim([0 100])
        line([x(vST) x(vST)],[0 100],'Color','b', ...
            'LineWidth',2,'LineStyle','--')
        line([x(vED) x(vED)],[0 100],'Color','r', ...
            'LineWidth',2,'LineStyle','--')
        title(sprintf('Cluster %d, VIS: %d', ...
            clst(c),stims(v,1)))
        
        for n = 1:length(msk2plt)
            subplot(length(msk2plt)+1,1,n+1)
            imagesc(M{n},imrange);
            axis off
            colormap(flip(brewermap(255,'RdBu'),1));
            line([vST vST],[0 size(M{n},1)],'Color','b', ...
                'LineWidth',2,'LineStyle','--')
            line([vED vED],[0 size(M{n},1)],'Color','r', ...
                'LineWidth',2,'LineStyle','--')
            xlim([findnearest(x,xlims(1)) findnearest(x,xlims(2))])
            title(ZBBMaskDatabaseNames{msk2plt(n)})
        end
    end
end

%% Escape bevahiour analysis

B = [];
T = [];
for f = 1:length(folders)
    datadir = folders{f};
    load(fullfile(datadir,'gmbf'),'gmbf');
    load(fullfile(datadir,'gmv'),'gmv');
    ps = [gmbf.b.p]';
    st = [gmbf.b.st]';
    ed = [gmbf.b.ed]';
    B = cat(1,B,cat(2,[gmbf.b.max_angl]',[gmbf.b.max_vel]'));
    T = cat(1,T,cat(2,ps,gmv.visstim(ps,1),st,ed));
end

vSTt = (gm.trfr+2)*gm.frtime./1000;
vEDt = gmv.vistypz(ismember(gmv.vistypz(:,1:3),stims(1,1:3),'rows'),5);

% ix = find(T(:,2) == 5100 & T(:,3) > vSTt & T(:,3) <= vEDt);
ix = find(T(:,2) == 5100);

k = 2;
options = statset('kmeans');
options = statset(options,'MaxIter',500);

[Idxbk, Centbk, Sumdistbk, Dbk] = kmeans(abs(B(ix,:)),k, ....
    'distance','sqeuclidean','replicates',5,'options',options);

figure
scatter(abs(B(ix,1)),abs(B(ix,2)),[],Idxbk,'filled')

[~,mix] = max(abs(B(ix,2)));
loomidx = Idxbk(mix);

figure
histogram(sign(B(ix(Idxbk == loomidx),1)),'Normalization','probability')
ylabel('Probability')
xlabel('Escape direction')
set(gca,'Box','off','TickDir','out','XTick',[-1 1],'XTickLabel',{'Left','Right'});