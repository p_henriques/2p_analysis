folders = { ...
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180301\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180305\f2\deep';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180419\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f1_1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180427\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180503\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180504\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180509\f3';
    };

nfolders = length(folders);

%%  Load Mask database names

zbbanatix = [2:3 6:34 44:46 48:74 77 113 305:306];
load('ZBBMaskDatabase');
ZBBMaskDatabaseNames = ZBBMaskDatabaseNames(zbbanatix);
ZBBMaskDatabase = ZBBMaskDatabase(:,zbbanatix);
nmsks = length(ZBBMaskDatabaseNames);

%% Z-conv matrix

convz = 2;  % z threshold to consider for z fraction map
ylabs = {'Fraction of Conv+ ROIs','Mean Conv Z score'};
tlabs = {'Mean LConvZ ON','Mean RConvZ ON','Mean LConvZ OFF','Mean RConvZ OFF', ...
    'Max LConvZ ON','Max RConvZ ON','Max LConvZ OFF','Max RConvZ OFF', ...
    'Mean ConvZ ON','Mean ConvZ OFF'};
zbbmskix = [63];    % ZBB mask ID to analyse
plotboundaries = 1; % Plot mask boundaries
mskbplt = {[64 65];[9 30 31];[26]};   % ZBB mask IDs to plot boundaries
binspace = 1; % microns
binoffset = 10; % microns
axisoffset = 5; % pixels

roisix = sum(zbbmsk(:,zbbmskix),2) > 0;
ndim = size(ConvM,2);

% Bin edges
edgx = floor(min(roiszbbC(:,1)))-binoffset:binspace:ceil(max(roiszbbC(:,1)))+binoffset;
edgy = floor(min(roiszbbC(:,2)))-binoffset:binspace:ceil(max(roiszbbC(:,2)))+binoffset;
fish = unique(T(:,1));
nfish = length(fish);

[N,Xedges,Yedges,binX,binY] = histcounts2(roiszbbC(:,1),roiszbbC(:,2),edgx,edgy);

Amn = zeros([size(N),2,nfish]);
h = waitbar(0,'Init');
for f = 1:nfish
    waitbar(f/nfish,h,'Processing')
    for i = 1:size(N,1)
        for j = 1:size(N,2)
            for d = 1
                ix = binX == i & binY == j & T(:,1) == fish(f);
                if any(ix)
                    if d == 1
                        X = ConvM(ix & roisix,[1 3]);
                    else
                        X = ConvM(ix & roisix,[2 4]);
                    end
                    Amn(i,j,d,f) = nanmean(X(:));
                end
            end
        end
    end
end

close(h)
Amn(isnan(Amn)) = 0;

%%

medfsz = 2; % Size of median filter
gaussfsz = 5;   % Size of gaussian smoothing filter
fish2a = [1,3:9];   % Fish to use
ndim = size(Amn,3);
nfish = size(Amn,4);

% Soft median filter
Amnf = zeros(size(Amn));
for f = 1:nfish
    for d = 1:ndim
        Amnf(:,:,d,f) = medfilt2(Amn(:,:,d,f),[medfsz medfsz]);
    end
end

% Average across fish
Amnfm = nanmean(Amnf(:,:,:,fish2a),4);

% Gaussian filter
Amnfmf = zeros([size(N) ndim]);
for d = 1:ndim
    Amnfmf(:,:,d) = imgaussfilt(Amnfm(:,:,d),gaussfsz);
end

%%

clims1 = [0 0.03];
clims2 = [-0.1 0.1];
intscale = 0.25;

for d = 1:ndim
    figure
    thr = prctile(reshape(Amnfmf(:,:,d),numel(N),1),99.9);
    imagesc(Amnfmf(:,:,d),[-thr thr])
    col = 'k';
    colormap(flip(brewermap(255,'RdBu'),1));
    set(gca,'XDir','reverse');
    axis off equal
    if plotboundaries
        hold on
        for n = 1:size(mskbplt,1)
            for j = 1:length(mskbplt{n,1})
                if j == 1
                    mskrange = round(min(roiszbbC(zbbmsk(:,mskbplt{n}(j)),3))): ...
                        round(max(roiszbbC(zbbmsk(:,mskbplt{n}(j)),3)));
                end
                msk = reshape(full(ZBBMaskDatabase(:,mskbplt{n}(j))),[616,1030,420]);
                msk = msk(:,:,mskrange);
                if j == 1
                    %                 [~,mskix] = max(squeeze(sum(sum(msk,1),2)));
                    mskix = round(mean(1:length(mskrange)));
                end
                
                % Smooth mask
                M = msk(:,:,mskix);
                F = griddedInterpolant(double(M));
                [sx,sy,sz] = size(M);
                
                xq = (0:intscale:sx)';
                yq = (0:intscale:sy)';
                zq = (0:intscale:sz)';
                
                Mf = imgaussfilt(F({xq,yq}),3) > 0.5;
                mskB = bwboundaries(Mf);
                for m = 1:size(mskB,1)
                    mskB{m,1} = mskB{m,1}.*intscale;
                end
                
                for l = 1:length(mskB)
                    boundary = [mskB{l}-[edgy(1) edgx(1)]]./binspace;
                    plot(boundary(:,1), boundary(:,2),':', ...
                        'Color',col,'LineWidth',1);
                end
            end
        end
    end
end
