
calcium = X(:)./nanmean(X(:));
dt = gm.frtime/1000;
if strcmp(gm.ScanType,'Q1')
    dt = dt/4;
end

%%

amin = .1;
amax = 5;

taumin = .1;
taumax = 1.5;

pax = spk_autocalibration('par');
pax.dt = dt;
pax.amin = amin;
pax.amax = amax;
pax.taumin = taumin;
pax.taumax = taumax;
% pax.mlspikepar.dographsummary = false;

[tauest,aest,sigmaest,events] = spk_autocalibration(calcium,pax);

%%

par = tps_mlspikes('par');
par.dt = dt;
par.a = 1; % DF/F for one spike
par.tau = 1; % decay time constant (second)
par.saturation = 0.1; % OGB dye saturation
par.finetune.sigma = .5;
par.dographsummary = false;

% spike estimation
[spikest,fit,drift] = spk_est(calcium,par);

% display
figure(1)
spk_display(dt,{spikest},{calcium fit drift})
set(1,'numbertitle','off','name','MLspike alone')