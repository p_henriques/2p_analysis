
load('Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\analysis\Convergence\Z_conv_profile_matrix_180920.mat');

%%

folders = { ...
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180301\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180305\f2\deep';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180419\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f1_1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180427\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180503\f2';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180504\f1';
    'Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180509\f3';
    };

nfolders = length(folders);

%%  Load Mask database names

zbbanatix = [2:3 6:34 44:46 48:74 77 113 305:306];
load('ZBBMaskDatabase','ZBBMaskDatabaseNames');
ZBBMaskDatabaseNames = ZBBMaskDatabaseNames(zbbanatix);
nmsks = length(ZBBMaskDatabaseNames);

%%

zbbmsk = [];
roiszbbC = [];
ConvM = [];
T = [];
h = waitbar(0, 'Init');
for f = 1:length(folders)
    waitbar(f/length(folders),h, 'Processing...')
    datadir = folders{f};
    
    load(fullfile(datadir,'gmConvMod_profile'));
    load(fullfile(datadir,'gmranat'));
    
    ConvM = cat(1,ConvM, ...
        cat(3, ...
        cat(1,gmConvMod_profile.rois.Z_on_Lconv_Mn), ...
        cat(1,gmConvMod_profile.rois.Z_on_Rconv_Mn), ...
        cat(1,gmConvMod_profile.rois.Z_off_Lconv_Mn), ...
        cat(1,gmConvMod_profile.rois.Z_off_Rconv_Mn)));
    T = cat(1,T, ...
        cat(2,ones(size(gmConvMod_profile.rois,2),1).*f, ...
        [1:size(gmConvMod_profile.rois,2)]'));
    
    nzs = size(gmranat.z,2);
    for z = 1:nzs
        zbbmsk = cat(1,zbbmsk, ...
            cat(1,gmranat.z(z).STATScrop.Masks_ZBB));
        roiszbbC = cat(1,roiszbbC, ...
            cat(1,gmranat.z(z).STATScrop.Centroid_ZBB));
    end
    
    clear gmConvMod_profile gmranat
end
zbbmsk = logical(zbbmsk(:,zbbanatix));
close(h);

%%

tlabs = {'Ipsilateral','Contralateral'};
x = [-55:54].*0.275;
ylims = [-0.2 1.2];
nmax = 50;

aniix = 64;
pniix = 65;
chbix = 62;
otix = 31;
gtix = 9;

msk2plt = [64 65 62 31 9 30 39 45 26];

X = cell(length(msk2plt),2,2,2);
for n = 1:length(msk2plt)
    ix = zbbmsk(:,msk2plt(n));
    lix = roiszbbC(:,2) > 308;
    
    if sum(ix) > nmax
        figure('position',[420,445,924,435])
        for i = 1:2
            if i == 1
                % Ipsilateral
                M1 = cat(1,ConvM(ix & lix,:,1),ConvM(ix & ~lix,:,2));
                M2 = cat(1,ConvM(ix & lix,:,3),ConvM(ix & ~lix,:,4));
            else
                % Contralateral
                M1 = cat(1,ConvM(ix & ~lix,:,1),ConvM(ix & lix,:,2));
                M2 = cat(1,ConvM(ix & ~lix,:,3),ConvM(ix & lix,:,4));
            end
            
            X{n,1,1,i} = nanmean(M1(:,55-4-8:55-4),2); % Pre stim
            X{n,2,1,i} = nanmean(M1(:,55+4:55+4+8),2); % Post stim
            X{n,1,2,i} = nanmean(M2(:,55-4-8:55-4),2); % Pre spont
            X{n,2,2,i} = nanmean(M2(:,55+4:55+4+8),2); % Post spont
            
            y1 = nanmean(M1);
            y1err = nansem(M1);
            y2 = nanmean(M2);
            y2err = nansem(M2);
            
            subplot(1,2,i)
            p1 = shadedErrorBar(x,y1,y1err,'b',1);
            hold on
            p2 = shadedErrorBar(x,y2,y2err,'r',1);
            axis square
            set(gca,'Box','off','TickDir','out')
            xlabel('Time to convergence (s)')
            ylabel('CZ-Score')
            line([0 0],ylims,'LineStyle','--','Color','k')
            axis([x(1) x(end) ylims])
            %         legend([p1.mainLine;p2.mainLine], ...
            %             {'Stim ON','Spontaneous'})
            title(tlabs{i})
        end
        
        % 3-way anova
        Y = cat(1, ...
            X{n,1,1,1},X{n,1,2,1},X{n,1,1,2},X{n,1,2,2}, ...
            X{n,2,1,1},X{n,2,2,1},X{n,2,1,2},X{n,2,2,2});
        g1 = [ones(length(X{n,1,1,1})*4,1); ones(length(X{n,1,1,1})*4,1).*2];   % Pre/Post
        g2 = repmat([ones(length(X{n,1,1,1})*2,1); ones(length(X{n,1,1,1})*2,1).*2],2,1);   % Ipsi/contra
        g3 = repmat([ones(length(X{n,1,1,1}),1); ones(length(X{n,1,1,1}),1).*2],4,1);   % Stim/Spont
        
        [p,tbl,stats] = anovan(Y,{g1 g2 g3},'model','interaction',...
            'varnames',{'c-time','c-side','c-type'},'display','off');
        for j = 1:length(p)
            text(0.05,0.9-(0.05*(j-1)), ...
                sprintf('%s: %0.3f',tbl{j+1,1},p(j)),'Units','normalized')
        end
        mtit(ZBBMaskDatabaseNames{msk2plt(n)})
    end
end
