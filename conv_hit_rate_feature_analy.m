%% Folders

folders = { ...
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180301\f1';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180305\f2\deep';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180419\f1';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f1_1';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f2';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180427\f2';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180503\f2';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180504\f1';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180509\f3';
    };

nfolders = length(folders);

%%

zbbanatix = [1:75 77 79 113 302 305:306];
load('ZBBMaskDatabase','ZBBMaskDatabaseNames');
ZBBMaskDatabaseNames = ZBBMaskDatabaseNames(zbbanatix);
nmsks = length(ZBBMaskDatabaseNames);

%%

zbbmsk = [];
roiszbbC = [];
HR = [];
Xon = table;
Xoff = table;
h = waitbar(0, 'Init');
for f = 1:length(folders)
    waitbar(f/length(folders),h, 'Processing...')
    datadir = folders{f};
    
    load(fullfile(datadir,'CvR'));
    load(fullfile(datadir,'gmranat'));
    
    nzs = size(gmranat.z,2);
    M = [];
    C = [];
    for z = 1:nzs
        M = cat(1,M, ...
            cat(1,gmranat.z(z).STATScrop.Masks_ZBB));
        C = cat(1,C, ...
            cat(1,gmranat.z(z).STATScrop.Centroid_ZBB));
    end
    
    for i = 1:size(CvR.rois,2)
        sz = size(CvR.rois(i).Xon,1);
        Xon = cat(1,Xon,CvR.rois(i).Xon(:,1:end-1));
        
        HR = cat(1,HR, ...
            ones(sz,1).* ...
            [CvR.rois(i).Z_on_conv, ...
            CvR.rois(i).Z_on_conv_only, ...
            CvR.rois(i).Z_on_pre, ...
            CvR.rois(i).Z_on_post]);
        
        zbbmsk = cat(1,zbbmsk,ones(sz,1).*M(i,:));
        roiszbbC = cat(1,roiszbbC,ones(sz,1).*C(i,:));
    end    
            
    clear CvR gmranat
end
zbbmsk = logical(zbbmsk(:,zbbanatix));
close(h);


%%

binedg = -10.5:2.5;

aniix = zbbmsk(:,80) == 1;
pniix = zbbmsk(:,81) == 1;
gcix = zbbmsk(:,12) == 1;
leftix = roiszbbC(:,2) > 308;
convpix = HR(:,1) >= 2;
ia = logical(Xon.isconvactive);

figure
histogram(Xon.rsp_st(aniix & convpix & ia)-55,binedg,'Normalization','probability');
hold on
histogram(Xon.rsp_st(pniix & convpix & ia)-55,binedg,'Normalization','probability');
xticks = get(gca,'XTick');
set(gca,'XTickLabel',xticks.*0.275)
xlabel('Ca2+ response onset (s)')
ylabel('Probability')
legend({'anterior NI','posterior NI'},'Location','northwest')

%%  

ix = (aniix | pniix) & convpix;
T = cat(2,Xon(ix,[3 8 11 12 14 19:20]), ...
    table(leftix(ix),'VariableNames',{'side'}));

T(isnan(T.tail_max_angl),:) = [];
T(isnan(T.st_ang_deg),:) = [];

T.eye_Rdelta = -T.eye_Rdelta;
eyeDeltaI = (T.eye_Ldelta-T.eye_Rdelta)./(T.eye_Ldelta+T.eye_Rdelta);
T.eye_Ldelta = eyeDeltaI;
T.Properties.VariableNames{6} = 'eye_DeltaI';
T.eye_Rdelta = [];

T.tail_max_angl = T.tail_max_angl > 0;
T.st_ang_deg = T.st_ang_deg > 0;
T.nbts(T.nbts > 1) = 2;
T.isconvactive = logical(T.isconvactive);
T.vis(T.vis == 3 | T.vis == 5) = 0;
T.vis(T.vis == 4 | T.vis == 6) = 1;

mdl = stepwiseglm(T,'linear', ...
    'Distribution','binomial' , ...
    'Link','logit', ...
    'ResponseVar','isconvactive', ...
    'CategoricalVars',[1,4,7]);    

