
%% Clustering Pedro VRV data July 18

%% LOAD DATA

load('Y:\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\analysis\Clustering\kmeans\180707\clustering_vvec1_VisOnly_PCA_180707.mat')

%% Package into mf

mf.datasetz = folders;
mf.fishID = Xinfo.fish;
mf.roilabels = Xinfo.rois;
mf.X = X_all_z;

clearvars -except mf

%% Batch process

if isfield(mf, 'btch')
    mf = rmfield(mf, 'btch');
end

mf.batchidz = [1:numel(mf.datasetz)]';

mf.batchnotes = 'by FishIDs; IHBcluster}';

mf.batch_corrthr_set = [0.95 0.90 0.85 0.80 0.75 0.70];
mf.batch_minoccupancy = 5;
mf.batch_Idxcorr = 0.75;

h = waitbar(0, '');
for b = 1:numel(mf.batchidz)
    waitbar(b/numel(mf.batchidz), h, ['Processing batch ' num2str(b) ' of ' num2str(numel(mf.batchidz)) '...'])
    
    thisID = mf.batchidz(b)
    
    bMFix = find( mf.fishID==thisID );
    
    mf.btch(b).MFix_in = bMFix;
    mf.btch(b).num_in = numel(bMFix);
    
    % get data
    X = mf.X(bMFix, :);
    
    % ihbcluster (transpose data for ihbcluster)
    [Idx, Cent, nclustz, clsizesorder, corrthr_set] = ihbcluster_v3(X', mf.batch_corrthr_set, mf.batch_minoccupancy, 0, 3);
    
    if b==1
        mf.batch_corrthr_set = corrthr_set;
    end
    
    mf.btch(b).Idx_set = Idx;
    
    Idx_out = Idx(:, corrthr_set==mf.batch_Idxcorr);

    kill_ix = find(Idx_out==0); % remove unclustered
    Idx_out(kill_ix) = [];
    bMFix(kill_ix) = [];
    
    mf.btch(b).MFix = bMFix;
    mf.btch(b).Idx = Idx_out + b*10000; % add unique id
    mf.btch(b).kill_ix = kill_ix; % keep a record for plotting section!
    mf.btch(b).num_out = numel(bMFix);

end
disp('Done.')
close(h)


%% OPTION Multi-batch option B: ihbcluster pipeline : Change Idx corr

mf.batch_Idxcorr = 0.9;

num_batchclust = [];
for b = 1:numel(mf.batchidz)
    thisID = mf.batchidz(b)
    
    bMFix = mf.btch(b).MFix_in;
    
    % apply new corr
    Idx = mf.btch(b).Idx_set;
    corrthr_set = mf.batch_corrthr_set;
    Idx_out = Idx(:, corrthr_set==mf.batch_Idxcorr);
    kill_ix = find(Idx_out==0); 
    Idx_out(kill_ix) = [];
    bMFix(kill_ix) = [];
    mf.btch(b).MFix = bMFix;
    mf.btch(b).Idx = Idx_out + b*10000; 
    mf.btch(b).kill_ix = kill_ix;
    mf.btch(b).num_out = numel(bMFix);
    
    num_batchclust(b) = numel(unique(Idx_out));
end

%% num in vs out

nin = [];
nout = [];

for b = 1:length(mf.btch)
    nin(b) = numel(mf.btch(b).MFix_in);
    nout(b) = numel(mf.btch(b).MFix);
end

figure
plot(nin, nout, '.')
    
figure
plot(nout./nin, '-o')

sum(nin)
sum(nout)


%% create selection array (for Pedros data use everything)

mf.goodbatchix = [];

for b = 1:length(mf.btch)
    b
    
    Idx = mf.btch(b).Idx;
    uIdx = unique(Idx);
    % apply selection. currently use all clusters and all batches
    mf.btch(b).goodIdx = uIdx; % use all of the clusters
    
    if 1 %numel(mf.btch(b).goodIdx) >= mf.batch_goodnclusters
        mf.goodbatchix = [mf.goodbatchix; b];
    end
    
end



%% Supercluster

% AA. inter-batchcluster distance matrix::

mf.batch_dist_notes = 'Correlation-Centroid';
% mf.super_numPCs = [10:1:40];
mf.super_intrazscore = false;

mf.super_minclustsize_a = 2;    % eliminate singleton clusters
mf.super_minclustn_a = 3;
mf.super_maxclustn_a = 30;

mf.batch_dist_method = 'correlation-centroid';
mf.batch_dist_dmat = [];

for p = 1 %:numel(mf.super_numPCs)
%     thisnumPCs = mf.super_numPCs(p)
    
    % 1. get data and batch labels
    X = [];
    Idx = [];
    for b = mf.goodbatchix(:)'
        b;
        MFix = mf.btch(b).MFix; % indices in the full multi-fish matrices
        thisidx = mf.btch(b).Idx(:); % unique for each batch due to 10000 addition, above

        % sort out only the good clusters:
        LIA = ismember(thisidx, mf.btch(b).goodIdx(:,1));
        thisidx = thisidx(LIA);
        MFix = MFix(LIA);

        Idx = [Idx; thisidx];
        if mf.super_intrazscore
%             Xbatch = zscore( mf.pca_score_all(MFix, 1:thisnumPCs) ); % intrafish zscoring, old method
            Xbatch = mf.X(MFix, :); % now using ifn data (zscored from batch MFix_(out) bouts)
        else
            Xbatch = mf.X(MFix, :);
        end

        X = [X;  Xbatch];
    end

    % 2. Compute intercluster distances
    [dmat, Idx_out] = gcclusterdistance(X, Idx, mf.batch_dist_method);
    mf.batch_dist_dmat = cat(3, mf.batch_dist_dmat, dmat);
    mf.batch_dist_Idx = Idx_out; % record all batch Idx's in dmat order
end

% 3. Cluster for each distance method seperately
superIdx_beta_set = [];
for i = 1:size(mf.batch_dist_dmat, 3)
    dmat = mf.batch_dist_dmat(:,:,i);
    D = squareform(dmat);
    Z = linkage(D, 'average');
    nheightz = Z(:, 3); % find longest link or otherwise determine clusters
    snheightz = sort(nheightz, 'descend');
    deltaheight = [0; -diff(snheightz)];
    [~, ix] = max(deltaheight);
    nclust = ix;
    nclust = min([nclust mf.super_maxclustn_a]);    % control maximum number of clusters
    nclust = max([nclust mf.super_minclustn_a]);    % control min number
%     nclust = 7;                                   % or specify exact number here
    cutheight = mean([snheightz(nclust-1) snheightz(nclust)]);
    theseIdx = cluster(Z, 'maxclust', nclust);
    superIdx_beta_set = [superIdx_beta_set, theseIdx];
    
        figure('Name', ['Hierarchical superclustering: `' mf.batch_dist_method])
        [~, sortingvector] = sort(theseIdx, 'ascend');
        dr = [0 1];
        subplot(1,3,1)
        imagesc(dmat, dr), colormap(gray), title('dmat')
        subplot(1,3,2)
        imagesc(dmat(sortingvector,sortingvector), dr), colormap(hot), title('dmat sorted')
        subplot(1,3,3)
        dendrogram(Z, 'ColorThreshold', cutheight)
        hold on
        zz = get(gca, 'XLim');
        line([zz(1) zz(2)], [cutheight cutheight], 'Color', 'c', 'LineWidth', 2);
        title('dendrogram')
end


% Concensus clustering with EviAcc
superIdx_beta = EviAcc(superIdx_beta_set, mf.super_minclustsize_a, mf.super_maxclustn_a, mf.super_minclustn_a, 'average');

% Assign new superIdx
superIdx_a = [];
superMFix_a = [];

for b = mf.goodbatchix(:)'
    bMFix = mf.btch(b).MFix;
    bIdx = mf.btch(b).Idx;
    bIdxu = unique(bIdx);
    bIdx_new = nan(size(bIdx));
    for i = 1:numel(bIdxu)
        newIdx = superIdx_beta(mf.batch_dist_Idx == bIdxu(i));
        if ~isempty(newIdx)
            bIdx_new(bIdx == bIdxu(i)) = newIdx;
            mf.btch(b).goodIdx(mf.btch(b).goodIdx(:,1)==bIdxu(i), 2) = newIdx;
        else
            fprintf(2, ['WARNING: Idx not found. b = ' num2str(b), ' bIdx = ' num2str(bIdxu(i))])
            bIdx_new(bIdx == bIdxu(i)) = 0;
            mf.btch(b).goodIdx(mf.btch(b).goodIdx(:,1)==bIdxu(i), 2) = 0;
        end
    end
    
    mf.btch(b).superIdx_a = bIdx_new;
    superMFix_a = [superMFix_a; bMFix(bIdx_new>0)];
    superIdx_a = [superIdx_a; bIdx_new(bIdx_new>0)];
end

mf.superunique_a = unique(superIdx_a);
mf.superIdx_a = superIdx_a;
mf.superMFix_a = superMFix_a;

    X = mf.X(mf.superMFix_a, :);
    Idx = mf.superIdx_a;
    metadata = mf.fishID(mf.superMFix_a);

    DispClusterRaster(X, Idx, metadata)


%% process super a

% compute centroids (regular and ifn)
cent = [];
cent_ifn = [];
supersize_a = [];
for c = 1:length(mf.superunique_a)
    cent(c, :) = mean( mf.X(mf.superMFix_a(mf.superIdx_a==mf.superunique_a(c)), :), 1);
    
    supersize_a(c) = sum(mf.superIdx_a==mf.superunique_a(c));
end
mf.supersize_a = supersize_a;
mf.superCent_a = cent;


clear cent


%  Bout distance to (ifn) supercentroid
superBoutDistto_a_ifn_Cent_a = nan(size(mf.superIdx_a, 1), 3);  % how far are the bouts from the cluster ifn centroid?

for i = 1:numel(mf.superunique_a)
    
    clcentroid = mf.superCent_a(i, :);
    
    c = mf.superunique_a(i);
    
    ixx = find(mf.superIdx_a==c); % indices in super_a
    
    MFix = mf.superMFix_a(ixx);
    
    clbouts = mf.X(MFix, :);
    
    % Correlation distance
    d_Corr = 1 - ( ( zscore(clbouts, 0, 2)*zscore(clcentroid)' ) ./ (length(clcentroid)-1) );
    
    % Euclidean distance
    d_Euc = sqrt(diag((clbouts - clcentroid)*(clbouts - clcentroid)'));
    
    % Chebychev distance (max abs distance from centroid along any dimension)
    d_Chebychev = max(abs(clbouts - clcentroid), [], 2);
     
    superBoutDistto_a_ifn_Cent_a(ixx, 1) = d_Corr;
    superBoutDistto_a_ifn_Cent_a(ixx, 2) = d_Euc;
    superBoutDistto_a_ifn_Cent_a(ixx, 3) = d_Chebychev;
end
mf.superBoutDistto_a_ifn_Cent_a = superBoutDistto_a_ifn_Cent_a;

%% super b
% 
mf.super_notes_b = 'Clusters trimmed or reassigned using corr distance to ifn_a centroid';
mf.super_a_thresholdcriteria = [70 1];  % <<< choose threshold based on min of 70th percentile (or 0.4)
dcol = 1;                               % column 1 id corr distance
% 
% % ---------------
% 
% % Establish thresholds
% super_a_thresholds = [];
% for i = 1:numel(mf.superunique_a)
%     c = mf.superunique_a(i);
%     theseixx = find(mf.superIdx_a==c);
%     thesed = mf.superBoutDistto_a_ifn_Cent_a(theseixx, dcol);
%     thisthreshold = min([prctile(thesed, mf.super_a_thresholdcriteria(1)), mf.super_a_thresholdcriteria(2)]); 
%     super_a_thresholds(i) = thisthreshold;
%     
%         figure('Name', ['Super a :' num2str(c)])
%         hist(thesed, 50)
% end
% mf.super_a_thresholds_b = super_a_thresholds; % ``thresholds, derived from super_a data but applied for super_b``

mf.super_a_thresholds_b = 0.5.*ones(1, numel(mf.superunique_a)); % set threshold to 0.4
super_a_thresholds = mf.super_a_thresholds_b;

X = mf.X(mf.superMFix_a, :);
C = 1 - ( (zscore(X, 0, 2)*zscore(mf.superCent_a'))./(size(X,2)-1) );
[mmin, mix] = min(C, [], 2);
superIdx_b1 = mix;
superMFix_b1 = mf.superMFix_a;
superdisttocentroid_b1 = mmin;
clear X C

superMFix_b2 = [];
superIdx_b2 = [];
for i = 1:numel(mf.superunique_a)
    c = mf.superunique_a(i)
    theseixx = find(superIdx_b1==c); % index position within super_b1 list
    thesed = superdisttocentroid_b1(theseixx);
    goodmembers = theseixx(thesed <= super_a_thresholds(i));
    superMFix_b2 = [superMFix_b2; superMFix_b1(goodmembers)];
    superIdx_b2 = [superIdx_b2; superIdx_b1(goodmembers)];
end
mf.superMFix_b = superMFix_b2;
mf.superIdx_b = superIdx_b2;
mf.superunique_b = unique(superIdx_b2);

    X = mf.X(mf.superMFix_b, :);
    Idx = mf.superIdx_b;
    metadata = mf.fishID(mf.superMFix_b);

    DispClusterRaster(X, Idx, metadata)

% compute centroids
cent = [];

supersize_b = [];
for c = 1:length(mf.superunique_b)
    cent(c, :) = mean( mf.X(mf.superMFix_b(mf.superIdx_b==mf.superunique_b(c)), :) );
    
    supersize_b(c) = sum(mf.superIdx_b==mf.superunique_b(c));
end
mf.supersize_b = supersize_b;
mf.superCent_b = cent;

clear cent cent_ifn


%% super x

mf.super_notes_x = '0731_isaac';

mf.super_x_batch_Idxcorr = 0.7; % corrthr from batch process to use for batch processed bouts
mf.super_a_thresholds_x = mf.super_a_thresholds_b; % use same thresholds as for super_b and f (derived from super_a distributions)

% which types shall we keep ?
% provide current cluster id's for clusters to be retained, and a name
mf.superunique_x = [1; 2; 3]; % note, numbers will be changed below
mf.supernamez_x = {
    'WF';
    'spot CW';
    'spot CCW';
    };


% Step 1: assign to selected superclusters only, (as per super_f)
corrthr_set = mf.batch_corrthr_set;

superXIdx = [];
superXMFix = [];
C_all = [];
Cdiff_all = [];

for b = 1:length(mf.btch)
    b
    % get bouts
    % OPTION 2: batch clustered bouts from a chosen corr threshold
%     MFix = find(mf.fishID==d & (mf.fullfeatures==1 | mf.bouttype_gcbf>0));
    MFix = mf.btch(b).MFix_in;
    Idx_out = mf.btch(b).Idx_set(:, corrthr_set==mf.super_x_batch_Idxcorr);
    MFix = MFix(Idx_out>0);
    
%     X = zscore( mf.pca_score_all(MFix, 1:mf.super_CentroidnumPCs) ); % intrafish zscore
    X = mf.X(MFix, :);
    C = 1 - ( (zscore(X, 0, 2)*zscore(mf.superCent_b(mf.superunique_x, :)'))./(size(X,2)-1) );
    Cdiff = C - mf.super_a_thresholds_x(mf.superunique_x);
    Cfilt = C;
    Cfilt(Cdiff>0) = nan; % kill (nan out) distances above threshold
    [mmin, mix] = min(Cfilt, [], 2);
    % new assigned
    newass = ~isnan(mmin);
    superXIdx = [superXIdx; mf.superunique_x(mix(newass))];
    superXMFix = [superXMFix; MFix(newass)];
    C_all = [C_all; C(newass,:)];
    Cdiff_all = [Cdiff_all; Cdiff(newass,:)];
end

kill_ix = isnan(superXIdx);
superXIdx(kill_ix) = [];
superXMFix(kill_ix) = [];
C_all(kill_ix, :) = [];

% record distance of each bout to relevant super_b centroid
[~, loc] = ismember(superXIdx, mf.superunique_x);
superBoutDistto_b_ifn_Cent_x = [];
for i = 1:size(C_all, 1)
    superBoutDistto_b_ifn_Cent_x(i,1) = C_all(i, loc(i));
end

mf.superIdx_x = superXIdx;
mf.superMFix_x = superXMFix;
mf.superBoutDistto_b_ifn_Cent_x = superBoutDistto_b_ifn_Cent_x;

cent = [];
supersize_x = [];
for i = 1:numel(mf.superunique_x)
    c = mf.superunique_x(i);
    MFix = mf.superMFix_x(mf.superIdx_x==c);
    cent(i, :) = mean( mf.X(MFix, :) );
    
    supersize_x(i) = numel(MFix);
end
mf.supersize_x = supersize_x;
mf.superCent_x = cent;

clear cent cent_ifn


    X = mf.X(mf.superMFix_x, :);
    Idx = mf.superIdx_x;
    metadata = mf.fishID(mf.superMFix_x);

    DispClusterRaster(X, Idx, metadata)


%% Test: cluster breakdown

Idx_z = [];
MFix_z = [];

for c = mf.superunique_x(:)'
    
    MFix = mf.superMFix_b(mf.superIdx_b==c);
    X = mf.X(MFix, :);
    
    [Idx, Cent, nclustz, clsizesorder, corrthr_set] = ihbcluster_v3(X', mf.batch_corrthr_set, mf.batch_minoccupancy, 0, 3);
    
    Idx_z = [Idx_z; Idx];
    MFix_z = [MFix_z; MFix];
    
    
    Idx = Idx(:, 1);
    killix = Idx==0;
    MFix = MFix;
    MFix(killix) = [];
    Idx(killix) = [];
    X = mf.X(MFix, :);
    metadata = mf.fishID(MFix);
    
    figure
    DispClusterRaster(X, Idx, metadata)
    
end






%%









    
