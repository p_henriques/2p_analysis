
folders = { ...
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180301\f1';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180305\f2\deep';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180419\f1';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f1_1';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180420\f2';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180427\f2';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180503\f2';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180504\f1';
    '\\128.40.168.141\bdata3\Pedro2\2P\NI_AF7_OT\HuC_H2B_GC6s\180509\f3';
    };

nfolders = length(folders);

%%  Load Mask database names

zbbanatix = [1:75 77 79 113 302];
load('\\128.40.168.141\bdata2\Registration\ZBBMaskDatabase','ZBBMaskDatabaseNames');
ZBBMaskDatabaseNames = ZBBMaskDatabaseNames(zbbanatix);
nmsks = length(ZBBMaskDatabaseNames);

%%  Build matrices

stdn = 2;
C = [];
zbbMSK = [];
zbbhr = NaN(nmsks,11,nfolders);
h = waitbar(0, 'Init');
for f = 1:length(folders)
    waitbar(f/length(folders),h, 'Processing...')
    datadir = folders{f};
    
    load(fullfile(datadir,'CvR'));
    load(fullfile(datadir,'gmranat'));
    
    % Get ZBB labels
    nzs = size(gmranat.z,2);
    zbbmsk = [];
    roiszbbC = [];
    for z = 1:nzs
        zbbmsk = cat(1,zbbmsk, ...
            cat(1,gmranat.z(z).STATScrop.Masks_ZBB));
        roiszbbC = cat(1,roiszbbC, ...
            cat(1,gmranat.z(z).STATScrop.Centroid_ZBB));
    end
    zbbmsk = logical(zbbmsk(:,zbbanatix));
    zbbMSK = cat(1,zbbMSK,zbbmsk);
    
    rois2a = 1:size(CvR.rois,2);
    nrois = length(rois2a);
    
    hitrate_on_conv = [CvR.rois(rois2a).hitrate_on_conv];
    hitrate_off_conv = [CvR.rois(rois2a).hitrate_off_conv];
    hitrate_shuff_mean = [CvR.rois(rois2a).hitrate_shuffle_mean];
    hitrate_shuff_std = [CvR.rois(rois2a).hitrate_shuffle_std];
    dprime = norminv(hitrate_on_conv)-norminv(hitrate_shuff_mean);
    Z_conv = [CvR.rois(rois2a).Z_conv];
    Z_conv_off = (hitrate_off_conv-hitrate_shuff_mean)./hitrate_shuff_std;
    
    convix = Z_conv >= stdn;
    convoffix = Z_conv_off >= stdn;
    
    P = [hitrate_on_conv; hitrate_shuff_std; zeros(1,nrois)]';
    D = point_to_line(P,[0 0 0],[1 1 0]);   % Distance to unity line
    D(~convix) = -D(~convix);
    
    C = cat(1,C, ...
        [ones(nrois,1).*f, ...
        roiszbbC(rois2a,:), ...
        D, ...
        hitrate_on_conv', ...
        hitrate_off_conv', ...
        convix', ...
        convonix', ...
        dprime', ...
        Z_conv']);
    
    for i = 1:length(zbbhr)
        roismsk = find(zbbmsk(:,i) == 1);
        nrois = length(roismsk);
        hitrate_on_conv = [CvR.rois(roismsk).hitrate_on];
        hitrate_off_conv = [CvR.rois(roismsk).hitrate_off];
        hitrate_shuff_mean = [CvR.rois(roismsk).hitrate_shuffle_mean];
        hitrate_shuff_std = [CvR.rois(roismsk).hitrate_shuffle_std];
        dprime = norminv(hitrate_on_conv)-norminv(hitrate_shuff_mean);
        
        convix = hitrate_on_conv > hitrate_shuff_mean+(stdn.*hitrate_shuff_std);
        convonix = hitrate_on_conv > hitrate_shuff_mean+(stdn.*hitrate_shuff_std) & ...
            hitrate_on_conv > hitrate_off_conv;
        
        P = [hitrate_on_conv; hitrate_shuff_std; zeros(1,nrois)]';
        D = point_to_line(P,[0 0 0],[1 1 0]);   % Distance to unity line
        D(~convix) = -D(~convix);
        Z_conv = (hitrate_on_conv-hitrate_shuff_mean)./hitrate_shuff_std;
        
        zbbhr(i,1,f) = sum(convix)/nrois;
        zbbhr(i,2,f) = nanmean(D(convix));
        zbbhr(i,3,f) = nanmean(D);
        zbbhr(i,4,f) = nanmean(dprime(convix));
        zbbhr(i,5,f) = nanmean(dprime);
        zbbhr(i,6,f) = nrois;
        zbbhr(i,7,f) = nanmean(hitrate_on_conv);
        zbbhr(i,8,f) = nanmean(hitrate_off_conv);
        zbbhr(i,9,f) = nanmean(hitrate_shuff_mean);
        zbbhr(i,10,f) = nanmean(Z_conv);
        zbbhr(i,11,f) = nanstd(Z_conv);
    end
end
zbbMSK = logical(zbbMSK);
close(h)

%%  Histogram of fraction of conv positive cells per anatomical area

H = cat(3,nanmean(zbbhr,3),nansem(zbbhr,3));
[hrs,hrsix] = sort(H(:,1,1),'descend','MissingPlacement','last');

figure
bar(hrs(~isnan(hrs)))
hold on
errorbar(H(hrsix(~isnan(hrs)),1,1),H(hrsix(~isnan(hrs)),1,2),'r.')
set(gca,'XTick',1:sum(~isnan(hrs)), ...
    'XTickLabel',ZBBMaskDatabaseNames(hrsix(~isnan(hrs))), ...
    'XTickLabelRotation',45)
box off
ylabel('Conv + fraction of cells')
xlabel('Anatomical ID')

%%  Bar chart of mean distance to unity line per anatomical area

x = nanmean(zbbhr(:,9,:),3);
y = nanmean(zbbhr(:,7,:),3);
P = [y x zeros(length(x),1)];
D = point_to_line(P,[0 0 0],[1 1 0]);   % Distance to unity line
D(y < x) = -D(y < x);

figure
scatter(x,y,[],D,'filled')
colormap(flip(brewermap(255,'RdBu'),1));
caxis([-max(abs(D)) max(abs(D))]);
axis square
axis([0 0.15 0 0.15])
cbar = colorbar;
ylabel(cbar,'D')
line([0 1],[0 1],'LineStyle','--','Color','k')
xlabel('False alarm rate')
ylabel('Hit rate')

[s,six] = sort(D,'descend','MissingPlacement','last');
figure
bar(s(~isnan(s)))
set(gca,'XTick',1:sum(~isnan(s)), ...
    'XTickLabel',ZBBMaskDatabaseNames(six(~isnan(s))), ...
    'XTickLabelRotation',45, ...
    'TickDir','out','Box','off')
grid on
xlabel('Anatomical ID')
ylabel('Mean Distance to unity')

%%  Bar chart of mean Z score per anatomical area (pooled across fish)

nmsks = length(ZBBMaskDatabaseNames);
y = NaN(nmsks,3);
for i = 1:nmsks
    ix = zbbMSK(:,i) == 1;
    y(i,1) = nanmean(C(ix,11));
    y(i,2) = nansem(C(ix,11));
    y(i,3) = sum(ix);
end
y(y(:,3) < 20,:) = nan;
[s,six] = sort(y(:,1),'descend','MissingPlacement','last');

figure
bar(s(~isnan(s)))
hold on
errorbar(s(~isnan(s)),y(six(~isnan(s)),2),'.r')
set(gca,'XTick',1:sum(~isnan(s)), ...
    'XTickLabel',ZBBMaskDatabaseNames(six(~isnan(s))), ...
    'XTickLabelRotation',45, ...
    'TickDir','out','Box','off')
xlabel('Anatomical ID')
ylabel('Mean Z-score')

% Plot histogram for top 9 Zscores and do test agains huc-brain
Zbrain = C(zbbMSK(:,78) == 1,11);
figure
for i = 1:9
    subplot(3,3,i)
    Z_conv = C(zbbMSK(:,six(i)) == 1,11);
    histogram(Z_conv,-2:0.5:6, ...
        'Normalization','probability');
    hold on
    histogram(Zbrain,-2:0.5:6, ...
        'Normalization','probability');
    [~,p] = kstest2(Z_conv,Zbrain);
    text(1,0.3,sprintf('p = %1.4f',p))
    title(ZBBMaskDatabaseNames{six(i)})
    axis([-2 6 0 0.4],'square')
end

%% Plot Centroids in ZBB space with color coding of distance to unity

dprimemin = 1;
zmin = 2;

% Plot Distance to unity
figure
ix = C(:,8) == 1;
scatter(C(ix,2),C(ix,3),15,C(ix,5),'filled')
colormap(brewermap(255,'Greys'));
caxis([0 0.2])
view([90 90]);
alpha 0.2
axis off equal
set(gca,'ZDir','reverse','YDir','reverse')
cbar = colorbar;
ylabel(cbar,'D')

% Plot dprime
figure
ix = C(:,10) >= dprimemin;
scatter(C(ix,2),C(ix,3),20,C(ix,10),'filled')
colormap hot
caxis([dprimemin 3])
view([90 90]);
alpha 0.2
axis off equal
set(gca,'ZDir','reverse','YDir','reverse')
cbar = colorbar;
ylabel(cbar,'\it d''','Interpreter','tex')

% Plot Z score
figure
ix = C(:,11) >= zmin;
scatter(C(ix,2),C(ix,3),20,C(ix,11),'filled')
colormap(brewermap(255,'Reds'))
caxis([zmin 3])
view([90 90]);
alpha 0.2
axis off equal
set(gca,'ZDir','reverse','YDir','reverse')
cbar = colorbar;
ylabel(cbar,'Z')

% Project Z score image along first 2 dimentions
[N,Xedges,Yedges,binX,binY] = histcounts2(C(:,2),C(:,3),[100 100]);
A = zeros(size(N));
for i = 1:size(N,1)
    for j = 1:size(N,2)
        ix = binX == i & binY == j;
        if any(ix)
            A(i,j) = nanmean(C(ix,11));
        end
    end
end
figure
imagesc(A,[0 2])
colormap hot

% Plot anatomical boundaries
hold on
mskixs = [75 12 33 34];
mskrange = round(prctile(C(:,4),5)):round(prctile(C(:,4),95));
%     colors = get(gca,'colororder');
colors = {'k','k','k','k'};
for i = 1:length(mskixs)
    msk = reshape(full(ZBBMaskDatabase(:,mskixs(i))),[616,1030,420]);
    msk = msk(:,:,mskrange);
    [~,mskix] = max(squeeze(sum(sum(msk,1),2)));
    mskB = bwboundaries(msk(:,:,mskix));
    
    for j = 1:length(mskB)
        boundary = mskB{j};
        plot(boundary(:,2), boundary(:,1),'-', ...
            'Color',colors{i},'LineWidth',0.5);
    end
end

%%  Plot NI cells centroids colored as Z

lims = [450 550 150 270 100 250; ...
    450 550 350 350+120 100 250];

zbbixs = [75,79];
ix = sum(zbbMSK(:,zbbixs),2) >= 1 & ...
    C(:,11) >= 0;
nilabs = {'Right NI', 'Left NI'};
for i = 1:2
    figure
    scatter(C(ix,2),C(ix,3),[],C(ix,11),'filled')
    colormap(brewermap(255,'Reds'))
    caxis([-1 3])
    alpha 0.5
    axis(lims(i,1:4))
    axis off equal
    set(gca,'YDir','reverse')
    view([90 90]);
    title(nilabs{i});
    cbar = colorbar;
    ylabel(cbar,'Z')
    
    hold on
    % Plot mask boudaries
    mskixs = zbbixs;
    mskrange = round(prctile(C(ix,4),5)):round(prctile(C(ix,4),95));
    if length(mskixs) ~=2
        colors = get(gca,'ColorOrder');
    else
        colors = [0 1 0; 1 0 1];
    end
    for j = 1:length(mskixs)
        msk = reshape(full(ZBBMaskDatabase(:,mskixs(j))),[616,1030,420]);
        msk = msk(:,:,mskrange);
        [~,mskix] = max(squeeze(sum(sum(msk,1),2)));
        mskB = bwboundaries(msk(:,:,mskix));
        
        for k = 1:length(mskB)
            boundary = mskB{k};
            plot(boundary(:,2), boundary(:,1),'-', ...
                'Color',colors(j,:),'LineWidth',0.5);
        end
    end
end


%%  Build Ca response matrix of conv + cells

zbbix = [79];

Mon = [];
Moff = [];
Von = [];
Voff = [];
h = waitbar(0,'Init');
for f = 1:length(folders)
    waitbar(f/length(folders),h,'Building');
    datadir = folders{f};
    load(fullfile(datadir,'CvR'));
    load(fullfile(datadir,'gmranat'));
    
    % Get ZBB labels
    nzs = size(gmranat.z,2);
    zbbmsk = [];
    roisC = [];
    roiszbbC = [];
    for z = 1:nzs
        zbbmsk = cat(1,zbbmsk, ...
            cat(1,gmranat.z(z).STATScrop.Masks_ZBB));
        roisC = cat(1,roisC, ...
            cat(1,gmranat.z(z).STATScrop.Centroid));
        roiszbbC = cat(1,roiszbbC, ...
            cat(1,gmranat.z(z).STATScrop.Centroid_ZBB));
    end
    zbbmsk = logical(zbbmsk(:,zbbanatix));
    
    rois2a = find(sum(zbbmsk(:,zbbix),2) > 0);
    nrois = length(rois2a);
    
    hitrate_on_conv = [CvR.rois(rois2a).hitrate_on];
    hitrate_off_conv = [CvR.rois(rois2a).hitrate_off];
    hitrate_shuff_mean = [CvR.rois(rois2a).hitrate_shuffle_mean];
    hitrate_shuff_std = [CvR.rois(rois2a).hitrate_shuffle_std];
    dprime = norminv(hitrate_on_conv)-norminv(hitrate_shuff_mean);
    P = [hitrate_on_conv; hitrate_shuff_std; zeros(1,nrois)]';
    D = point_to_line(P,[0 0 0],[1 1 0]);   % Distance to unity line
    Z_conv = (hitrate_on_conv-hitrate_shuff_mean)./hitrate_shuff_std;
    
    convix = Z_conv >= stdn;
    D(~convix) = -D(~convix);
    
    rois2a = rois2a(convix);
    for roi = rois2a'
        len = size(CvR.rois(roi).Xon,1);
        Mon = cat(1,Mon,cat(2, ...
            CvR.rois(roi).Xon.Fz{:})');
        Von = cat(1,Von, ...
            [ones(len,1).*f, ...
            ones(len,1).*roi, ...
            CvR.rois(roi).Xon.vis, ...
            CvR.rois(roi).Xon.nbts, ...
            CvR.rois(roi).Xon.p, ...
            CvR.rois(roi).Xon.isactive, ...
            ones(len,1).*CvR.rois(roi).hitrate_on, ...
            ones(len,1).*CvR.rois(roi).hitrate_off, ...
            ones(len,1).*CvR.rois(roi).hitrate_shuffle_mean, ...
            ones(len,1).*CvR.rois(roi).hitrate_shuffle_std, ...
            ]);
        
        len = size(CvR.rois(roi).Xoff,1);
        Moff = cat(1,Moff,cat(2, ...
            CvR.rois(roi).Xoff.Fz{:})');
        Voff = cat(1,Voff, ...
            [ones(len,1).*f, ...
            ones(len,1).*roi, ...
            CvR.rois(roi).Xoff.vis, ...
            CvR.rois(roi).Xoff.nbts, ...
            CvR.rois(roi).Xoff.p, ...
            CvR.rois(roi).Xoff.isactive, ...
            ones(len,1).*CvR.rois(roi).hitrate_on, ...
            ones(len,1).*CvR.rois(roi).hitrate_off, ...
            ones(len,1).*CvR.rois(roi).hitrate_shuffle_mean, ...
            ones(len,1).*CvR.rois(roi).hitrate_shuffle_std, ...
            ]);
    end
end
close(h)

%%  Plot active cells segregated by behaviour

ylims = [-1 6];
vs = [3,5];
% figure('name',ZBBMaskDatabaseNames{zbbix});
for i = 1:length(vs)
    v = vs(i);
    %     subplot(1,2,i)
    
    % Spontaneous responses; one bout
    ix1 = (Voff(:,3) == v | Voff(:,3) == v+1) & ...
        Voff(:,4) == 1 & Voff(:,6) == 1;
    %     shadedErrorBar(1:110,nanmean(Moff(ix1,:),1), ...
    %         nansem(Moff(ix1,:),1),'-k',1); axis square
    %     line([55 55],[ylims(1) ylims(2)],'Color','k', ...
    %         'LineStyle','--')
    %     hold on
    
    % No response; one bout
    %     ix2 = (Von(:,3) == v | Von(:,3) == v+1) & ...
    %         Von(:,4) == 1 & Von(:,6) == 0;
    %     shadedErrorBar(1:110,nanmean(Mon(ix2,:),1), ...
    %         nansem(Mon(ix2,:),1),'-k',1); axis square
    %     line([55 55],[ylims(1) ylims(2)],'Color','k', ...
    %         'LineStyle','--')
    %     hold on
    
    % One bout response
    ix3 = (Von(:,3) == v | Von(:,3) == v+1) & ...
        Von(:,4) == 1 & Von(:,6) == 1;
    %     shadedErrorBar(1:110,nanmean(Mon(ix3,:),1), ...
    %         nansem(Mon(ix3,:),1),'-b',1); axis square
    %     line([55 55],[ylims(1) ylims(2)],'Color','k', ...
    %         'LineStyle','--')
    
    % Two bout responses
    ix4 = (Von(:,3) == v | Von(:,3) == v+1) & ...
        Von(:,4) == 2 & Von(:,6) == 1;
    %     shadedErrorBar(1:110,nanmean(Mon(ix4,:),1), ...
    %         nansem(Mon(ix4,:),1),'-r',1); axis square
    %     line([55 55],[ylims(1) ylims(2)],'Color','k', ...
    %         'LineStyle','--')
    %     axis square
    
    % Three plus bout responses
    ix5 = (Von(:,3) == v | Von(:,3) == v+1) & ...
        Von(:,4) >= 3 & Von(:,6) == 1;
    %     shadedErrorBar(1:110,nanmean(Mon(ix5,:),1), ...
    %         nansem(Mon(ix5,:),1),'-g',1); axis square
    %     line([55 55],[ylims(1) ylims(2)],'Color','k', ...
    %         'LineStyle','--')
    %     axis square
    %     title(sprintf('VIS = %s',num2str(gmv.vistypz(v,1))))
    %     axis([1 110 ylims])
    
    X1 = catpad(1,nanmean(Moff(ix1,55:end),1), ...
        nanmean(Mon(ix3,55:end),1), ...
        nanmean(Mon(ix4,55:end),1))';
    X2 = catpad(1,max(Moff(ix1,55:end),[],1), ...
        max(Mon(ix3,55:end),[],1), ...
        max(Mon(ix4,55:end),[],1))';
    
    figure('name',sprintf('VIS = %s',num2str(gmv.vistypz(v,1))))
    subplot(1,2,1)
    scatterSpread(X1,'b',[],0.2)
    hold on
    boxplot(X1)
    ylabel('Mean zF post conv')
    set(gca,'XTick',1:3, ...
        'XTickLabel',{'Spont','One Bout','Two Bouts'}, ...
        'XTickLabelRotation',45, ...
        'Box','off','TickDir','out')
    
    subplot(1,2,2)
    scatterSpread(X2,'b',[],0.2)
    hold on
    boxplot(X2)
    ylabel('Max zF post conv')
    set(gca,'XTick',1:3, ...
        'XTickLabel',{'Spont','One Bout','Two Bouts'}, ...
        'XTickLabelRotation',45, ...
        'Box','off','TickDir','out')
end

%%  Table of mean and max zF for conv+ cells in conv epocs

showplots = 0;
stdn = 2;   % Standard deviations above shuffle mean to consider conv+ cell
zbbix = [75 79];    % zbb mask indexes

k = 1;
T = table;
h = waitbar(0,'Init');
for f = 1:length(folders)
    waitbar(f/length(folders),h,'Processing')
    datadir = folders{f};
    load(fullfile(datadir,'CvR'));
    load(fullfile(datadir,'gmranat'));
    
    % Get ZBB labels
    nzs = size(gmranat.z,2);
    zbbmsk = [];
    roiszbbC = [];
    for z = 1:nzs
        zbbmsk = cat(1,zbbmsk, ...
            cat(1,gmranat.z(z).STATScrop.Masks_ZBB));
        roiszbbC = cat(1,roiszbbC, ...
            cat(1,gmranat.z(z).STATScrop.Centroid_ZBB));
    end
    zbbmsk = logical(zbbmsk(:,zbbanatix));
    
    convpos = [CvR.rois.hitrate_on] >=  ...
        [CvR.rois.hitrate_shuffle_mean]+stdn*[CvR.rois.hitrate_shuffle_std];
    
    rois2a = find(sum(zbbmsk(:,zbbix),2) > 0 & convpos');
    lroisix = roiszbbC(rois2a,2) > 308;
    
    for z = unique([CvR.rois(rois2a).z])
        roiszix = [CvR.rois(rois2a).z] == z;
        lrois = rois2a(lroisix & roiszix');
        rrois = rois2a(~lroisix & roiszix');
        
        if ~isempty(lrois) || ~isempty(rrois)
            if ~isempty(lrois)
                len = size(CvR.rois(lrois(1)).Xon,1);
            else
                len = size(CvR.rois(rrois(1)).Xon,1);
            end
            
            for i = 1:len
                ML = [];
                ial = [];
                if ~isempty(lrois)
                    for j = 1:length(lrois)
                        roi = lrois(j);
                        ML = cat(1,ML,CvR.rois(roi).Xon.Fz{i}');
                        ial = cat(1,ial,CvR.rois(roi).Xon.isactive(i));
                    end
                end
                ML = ML(logical(ial),:);
                
                MR = [];
                iar = [];
                if ~isempty(rrois)
                    for j = 1:length(rrois)
                        roi = rrois(j);
                        MR = cat(1,MR,CvR.rois(roi).Xon.Fz{i}');
                        iar = cat(1,iar,CvR.rois(roi).Xon.isactive(i));
                    end
                end
                MR = MR(logical(iar),:);
                
                T.fish(k) = f;
                T.nrois_L(k) = length(lrois);
                T.nrois_R(k) = length(rrois);
                T.p(k) = CvR.rois(roi).Xon.p(i);
                T.nbts(k) = CvR.rois(roi).Xon.nbts(i);
                T.angle(k) = CvR.rois(roi).Xon.st_ang_deg(i);
                
                if ~isempty(ML)
                    T.Fz_mean2_L_post(k) = nanmean(nanmean(ML(:,55:end),1));
                    T.Fz_max2_L_post(k) = nanmean(nanmax(ML(:,55:end),[],1));
                else
                    T.Fz_mean2_L_post(k) = nan;
                    T.Fz_max2_L_post(k) = nan;
                end
                
                if ~isempty(MR)
                    T.Fz_mean2_R_post(k) = nanmean(nanmean(MR(:,55:end),1));
                    T.Fz_max2_R_post(k) = nanmean(nanmax(MR(:,55:end),[],1));
                else
                    T.Fz_mean2_R_post(k) = nan;
                    T.Fz_max2_R_post(k) = nan;
                end
                k = k+1;
            end
        end
    end
end

close(h)

%%  Spot angle and NI activation

bins = [-90 0 90];
Y = discretize(T.angle,bins);

FmL = [];
FmR = [];
FmxL = [];
FmxR = [];
for i = 1:length(bins)-1
    ix = Y == i;
    FmL = catpad(2,FmL,T.Fz_mean2_L_post(ix));
    FmR = catpad(2,FmR,T.Fz_mean2_R_post(ix));
    FmxL = catpad(2,FmxL,T.Fz_max2_L_post(ix));
    FmxR = catpad(2,FmxR,T.Fz_max2_R_post(ix));
end

% Mean zF
ylims = [nanmin([FmL(:);FmR(:)])-2 ...
    nanmax([FmL(:);FmR(:)])+2];

figure
subplot(1,2,1)
scatterSpread(FmL,'b')
for i = 1:length(bins)-2
    for j = 2:length(bins)-1
        [~,p] = ttest2(FmL(:,i),FmL(:,j));
        sigstar_ph([i,j],p)
    end
end
set(gca,'XTick',0.5:length(bins),'XTickLabel',bins)
axis([0 length(bins) ylims],'square')
ylabel('Mean zF')
xlabel('Spot angle @ convergence')
title('Left')

subplot(1,2,2)
scatterSpread(FmR,'r')
for i = 1:length(bins)-2
    for j = 2:length(bins)-1
        [~,p] = ttest2(FmR(:,i),FmR(:,j));
        sigstar_ph([i,j],p)
    end
end
set(gca,'XTick',0.5:length(bins),'XTickLabel',bins)
axis([0 length(bins) ylims],'square')
ylabel('Mean zF')
xlabel('Spot angle @ convergence')
title('Right')

% Max zF
ylims = [nanmin([FmxL(:);FmxR(:)])-1 ...
    nanmax([FmxL(:);FmxR(:)])+1];

figure
subplot(1,2,1)
scatterSpread(FmxL,'b')
for i = 1:length(bins)-2
    for j = 2:length(bins)-1
        [~,p] = ttest2(FmxL(:,i),FmxL(:,j));
        sigstar_ph([i,j],p)
    end
end
set(gca,'XTick',0.5:length(bins),'XTickLabel',bins)
axis([0 length(bins) ylims],'square')
ylabel('Max zF')
xlabel('Spot angle @ convergence')
title('Left')

subplot(1,2,2)
scatterSpread(FmxR,'r')
for i = 1:length(bins)-2
    for j = 2:length(bins)-1
        [~,p] = ttest2(FmxR(:,i),FmxR(:,j));
        sigstar_ph([i,j],p)
    end
end
set(gca,'XTick',0.5:length(bins),'XTickLabel',bins)
axis([0 length(bins) ylims],'square')
ylabel('Max zF')
xlabel('Spot angle @ convergence')
title('Right')

%%  Bout number

ix1 = T.nbts == 1;
ix2 = T.nbts == 2;
ix3 = T.nbts >= 3;

FmL = catpad(2,T.Fz_mean2_L_post(ix1), ...
    T.Fz_mean2_L_post(ix2));
FmR = catpad(2,T.Fz_mean2_R_post(ix1), ...
    T.Fz_mean2_R_post(ix2));
Fm = cat(1,FmL,FmR);

FmxL = catpad(2,T.Fz_max2_L_post(ix1), ...
    T.Fz_max2_L_post(ix2));
FmxR = catpad(2,T.Fz_max2_R_post(ix1), ...
    T.Fz_max2_R_post(ix2));
Fmx = cat(1,FmxL,FmxR);

figure
subplot(1,2,1)
scatterSpread(Fm,'b');
[~,p] = ttest2(Fm(:,1),Fm(:,2));
sigstar_ph([1,2],p)
xlabel('Number of hunting bouts')
ylabel('Mean zF');
set(gca,'XTick',1:2);
axis square

subplot(1,2,2)
scatterSpread(Fmx,'b');
[~,p] = ttest2(Fmx(:,1),Fmx(:,2));
sigstar_ph([1,2],p)
xlabel('Number of hunting bouts')
ylabel('Max zF');
set(gca,'XTick',1:2);
axis square
